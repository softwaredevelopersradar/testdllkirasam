﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(double), targetType: typeof(string))]
    public class LengthConverter: System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string ss = "";

            ss = System.Convert.ToString((double)value);

            return ss;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            double dd = 0;
            string ss = "";

            ss = (string)value;
            dd = System.Convert.ToDouble(ss);

            return dd;
        }

    } // Class
} // Namespace
