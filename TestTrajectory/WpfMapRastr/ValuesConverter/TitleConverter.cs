﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Data;

namespace ValuesConverter
{
    [ValueConversion(sourceType: typeof(string), targetType: typeof(string))]
    public class TitleConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string ss = "";
            ss = (string)value;

            if (ss == "")
                return "";
            else
                return ss;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {

            string ss = "";
            ss = (string)value;

            if (ss == "")
                return "";
            else
                return ss;
        }

    } // Class
} // Namespace
