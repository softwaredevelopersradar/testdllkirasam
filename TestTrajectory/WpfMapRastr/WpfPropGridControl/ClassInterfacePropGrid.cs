﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfPropGridControl
{
    public class ClassInterfacePropGrid : InterfacePropGrid
    {

        // Constructor **************************************************************************
        public ClassInterfacePropGrid()
        {
            ;
        }
        // ************************************************************************** Constructor

        // VAR **********************************************************************************

        // ......................................................................................
        //private bool _MapOpened = false;
        //public bool _ShowJS = false;
        public int ID_DB_Int = 0;
        // ......................................................................................


        // ********************************************************************************** VAR

        // Properties ***************************************************************************

        // ......................................................................................
        // =1 -> Tha map is opened
        /*
                public bool MapOpened
                {
                    get { return _MapOpened; }
                }
                // ......................................................................................
                public bool ShowJS
                {
                    get { return _ShowJS; }
                    set { _ShowJS = value; }
                }
                // ......................................................................................
        */
        public int _ID_DB_Int
        { 
        get { return ID_DB_Int; }
        set { ID_DB_Int = value; }
        }

        // ......................................................................................

        // *************************************************************************** Properties






    } // class
} // namespace
