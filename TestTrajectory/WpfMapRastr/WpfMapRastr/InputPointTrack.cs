﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastr
{
    public class InputPointTrack
    {
        // Широта, град, WGS84
        public double LatMark { get; set; }
        // Долгота, град, WGS84
        public double LongMark { get; set; }
        // Высота, м
        public double HMark { get; set; }
        // Время прихода отметки, с
        public double Time { get; set; }
        // Частота, КГц
        public float Freq { get; set; }
        // Ширина полосы, КГц
        public float dFreq { get; set; }
        //
        public DateTime  _time { get; set; }

        // Задержки от трех станций
        public double dt1 { get; set; }
        public double dt2 { get; set; }
        public double dt3 { get; set; }

    } // Class
} // Namespace
