﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

namespace WpfMapRastr
{
    // Load Colors for trajectories
    public partial class MainWindow
    {
        // LoadColors ********************************************************************************
        // Загрузка цветов для траекторий

        public void LoadColors()
        {

            // Болотный
            ClassColor objClassColor6 = new ClassColor();
            objClassColor6.Color1 = 184;
            objClassColor6.Color2 = 134;
            objClassColor6.Color3 = 11;
            GlobalVarMapMain.lstClassColor.Add(objClassColor6);

            // Сиреневый
            ClassColor objClassColor3 = new ClassColor();
            objClassColor3.Color1 = 138;
            objClassColor3.Color2 = 43;
            objClassColor3.Color3 = 226;
            GlobalVarMapMain.lstClassColor.Add(objClassColor3);


            // Голубой
            ClassColor objClassColor1 = new ClassColor();
            objClassColor1.Color1 = 0;
            objClassColor1.Color2 = 255;
            objClassColor1.Color3 = 255;
            GlobalVarMapMain.lstClassColor.Add(objClassColor1);


            // Синий
            ClassColor objClassColor2 = new ClassColor();
            objClassColor2.Color1 = 0;
            objClassColor2.Color2 = 0;
            objClassColor2.Color3 = 255;
            GlobalVarMapMain.lstClassColor.Add(objClassColor2);

            // Светло коричневый
            ClassColor objClassColor4 = new ClassColor();
            objClassColor4.Color1 = 210;
            objClassColor4.Color2 = 105;
            objClassColor4.Color3 = 30;
            GlobalVarMapMain.lstClassColor.Add(objClassColor4);

            // Морской волны
            ClassColor objClassColor5 = new ClassColor();
            objClassColor5.Color1 = 0;
            objClassColor5.Color2 = 139;
            objClassColor5.Color3 = 139;
            GlobalVarMapMain.lstClassColor.Add(objClassColor5);

/*
            // Болотный
            ClassColor objClassColor6 = new ClassColor();
            objClassColor6.Color1 = 184;
            objClassColor6.Color2 = 134;
            objClassColor6.Color3 = 11;
            GlobalVarMapMain.lstClassColor.Add(objClassColor6);
*/

            // Темно зеленый
            ClassColor objClassColor7 = new ClassColor();
            objClassColor7.Color1 = 0;
            objClassColor7.Color2 = 100;
            objClassColor7.Color3 = 0;
            GlobalVarMapMain.lstClassColor.Add(objClassColor7);

            // Темно бордовый
            ClassColor objClassColor8 = new ClassColor();
            objClassColor8.Color1 = 139;
            objClassColor8.Color2 = 0;
            objClassColor8.Color3 = 0;
            GlobalVarMapMain.lstClassColor.Add(objClassColor8);

            // Ярко розовый
            ClassColor objClassColor9 = new ClassColor();
            objClassColor9.Color1 = 255;
            objClassColor9.Color2 = 20;
            objClassColor9.Color3 = 147;
            GlobalVarMapMain.lstClassColor.Add(objClassColor9);

            // Серый
            ClassColor objClassColor10 = new ClassColor();
            objClassColor10.Color1 = 105;
            objClassColor10.Color2 = 105;
            objClassColor10.Color3 = 105;
            GlobalVarMapMain.lstClassColor.Add(objClassColor10);

            // Желтый
            ClassColor objClassColor11 = new ClassColor();
            objClassColor11.Color1 = 255;
            objClassColor11.Color2 = 215;
            objClassColor11.Color3 = 0;
            GlobalVarMapMain.lstClassColor.Add(objClassColor11);

            // Светло розовый
            ClassColor objClassColor12 = new ClassColor();
            objClassColor12.Color1 = 255;
            objClassColor12.Color2 = 105;
            objClassColor12.Color3 = 180;
            GlobalVarMapMain.lstClassColor.Add(objClassColor12);

            // Темно сиреневый
            ClassColor objClassColor13 = new ClassColor();
            objClassColor13.Color1 = 75;
            objClassColor13.Color2 = 0;
            objClassColor13.Color3 = 130;
            GlobalVarMapMain.lstClassColor.Add(objClassColor13);

            // Светло салатный
            ClassColor objClassColor14 = new ClassColor();
            objClassColor14.Color1 = 144;
            objClassColor14.Color2 = 238;
            objClassColor14.Color3 = 144;
            GlobalVarMapMain.lstClassColor.Add(objClassColor14);

            // Ярко лимонный
            ClassColor objClassColor15 = new ClassColor();
            objClassColor15.Color1 = 255;
            objClassColor15.Color2 = 255;
            objClassColor15.Color3 = 0;
            GlobalVarMapMain.lstClassColor.Add(objClassColor15);

            // Темно болотный
            ClassColor objClassColor16 = new ClassColor();
            objClassColor16.Color1 = 128;
            objClassColor16.Color2 = 128;
            objClassColor16.Color3 = 0;
            GlobalVarMapMain.lstClassColor.Add(objClassColor16);

            // Темно-темно фиолетовый
            ClassColor objClassColor17 = new ClassColor();
            objClassColor17.Color1 = 0;
            objClassColor17.Color2 = 0;
            objClassColor17.Color3 = 128;
            GlobalVarMapMain.lstClassColor.Add(objClassColor17);

            // Ярко оранжевый
            ClassColor objClassColor18 = new ClassColor();
            objClassColor18.Color1 = 255;
            objClassColor18.Color2 = 69;
            objClassColor18.Color3 = 0;
            GlobalVarMapMain.lstClassColor.Add(objClassColor18);

            // Светло бежевый
            ClassColor objClassColor19 = new ClassColor();
            objClassColor19.Color1 = 255;
            objClassColor19.Color2 = 218;
            objClassColor19.Color3 = 155;
            GlobalVarMapMain.lstClassColor.Add(objClassColor19);

            // Темно коричневый
            ClassColor objClassColor20 = new ClassColor();
            objClassColor20.Color1 = 139;
            objClassColor20.Color2 = 69;
            objClassColor20.Color3 = 19;
            GlobalVarMapMain.lstClassColor.Add(objClassColor20);

        }
        // ******************************************************************************** LoadColors

    } // Class
} // Namespace
