﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

namespace WpfMapRastr
{
    // Запись некоторых данных в файл
    public partial class MainWindow
    {
        
                public void FileKalman()
                {

                // -------------------------------------------------------------------------------------------------
                    // Get current dir
                    string dir = "";
                    dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    // -------------------------------------------------------------------------------------------------

                    // FILE_RDM

                    string dir1 = "";
                    string sltlng = "";
                    int m2 = 0;
                    string dirK = "";
                    int i1H = 0;
                    int i2H = 0;
                    string zap = "";
                    double xxx13 = 0;
                    double yyy13 = 0;
                    double zzz13 = 0;
                    double ltt23 = 0;
                    double lng23 = 0;
                    double hhh23 = 0;
                    string Klmn = "";

                    dirK = dir + "\\FILES\\" + "Kalman.txt";
                    StreamWriter swK = new StreamWriter(dirK, false, System.Text.Encoding.Default);

                    for (i1H = 0; i1H < GlobalVarMapMain.lsttr.Count; i1H++)
                    {
                        for (i2H = 1; i2H < GlobalVarMapMain.lsttr[i1H].listPointsTrajFiltr_I.Count; i2H++)
                        {


                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // #@#
                            ClassGeoCalculator.MZSK_Geoc_V(
                                                      GlobalVarMapMain.latRLS_rad,
                                                      GlobalVarMapMain.longRLS_rad,
                                                      GlobalVarMapMain.XRLS,
                                                      GlobalVarMapMain.YRLS,
                                                      GlobalVarMapMain.ZRLS,
                                                      GlobalVarMapMain.lsttr[i1H].listPointsTrajFiltr_I[i2H].X,
                                                      GlobalVarMapMain.lsttr[i1H].listPointsTrajFiltr_I[i2H].Y,
                                                      GlobalVarMapMain.lsttr[i1H].listPointsTrajFiltr_I[i2H].Z,
                                                      ref xxx13,
                                                      ref yyy13,
                                                      ref zzz13
                                                    );

                            ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13, yyy13, zzz13, ref ltt23, ref lng23, ref hhh23);

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            Klmn = Convert.ToString(GlobalVarMapMain.lsttr[i1H].listPointsTraj_I[i2H+2].timePoint) + " " +
                                   Convert.ToString(GlobalVarMapMain.lsttr[i1H].listPointsTrajFiltr_I[i2H].X) + " " +
                                   Convert.ToString(GlobalVarMapMain.lsttr[i1H].listPointsTrajFiltr_I[i2H].Y) + " " +
                                   Convert.ToString(GlobalVarMapMain.lsttr[i1H].listPointsTrajFiltr_I[i2H].Z) + " " +
                                  Convert.ToString(ltt23)+ " "+Convert.ToString(lng23)+" "+ Convert.ToString(hhh23);

                                  swK.WriteLine(Klmn);

                        } // for2

                    } // for1
                }
        

    } // Class
} // Namespace
