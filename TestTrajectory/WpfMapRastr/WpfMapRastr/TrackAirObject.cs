﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMapRastr
{
    public class TrackAirObject
    {

        public int ID { get; set; }

        // List of marks
        public List<Mark> Marks { get; set; }

        // Track state (1-active, 0-unactive)
        public byte State { get; set; }

        // Speed 
        public double Speed { get; set; }

        // Note(Примечание)
        public string Note { get; set; }

        // Матрица оценок параметров траектории 6x1 (состояние фильтра на (n-1) шаге
        // (или после инициализации фильтра)
        // S(n-1)=|x Vx y Vy z Vz| транспонированная
        public double[,] S_N_1 = new double[6, 1];
        // Корреляционная матрица 6x6 ошибок оценок параметров траектории (на (n-1) 
        // шаге или после инициализации фильтра)
        public double[,] PSI_N_1 = new double[6, 6];

        // Матрица оценок параметров траектории 6x1 (состояние фильтра на (n) шаге
        // (или после инициализации фильтра)
        // S(n-1)=|x Vx y Vy z Vz| транспонированная
        public double[,] S_N = new double[6, 1];
        // Корреляционная матрица 6x6 ошибок оценок параметров траектории (на (n) 
        // шаге или после инициализации фильтра)
        public double[,] PSI_N = new double[6, 6];


        // Параметры 
        //public Parameters parameters = new Parameters();
        // RLS
        public List<Station> stations = new List<Station>();

        // Генераторы случайных чисел (Гаусс)
        public MRND objClassMRND_X;
        public MRND objClassMRND_Y;
        public MRND objClassMRND_Z;

        // =true -> идет текущий опрос траектории
        public bool AddTek = false;
        // =true -> попадаетв строб
        public bool PopStrob =false;
        // Расстояние между реальным и экстраполированным значением
        public double dRR = -1;

        // =true -> из трасс, где отметка попала в строб,
        //          это трасса с минимальным dRR
        public bool fl_dRR_min = false;

        // Строб
        public double StrobX=0;
        public double StrobY=0;
        public double StrobZ=0;

        // Constructor *******************************************************************

        public TrackAirObject(
                           List<Station> _stations
                             )
        {
            //parameters = _objParameters;
            stations = _stations;

            objClassMRND_X = new MRND();
            objClassMRND_Y = new MRND();
            objClassMRND_Z = new MRND();

        }
        // ******************************************************************* Constructor

        // AddMark *************************************************************************
        // Добавить отметку в эту трассу ВО + вильтр Калмана 

        public bool AddMarkAirObject(
                                  // Входная отметка
                                  TimeMarkCoord MarkAirObject,

                                  // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                                  double twaitAirObject,
                                  // СКО ускорения на плоскости
                                  double CKOA_pl,
                                  // СКО ускорения по высоте
                                  double CKOA_H,
                                  // Допустимая скорость дрона
                                  double Vdop,
                                  // СКО по координатам для формирования статической матрицы D 
                                  // ошибок измерения в данный момент времени
                                  double CKO_X,
                                  double CKO_Y,
                                  double CKO_Z,
                                  // ==true -> использовать динамическую матрицу ошибок измерений, 
                                  // ==false -> использовать статическую матрицу ошибок измерений
                                  bool flagStatDyn,

                                  // Индекс в листе трасс ВО
                                  int indvo,
                                  // Лист трасс ВО
                                  ref List<TrackAirObject> _listTrackAirObject


                                 )
        {
            int index = -1;
            int i1 = -1;
            double dt = 0;

            TrackAirObject objTrackAirObject = new TrackAirObject(stations);
            objTrackAirObject = _listTrackAirObject[indvo];

            if (objTrackAirObject.Marks != null)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // В списке отметок ищем последнюю с Skip=0(false)

                // NOT FIRST ##########################################################################

                if (objTrackAirObject.Marks.Count>0)
                {
                    index = -1;
                    for (i1 = objTrackAirObject.Marks.Count-1; i1>=0; i1--)
                    {
                        if(objTrackAirObject.Marks[i1].Skip==false)
                        {
                            index = i1;
                            i1 = -1; // Выход из for
                        }

                    } // FOR

                } // if(objTrackAirObject.Marks.Count>0)

                // ########################################################################## NOT FIRST

                // FIRST #############################################################################
                // Это первая отметка (после инициализации фильтра Калмана)
                // Здесь этого не может быть (после инициализации фильтра уже должна быть одна отметка)

                else
                    return false;
                // ############################################################################# FIRST


                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                if (index >= 0)
                {
                    dt = MarkAirObject.Time - objTrackAirObject.Marks[index].Time;

                    // Time<=Tdop @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    // dt>parameters.twaitAirObject

                    if (dt <= twaitAirObject)
                    {

                        // ----------------------------------------------------------------------------
                        // !!! Это будет новая отметка

                        Mark objMark1 = new Mark();
                        // ----------------------------------------------------------------------------
                        // Реальные координаты :XYZ в МЗСК

                        objMark1.CoordReal = new TimeMarkCoord(
                                                               MarkAirObject.Latitude,
                                                               MarkAirObject.Longitude,
                                                               MarkAirObject.Altitude,
                                                               //parameters,
                                                               stations,
                                                               MarkAirObject.Time,
                                                               MarkAirObject.Freq,
                                                               MarkAirObject.dFreq,
                                                               MarkAirObject._time
                                                              );
                        // ----------------------------------------------------------------------------

                        // ФИЛЬТР КАЛМАНА >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // ----------------------------------------------------------------------------
                        // Vars

                        Matrix objMatrix = new Matrix();

                        int i = 0;

                        // Интервал времени между n-й и (n-1)-й отметками
                        double dtn = 0;

                        // Случайные ускорения Axy-плоскость, Az - высота
                        double Ax = 0;
                        double Ay = 0;
                        double Az = 0;

                        // Динамическая матрица связи 6x6
                        double[,] FN = new double[6, 6];
                        double[,] FNT = new double[6, 6]; // транспонированная

                        // Динамическая матрица возмущений 6x3
                        double[,] GN = new double[6, 3];
                        double[,] GNT = new double[3, 6]; // транспонированная

                        // Матрица случайных ускорений 3x1
                        // AN=|Ax Ay Az| транспонированная
                        double[,] AN = new double[3, 1];
                        // Дисперсионная матрица ускорений 6x6
                        double[,] DA = new double[6, 6];
                        // Экстраполированное значение состояния траектории (предсказание в текущий момент) 6x1
                        double[,] SEN = new double[6, 1];

                        // Экстраполированная корреляционная матрица ошибок оценки траектории 6x6
                        double[,] PSI_Ext_N = new double[6, 6];

                        // Матрица взаимосвязи между вектором измеряемых параметров 3x1 (координаты ВО) 
                        // и вектором наблюдаемых параметров 6x1 (координаты ВО и составляющие вектора скорости 
                        // по каждой из координат)
                        // 3x6
                        double[,] HN = new double[3, 6];
                        double[,] HNT = new double[6, 3]; // транспонированная

                        // Коэффициент усиления фильтра
                        double[,] KN = new double[6, 3];

                        // Состояние траектории на n-ом шаге (расчет оценки параметроа траектории)
                        double[,] SN = new double[6, 1];

                        // Корреляционная матрица ошибок оценки траектории 6x6
                        double[,] PSI_N = new double[6, 6];

                        // Для строба
                        double ThetaX = 0;
                        double ThetaY = 0;
                        double ThetaZ = 0;

                        // ----------------------------------------------------------------------------
                        // Интервал времени между n-й и (n-1)-й отметками

                        if (Marks.Count > 0)
                        {
                            dtn = MarkAirObject.Time - objTrackAirObject.Marks[index].Time;
                        }
                        // ----------------------------------------------------------------------------
                        // Динамическая матрица связи 6x6

                        // 1-я строка
                        FN[0, 0] = 1;
                        FN[0, 1] = dtn;
                        for (i = 0; i < 4; i++)
                        { FN[0, i + 2] = 0; }
                        // 2-я строка
                        FN[1, 0] = 0;
                        FN[1, 1] = 1;
                        for (i = 0; i < 4; i++)
                        { FN[1, i + 2] = 0; }
                        // 3-я строка
                        FN[2, 0] = 0; FN[2, 1] = 0; FN[2, 2] = 1; FN[2, 3] = dtn; FN[2, 4] = 0; FN[2, 5] = 0;
                        // 4-я строка
                        FN[3, 0] = 0; FN[3, 1] = 0; FN[3, 2] = 0; FN[3, 3] = 1; FN[3, 4] = 0; FN[3, 5] = 0;
                        // 5-я строка
                        for (i = 0; i < 4; i++)
                        { FN[4, i] = 0; }
                        FN[4, 4] = 1;
                        FN[4, 5] = dtn;
                        // 6-я строка
                        for (i = 0; i < 5; i++)
                        { FN[5, i] = 0; }
                        FN[5, 5] = 1;

                        // Транспонированная
                        objMatrix.MTransp(
                                6,
                                6,
                                FN,
                                ref FNT
                               );
                        // ----------------------------------------------------------------------------
                        // Динамическая матрица возмущений 6x3

                        // 1-я строка
                        GN[0, 0] = (dtn * dtn) / 2; GN[0, 1] = 0; GN[0, 2] = 0;
                        // 2-я строка
                        GN[1, 0] = dtn; GN[1, 1] = 0; GN[1, 2] = 0;
                        // 3-я строка
                        GN[2, 0] = 0; GN[2, 1] = (dtn * dtn) / 2; GN[2, 2] = 0;
                        // 4-я строка
                        GN[3, 0] = 0; GN[3, 1] = dtn; GN[3, 2] = 0;
                        // 5-я строка
                        GN[4, 0] = 0; GN[4, 1] = 0; GN[4, 2] = (dtn * dtn) / 2;
                        // 6-я строка
                        GN[5, 0] = 0; GN[5, 1] = 0; GN[5, 2] = dtn;

                        // Транспонированная
                        objMatrix.MTransp(
                                6,
                                3,
                                GN,
                                ref GNT
                               );
                        // ----------------------------------------------------------------------------
                        // Случайные ускорения Axy-плоскость, Az - высота 
                        // Нормальное распределение Гаусса

                        Ax = objClassMRND_X.RandG(0, CKOA_pl);
                        Ay = objClassMRND_Y.RandG(0, CKOA_H);
                        Az = objClassMRND_Z.RandG(0, CKOA_pl);
                        // ----------------------------------------------------------------------------
                        // Матрица случайных ускорений 3x1
                        // AN=|Ax Az Ay| транспонированная

                        AN[0, 0] = Ax;
                        AN[1, 0] = Az;
                        AN[2, 0] = Ay;
                        // ----------------------------------------------------------------------------
                        // Дисперсионная матрица ускорений 6x6

                        // 1-я строка
                        DA[0, 0] = CKOA_pl * CKOA_pl;
                        for (i = 0; i < 5; i++)
                        { DA[0, i + 1] = 0; }
                        // 2-я строка
                        DA[1, 0] = 0;
                        DA[1, 1] = CKOA_pl * CKOA_pl;
                        for (i = 0; i < 4; i++)
                        { DA[1, i + 2] = 0; }
                        // 3-я строка
                        DA[2, 0] = 0;
                        DA[2, 1] = 0;
                        DA[2, 2] = CKOA_pl * CKOA_pl;
                        for (i = 0; i < 3; i++)
                        { DA[2, i + 3] = 0; }
                        // 4-я строка
                        for (i = 0; i < 3; i++)
                        { DA[3, i] = 0; }
                        DA[3, 3] = CKOA_pl * CKOA_pl;
                        DA[3, 4] = 0;
                        DA[3, 5] = 0;
                        // 5-я строка
                        for (i = 0; i < 4; i++)
                        { DA[4, i] = 0; }
                        DA[4, 4] = CKOA_H * CKOA_H;
                        DA[4, 5] = 0;
                        // 6-я строка
                        for (i = 0; i < 5; i++)
                        { DA[5, i] = 0; }
                        DA[5, 5] = CKOA_H * CKOA_H;
                        // ----------------------------------------------------------------------------
                        // Экстраполированное значение состояния траектории (предсказание в текущий момент) 6x1

                        double[,] SEN_Dop1 = new double[6, 1];
                        double[,] SEN_Dop2 = new double[6, 1];

                        SEN_Dop1 = objMatrix.MatrixMultiplication(FN, S_N_1);
                        SEN_Dop2 = objMatrix.MatrixMultiplication(GN, AN);
                        SEN = objMatrix.AddMatrix(SEN_Dop1, SEN_Dop2);
                        // ----------------------------------------------------------------------------
                        // Экстраполированная корреляционная матрица ошибок оценки траектории 6x6

                        double[,] PSI_Ext_Dop1 = new double[6, 6];
                        double[,] PSI_Ext_Dop2 = new double[6, 6];
                        double[,] PSI_Ext_Dop3 = new double[6, 6];
                        double[,] PSI_Ext_Dop4 = new double[6, 6];

                        PSI_Ext_Dop1 = objMatrix.MatrixMultiplication(FN, PSI_N_1);
                        PSI_Ext_Dop2 = objMatrix.MatrixMultiplication(PSI_Ext_Dop1, FNT);
                        PSI_Ext_Dop3 = objMatrix.MatrixMultiplication(GN, GNT);
                        PSI_Ext_Dop4 = objMatrix.MatrixMultiplication(PSI_Ext_Dop3, DA);
                        PSI_Ext_N = objMatrix.AddMatrix(PSI_Ext_Dop2, PSI_Ext_Dop4);
                        // ----------------------------------------------------------------------------
                        // Матрица взаимосвязи между вектором измеряемых параметров 3x1 (координаты ВО) 
                        // и вектором наблюдаемых параметров 6x1 (координаты ВО и составляющие вектора скорости 
                        // по каждой из координат)
                        // 3x6

                        // 1-я строка
                        HN[0, 0] = 1;
                        for (i = 1; i < 6; i++)
                        { HN[0, i] = 0; }
                        // 2-я строка
                        HN[1, 0] = 0;
                        HN[1, 1] = 0;
                        HN[1, 2] = 1;
                        for (i = 3; i < 6; i++)
                        { HN[1, i] = 0; }
                        // 3-я строка
                        for (i = 0; i < 4; i++)
                        { HN[2, i] = 0; }
                        HN[2, 4] = 1;
                        HN[2, 5] = 0;

                        // Транспонированная
                        objMatrix.MTransp(
                                3,
                                6,
                                HN,
                                ref HNT
                               );
                        // ----------------------------------------------------------------------------
                        // Записываем в отметку экстраполированные значения

                        objMark1.CoordExtr = new TimeMarkCoord(
                                                               MarkAirObject.Latitude,
                                                               MarkAirObject.Longitude,
                                                               MarkAirObject.Altitude,
                                                               //parameters,
                                                               stations,
                                                               MarkAirObject.Time,
                                                               MarkAirObject.Freq,
                                                               MarkAirObject.dFreq,
                                                               MarkAirObject._time
                                                              );

                        objMark1.CoordExtr.Xe = SEN[0, 0];
                        objMark1.CoordExtr.Ze = SEN[2, 0];
                        objMark1.CoordExtr.Ye = SEN[4, 0];
                        objMark1.CoordExtr.VXe = SEN[1, 0];
                        objMark1.CoordExtr.VZe = SEN[3, 0];
                        objMark1.CoordExtr.VYe = SEN[5, 0];


                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // 07_04

                        var matrixCtime = new double[1, 3];
                        matrixCtime = Calc_dt_time_ext(objMark1.CoordExtr.Xe, objMark1.CoordExtr.Ye, objMark1.CoordExtr.Ze);

                        objMark1.CoordExtr.dt2e = matrixCtime[0, 0];
                        objMark1.CoordExtr.dt3e = matrixCtime[0, 1];
                        objMark1.CoordExtr.dt4e = matrixCtime[0, 2];

                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // ----------------------------------------------------------------------------
                        // Расчет матрицы 3x3 ошибок измерения координат ИРИ в заданной точке 

                        //if (flagStatDyn==true)
                        //{
                        objMark1.CoordExtr.Error = objMark1.CoordExtr.ErrorMeasurement(
                                                                stations,
                                                                MarkAirObject.Latitude,
                                                                MarkAirObject.Longitude,
                                                                MarkAirObject.Altitude,
                                                                MarkAirObject.Time,
                                                                objMark1.CoordExtr.Xe,
                                                                objMark1.CoordExtr.Ye,
                                                                objMark1.CoordExtr.Ze
                                                               );
                        //}
                        //else
                        //{
                            objMark1.CoordExtr.ErrorStat = objMark1.CoordExtr.ErrorMeasurementStat(
                                                                                              CKO_X,
                                                                                              CKO_Y,
                                                                                              CKO_Z
                                                                                              );
                        //}
                        // ----------------------------------------------------------------------------
                        // Коэффициент усиления фильтра

                        double[,] KN_dop1 = new double[3, 6];
                        double[,] KN_dop2 = new double[3, 3];
                        double[,] KN_dop3 = new double[3, 3];
                        double[,] KN_dop4 = new double[3, 3];
                        double[,] KN_dop5 = new double[6, 3];

                        KN_dop1 = objMatrix.MatrixMultiplication(HN, PSI_Ext_N);
                        KN_dop2 = objMatrix.MatrixMultiplication(KN_dop1, HNT);

                        if (flagStatDyn == true)
                            KN_dop3 = objMatrix.AddMatrix(KN_dop2, objMark1.CoordExtr.Error);
                        else
                            KN_dop3 = objMatrix.AddMatrix(KN_dop2, objMark1.CoordExtr.ErrorStat);

                        objMatrix.MInv(
                              3,
                              KN_dop3,
                              ref KN_dop4
                            );

                        KN_dop5 = objMatrix.MatrixMultiplication(PSI_Ext_N, HNT);

                        KN = objMatrix.MatrixMultiplication(KN_dop5, KN_dop4);
                        // ----------------------------------------------------------------------------
                        // Состояние траектории на n-ом шаге (расчет оценки параметроа траектории) S(n) 6x1

                        double[,] SN_dop1 = new double[3, 1];
                        double[,] SN_dop2 = new double[3, 1];
                        double[,] SN_dop3 = new double[6, 1];

                        // .............................................................................
                        // Матрица n-го измерения (XZY)

                        double[,] YN = new double[3, 1];

                        YN[0, 0] = objMark1.CoordReal.X;
                        YN[1, 0] = objMark1.CoordReal.Z;
                        YN[2, 0] = objMark1.CoordReal.Y;
                        // .............................................................................
                        SN_dop1 = objMatrix.MatrixMultiplication(HN, SEN);
                        SN_dop2 = objMatrix.SubMatrix(YN, SN_dop1);
                        SN_dop3 = objMatrix.MatrixMultiplication(KN, SN_dop2);
                        SN = objMatrix.AddMatrix(SEN, SN_dop3);
                        // .............................................................................
                        // Добавляем S(n) как следующую точку фильтра
                        // !!! Здесь просто заполняем фильтрованные координаты (саму отметку пока не добавляем)
                        // S(n)=|Xn Vx Zn Zy Yn Yz| транспонированная

                        objMark1.CoordFilter = new TimeMarkCoord(
                                                               MarkAirObject.Latitude,
                                                               MarkAirObject.Longitude,
                                                               MarkAirObject.Altitude,
                                                               //parameters,
                                                               stations,
                                                               MarkAirObject.Time,
                                                               MarkAirObject.Freq,
                                                               MarkAirObject.dFreq,
                                                               MarkAirObject._time
                                                              );

                        objMark1.CoordFilter.X = SN[0, 0];
                        objMark1.CoordFilter.VX = SN[1, 0];
                        objMark1.CoordFilter.Z = SN[2, 0];
                        objMark1.CoordFilter.VZ = SN[3, 0];
                        objMark1.CoordFilter.Y = SN[4, 0];
                        objMark1.CoordFilter.VY = SN[5, 0];
                        // .............................................................................
                        // Пока сохраняем состояние траектории на n-ом шаге

                        objMatrix.ChangeMatrix(SN, ref objTrackAirObject.S_N);

                        // .............................................................................
                        // В классе траектории рассчитанное S(n) -> пишем на место S(n-1) для следующего шага
                        // !!! Здесь пока не пишем (поменяем при втром обходе трасс у той траектории, где dRR=min)

                        //objMatrix.ChangeMatrix(SN, ref S_N_1);
                        // ............................................................................

                        // ----------------------------------------------------------------------------
                        // Корреляционная матрица ошибок оценки траектории 6x6
                        // !!! Здесь пока не пишем (поменяем при втром обходе трасс у той траектории, где dRR=min)
                        // Пока сохраняем матрицу на n-ом шаге

                        double[,] PSI_N_dop1 = new double[6, 6];
                        double[,] PSI_N_dop2 = new double[6, 6];

                        PSI_N_dop1 = objMatrix.MatrixMultiplication(KN, HN);
                        PSI_N_dop2 = objMatrix.MatrixMultiplication(PSI_N_dop1, PSI_Ext_N);
                        PSI_N = objMatrix.SubMatrix(PSI_Ext_N, PSI_N_dop2);

                        // !!! Пока здесь не пишем
                        // PSI_N пишем на место PSI_N_1 в классе траектории для следующего шага
                        //objMatrix.ChangeMatrix(PSI_N, ref PSI_N_1);
                        objMatrix.ChangeMatrix(PSI_N, ref objTrackAirObject.PSI_N);

                        // -------------------------------------------------------------------------------------
                        // Строб
                        // !!! dtn - рразность времени между текущей отметкой и последней отметкой со Skip=false(0)
                        //     Это отметка[index]

                        if (flagStatDyn == true)
                        {
                            ThetaX = objMark1.CoordExtr.Error[0, 0];
                            ThetaZ = objMark1.CoordExtr.Error[1, 1];
                            ThetaY = objMark1.CoordExtr.Error[2, 2];
                        }
                        else
                        {
                            ThetaX = objMark1.CoordExtr.ErrorStat[0, 0];
                            ThetaZ = objMark1.CoordExtr.ErrorStat[1, 1];
                            ThetaY = objMark1.CoordExtr.ErrorStat[2, 2];
                        }

                        // Считаем строб (эллипсоид)
                        // !!! Полуоси эллипсоида -> это StrobX,StrobZ,StrobY
                        StrobX = Vdop * dtn + ThetaX;
                        StrobY = Vdop * dtn + ThetaY;
                        StrobZ = Vdop * dtn + ThetaZ;
                        // ------------------------------------------------------------------------------------
                        // Проверка по стробу
                        // Определяем, лежит ли отметка внутри эллипсоида

                        // .............................................................................
                        // Попала
                        /*
                                                if (
                                                   (  (((objMark1.CoordReal.X- objTrackAirObject.Marks[index].CoordReal.X) * 
                                                        (objMark1.CoordReal.X- objTrackAirObject.Marks[index].CoordReal.X)) / (StrobX * StrobX)) +
                                                     (((objMark1.CoordReal.Y - objTrackAirObject.Marks[index].CoordReal.Y) *
                                                       (objMark1.CoordReal.Y - objTrackAirObject.Marks[index].CoordReal.Y)) / (StrobY * StrobY)) +
                                                     (((objMark1.CoordReal.Z - objTrackAirObject.Marks[index].CoordReal.Z) *
                                                        (objMark1.CoordReal.Z - objTrackAirObject.Marks[index].CoordReal.Z)) / (StrobZ * StrobZ)) ) <= 1
                                                  )
                        */
                        if (
                           ((((objMark1.CoordReal.X - objTrackAirObject.Marks[index].CoordReal.X) *
                                (objMark1.CoordReal.X - objTrackAirObject.Marks[index].CoordReal.X)) / (StrobX * StrobX)) +
                             //(((objMark1.CoordReal.Y - objTrackAirObject.Marks[index].CoordReal.Y) *
                             //  (objMark1.CoordReal.Y - objTrackAirObject.Marks[index].CoordReal.Y)) / (StrobY * StrobY)) +
                             (((objMark1.CoordReal.Z - objTrackAirObject.Marks[index].CoordReal.Z) *
                                (objMark1.CoordReal.Z - objTrackAirObject.Marks[index].CoordReal.Z)) / (StrobZ * StrobZ))) <= 1
                          )

                        {
                            objTrackAirObject.PopStrob = true;
                            objTrackAirObject.AddTek = true;

                            // Пока везде ставим 1 (при втором обходе трасс в трассе с dRR=min выставится в 0)
                            objMark1.Skip = true; // 1

                            // Расстояние между реальным значением и экстраполированным
                            objTrackAirObject.dRR = Math.Sqrt(
                                                              (objMark1.CoordReal.X - objMark1.CoordExtr.Xe) * (objMark1.CoordReal.X - objMark1.CoordExtr.Xe) +
                                                              (objMark1.CoordReal.Y - objMark1.CoordExtr.Ye) * (objMark1.CoordReal.Y - objMark1.CoordExtr.Ye) +
                                                              (objMark1.CoordReal.Z - objMark1.CoordExtr.Ze) * (objMark1.CoordReal.Z - objMark1.CoordExtr.Ze)
                                                              );

                        } // Попала
                        // ............................................................................
                        // Не попала

                        else
                        {
                            objTrackAirObject.PopStrob = false;
                            objTrackAirObject.AddTek = true;

                            objMark1.Skip = true; // 1

                            objTrackAirObject.dRR = -1;

                        } // Не попала
                          // ............................................................................

                        // ------------------------------------------------------------------------------

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ФИЛЬТР КАЛМАНА

                        // ----------------------------------------------------------------------------
                        // Добавляем отметку

                        objMark1.ID = objTrackAirObject.Marks.Count;
                        objMark1.Time = MarkAirObject.Time;
                        objTrackAirObject.Marks.Add(objMark1);
                        // ----------------------------------------------------------------------------

                        return true;

                    } // if(dt<=parameters.twaitAirObject)
                    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Time<=Tdop


                    // Time>Tdop @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    // if(dt>parameters.twaitAirObject)

                    else
                    {
                        return false;
                    }
                    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Time>Tdop

                } // if (index>=0)

                // index=-1;
                else
                    return false;

            } // objTrackAirObject.Marks != null

            // objTrackAirObject.Marks != null=null
            else
                return false;
        }
        // ************************************************************************* AddMark

        // Calc_dt_ext *********************************************************************
        // Расчет временных задержек
        // 07_04

        public double[,] Calc_dt_time_ext(
                                // координаты обэекта в МЗСК
                                double X,
                                double Y,
                                double Z
                               )
        {
            // ....................................................................................
            double RCC = 0;

            // Расстояние между ППi и ИРИ
            double R0 = 0;
            double R1 = 0;
            double R2 = 0;
            double R3 = 0;

            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3
            double dr01 = 0;
            double dr02 = 0;
            double dr03 = 0;

            //double cc = 299792458;
            double cc = 299796459.2;

            var matrixC = new double[1, 3];
            // ....................................................................................

            RCC = Math.Sqrt(X * X + Y * Y + Z * Z);
            R0 = RCC;

            R1 = Math.Sqrt((X - stations[1].Position.X) * (X - stations[1].Position.X)
                           + (Y - stations[1].Position.Y) * (Y - stations[1].Position.Y) +
                             (Z - stations[1].Position.Z) * (Z - stations[1].Position.Z));


            R2 = Math.Sqrt((X - stations[2].Position.X) * (X - stations[2].Position.X)
                           + (Y - stations[2].Position.Y) * (Y - stations[2].Position.Y) +
                             (Z - stations[2].Position.Z) * (Z - stations[2].Position.Z));

            R3 = Math.Sqrt((X - stations[3].Position.X) * (X - stations[3].Position.X)
                           + (Y - stations[3].Position.Y) * (Y - stations[3].Position.Y) +
                             (Z - stations[3].Position.Z) * (Z - stations[3].Position.Z));

            // ....................................................................................
            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3

            dr01 = R1 - R0;
            dr02 = R2 - R0;
            dr03 = R3 - R0;

            matrixC[0, 0] = dr01 / cc;
            matrixC[0, 1] = dr02 / cc;
            matrixC[0, 2] = dr03 / cc;
            // .......................................................................................

            return matrixC;
        }
        // ********************************************************************* Calc_dt_ext


    } // Class
} // Namespace
