﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using GeoCalculator;

using fDRM;

namespace WpfMapRastr
{
    // Формироввание листов источников и листов ВО
    public class TracksDefinition
    {
        // Параметры ********************************************************************

        // Максимально допустимое время ожидания отметки
        public double twait { get; set; }
        // Максимально допустимое время ожидания отметки для сопровождаемой трассы
        public double twaitAirObject { get; set; }
        // Допустимая скорость
        public double Vdop { get; set; }
        // СКО ускорения на плоскости
        public double CKOA_pl { get; set; }
        // СКО ускорения по высоте
        public double CKOA_H { get; set; }
        // СКО по координатам для формирования статической матрицы D 
        // ошибок измерения в данный момент времени
        public double CKO_X { get; set; }
        public double CKO_Y { get; set; }
        public double CKO_Z { get; set; }
        // Количество точек для инициализации фильтра
        public int numbInit { get; set; }
        // ==true -> использовать динамическую матрицу ошибок измерений, 
        // ==false -> использовать статическую матрицу ошибок измерений
        public bool flagStatDyn { get; set; }
        // При инициализации фильтра
        public bool flagStatDynInit { get; set; }

        // =false->инверсия мнимых точек
        // =true->отбрасывание мнимых точек
        public bool flagMnimPoints { get; set; }

        // Отсеивание по высоте
        public double Hmax { get; set; }
        
        // замена рассчитанной высоты введенной
        public double HInput { get; set; }

        public List<Station> listStations { get; set; }

        // *****************************************************************

        // Лист трасс ВО
        public List<TrackAirObject> _listTrackAirObject;

        // Лист трасс источников
        public List<TrackSource> listTrackSource = new List<TrackSource>();


        // Constructor *******************************************************************
        public TracksDefinition(
                                // Лист траекторий ВО
                                ref List<TrackAirObject> _listTrackAirObject1
                               )
        {
            //stations = _stations;
            _listTrackAirObject = new List<TrackAirObject>();
            _listTrackAirObject = _listTrackAirObject1;
        }
        // ******************************************************************* Constructor

        // MainFunction ******************************************************************

        public void f_TracksDefinition(

                                // Входная отметка
                                InputPointTrack objInputPointTrack
                                     )
        {
            // VAR --------------------------------------------------------------------------

            double LatMark = 0;
            double LongMark = 0;
            double HMark = 0;
            double Time = 0;

            int iAirObject = 0;
            int iSource = 0;

            // =true, если отметка вошла в трассу ВО
            bool flagVO = false;
            bool flagVO1 = false;

            // =false, если отметка добавилась к одной из трасс по времени, но
            // не попала в строб ни к одной из трасс
            bool flagPopStrobGeneral = false;

            // = true, если отметка подошла к i-й трассе источника
            bool flSourceTrack = false;
            // = true, если отметка подошла к одной из трасс источников
            bool flSourceTrackAll = false;


            bool fl_min = false;
            double dRR_min = 1000000000;
            int index_min = -1;

            int indtmp = -1;

            Matrix objMatrix = new Matrix();

            double XRLS = 0;
            double YRLS = 0;
            double ZRLS = 0;
            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double lt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;
            // -------------------------------------------------------------------------- VAR

            // Change 16_12
            // !!! Пока заккомментировать, чтобы для отладки на f_DRM подавать все отсчеты
            // !!! В рабочем откомментировать
            if ((objInputPointTrack.HMark == -1) && (objInputPointTrack.LatMark == -1) && (objInputPointTrack.LongMark == -1))
                return;
            if ((objInputPointTrack.HMark < 0) || (objInputPointTrack.HMark > Hmax))
                return;


            // OTL OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
            // !!! Для отладки функции RDM
            // Change 16_12

            /*
                        // Вычисление координат по задержкам >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        // Входной лист для подачи в функцию ...........................................
                        List<ClassObject> LstClassObject1 = new List<ClassObject>();

                        // SP1 (base)
                        ClassObject objClassObject1 = new ClassObject();
                        objClassObject1.Latitude = listStations[0].Position.Latitude;
                        objClassObject1.Longitude = listStations[0].Position.Longitude;
                        objClassObject1.Altitude = listStations[0].Position.Altitude;

                        objClassObject1.tau = 0;

                        objClassObject1.BaseStation = true;
                        objClassObject1.IndexStation = 1;
                        LstClassObject1.Add(objClassObject1);

                        // SP2
                        ClassObject objClassObject2 = new ClassObject();
                        objClassObject2.Latitude = listStations[1].Position.Latitude;
                        objClassObject2.Longitude = listStations[1].Position.Longitude;
                        objClassObject2.Altitude = listStations[1].Position.Altitude;

                        objClassObject2.tau = objInputPointTrack.dt1;
                        // Otl
                        //objClassObject2.tau = 1.983E-7;
                        //objClassObject2.tau = objInputPointTrack.dt1-9E-9;
                        objClassObject2.tau = objInputPointTrack.dt1 - 6E-9;
                        objClassObject2.tau = objInputPointTrack.dt1 - 5E-9;


                        objClassObject2.BaseStation = false;
                        objClassObject2.IndexStation = 2;
                        LstClassObject1.Add(objClassObject2);

                        // SP3
                        ClassObject objClassObject3 = new ClassObject();
                        objClassObject3.Latitude = listStations[2].Position.Latitude;
                        objClassObject3.Longitude = listStations[2].Position.Longitude;
                        objClassObject3.Altitude = listStations[2].Position.Altitude;

                        objClassObject3.tau = objInputPointTrack.dt2;
                        // Otl
                        //objClassObject3.tau = -2.728E-7;
                        //objClassObject3.tau = objInputPointTrack.dt2-7.2E-9;
                        objClassObject3.tau = objInputPointTrack.dt2 - 14E-9;
                        objClassObject3.tau = objInputPointTrack.dt2 - 3E-9;

                        objClassObject3.BaseStation = false;
                        objClassObject3.IndexStation = 3;
                        LstClassObject1.Add(objClassObject3);

                        // SP4
                        ClassObject objClassObject4 = new ClassObject();
                        objClassObject4.Latitude = listStations[3].Position.Latitude;
                        objClassObject4.Longitude = listStations[3].Position.Longitude;
                        objClassObject4.Altitude = listStations[3].Position.Altitude;

                        objClassObject4.tau = objInputPointTrack.dt3;
                        // Otl
                        //objClassObject4.tau = -1.058E-7;
                       //objClassObject4.tau = objInputPointTrack.dt3-12.8E-9;
                        objClassObject4.tau = objInputPointTrack.dt3 - 3E-9;
                        objClassObject4.tau = objInputPointTrack.dt3 + 10E-9;

                        objClassObject4.BaseStation = false;
                        objClassObject4.IndexStation = 4;
                        LstClassObject1.Add(objClassObject4);
                        // ............................................ Входной лист для подачи в функцию

                        // Расчет координат по задержкам ................................................


                        ClassDRM_dll objClassDRM_dll = new ClassDRM_dll();
                        ClassObjectTmp ObjClassObjectTmp1 = new ClassObjectTmp();
                        ObjClassObjectTmp1 = objClassDRM_dll.f_DRM(
                                                                    LstClassObject1,
                                                                    flagMnimPoints,
                                                                    HInput,
                                                                    false,
                                                                    true,
                                                                    true
                                                                    //ref GlobalVarMapMain.sh1RDM,
                                                                    //ref GlobalVarMapMain.sh2RDM
                                                                    );

                        // ................................................ Расчет координат по задержкам

                        // .............................................................................
                        // Рассчитанные координаты пишем в файл
                        // Change 16_12 Black

                        string sfilter = "";

                        sfilter = 
                                      Convert.ToString(ObjClassObjectTmp1.Latitude) + " " +
                                      Convert.ToString(ObjClassObjectTmp1.Longitude) +
                                      " " + Convert.ToString(ObjClassObjectTmp1.Altitude) +
                                      " " + Convert.ToString(LstClassObject1[1].tau) +
                                      " " + Convert.ToString(LstClassObject1[2].tau) +
                                      " " + Convert.ToString(LstClassObject1[3].tau)+
                                      " "+ Convert.ToString(ObjClassObjectTmp1.FlagRezCalc);

                        GlobalVarMapMain.swK.WriteLine(sfilter);

                        // .............................................................................
                        // Change 16_12

                        if (
                            (ObjClassObjectTmp1.FlagRezCalc != 0)
                            )
                            return;

                        // Change 16_12
                        if (
                            (ObjClassObjectTmp1.Altitude <0)||
                            (ObjClassObjectTmp1.Altitude >Hmax)
                            )
                            return;
                        // .............................................................................

                        // -----------------------------------------------------------------------------
                        LatMark = ObjClassObjectTmp1.Latitude;
                        LongMark = ObjClassObjectTmp1.Longitude;
                        HMark = ObjClassObjectTmp1.Altitude;

                        Time = objInputPointTrack.Time;
                        // -----------------------------------------------------------------------------

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Вычисление координат по задержкам
            */
            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO OTL




            // -----------------------------------------------------------------------------

            // !!! После отладки восстановить
            LatMark = objInputPointTrack.LatMark;
            LongMark = objInputPointTrack.LongMark;
            HMark = objInputPointTrack.HMark;
            Time = objInputPointTrack.Time;

            // -----------------------------------------------------------------------------

            // MAIN @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            // Основной цикл формирования траекторий

            // MARK *************************************************************************
            // Обработка входной отметки

            // Формируем отметку MarkCoord (здесь же пересчет в декартовы идет)

            MarkCoord _MarkCoord = new MarkCoord(
                                                    LatMark,
                                                    LongMark,
                                                    HMark,
                                                    listStations[0].Position.LatitudeRLSBase,
                                                    listStations[0].Position.LongitudeRLSBase,
                                                    listStations[0].Position.AltitudeRLSBase
                                                                             );

            // Создаем отметку TimeMarkCoord
            TimeMarkCoord objTimeMarkCoord = new TimeMarkCoord(
                                                               // Входная отметка
                                                               _MarkCoord.Latitude,
                                                               _MarkCoord.Longitude,
                                                               _MarkCoord.Altitude,
                                                                // RLS
                                                                listStations,
                                                                // Время прихода отметки
                                                                Time,
                                                                objInputPointTrack.Freq,
                                                                objInputPointTrack.dFreq,
                                                                objInputPointTrack._time
                                                               );


            // Change 16_12
            if ((objTimeMarkCoord.Altitude < 0) || (objTimeMarkCoord.Altitude > Hmax))
                return;


            // Экстраполированные координаты
            // Если пойдет в ВО -> экстраполированные координаты пересчитаются в фильтре
            // и перепишутся, матрица там же пересчитается
            objTimeMarkCoord.Xe = objTimeMarkCoord.X;
            objTimeMarkCoord.Ye = objTimeMarkCoord.Y;
            objTimeMarkCoord.Ze = objTimeMarkCoord.Z;

            // Матрица ошибок измерений 3x3
            //if (flagStatDyn == true)
            //{
            objTimeMarkCoord.Error = objTimeMarkCoord.ErrorMeasurement(
                                                                 listStations,
                                                               // Входная отметка
                                                               _MarkCoord.Latitude,
                                                               _MarkCoord.Longitude,
                                                               _MarkCoord.Altitude,
                                                                // Время прихода отметки
                                                                Time,
                                                                objTimeMarkCoord.Xe,
                                                                objTimeMarkCoord.Ye,
                                                                objTimeMarkCoord.Ze
                                                                      );
            //}
            //else
            //{
            objTimeMarkCoord.ErrorStat = objTimeMarkCoord.ErrorMeasurementStat(
                                                                           CKO_X,
                                                                           CKO_Y,
                                                                           CKO_Z
                                                                          );
            //}


            // ************************************************************************* MARK

            // FIRST ************************************************************************
            // Первая отметка

            if (
                (listTrackSource != null) &&
                (listTrackSource.Count == 0) &&
                (_listTrackAirObject.Count == 0)
               )
            {
                // Создаем одну траекторию источников
                TrackSource objTrackSource = new TrackSource(
                                                             // RLS
                                                             listStations,
                                                             // Лист траекторий ВО
                                                             ref _listTrackAirObject
                                                            );

                // В этой траектории создаем лист отметок
                objTrackSource.TimeMarksCoord = new List<TimeMarkCoord>();
                // Добавляем отметку в лист отметок этой траектории
                objTrackSource.AddMarkSource(objTimeMarkCoord);
                objTrackSource.TimeMarksCoord[0].ID = 0;
                // Добавляем траекторию в лист траекторий источников
                listTrackSource.Add(objTrackSource);
                listTrackSource[0].ID = 0;

                return;
            }
            // ************************************************************************ FIRST

            // TRACKS_AIR_OBJECTS ***********************************************************
            // Цикл по трассам ВО

            // ------------------------------------------------------------------------------
            if (_listTrackAirObject.Count > 0)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 1-й обход

                flagVO = false;
                flagVO1 = false;

                for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                {
                    flagVO1 = _listTrackAirObject[iAirObject].AddMarkAirObject(

                                  // Входная отметка
                                  objTimeMarkCoord,

                                  // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                                  twaitAirObject,
                                  // СКО ускорения на плоскости
                                  CKOA_pl,
                                  // СКО ускорения по высоте
                                  CKOA_H,
                                  // Допустимая скорость дрона
                                  Vdop,
                                  // СКО по координатам для формирования статической матрицы D 
                                  // ошибок измерения в данный момент времени
                                  CKO_X,
                                  CKO_Y,
                                  CKO_Z,
                                  // ==true -> использовать динамическую матрицу ошибок измерений, 
                                  // ==false -> использовать статическую матрицу ошибок измерений
                                  flagStatDyn,

                                  iAirObject,
                                  // Лист трасс ВО
                                  ref _listTrackAirObject

                                                 );

                    if (flagVO1 == true)
                    {
                        flagVO = true;
                        if (_listTrackAirObject[iAirObject].PopStrob == true)
                            flagPopStrobGeneral = true;

                    }
                } // FOR(listTrackAirObjects)
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // 2-й обход
                  // В трассах, в которых отметка попала в строб, ищем трассу с минимальным расстоянием
                  // между реальной и экстраполированной точкой

                fl_min = false;
                dRR_min = 1000000000;
                index_min = -1;

                if (flagVO == true)
                {
                    for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                    {

                        // IF1
                        if (
                            (_listTrackAirObject[iAirObject].AddTek == true) &&
                            (_listTrackAirObject[iAirObject].dRR != -1) &&
                            (_listTrackAirObject[iAirObject].PopStrob == true) &&
                            (Math.Abs(_listTrackAirObject[iAirObject].dRR) < Math.Abs(dRR_min))
                          )
                        {
                            dRR_min = _listTrackAirObject[iAirObject].dRR;
                            index_min = iAirObject;
                            //_listTrackAirObject[iAirObject].fl_dRR_min = true;
                            fl_min = true;
                        } // IF1

                    } // FOR

                    // IF2
                    if (
                      (fl_min == true) &&
                      (index_min != -1)
                      )
                    {
                        _listTrackAirObject[index_min].fl_dRR_min = true;

                    } // IF2

                } // flagVO==true
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // 3-й обход

                if (flagVO == true)
                {
                    for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                    {

                        if ((_listTrackAirObject[iAirObject].AddTek == true))
                        {
                            _listTrackAirObject[iAirObject].AddTek = false;

                            if (_listTrackAirObject[iAirObject].Marks.Count > 0)
                            {
                                indtmp = _listTrackAirObject[iAirObject].Marks.Count - 1;

                            }
                            // ..............................................................
                            // IF3

                            if (
                               (_listTrackAirObject[iAirObject].PopStrob == true) &&
                               (_listTrackAirObject[iAirObject].fl_dRR_min == true) &&
                               (_listTrackAirObject[iAirObject].dRR != -1)
                              )
                            {
                                if (_listTrackAirObject[iAirObject].Marks.Count > 0)
                                {
                                    _listTrackAirObject[iAirObject].Marks[indtmp].Skip = false;

                                    objMatrix.ChangeMatrix(_listTrackAirObject[iAirObject].PSI_N, ref _listTrackAirObject[iAirObject].PSI_N_1);
                                    objMatrix.ChangeMatrix(_listTrackAirObject[iAirObject].S_N, ref _listTrackAirObject[iAirObject].S_N_1);

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // Пересчет широты и долготы

                                    // base
                                    double latRLS_rad = listStations[0].Position.Latitude * Math.PI / 180;
                                    double longRLS_rad = listStations[0].Position.Longitude * Math.PI / 180;
                                    XRLS = 0;
                                    YRLS = 0;
                                    ZRLS = 0;
                                    xxx13 = 0;
                                    yyy13 = 0;
                                    zzz13 = 0;
                                    lt23 = 0;
                                    lng23 = 0;
                                    hhh23 = 0;

                                    // MZSK -> GEOC
                                    // XYZ RLSBase,m (in geocentric CS)
                                    ClassGeoCalculator.f_BLH_XYZ_84_1
                                                                     (
                                                                      listStations[0].Position.Latitude,
                                                                      listStations[0].Position.Longitude,
                                                                      listStations[0].Position.Altitude,
                                                                      ref XRLS,
                                                                      ref YRLS,
                                                                      ref ZRLS
                                                                      );

                                    ClassGeoCalculator.MZSK_Geoc_V(
                                                              // Base
                                                              latRLS_rad,
                                                              longRLS_rad,
                                                              XRLS, // BaseGeoc
                                                              YRLS,
                                                              ZRLS,

                                                              // Object, MZSK,Filtr
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.X,
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Y,
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Z,
                                                              // Object,Geoc,m
                                                              ref xxx13,
                                                              ref yyy13,
                                                              ref zzz13
                                                            );

                                    ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13,
                                                                      yyy13,
                                                                      zzz13,
                                                                      ref lt23,
                                                                      ref lng23,
                                                                      ref hhh23
                                                                      );
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint = new TimeMarkCoord(
                                                                                                                lt23,
                                                                                                                lng23,
                                                                                                                hhh23,
                                                                                                                listStations,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].Time,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Freq,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.dFreq,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter._time
                                                                                                                  );

                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Latitude = lt23;
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Longitude = lng23;
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Altitude = hhh23;

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                }

                            } // IF3
                            // ..............................................................
                            // ELSE on IF3

                            else
                            {
                                _listTrackAirObject[iAirObject].Marks[indtmp].Skip = true;

                                // Экстаполированные координаты пишем в фильтрованные
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.X =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Xe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Y =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Ye;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Z =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Ze;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VX =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VXe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VY =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VYe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VZ =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VZe;

                            } // ELSE on IF3
                            // ..............................................................
                            _listTrackAirObject[iAirObject].fl_dRR_min = false;
                            _listTrackAirObject[iAirObject].dRR = -1;
                            _listTrackAirObject[iAirObject].PopStrob = false;

                            // ..............................................................


                        } // _listTrackAirObject[iAirObject].AddTek == true

                    } // FOR

                } // flagVO==true
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            } // listTrackAirObject.Count > 0
            // ------------------------------------------------------------------------------
            // Еще нет трасс ВО

            else
                flagVO = false;
            // ------------------------------------------------------------------------------

            // *********************************************************** TRACKS_AIR_OBJECTS

            // SOURCES **********************************************************************
            // Цикл по трассам источников

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // IF1

            if ((
                (listTrackSource.Count > 0) &&
                (flagVO == false) // отметка не подошла к трассам ВО
                ) ||
                (
                 // Отметка попала в одну изтрасс по времени, но не попала в строб
                 // ни в одну из трасс
                 (listTrackSource.Count > 0) &&
                 (flagVO == true)&&
                 (flagPopStrobGeneral==false)
                )
                )
            {
                flSourceTrackAll = false;

                for (iSource = 0; iSource < listTrackSource.Count; iSource++)
                {
                    flSourceTrack = false;

                    // Подходит ли к трассе >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Проверка: подходит ли отметка к трассе источника

                    flSourceTrack = listTrackSource[iSource].DefineMarkTrack(objTimeMarkCoord, twait, Vdop, flagStatDynInit);

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Подходит ли к трассе

                    // Подходит >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Отметка вошла в трассу источников

                    if (flSourceTrack == true)
                    {

                        flSourceTrackAll = true;

                        listTrackSource[iSource].AddMarkSource(objTimeMarkCoord);

                        // Инициализация фильтра
                        if (listTrackSource[iSource].TimeMarksCoord.Count == numbInit)
                        {
                            listTrackSource[iSource].InitKalman(
                                                                   flagStatDynInit,
                                                                   // Лист траекторий ВО
                                                                   ref _listTrackAirObject
                                                               );

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Пересчет широты и долготы

                            // base
                            double latRLS_rad1 = listStations[0].Position.Latitude * Math.PI / 180;
                            double longRLS_rad1 = listStations[0].Position.Longitude * Math.PI / 180;
                            double XRLS1 = 0;
                            double YRLS1 = 0;
                            double ZRLS1 = 0;
                            double xxx131 = 0;
                            double yyy131 = 0;
                            double zzz131 = 0;
                            double lt231 = 0;
                            double lng231 = 0;
                            double hhh231 = 0;

                            // MZSK -> GEOC
                            // XYZ RLSBase,m (in geocentric CS)
                            ClassGeoCalculator.f_BLH_XYZ_84_1
                                                             (
                                                              listStations[0].Position.Latitude,
                                                              listStations[0].Position.Longitude,
                                                              listStations[0].Position.Altitude,
                                                              ref XRLS1,
                                                              ref YRLS1,
                                                              ref ZRLS1
                                                              );

                            ClassGeoCalculator.MZSK_Geoc_V(
                                                      // Base1
                                                      latRLS_rad1,
                                                      longRLS_rad1,
                                                      XRLS1, // BaseGeoc
                                                      YRLS1,
                                                      ZRLS1,

                                                      // Object, MZSK,Filtr
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.X,
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Y,
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Z,
                                                      // Object,Geoc,m
                                                      ref xxx131,
                                                      ref yyy131,
                                                      ref zzz131
                                                    );

                            ClassGeoCalculator.f_XYZ_BLH_84_1(xxx131,
                                                              yyy131,
                                                              zzz131,
                                                              ref lt231,
                                                              ref lng231,
                                                              ref hhh231
                                                              );

                            _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordPaint = new TimeMarkCoord(
                                                                                                                      lt231,
                                                                                                                      lng231,
                                                                                                                      hhh231,
                                                                                                                      listStations,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].Time,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Freq,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.dFreq,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter._time
                                                                                                                      );

                            // Убираем эту траекторию источников
                            listTrackSource.Remove(listTrackSource[iSource]);

                            // Уменьшить ID на 1 в последующих траекториях
                            for (int iSourcetmp = iSource; iSourcetmp < listTrackSource.Count; iSourcetmp++)
                                listTrackSource[iSourcetmp].ID -= 1;

                            iSource -= 1;
                        }

                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Подходит


                } // FOR(listTrackSource)


                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // !!! Отметка не подошла ни к одной траектории

                if(flSourceTrackAll==false)
                {
                    // Создаем одну траекторию источников
                    TrackSource objTrackSource1 = new TrackSource(
                                                                 // RLS
                                                                 listStations,
                                                                 // Лист траекторий ВО
                                                                 ref _listTrackAirObject
                                                                );

                    // В этой траектории создаем лист отметок
                    objTrackSource1.TimeMarksCoord = new List<TimeMarkCoord>();
                    // Добавляем отметку в лист отметок этой траектории
                    objTrackSource1.AddMarkSource(objTimeMarkCoord);
                    // Добавляем траекторию в лист траекторий источников
                    listTrackSource.Add(objTrackSource1);
                    listTrackSource[listTrackSource.Count - 1].ID = listTrackSource.Count - 1;

                }
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



            } // listTrackSource.Count > 0  !!!IF1
              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // !!! ELSE по IF1

            else if (
                (listTrackSource.Count == 0) &&
                (_listTrackAirObject.Count > 0) &&
                (flagVO == false) // отметка не подошла к трассам ВО
                )
            {
                // Создаем одну траекторию источников
                TrackSource objTrackSource1 = new TrackSource(
                                                             // RLS
                                                             listStations,
                                                             // Лист траекторий ВО
                                                             ref _listTrackAirObject
                                                            );

                // В этой траектории создаем лист отметок
                objTrackSource1.TimeMarksCoord = new List<TimeMarkCoord>();
                // Добавляем отметку в лист отметок этой траектории
                objTrackSource1.AddMarkSource(objTimeMarkCoord);
                // Добавляем траекторию в лист траекторий источников
                listTrackSource.Add(objTrackSource1);
                listTrackSource[listTrackSource.Count - 1].ID = listTrackSource.Count - 1;

                return;
            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // ********************************************************************** SOURCES


            // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ MAIN


        }
        // ****************************************************************** MainFunction






        // MainFunction ******************************************************************

        public void f_TracksDefinition_H(

                                // Входная отметка
                                InputPointTrack objInputPointTrack
                                     )
        {
            // VAR --------------------------------------------------------------------------

            double LatMark = 0;
            double LongMark = 0;
            double HMark = 0;
            double Time = 0;

            int iAirObject = 0;
            int iSource = 0;

            // =true, если отметка вошла в трассу ВО
            bool flagVO = false;
            bool flagVO1 = false;

            // = true, если отметка подошла к i-й трассе источника
            bool flSourceTrack = false;
            // = true, если отметка подошла к одной из трасс источников
            bool flSourceTrackAll = false;


            bool fl_min = false;
            double dRR_min = 1000000000;
            int index_min = -1;

            int indtmp = -1;

            Matrix objMatrix = new Matrix();

            double XRLS = 0;
            double YRLS = 0;
            double ZRLS = 0;
            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double lt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;
            // -------------------------------------------------------------------------- VAR

            // Change 16_12
            // !!! Пока заккомментировать, чтобы для отладки на f_DRM подавать все отсчеты
            // !!! В рабочем откомментировать
            if ((objInputPointTrack.HMark == -1) && (objInputPointTrack.LatMark == -1) && (objInputPointTrack.LongMark == -1))
                return;
            if ((objInputPointTrack.HMark < 0) || (objInputPointTrack.HMark > Hmax))
                return;


            // OTL OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
            // !!! Для отладки функции RDM
            // Change 16_12
/*
            // Вычисление координат по задержкам >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // Входной лист для подачи в функцию ...........................................
            List<ClassObject> LstClassObject1 = new List<ClassObject>();

            // SP1 (base)
            ClassObject objClassObject1 = new ClassObject();
            objClassObject1.Latitude = listStations[0].Position.Latitude;
            objClassObject1.Longitude = listStations[0].Position.Longitude;
            objClassObject1.Altitude = listStations[0].Position.Altitude;

            objClassObject1.tau = 0;

            objClassObject1.BaseStation = true;
            objClassObject1.IndexStation = 1;
            LstClassObject1.Add(objClassObject1);

            // SP2
            ClassObject objClassObject2 = new ClassObject();
            objClassObject2.Latitude = listStations[1].Position.Latitude;
            objClassObject2.Longitude = listStations[1].Position.Longitude;
            objClassObject2.Altitude = listStations[1].Position.Altitude;

            objClassObject2.tau = objInputPointTrack.dt1;
            // Otl
            //objClassObject2.tau = 1.983E-7;
            //objClassObject2.tau = objInputPointTrack.dt1-9E-9;
            objClassObject2.tau = objInputPointTrack.dt1 - 6E-9;
            objClassObject2.tau = objInputPointTrack.dt1 - 5E-9;


            objClassObject2.BaseStation = false;
            objClassObject2.IndexStation = 2;
            LstClassObject1.Add(objClassObject2);

            // SP3
            ClassObject objClassObject3 = new ClassObject();
            objClassObject3.Latitude = listStations[2].Position.Latitude;
            objClassObject3.Longitude = listStations[2].Position.Longitude;
            objClassObject3.Altitude = listStations[2].Position.Altitude;

            objClassObject3.tau = objInputPointTrack.dt2;
            // Otl
            //objClassObject3.tau = -2.728E-7;
            //objClassObject3.tau = objInputPointTrack.dt2-7.2E-9;
            objClassObject3.tau = objInputPointTrack.dt2 - 14E-9;
            objClassObject3.tau = objInputPointTrack.dt2 - 3E-9;

            objClassObject3.BaseStation = false;
            objClassObject3.IndexStation = 3;
            LstClassObject1.Add(objClassObject3);

            // SP4
            ClassObject objClassObject4 = new ClassObject();
            objClassObject4.Latitude = listStations[3].Position.Latitude;
            objClassObject4.Longitude = listStations[3].Position.Longitude;
            objClassObject4.Altitude = listStations[3].Position.Altitude;

            objClassObject4.tau = objInputPointTrack.dt3;
            // Otl
            //objClassObject4.tau = -1.058E-7;
            //objClassObject4.tau = objInputPointTrack.dt3-12.8E-9;
            objClassObject4.tau = objInputPointTrack.dt3 - 3E-9;
            objClassObject4.tau = objInputPointTrack.dt3 + 10E-9;

            objClassObject4.BaseStation = false;
            objClassObject4.IndexStation = 4;
            LstClassObject1.Add(objClassObject4);
            // ............................................ Входной лист для подачи в функцию

            // Расчет координат по задержкам ................................................


            ClassDRM_dll objClassDRM_dll = new ClassDRM_dll();
            ClassObjectTmp ObjClassObjectTmp1 = new ClassObjectTmp();
            ObjClassObjectTmp1 = objClassDRM_dll.f_DRM(
                                                        LstClassObject1,
                                                        flagMnimPoints,
                                                        HInput
                                                        //ref GlobalVarMapMain.sh1RDM,
                                                        //ref GlobalVarMapMain.sh2RDM
                                                        );

            // ................................................ Расчет координат по задержкам

            // .............................................................................
            // Рассчитанные координаты пишем в файл
            // Change 16_12

            string sfilter = "";

            sfilter =
                          Convert.ToString(ObjClassObjectTmp1.Latitude) + " " +
                          Convert.ToString(ObjClassObjectTmp1.Longitude) +
                          " " + Convert.ToString(ObjClassObjectTmp1.Altitude) +
                          " " + Convert.ToString(LstClassObject1[1].tau) +
                          " " + Convert.ToString(LstClassObject1[2].tau) +
                          " " + Convert.ToString(LstClassObject1[3].tau) +
                          " " + Convert.ToString(ObjClassObjectTmp1.FlagRezCalc);

            GlobalVarMapMain.swK.WriteLine(sfilter);

            // .............................................................................
            // Change 16_12

            if (
                (ObjClassObjectTmp1.FlagRezCalc != 0)
                )
                return;

            // Change 16_12
            if (
                (ObjClassObjectTmp1.Altitude < 0) ||
                (ObjClassObjectTmp1.Altitude > Hmax)
                )
                return;
            // .............................................................................

            // -----------------------------------------------------------------------------
            LatMark = ObjClassObjectTmp1.Latitude;
            LongMark = ObjClassObjectTmp1.Longitude;
            HMark = ObjClassObjectTmp1.Altitude;

            Time = objInputPointTrack.Time;
            // -----------------------------------------------------------------------------

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Вычисление координат по задержкам
*/
            // OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO OTL




            // -----------------------------------------------------------------------------
            
                        // !!! После отладки восстановить
                        LatMark = objInputPointTrack.LatMark;
                        LongMark = objInputPointTrack.LongMark;
                        HMark = objInputPointTrack.HMark;
                        Time = objInputPointTrack.Time;
            
            // -----------------------------------------------------------------------------

            // MAIN @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            // Основной цикл формирования траекторий

            // MARK *************************************************************************
            // Обработка входной отметки

            // Формируем отметку MarkCoord (здесь же пересчет в декартовы идет)

            MarkCoord _MarkCoord = new MarkCoord(
                                                    LatMark,
                                                    LongMark,
                                                    HMark,
                                                    listStations[0].Position.LatitudeRLSBase,
                                                    listStations[0].Position.LongitudeRLSBase,
                                                    listStations[0].Position.AltitudeRLSBase
                                                                             );

            // Создаем отметку TimeMarkCoord
            TimeMarkCoord objTimeMarkCoord = new TimeMarkCoord(
                                                               // Входная отметка
                                                               _MarkCoord.Latitude,
                                                               _MarkCoord.Longitude,
                                                               _MarkCoord.Altitude,
                                                                // RLS
                                                                listStations,
                                                                // Время прихода отметки
                                                                Time,
                                                                objInputPointTrack.Freq,
                                                                objInputPointTrack.dFreq,
                                                                objInputPointTrack._time
                                                               );


            // Change 16_12
            if ((objTimeMarkCoord.Altitude < 0) || (objTimeMarkCoord.Altitude > Hmax))
                return;


            // Экстраполированные координаты
            // Если пойдет в ВО -> экстраполированные координаты пересчитаются в фильтре
            // и перепишутся, матрица там же пересчитается
            objTimeMarkCoord.Xe = objTimeMarkCoord.X;
            objTimeMarkCoord.Ye = objTimeMarkCoord.Y;
            objTimeMarkCoord.Ze = objTimeMarkCoord.Z;

            // Матрица ошибок измерений 3x3
            //if (flagStatDyn == true)
            //{
            objTimeMarkCoord.Error = objTimeMarkCoord.ErrorMeasurement(
                                                                 listStations,
                                                               // Входная отметка
                                                               _MarkCoord.Latitude,
                                                               _MarkCoord.Longitude,
                                                               _MarkCoord.Altitude,
                                                                // Время прихода отметки
                                                                Time,
                                                                objTimeMarkCoord.Xe,
                                                                objTimeMarkCoord.Ye,
                                                                objTimeMarkCoord.Ze
                                                                      );
            //}
            //else
            //{
            objTimeMarkCoord.ErrorStat = objTimeMarkCoord.ErrorMeasurementStat(
                                                                           CKO_X,
                                                                           CKO_Y,
                                                                           CKO_Z
                                                                          );
            //}


            // ************************************************************************* MARK

            // FIRST ************************************************************************
            // Первая отметка

            if (
                (listTrackSource != null) &&
                (listTrackSource.Count == 0) &&
                (_listTrackAirObject.Count == 0)
               )
            {
                // Создаем одну траекторию источников
                TrackSource objTrackSource = new TrackSource(
                                                             // RLS
                                                             listStations,
                                                             // Лист траекторий ВО
                                                             ref _listTrackAirObject
                                                            );

                // В этой траектории создаем лист отметок
                objTrackSource.TimeMarksCoord = new List<TimeMarkCoord>();
                // Добавляем отметку в лист отметок этой траектории
                objTrackSource.AddMarkSource(objTimeMarkCoord);
                objTrackSource.TimeMarksCoord[0].ID = 0;
                // Добавляем траекторию в лист траекторий источников
                listTrackSource.Add(objTrackSource);
                listTrackSource[0].ID = 0;

                return;
            }
            // ************************************************************************ FIRST

            // TRACKS_AIR_OBJECTS ***********************************************************
            // Цикл по трассам ВО

            // ------------------------------------------------------------------------------
            if (_listTrackAirObject.Count > 0)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 1-й обход

                flagVO = false;
                flagVO1 = false;

                for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                {
                    flagVO1 = _listTrackAirObject[iAirObject].AddMarkAirObject(

                                  // Входная отметка
                                  objTimeMarkCoord,

                                  // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                                  twaitAirObject,
                                  // СКО ускорения на плоскости
                                  CKOA_pl,
                                  // СКО ускорения по высоте
                                  CKOA_H,
                                  // Допустимая скорость дрона
                                  Vdop,
                                  // СКО по координатам для формирования статической матрицы D 
                                  // ошибок измерения в данный момент времени
                                  CKO_X,
                                  CKO_Y,
                                  CKO_Z,
                                  // ==true -> использовать динамическую матрицу ошибок измерений, 
                                  // ==false -> использовать статическую матрицу ошибок измерений
                                  flagStatDyn,

                                  iAirObject,
                                  // Лист трасс ВО
                                  ref _listTrackAirObject

                                                 );

                    if (flagVO1 == true)
                        flagVO = true;

                } // FOR(listTrackAirObjects)
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // 2-й обход
                  // В трассах, в которых отметка попала в строб, ищем трассу с минимальным расстоянием
                  // между реальной и экстраполированной точкой

                fl_min = false;
                dRR_min = 1000000000;
                index_min = -1;

                if (flagVO == true)
                {
                    for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                    {

                        // IF1
                        if (
                            (_listTrackAirObject[iAirObject].AddTek == true) &&
                            (_listTrackAirObject[iAirObject].dRR != -1) &&
                            (_listTrackAirObject[iAirObject].PopStrob == true) &&
                            (Math.Abs(_listTrackAirObject[iAirObject].dRR) < Math.Abs(dRR_min))
                          )
                        {
                            dRR_min = _listTrackAirObject[iAirObject].dRR;
                            index_min = iAirObject;
                            //_listTrackAirObject[iAirObject].fl_dRR_min = true;
                            fl_min = true;
                        } // IF1

                    } // FOR

                    // IF2
                    if (
                      (fl_min == true) &&
                      (index_min != -1)
                      )
                    {
                        _listTrackAirObject[index_min].fl_dRR_min = true;

                    } // IF2

                } // flagVO==true
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // 3-й обход

                if (flagVO == true)
                {
                    for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                    {

                        if ((_listTrackAirObject[iAirObject].AddTek == true))
                        {
                            _listTrackAirObject[iAirObject].AddTek = false;

                            if (_listTrackAirObject[iAirObject].Marks.Count > 0)
                            {
                                indtmp = _listTrackAirObject[iAirObject].Marks.Count - 1;

                            }
                            // ..............................................................
                            // IF3

                            if (
                               (_listTrackAirObject[iAirObject].PopStrob == true) &&
                               (_listTrackAirObject[iAirObject].fl_dRR_min == true) &&
                               (_listTrackAirObject[iAirObject].dRR != -1)
                              )
                            {
                                if (_listTrackAirObject[iAirObject].Marks.Count > 0)
                                {
                                    _listTrackAirObject[iAirObject].Marks[indtmp].Skip = false;

                                    objMatrix.ChangeMatrix(_listTrackAirObject[iAirObject].PSI_N, ref _listTrackAirObject[iAirObject].PSI_N_1);
                                    objMatrix.ChangeMatrix(_listTrackAirObject[iAirObject].S_N, ref _listTrackAirObject[iAirObject].S_N_1);

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // Пересчет широты и долготы

                                    // base
                                    double latRLS_rad = listStations[0].Position.Latitude * Math.PI / 180;
                                    double longRLS_rad = listStations[0].Position.Longitude * Math.PI / 180;
                                    XRLS = 0;
                                    YRLS = 0;
                                    ZRLS = 0;
                                    xxx13 = 0;
                                    yyy13 = 0;
                                    zzz13 = 0;
                                    lt23 = 0;
                                    lng23 = 0;
                                    hhh23 = 0;

                                    // MZSK -> GEOC
                                    // XYZ RLSBase,m (in geocentric CS)
                                    ClassGeoCalculator.f_BLH_XYZ_84_1
                                                                     (
                                                                      listStations[0].Position.Latitude,
                                                                      listStations[0].Position.Longitude,
                                                                      listStations[0].Position.Altitude,
                                                                      ref XRLS,
                                                                      ref YRLS,
                                                                      ref ZRLS
                                                                      );

                                    ClassGeoCalculator.MZSK_Geoc_V(
                                                              // Base
                                                              latRLS_rad,
                                                              longRLS_rad,
                                                              XRLS, // BaseGeoc
                                                              YRLS,
                                                              ZRLS,

                                                              // Object, MZSK,Filtr
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.X,
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Y,
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Z,
                                                              // Object,Geoc,m
                                                              ref xxx13,
                                                              ref yyy13,
                                                              ref zzz13
                                                            );

                                    ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13,
                                                                      yyy13,
                                                                      zzz13,
                                                                      ref lt23,
                                                                      ref lng23,
                                                                      ref hhh23
                                                                      );
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint = new TimeMarkCoord(
                                                                                                                lt23,
                                                                                                                lng23,
                                                                                                                hhh23,
                                                                                                                listStations,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].Time,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Freq,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.dFreq,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter._time
                                                                                                                  );

                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Latitude = lt23;
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Longitude = lng23;


                                    // 1204
                                    //_listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Altitude = hhh23;
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Altitude = HMark;

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                }

                            } // IF3
                            // ..............................................................
                            // ELSE on IF3

                            else
                            {
                                _listTrackAirObject[iAirObject].Marks[indtmp].Skip = true;

                                // Экстаполированные координаты пишем в фильтрованные
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.X =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Xe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Y =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Ye;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Z =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Ze;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VX =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VXe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VY =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VYe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VZ =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VZe;

                            } // ELSE on IF3
                            // ..............................................................
                            _listTrackAirObject[iAirObject].fl_dRR_min = false;
                            _listTrackAirObject[iAirObject].dRR = -1;
                            _listTrackAirObject[iAirObject].PopStrob = false;

                            // ..............................................................


                        } // _listTrackAirObject[iAirObject].AddTek == true

                    } // FOR

                } // flagVO==true
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            } // listTrackAirObject.Count > 0
            // ------------------------------------------------------------------------------
            // Еще нет трасс ВО

            else
                flagVO = false;
            // ------------------------------------------------------------------------------

            // *********************************************************** TRACKS_AIR_OBJECTS

            // SOURCES **********************************************************************
            // Цикл по трассам источников

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // IF1

            if (
                (listTrackSource.Count > 0) &&
                (flagVO == false) // отметка не подошла к трассам ВО
                )
            {
                flSourceTrackAll = false;

                for (iSource = 0; iSource < listTrackSource.Count; iSource++)
                {
                    flSourceTrack = false;

                    // Подходит ли к трассе >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Проверка: подходит ли отметка к трассе источника

                    flSourceTrack = listTrackSource[iSource].DefineMarkTrack(objTimeMarkCoord, twait, Vdop, flagStatDynInit);

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Подходит ли к трассе

                    // Подходит >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Отметка вошла в трассу источников

                    if (flSourceTrack == true)
                    {

                        flSourceTrackAll = true;

                        listTrackSource[iSource].AddMarkSource(objTimeMarkCoord);

                        // Инициализация фильтра
                        if (listTrackSource[iSource].TimeMarksCoord.Count == numbInit)
                        {
                            listTrackSource[iSource].InitKalman(
                                                                   flagStatDynInit,
                                                                   // Лист траекторий ВО
                                                                   ref _listTrackAirObject
                                                               );

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Пересчет широты и долготы

                            // base
                            double latRLS_rad1 = listStations[0].Position.Latitude * Math.PI / 180;
                            double longRLS_rad1 = listStations[0].Position.Longitude * Math.PI / 180;
                            double XRLS1 = 0;
                            double YRLS1 = 0;
                            double ZRLS1 = 0;
                            double xxx131 = 0;
                            double yyy131 = 0;
                            double zzz131 = 0;
                            double lt231 = 0;
                            double lng231 = 0;
                            double hhh231 = 0;

                            // MZSK -> GEOC
                            // XYZ RLSBase,m (in geocentric CS)
                            ClassGeoCalculator.f_BLH_XYZ_84_1
                                                             (
                                                              listStations[0].Position.Latitude,
                                                              listStations[0].Position.Longitude,
                                                              listStations[0].Position.Altitude,
                                                              ref XRLS1,
                                                              ref YRLS1,
                                                              ref ZRLS1
                                                              );

                            ClassGeoCalculator.MZSK_Geoc_V(
                                                      // Base1
                                                      latRLS_rad1,
                                                      longRLS_rad1,
                                                      XRLS1, // BaseGeoc
                                                      YRLS1,
                                                      ZRLS1,

                                                      // Object, MZSK,Filtr
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.X,
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Y,
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Z,
                                                      // Object,Geoc,m
                                                      ref xxx131,
                                                      ref yyy131,
                                                      ref zzz131
                                                    );

                            ClassGeoCalculator.f_XYZ_BLH_84_1(xxx131,
                                                              yyy131,
                                                              zzz131,
                                                              ref lt231,
                                                              ref lng231,
                                                              ref hhh231
                                                              );

                            _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordPaint = new TimeMarkCoord(
                                                                                                                      lt231,
                                                                                                                      lng231,
                                                                                                                      hhh231,
                                                                                                                      listStations,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].Time,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Freq,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.dFreq,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter._time
                                                                                                                      );

                            // Убираем эту траекторию источников
                            listTrackSource.Remove(listTrackSource[iSource]);

                            // Уменьшить ID на 1 в последующих траекториях
                            for (int iSourcetmp = iSource; iSourcetmp < listTrackSource.Count; iSourcetmp++)
                                listTrackSource[iSourcetmp].ID -= 1;

                            iSource -= 1;
                        }

                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Подходит


                } // FOR(listTrackSource)


                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // !!! Отметка не подошла ни к одной траектории

                if (flSourceTrackAll == false)
                {
                    // Создаем одну траекторию источников
                    TrackSource objTrackSource1 = new TrackSource(
                                                                 // RLS
                                                                 listStations,
                                                                 // Лист траекторий ВО
                                                                 ref _listTrackAirObject
                                                                );

                    // В этой траектории создаем лист отметок
                    objTrackSource1.TimeMarksCoord = new List<TimeMarkCoord>();
                    // Добавляем отметку в лист отметок этой траектории
                    objTrackSource1.AddMarkSource(objTimeMarkCoord);
                    // Добавляем траекторию в лист траекторий источников
                    listTrackSource.Add(objTrackSource1);
                    listTrackSource[listTrackSource.Count - 1].ID = listTrackSource.Count - 1;

                }
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



            } // listTrackSource.Count > 0  !!!IF1
              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // !!! ELSE по IF1

            else if (
                (listTrackSource.Count == 0) &&
                (_listTrackAirObject.Count > 0) &&
                (flagVO == false) // отметка не подошла к трассам ВО
                )
            {
                // Создаем одну траекторию источников
                TrackSource objTrackSource1 = new TrackSource(
                                                             // RLS
                                                             listStations,
                                                             // Лист траекторий ВО
                                                             ref _listTrackAirObject
                                                            );

                // В этой траектории создаем лист отметок
                objTrackSource1.TimeMarksCoord = new List<TimeMarkCoord>();
                // Добавляем отметку в лист отметок этой траектории
                objTrackSource1.AddMarkSource(objTimeMarkCoord);
                // Добавляем траекторию в лист траекторий источников
                listTrackSource.Add(objTrackSource1);
                listTrackSource[listTrackSource.Count - 1].ID = listTrackSource.Count - 1;

                return;
            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // ********************************************************************** SOURCES


            // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ MAIN


        }
        // ****************************************************************** MainFunction





        // MainFunction1 ******************************************************************

        public void f_TracksDefinitionZad(

                                         // Входная отметка
                                         InputPointTrack objInputPointTrack
                                         )
        {
            // VAR --------------------------------------------------------------------------

            double LatMark = 0;
            double LongMark = 0;
            double HMark = 0;
            double Time = 0;

            int iAirObject = 0;
            int iSource = 0;

            // =true, если отметка вошла в трассу ВО
            bool flagVO = false;
            bool flagVO1 = false;

            // = true, если отметка подошла к i-й трассе источника
            bool flSourceTrack = false;
            // = true, если отметка подошла к одной из трасс источников
            bool flSourceTrackAll = false;


            bool fl_min = false;
            double dRR_min = 1000000000;
            int index_min = -1;

            int indtmp = -1;

            Matrix objMatrix = new Matrix();

            double XRLS = 0;
            double YRLS = 0;
            double ZRLS = 0;
            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double lt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;
            // -------------------------------------------------------------------------- VAR

            // Вычисление координат по задержкам >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // Входной лист для подачи в функцию ...........................................
            List<ClassObject> LstClassObject1 = new List<ClassObject>();

            // SP1 (base)
            ClassObject objClassObject1 = new ClassObject();
            objClassObject1.Latitude = listStations[0].Position.Latitude;
            objClassObject1.Longitude = listStations[0].Position.Longitude;
            objClassObject1.Altitude = listStations[0].Position.Altitude;
            objClassObject1.tau = 0;
            objClassObject1.BaseStation = true;
            objClassObject1.IndexStation = 1;
            LstClassObject1.Add(objClassObject1);

            // SP2
            ClassObject objClassObject2 = new ClassObject();
            objClassObject2.Latitude = listStations[1].Position.Latitude;
            objClassObject2.Longitude = listStations[1].Position.Longitude;
            objClassObject2.Altitude = listStations[1].Position.Altitude;
            objClassObject2.tau = objInputPointTrack.dt1;
            objClassObject2.BaseStation = false;
            objClassObject2.IndexStation = 2;
            LstClassObject1.Add(objClassObject2);

            // SP3
            ClassObject objClassObject3 = new ClassObject();
            objClassObject3.Latitude = listStations[2].Position.Latitude;
            objClassObject3.Longitude = listStations[2].Position.Longitude;
            objClassObject3.Altitude = listStations[2].Position.Altitude;
            objClassObject3.tau = objInputPointTrack.dt2;
            objClassObject3.BaseStation = false;
            objClassObject3.IndexStation = 3;
            LstClassObject1.Add(objClassObject3);

            // SP4
            ClassObject objClassObject4 = new ClassObject();
            objClassObject4.Latitude = listStations[3].Position.Latitude;
            objClassObject4.Longitude = listStations[3].Position.Longitude;
            objClassObject4.Altitude = listStations[3].Position.Altitude;
            objClassObject4.tau = objInputPointTrack.dt3;
            objClassObject4.BaseStation = false;
            objClassObject4.IndexStation = 4;
            LstClassObject1.Add(objClassObject4);
            // ............................................ Входной лист для подачи в функцию

            // Расчет координат по задержкам ................................................


            ClassDRM_dll objClassDRM_dll = new ClassDRM_dll();
            ClassObjectTmp ObjClassObjectTmp1 = new ClassObjectTmp();
            ObjClassObjectTmp1 = objClassDRM_dll.f_DRM(
                                                        LstClassObject1,
                                                        flagMnimPoints,
                                                        HInput,
                                                        true,
                                                        true,
                                                        true
                                                        //ref GlobalVarMapMain.sh1RDM,
                                                        //ref GlobalVarMapMain.sh2RDM
                                                        );
            // ................................................ Расчет координат по задержкам

            // -----------------------------------------------------------------------------
            LatMark = ObjClassObjectTmp1.Latitude;
            LongMark = ObjClassObjectTmp1.Longitude;
            HMark = ObjClassObjectTmp1.Altitude;

            Time = objInputPointTrack.Time;
            // -----------------------------------------------------------------------------

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Вычисление координат по задержкам


            // MAIN @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            // Основной цикл формирования траекторий

            // MARK *************************************************************************
            // Обработка входной отметки

            // Формируем отметку MarkCoord (здесь же пересчет в декартовы идет)
            MarkCoord _MarkCoord = new MarkCoord(
                                                    LatMark,
                                                    LongMark,
                                                    HMark,
                                                    listStations[0].Position.LatitudeRLSBase,
                                                    listStations[0].Position.LongitudeRLSBase,
                                                    listStations[0].Position.AltitudeRLSBase
                                                                             );

            // Создаем отметку TimeMarkCoord
            TimeMarkCoord objTimeMarkCoord = new TimeMarkCoord(
                                                               // Входная отметка
                                                               _MarkCoord.Latitude,
                                                               _MarkCoord.Longitude,
                                                               _MarkCoord.Altitude,
                                                                // RLS
                                                                listStations,
                                                                // Время прихода отметки
                                                                Time,
                                                                objInputPointTrack.Freq,
                                                                objInputPointTrack.dFreq,
                                                                objInputPointTrack._time
                                                               );

            // Otl
            //GlobalVarMapMain.sh3 += 1;
            //if (GlobalVarMapMain.sh3 == 1103)
            //    GlobalVarMapMain.sh3 = GlobalVarMapMain.sh3;

            // !!! 
            if ((objTimeMarkCoord.Altitude < 0) || (objTimeMarkCoord.Altitude > Hmax))
                return;


            // Экстраполированные координаты
            // Если пойдет в ВО -> экстраполированные координаты пересчитаются в фильтре
            // и перепишутся, матрица там же пересчитается
            objTimeMarkCoord.Xe = objTimeMarkCoord.X;
            objTimeMarkCoord.Ye = objTimeMarkCoord.Y;
            objTimeMarkCoord.Ze = objTimeMarkCoord.Z;

            // Матрица ошибок измерений 3x3
            //if (flagStatDyn == true)
            //{
            objTimeMarkCoord.Error = objTimeMarkCoord.ErrorMeasurement(
                                                                 listStations,
                                                               // Входная отметка
                                                               _MarkCoord.Latitude,
                                                               _MarkCoord.Longitude,
                                                               _MarkCoord.Altitude,
                                                                // Время прихода отметки
                                                                Time,
                                                                objTimeMarkCoord.Xe,
                                                                objTimeMarkCoord.Ye,
                                                                objTimeMarkCoord.Ze
                                                                      );
            //}
            //else
            //{
            objTimeMarkCoord.ErrorStat = objTimeMarkCoord.ErrorMeasurementStat(
                                                                           CKO_X,
                                                                           CKO_Y,
                                                                           CKO_Z
                                                                          );
            //}


            // ************************************************************************* MARK

            // FIRST ************************************************************************
            // Первая отметка

            if (
                (listTrackSource != null) &&
                (listTrackSource.Count == 0) &&
                (_listTrackAirObject.Count == 0)
               )
            {
                // Создаем одну траекторию источников
                TrackSource objTrackSource = new TrackSource(
                                                             // RLS
                                                             listStations,
                                                             // Лист траекторий ВО
                                                             ref _listTrackAirObject
                                                            );

                // В этой траектории создаем лист отметок
                objTrackSource.TimeMarksCoord = new List<TimeMarkCoord>();
                // Добавляем отметку в лист отметок этой траектории
                objTrackSource.AddMarkSource(objTimeMarkCoord);
                objTrackSource.TimeMarksCoord[0].ID = 0;
                // Добавляем траекторию в лист траекторий источников
                listTrackSource.Add(objTrackSource);
                listTrackSource[0].ID = 0;

                return;
            }
            // ************************************************************************ FIRST

            // TRACKS_AIR_OBJECTS ***********************************************************
            // Цикл по трассам ВО

            // ------------------------------------------------------------------------------
            if (_listTrackAirObject.Count > 0)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 1-й обход

                flagVO = false;
                flagVO1 = false;

                for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                {
                    flagVO1 = _listTrackAirObject[iAirObject].AddMarkAirObject(

                                  // Входная отметка
                                  objTimeMarkCoord,

                                  // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                                  twaitAirObject,
                                  // СКО ускорения на плоскости
                                  CKOA_pl,
                                  // СКО ускорения по высоте
                                  CKOA_H,
                                  // Допустимая скорость дрона
                                  Vdop,
                                  // СКО по координатам для формирования статической матрицы D 
                                  // ошибок измерения в данный момент времени
                                  CKO_X,
                                  CKO_Y,
                                  CKO_Z,
                                  // ==true -> использовать динамическую матрицу ошибок измерений, 
                                  // ==false -> использовать статическую матрицу ошибок измерений
                                  flagStatDyn,

                                  iAirObject,
                                  // Лист трасс ВО
                                  ref _listTrackAirObject

                                                 );

                    if (flagVO1 == true)
                        flagVO = true;

                } // FOR(listTrackAirObjects)
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // 2-й обход
                  // В трассах, в которых отметка попала в строб, ищем трассу с минимальным расстоянием
                  // между реальной и экстраполированной точкой

                fl_min = false;
                dRR_min = 1000000000;
                index_min = -1;

                if (flagVO == true)
                {
                    for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                    {

                        // IF1
                        if (
                            (_listTrackAirObject[iAirObject].AddTek == true) &&
                            (_listTrackAirObject[iAirObject].dRR != -1) &&
                            (_listTrackAirObject[iAirObject].PopStrob == true) &&
                            (Math.Abs(_listTrackAirObject[iAirObject].dRR) < Math.Abs(dRR_min))
                          )
                        {
                            dRR_min = _listTrackAirObject[iAirObject].dRR;
                            index_min = iAirObject;
                            //_listTrackAirObject[iAirObject].fl_dRR_min = true;
                            fl_min = true;
                        } // IF1

                    } // FOR

                    // IF2
                    if (
                      (fl_min == true) &&
                      (index_min != -1)
                      )
                    {
                        _listTrackAirObject[index_min].fl_dRR_min = true;

                    } // IF2

                } // flagVO==true
                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  // 3-й обход

                if (flagVO == true)
                {
                    for (iAirObject = 0; iAirObject < _listTrackAirObject.Count; iAirObject++)
                    {

                        if ((_listTrackAirObject[iAirObject].AddTek == true))
                        {
                            _listTrackAirObject[iAirObject].AddTek = false;

                            if (_listTrackAirObject[iAirObject].Marks.Count > 0)
                            {
                                indtmp = _listTrackAirObject[iAirObject].Marks.Count - 1;

                            }
                            // ..............................................................
                            // IF3

                            if (
                               (_listTrackAirObject[iAirObject].PopStrob == true) &&
                               (_listTrackAirObject[iAirObject].fl_dRR_min == true) &&
                               (_listTrackAirObject[iAirObject].dRR != -1)
                              )
                            {
                                if (_listTrackAirObject[iAirObject].Marks.Count > 0)
                                {
                                    _listTrackAirObject[iAirObject].Marks[indtmp].Skip = false;

                                    objMatrix.ChangeMatrix(_listTrackAirObject[iAirObject].PSI_N, ref _listTrackAirObject[iAirObject].PSI_N_1);
                                    objMatrix.ChangeMatrix(_listTrackAirObject[iAirObject].S_N, ref _listTrackAirObject[iAirObject].S_N_1);

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    // Пересчет широты и долготы

                                    // base
                                    double latRLS_rad = listStations[0].Position.Latitude * Math.PI / 180;
                                    double longRLS_rad = listStations[0].Position.Longitude * Math.PI / 180;
                                    XRLS = 0;
                                    YRLS = 0;
                                    ZRLS = 0;
                                    xxx13 = 0;
                                    yyy13 = 0;
                                    zzz13 = 0;
                                    lt23 = 0;
                                    lng23 = 0;
                                    hhh23 = 0;

                                    // MZSK -> GEOC
                                    // XYZ RLSBase,m (in geocentric CS)
                                    ClassGeoCalculator.f_BLH_XYZ_84_1
                                                                     (
                                                                      listStations[0].Position.Latitude,
                                                                      listStations[0].Position.Longitude,
                                                                      listStations[0].Position.Altitude,
                                                                      ref XRLS,
                                                                      ref YRLS,
                                                                      ref ZRLS
                                                                      );

                                    ClassGeoCalculator.MZSK_Geoc_V(
                                                              // Base
                                                              latRLS_rad,
                                                              longRLS_rad,
                                                              XRLS, // BaseGeoc
                                                              YRLS,
                                                              ZRLS,

                                                              // Object, MZSK,Filtr
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.X,
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Y,
                                                              _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Z,
                                                              // Object,Geoc,m
                                                              ref xxx13,
                                                              ref yyy13,
                                                              ref zzz13
                                                            );

                                    ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13,
                                                                      yyy13,
                                                                      zzz13,
                                                                      ref lt23,
                                                                      ref lng23,
                                                                      ref hhh23
                                                                      );
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint = new TimeMarkCoord(
                                                                                                                lt23,
                                                                                                                lng23,
                                                                                                                hhh23,
                                                                                                                listStations,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].Time,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Freq,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.dFreq,
                                                                                                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter._time
                                                                                                                  );

                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Latitude = lt23;
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Longitude = lng23;
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordPaint.Altitude = hhh23;

                                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                }

                            } // IF3
                            // ..............................................................
                            // ELSE on IF3

                            else
                            {
                                _listTrackAirObject[iAirObject].Marks[indtmp].Skip = true;

                                // Экстаполированные координаты пишем в фильтрованные
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.X =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Xe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Y =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Ye;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.Z =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.Ze;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VX =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VXe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VY =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VYe;
                                _listTrackAirObject[iAirObject].Marks[indtmp].CoordFilter.VZ =
                                    _listTrackAirObject[iAirObject].Marks[indtmp].CoordExtr.VZe;

                            } // ELSE on IF3
                            // ..............................................................
                            _listTrackAirObject[iAirObject].fl_dRR_min = false;
                            _listTrackAirObject[iAirObject].dRR = -1;
                            _listTrackAirObject[iAirObject].PopStrob = false;

                            // ..............................................................


                        } // _listTrackAirObject[iAirObject].AddTek == true

                    } // FOR

                } // flagVO==true
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            } // listTrackAirObject.Count > 0
            // ------------------------------------------------------------------------------
            // Еще нет трасс ВО

            else
                flagVO = false;
            // ------------------------------------------------------------------------------

            // *********************************************************** TRACKS_AIR_OBJECTS


            // SOURCES **********************************************************************
            // Цикл по трассам источников

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // IF1

            if (
                (listTrackSource.Count > 0) &&
                (flagVO == false) // отметка не подошла к трассам ВО
                )
            {
                flSourceTrackAll = false;

                for (iSource = 0; iSource < listTrackSource.Count; iSource++)
                {
                    flSourceTrack = false;

                    // Подходит ли к трассе >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Проверка: подходит ли отметка к трассе источника

                    flSourceTrack = listTrackSource[iSource].DefineMarkTrack(objTimeMarkCoord, twait, Vdop, flagStatDynInit);

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Подходит ли к трассе

                    // Подходит >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Отметка вошла в трассу источников

                    if (flSourceTrack == true)
                    {

                        flSourceTrackAll = true;

                        listTrackSource[iSource].AddMarkSource(objTimeMarkCoord);

                        // Инициализация фильтра
                        if (listTrackSource[iSource].TimeMarksCoord.Count == numbInit)
                        {
                            listTrackSource[iSource].InitKalman(
                                                                   flagStatDynInit,
                                                                   // Лист траекторий ВО
                                                                   ref _listTrackAirObject
                                                               );

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Пересчет широты и долготы

                            // base
                            double latRLS_rad1 = listStations[0].Position.Latitude * Math.PI / 180;
                            double longRLS_rad1 = listStations[0].Position.Longitude * Math.PI / 180;
                            double XRLS1 = 0;
                            double YRLS1 = 0;
                            double ZRLS1 = 0;
                            double xxx131 = 0;
                            double yyy131 = 0;
                            double zzz131 = 0;
                            double lt231 = 0;
                            double lng231 = 0;
                            double hhh231 = 0;

                            // MZSK -> GEOC
                            // XYZ RLSBase,m (in geocentric CS)
                            ClassGeoCalculator.f_BLH_XYZ_84_1
                                                             (
                                                              listStations[0].Position.Latitude,
                                                              listStations[0].Position.Longitude,
                                                              listStations[0].Position.Altitude,
                                                              ref XRLS1,
                                                              ref YRLS1,
                                                              ref ZRLS1
                                                              );

                            ClassGeoCalculator.MZSK_Geoc_V(
                                                      // Base1
                                                      latRLS_rad1,
                                                      longRLS_rad1,
                                                      XRLS1, // BaseGeoc
                                                      YRLS1,
                                                      ZRLS1,

                                                      // Object, MZSK,Filtr
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.X,
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Y,
                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Z,
                                                      // Object,Geoc,m
                                                      ref xxx131,
                                                      ref yyy131,
                                                      ref zzz131
                                                    );

                            ClassGeoCalculator.f_XYZ_BLH_84_1(xxx131,
                                                              yyy131,
                                                              zzz131,
                                                              ref lt231,
                                                              ref lng231,
                                                              ref hhh231
                                                              );

                            _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordPaint = new TimeMarkCoord(
                                                                                                                      lt231,
                                                                                                                      lng231,
                                                                                                                      hhh231,
                                                                                                                      listStations,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].Time,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.Freq,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter.dFreq,
                                                                                                                      _listTrackAirObject[_listTrackAirObject.Count - 1].Marks[0].CoordFilter._time
                                                                                                                      );

                            // Убираем эту траекторию источников
                            listTrackSource.Remove(listTrackSource[iSource]);

                            // Уменьшить ID на 1 в последующих траекториях
                            for (int iSourcetmp = iSource; iSourcetmp < listTrackSource.Count; iSourcetmp++)
                                listTrackSource[iSourcetmp].ID -= 1;

                            iSource -= 1;
                        }

                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Подходит


                } // FOR(listTrackSource)


                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // !!! Отметка не подошла ни к одной траектории

                if (flSourceTrackAll == false)
                {
                    // Создаем одну траекторию источников
                    TrackSource objTrackSource1 = new TrackSource(
                                                                 // RLS
                                                                 listStations,
                                                                 // Лист траекторий ВО
                                                                 ref _listTrackAirObject
                                                                );

                    // В этой траектории создаем лист отметок
                    objTrackSource1.TimeMarksCoord = new List<TimeMarkCoord>();
                    // Добавляем отметку в лист отметок этой траектории
                    objTrackSource1.AddMarkSource(objTimeMarkCoord);
                    // Добавляем траекторию в лист траекторий источников
                    listTrackSource.Add(objTrackSource1);
                    listTrackSource[listTrackSource.Count - 1].ID = listTrackSource.Count - 1;

                }
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



            } // listTrackSource.Count > 0  !!!IF1
              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // !!! ELSE по IF1

            else if (
                (listTrackSource.Count == 0) &&
                (_listTrackAirObject.Count > 0) &&
                (flagVO == false) // отметка не подошла к трассам ВО
                )
            {
                // Создаем одну траекторию источников
                TrackSource objTrackSource1 = new TrackSource(
                                                             // RLS
                                                             listStations,
                                                             // Лист траекторий ВО
                                                             ref _listTrackAirObject
                                                            );

                // В этой траектории создаем лист отметок
                objTrackSource1.TimeMarksCoord = new List<TimeMarkCoord>();
                // Добавляем отметку в лист отметок этой траектории
                objTrackSource1.AddMarkSource(objTimeMarkCoord);
                // Добавляем траекторию в лист траекторий источников
                listTrackSource.Add(objTrackSource1);
                listTrackSource[listTrackSource.Count - 1].ID = listTrackSource.Count - 1;

                return;
            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // ********************************************************************** SOURCES


            // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ MAIN


        }
        // ****************************************************************** MainFunction1



    } // Class
}  // Namespace
