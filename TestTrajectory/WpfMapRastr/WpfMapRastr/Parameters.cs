﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastr
{
    // Parameters from interface
    public class Parameters
    {

        // Максимально допустимое время ожидания отметки
        public double twait = 0;
        // Максимально допустимое время ожидания отметки для сопровождаемой трассы
        public double twaitAirObject = 0;

        // Допустимая скорость
        public double Vdop = 0;
        // СКО ускорения на плоскости
        public double CKOA_pl = 0;
        // СКО ускорения по высоте
        public double CKOA_H = 0;
        // СКО по координатам для формирования матрицы D 
        // ошибок измерения в данный момент времени
        public double CKO_X = 0;
        public double CKO_Y = 0;
        public double CKO_Z = 0;

        // Величина строба (расстояние от предыдущей отметки)
        // для идентификации отметки
        public double strob = 0;

        // Ошибки  измерения разности времени приема сигналов
        //public double Error_dt1 = 0;
        //public double Error_dt2 = 0;
        //public double Error_dt3 = 0;

        // Начальный индекс чтения файла
        public int indstart = 0;
        // Конечный индекс чтения файла
        public int indstop = 0;

        // Количество отображаемых точек в режиме реального времени
        public int numbp = 0;

        // Количество точек для инициализации фильтра
        public int numbInit = 0;

    } // Class
} // Namespace
