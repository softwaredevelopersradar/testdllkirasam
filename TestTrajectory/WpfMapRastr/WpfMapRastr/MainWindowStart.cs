﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;
using YamlReverseExpertise;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

namespace WpfMapRastr
{
    // Обработчик кнопки Start + таймер в тестовом режиме
    public partial class MainWindow
    {

        // TRACKS ********************************************************************************************
        // Обработчик кнопки START

        private void ButtonSv_Click(object sender, RoutedEventArgs e)
        {


            // MODE_TEST &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

            if (GlobalVarMapMain.ModeTest == true)
            {

                if ((GlobalVarMapMain.flWorkFile == false) && (GlobalVarMapMain.flReal == false))
                    return;

                ClearAll();

                if (GlobalVarMapMain.flWorkFile == true)
                {
                    // РАБОТА С ФАЙЛАМИ FILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILE

                    // ------------------------------------------------------------------------------------------
                    GlobalVarMapMain.lsttmp_aeroscope.Clear();
                    GlobalVarMapMain.lsttmp_dubl.Clear();
                    GlobalVarMapMain.lsttmp_dubl_my.Clear();
                    GlobalVarMapMain.lsttr.Clear();
                    GlobalVarMapMain.lsttmp1.Clear();
                    GlobalVarMapMain.lsttmp.Clear();

                    // *Pavel
                    GlobalVarMapMain.listTrackAirObject.Clear();

                    // ------------------------------------------------------------------------------------------
                    ClassTrajectory objClassTrajectory = new ClassTrajectory();
                    // ------------------------------------------------------------------------------------------

                    int i = 0;
                    int j = 0;
                    // ------------------------------------------------------------------------------------------

                    // Входные параметры *************************************************************************

                    // Ввод с интерфейса
                    //LoadParameters();
                    // Запись в класс для подачи в DLL
                    //LoadClassParameters();

                    // Ввод с интерфейса
                    LoadParametersGlobal();
                    // !!! Здесь считываются реальные координаты РС, которые внутри прописаны цифрами
                    LoadParametersRLS();

                    // !!! FOR OLD
                    LoadClassParameters();


                    // FOR PAVEL ******************************************************************************
                    // *Pavel
                    // NEW

                    // Ввод с интерфейса
                    //LoadParametersGlobal();
                    //LoadParametersRLS();

                    // *PavelEmulReal
                    // !!! Заполнение начальных параметров для дальнейшей работы фильтра, в т.ч. листа РС
                    try
                    {
                            GlobalVarMapMain.objTracksDefinition = new TracksDefinition(

                                                                                       // Лист трасс ВО
                                                                                       ref GlobalVarMapMain.listTrackAirObject
                                                                                       );

                            // Заполняем параметры (свойства класса)
                            GlobalVarMapMain.objTracksDefinition.listStations = new List<Station>();

                            // RLS1
                            Station objStation1 = new Station();
                            objStation1.Position = new MarkCoord(
                                                                 // RLSi
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS,
                                                                 // Base RLS
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS
                                                                );
                            objStation1.ErrorTime = 0;
                            objStation1.Position.ID = 0;
                            GlobalVarMapMain.objTracksDefinition.listStations.Add(objStation1);

                            // RLS2
                            Station objStation2 = new Station();
                            objStation2.Position = new MarkCoord(
                                                                 // RLSi
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS_2,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS_2,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS_2,
                                                                 // Base RLS
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS
                                                                );
                            objStation2.ErrorTime = GlobalVarMapMain.ObjClassParametersGlobal.Error_dt;
                            objStation2.Position.ID = 1;
                            GlobalVarMapMain.objTracksDefinition.listStations.Add(objStation2);

                            // RLS3
                            Station objStation3 = new Station();
                            objStation3.Position = new MarkCoord(
                                                                 // RLSi
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS_3,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS_3,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS_3,
                                                                 // Base RLS
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS
                                                                );
                            objStation3.ErrorTime = GlobalVarMapMain.ObjClassParametersGlobal.Error_dt;
                            objStation3.Position.ID = 2;
                            GlobalVarMapMain.objTracksDefinition.listStations.Add(objStation3);

                            // RLS4
                            Station objStation4 = new Station();
                            objStation4.Position = new MarkCoord(
                                                                 // RLSi
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS_4,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS_4,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS_4,
                                                                 // Base RLS
                                                                 GlobalVarMapMain.ObjClassParametersRLS.latRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.longRLS,
                                                                 GlobalVarMapMain.ObjClassParametersRLS.HRLS
                                                                );
                            objStation4.ErrorTime = GlobalVarMapMain.ObjClassParametersGlobal.Error_dt;
                            objStation4.Position.ID = 3;
                            GlobalVarMapMain.objTracksDefinition.listStations.Add(objStation4);


                        GlobalVarMapMain.objTracksDefinition.CKOA_H = GlobalVarMapMain.ObjClassParametersGlobal.CKOA_H;
                        GlobalVarMapMain.objTracksDefinition.CKOA_pl = GlobalVarMapMain.ObjClassParametersGlobal.CKOA_pl;
                        GlobalVarMapMain.objTracksDefinition.CKO_X = GlobalVarMapMain.ObjClassParametersGlobal.CKO_X;
                        GlobalVarMapMain.objTracksDefinition.CKO_Y = GlobalVarMapMain.ObjClassParametersGlobal.CKO_Y;
                        GlobalVarMapMain.objTracksDefinition.CKO_Z = GlobalVarMapMain.ObjClassParametersGlobal.CKO_Z;
                        GlobalVarMapMain.objTracksDefinition.numbInit = GlobalVarMapMain.ObjClassParametersGlobal.numbInit;
                        GlobalVarMapMain.objTracksDefinition.twait = GlobalVarMapMain.ObjClassParametersGlobal.twait;
                        GlobalVarMapMain.objTracksDefinition.twaitAirObject = GlobalVarMapMain.ObjClassParametersGlobal.twaitAirObject;
                        GlobalVarMapMain.objTracksDefinition.Vdop = GlobalVarMapMain.ObjClassParametersGlobal.Vdop;
                        // false -> stat DI
                        GlobalVarMapMain.objTracksDefinition.flagStatDyn = GlobalVarMapMain.ObjClassParametersGlobal.flDynStat;
                        GlobalVarMapMain.objTracksDefinition.flagStatDynInit = GlobalVarMapMain.ObjClassParametersGlobal.flDynStatInit;
                        GlobalVarMapMain.objTracksDefinition.Hmax = GlobalVarMapMain.ObjClassParametersGlobal.Hmax;

                        // =false->инверсия мнимых точек
                        // =true->отбрасывание мнимых точек
                        GlobalVarMapMain.objTracksDefinition.flagMnimPoints = GlobalVarMapMain.ObjClassParametersGlobal.flMnimPoints;

                        // Замена рассчитанной высоты введенной
                        GlobalVarMapMain.objTracksDefinition.HInput = GlobalVarMapMain.ObjClassParametersGlobal.HInput;

                    }
                    catch
                    {
                        int y = 0;
                    }


                    // Запись в файл установок >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Get current dir
                    string dir111 = "";
                    dir111 = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    string dirK = "";
                    dirK = dir111 + "\\FILES\\" + "fileParameters.txt";
                    StreamWriter swK = new StreamWriter(dirK, false, System.Text.Encoding.Default);

                    int fldyn = 0;
                    int fldyninit = 0;
                    int flmnimpoints = 0;
                    if (GlobalVarMapMain.ObjClassParametersGlobal.flDynStat == true)
                        fldyn = 1;
                    else
                        fldyn = 0;
                    if (GlobalVarMapMain.ObjClassParametersGlobal.flDynStatInit == true)
                        fldyninit= 1;
                    else
                        fldyninit = 0;
                    if (GlobalVarMapMain.ObjClassParametersGlobal.flMnimPoints == true)
                        flmnimpoints = 1;
                    else
                        flmnimpoints = 0;

                    // Parameters to file
                    string sprm = Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.twait) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.twaitAirObject) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.Vdop) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.CKOA_pl) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.CKOA_H) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.indstart) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.indstop) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.CKO_X) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.CKO_Y) + " " +
                                  Convert.ToString(fldyn) + " " +
                                  Convert.ToString(fldyninit) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.Error_dt * 1E9) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.numbInit) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.KDT) + " " +
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.Hmax)+ " "+
                                  Convert.ToString(flmnimpoints)+" "+
                                  Convert.ToString(GlobalVarMapMain.ObjClassParametersGlobal.HInput);

                    swK.WriteLine(sprm);
                    swK.Close();

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Запись в файл установок

                    // ****************************************************************************** FOR PAVEL

                    // ************************************************************************* Входные параметры

                    // Входной лист отметок *********************************************************************

                    // Get current dir
                    string dir = "";
                    dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                    // ___________________________________________________________________________________________
                    // FILE_Stations
                    // Для отрисовки

                    string dir3_3 = "";
                    string sltlng3_3 = "";
                    int m5_3 = 0;

                    dir3_3 = dir + "\\FILES\\" + "file_stations.txt";

                    try
                    {
                        StreamReader sr3_3 = new StreamReader(dir3_3);

                        do
                        {
                            sltlng3_3 = sr3_3.ReadLine();

                            if (DecSep == '.')
                                sltlng3_3 = sltlng3_3.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng3_3 = sltlng3_3.Replace('.', ',');

                            string[] words3_3 = sltlng3_3.Split(new char[] { ' ' });

                            ClassTrackPointDraw objClassTrackPoint69_3 = new ClassTrackPointDraw();
                            objClassTrackPoint69_3.Latitude = Convert.ToDouble(words3_3[0]);
                            objClassTrackPoint69_3.Longitude = Convert.ToDouble(words3_3[1]);
                            objClassTrackPoint69_3.Altitude = Convert.ToDouble(words3_3[2]);
                            GlobalVarMapMain.lsttmp_stations.Add(objClassTrackPoint69_3);
                        } while (sltlng3_3 != null);

                        sr3_3.Close();

                    }
                    catch
                    {

                    }
                    // __________________________________________________________________________________________


                    // Расчет и запись в файл временных задержек *****************************************
                    // !!! Создание файлов:
                    // aeroscope_time_output_OnRealInv (RDM, dtau*K)
                    // aeroscope_time_output (dtau from aeroscope)


                    FileTimeZad();
                    // ***************************************** Расчет и запись в файл временных задержек


                    // Отладочный кусок для вывода в EXEL *************************************************

/*
                    string dirEx = "";
                    string dirExWrite = "";
                    string sltlngEx = "";
                    string sltlngExWrite = "";

                    dirEx = dir + "\\FILES\\" + "aeroscope_time_output_OnRealInv1.txt";
                    dirExWrite = dir + "\\FILES\\" + "Point1Ex.txt";

                    try
                    {
                        StreamReader srEx = new StreamReader(dirEx);
                        StreamWriter srExWrite = new StreamWriter(dirExWrite, false, System.Text.Encoding.Default);

                        do
                        {
                            sltlngEx = srEx.ReadLine();

                            if (DecSep == '.')
                                sltlngEx = sltlngEx.Replace(',', '.');
                            if (DecSep == ',')
                                sltlngEx = sltlngEx.Replace('.', ',');

                            string[] wordsEx = sltlngEx.Split(new char[] { ' ' });

                            // ..................................................................

                            sltlngExWrite = wordsEx[0] + " " + wordsEx[4];

                            srExWrite.WriteLine(sltlngExWrite);

                            // ..................................................................

                        } while (sltlngEx != null);

                        srEx.Close();
                        srExWrite.Close();

                    } // try

                    catch
                    {

                    }
                    // ...........................................................................................
*/

                    // ************************************************* Отладочный кусок для вывода в EXEL


                    // ___________________________________________________________________________________________
                    // FILE_AEROSCOPE

                    // NEW_FROM_Pavel >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Вывод всего файла сразу

                    string dir3 = "";
                    string sltlng3 = "";
                    int m5 = 0;

                    GlobalVarMapMain.lsttmp_aeroscope.Clear();

                    //dir3 = dir + "\\FILES\\" + "aeroscope_time_input.txt";
                    dir3 = GlobalVarMapMain.DirAeroscope;

                    try
                    {
                        StreamReader sr3 = new StreamReader(dir3);

                        do
                        {
                            sltlng3 = sr3.ReadLine();

                            if (DecSep == '.')
                                sltlng3 = sltlng3.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng3 = sltlng3.Replace('.', ',');

                            string[] words3 = sltlng3.Split(new char[] { ' ' });

                            // ..................................................................

                            ClassTrackPoint objClassTrackPoint69 = new ClassTrackPoint();
                            objClassTrackPoint69.Latitude = Convert.ToDouble(words3[1]);
                            objClassTrackPoint69.Longitude = Convert.ToDouble(words3[2]);
                            objClassTrackPoint69.Altitude = Convert.ToDouble(words3[3]);

                            string[] words3time = words3[0].Split(new char[] { ':' });

                            objClassTrackPoint69.timePoint = Convert.ToDouble(words3time[0]) * 3600
                                                              + Convert.ToDouble(words3time[1]) * 60 +
                                                              Convert.ToDouble(words3time[2]);

                            GlobalVarMapMain.lsttmp_aeroscope.Add(objClassTrackPoint69);
                            // ..................................................................

                        } while (sltlng3 != null);


                    } // try

                    catch
                    {

                    }
                    // ...........................................................................................


                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NEW_FROM_Pavel

                    // От Валентина Ивановича >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    string dir3VI = "";
                    string sltlng3VI = "";
                    int m5VI = 0;

                    //dir3VI = dir + "\\FILES\\" + "aeroscope_time_inputVI.txt";
                    dir3VI = GlobalVarMapMain.DirVI;

                    GlobalVarMapMain.lsttmp_aeroscopeVI.Clear();

                    try
                    {
                        StreamReader sr3VI = new StreamReader(dir3VI);

                        do
                        {
                            sltlng3VI = sr3VI.ReadLine();

                            if (DecSep == '.')
                                sltlng3VI = sltlng3VI.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng3VI = sltlng3VI.Replace('.', ',');

                            string[] words3VI = sltlng3VI.Split(new char[] { ' ' });

                            // ..................................................................

                            ClassTrackPoint objClassTrackPoint69VI = new ClassTrackPoint();
                            objClassTrackPoint69VI.Latitude = Convert.ToDouble(words3VI[1]);
                            objClassTrackPoint69VI.Longitude = Convert.ToDouble(words3VI[2]);
                            objClassTrackPoint69VI.Altitude = Convert.ToDouble(words3VI[3]);

                            string[] words3timeVI = words3VI[0].Split(new char[] { ':' });

                            objClassTrackPoint69VI.timePoint = Convert.ToDouble(words3timeVI[0]) * 3600
                                                              + Convert.ToDouble(words3timeVI[1]) * 60 +
                                                              Convert.ToDouble(words3timeVI[2]);

                            GlobalVarMapMain.lsttmp_aeroscopeVI.Add(objClassTrackPoint69VI);
                            // ..................................................................

                        } while (sltlng3VI != null);


                    } // try

                    catch
                    {

                    }

                    double xxx1 = 0;
                    double yyy1 = 0;

                    /*
                                        var pp = ClassBearing.f_Triangulation
                                            (
                                             1000,
                                             100,
                                             190,
                                             355,

                                             55.662,
                                             28.2,
                                             54.41,
                                             28.1,

                                             1, // WGS84
                                             1000,

                                            ref xxx1,
                                            ref yyy1


                                            );
                    */
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> От Валентина Ивановича

                    // ___________________________________________________________________________________________
                    // FILE_VICTOR: Данные с фильтра Виктора (время, широта, долгота, высота)


                    /*
                                        f_Triangulation

                                            (
                                                 // Входные параметры:

                                                 // Max дальность отображения пеленга (m)
                                                 double Mmax_Pel1,
                                                 // Количество фиктивных точек в плоскости пеленгования пеленгатора
                                                 uint NumbFikt_Pel1,

                                                 // Пеленг (град)
                                                 double Theta_Pel1,
                                                 double Theta_Pel2,

                                                 // Широта и долгота стояния пеленгатора (град)
                                                 double LatP_Pel1,
                                                 double LongP_Pel1,
                                                 double LatP_Pel2,
                                                 double LongP_Pel2,

                                                 int fl_84_42,
                                                 double Smax,

                                                // Выходные параметры:

                                                // Координаты искомого ИРИ в опорной геоцентрической СК(м)
                                                ref double XIRI_Tr,
                                                ref double YIRI_Tr,
                                                ref double ZIRI_Tr,

                                                // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
                                                ref double RIRI_Tr,  // м
                                                ref double LatIRI_Tr,
                                                ref double LongIRI_Tr

                                            );

                    */



                    var pp = ClassBearing.f_Triangulation
                        (
                         1000,
                         100,
                         190,
                         355,

                         55.662,
                         28.2,
                         54.41,
                         28.1,

                         1, // WGS84
                         1000,

                        ref xxx1,
                        ref yyy1


                        );

                    string dir3_1 = "";
                    string sltlng3_1 = "";
                    int m5_1 = 0;
                    int m2t = 0;

                    GlobalVarMapMain.lsttmp_aeroscope_1.Clear();

                    dir3_1 = dir + "\\FILES\\" + "out.txt";

                    try
                    {
                        StreamReader sr3_1 = new StreamReader(dir3_1);

                        for (m2t = 0; m2t < GlobalVarMapMain.ObjClassParametersGlobal.indstart; m2t++)
                        {
                            sltlng3_1 = sr3_1.ReadLine();
                        }

                        for (m2t = 0; m2t < (GlobalVarMapMain.ObjClassParametersGlobal.indstop - GlobalVarMapMain.ObjClassParametersGlobal.indstart); m2t++)
                        {

                            sltlng3_1 = sr3_1.ReadLine();
                            if (DecSep == '.')
                                sltlng3_1 = sltlng3_1.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng3_1 = sltlng3_1.Replace('.', ',');
                            string[] words3 = sltlng3_1.Split(new char[] { '\t' });

                            // ..................................................................

                            ClassTrackPoint objClassTrackPoint69_1 = new ClassTrackPoint();

                            objClassTrackPoint69_1.Latitude = Convert.ToDouble(words3[1]);     // Lat
                            objClassTrackPoint69_1.Longitude = Convert.ToDouble(words3[2]);    // LONG
                            objClassTrackPoint69_1.Altitude = Convert.ToDouble(words3[3]);     // h

                            objClassTrackPoint69_1.timePoint = Convert.ToDouble(words3[0]) / 1000; //sec

                            GlobalVarMapMain.lsttmp_aeroscope_1.Add(objClassTrackPoint69_1);
                            // ..................................................................

                        }  // FOR

                        sr3_1.Close();

                    }

                    catch
                    {

                    }
                    // ___________________________________________________________________________________________
                    // FILE_RDM_DUBL -> Для отрисовки


                    // NEW >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Из входного файла Павла ReverseExpertise.yaml: RDM+задержки сделали файлы:
                    // aeroscope_time_output_OnRealInv:t(DateT)ime+Lat,LongH,-задержки*K -> !!! Это вход фильтра
                      

                    // Файл измеренных задержек и координат от Павла ----------------------------------
                    // Pavel

                    // Чтение из созданного файла ---------------------------------------------------------------
                    // !!! Он уже есть (см.выше)

                    string dir2 = "";
                    string sltlng2 = "";
                    int m3 = 0;

                    GlobalVarMapMain.lsttmp_dubl.Clear();

                    dir2 = dir + "\\FILES\\" + "aeroscope_time_output_OnRealInv.txt";

                    try
                    {
                        StreamReader sr2 = new StreamReader(dir2);

                        for (m3 = 0; m3 < GlobalVarMapMain.ObjClassParametersGlobal.indstart; m3++)
                        {
                            sltlng2 = sr2.ReadLine();
                        }
                        for (m3 = 0; m3 < (GlobalVarMapMain.ObjClassParametersGlobal.indstop - GlobalVarMapMain.ObjClassParametersGlobal.indstart); m3++)
                        {
                            sltlng2 = sr2.ReadLine();
                            if (DecSep == '.')
                                sltlng2 = sltlng2.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng2 = sltlng2.Replace('.', ',');
                            string[] words2 = sltlng2.Split(new char[] { ' ' });

                            // ...........................................................................

                            ClassTrackPoint objClassTrackPoint67 = new ClassTrackPoint();
                            objClassTrackPoint67.Latitude = Convert.ToDouble(words2[1]);
                            objClassTrackPoint67.Longitude = Convert.ToDouble(words2[2]);
                            objClassTrackPoint67.Altitude = Convert.ToDouble(words2[3]);

                            // sec
                            string[] words3time1 = words2[0].Split(new char[] { ':' });
                            objClassTrackPoint67.timePoint = Convert.ToDouble(words3time1[0]) * 3600
                                                              + Convert.ToDouble(words3time1[1]) * 60 +
                                                              Convert.ToDouble(words3time1[2]);

                            GlobalVarMapMain.lsttmp_dubl.Add(objClassTrackPoint67);
                            // ...........................................................................

                        }

                        sr2.Close();
                    }

                    catch
                    {

                    }
                    // --------------------------------------------------------------- Чтение из созданного файла

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NEW


                    // ___________________________________________________________________________________________
                    // FILE_RDM
                    // !!!OLD

                    string dir1 = "";
                    string sltlng = "";
                    int m2 = 0;

                    dir1 = dir + "\\FILES\\" + "file.txt";

                        StreamReader sr = new StreamReader(dir1);


                        //for (m2 = 0; m2 < GlobalVarMapMain.ObjClassParametersGlobal.indstart; m2++)
                        //{
                        //    sltlng = sr.ReadLine();
                        //}

                      // !!!Чтобы не мешал старый файл -> potom vse ybrat
                        //for (m2 = 0; m2 < (GlobalVarMapMain.ObjClassParametersGlobal.indstop - GlobalVarMapMain.ObjClassParametersGlobal.indstart); m2++)
                        for (m2 = 0; m2 < 1000; m2++)
                        {
                            sltlng = sr.ReadLine();
                            if (DecSep == '.')
                                sltlng = sltlng.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng = sltlng.Replace('.', ',');
                            string[] words = sltlng.Split(new char[] { ' ' });

                            // ..................................................................
                            // Old

                            ClassTrackPoint objClassTrackPoint66 = new ClassTrackPoint();
                            objClassTrackPoint66.Latitude = Convert.ToDouble(words[1]);
                            objClassTrackPoint66.Longitude = Convert.ToDouble(words[2]);
                            objClassTrackPoint66.Altitude = Convert.ToDouble(words[3]);
                            objClassTrackPoint66.timePoint = Convert.ToDouble(words[0]) / 1000; //sec

                            GlobalVarMapMain.lsttmp1.Add(objClassTrackPoint66);

                        } // FOR

                    sr.Close();
                    // ...........................................................................
                    // OLD

                    // !!! Первые точки идут в конец для подачи на фильтр
                    for (int iii = GlobalVarMapMain.lsttmp1.Count - 1; iii >= 0; iii--)
                    {
                        GlobalVarMapMain.lsttmp.Add(GlobalVarMapMain.lsttmp1[iii]);
                        GlobalVarMapMain.lsttmp1.Remove(GlobalVarMapMain.lsttmp1[iii]);
                    }
                    // ...........................................................................

                    // ___________________________________________________________________________________________
                    // FILE_RDM_ForNewFilter (Pavel)

                    string dir1_rdm = "";
                    string sltlng_rdm = "";
                    int m2_rdm = 0;

                    dir1_rdm = dir + "\\FILES\\" + "aeroscope_time_output_OnRealInv.txt";

                    StreamReader sr_rdm = new StreamReader(dir1_rdm);


                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Change 16_12 Black
                    // Для записи в файл после обработки временных задержек из файла Павла функцией f_DRM
                    // (координаты и флаг) -> Это для отладки

                    //GlobalVarMapMain.dirwork = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    //GlobalVarMapMain.dirKwork = GlobalVarMapMain.dirwork + "\\FILE_WRITE_FromRDM\\" + "Out_FromRDM.txt";
                    //GlobalVarMapMain.swK = new StreamWriter(GlobalVarMapMain.dirKwork, false, System.Text.Encoding.Default);
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                    for (m2_rdm = 0; m2_rdm < GlobalVarMapMain.ObjClassParametersGlobal.indstart; m2_rdm++)
                    {
                        sltlng_rdm = sr_rdm.ReadLine();
                    }

                    // Otl
                    GlobalVarMapMain.sh3 = 0;

                    for (m2_rdm = 0; m2_rdm < (GlobalVarMapMain.ObjClassParametersGlobal.indstop - GlobalVarMapMain.ObjClassParametersGlobal.indstart); m2_rdm++)
                    {
                        sltlng_rdm = sr_rdm.ReadLine();

                        if (sltlng_rdm == null)
                            break;

                        if (DecSep == '.')
                            sltlng_rdm = sltlng_rdm.Replace(',', '.');
                        if (DecSep == ',')
                            sltlng_rdm = sltlng_rdm.Replace('.', ',');
                        string[] words_rdm = sltlng_rdm.Split(new char[] { ' ' });

                        // ..................................................................
                        // PROBA
                        // *PavelEmulReal

                        InputPointTrack objInputPointTrack = new InputPointTrack();
                        objInputPointTrack.LatMark = Convert.ToDouble(words_rdm[1]);
                        objInputPointTrack.LongMark = Convert.ToDouble(words_rdm[2]);
                        objInputPointTrack.HMark = Convert.ToDouble(words_rdm[3]);

                        objInputPointTrack.dt1= Convert.ToDouble(words_rdm[4])/1000000000;
                        objInputPointTrack.dt2 = Convert.ToDouble(words_rdm[5]) / 1000000000;
                        objInputPointTrack.dt3 = Convert.ToDouble(words_rdm[6]) / 1000000000;

                        // sec
                        string[] words3time2 = words_rdm[0].Split(new char[] { ':' });
                        objInputPointTrack.Time = Convert.ToDouble(words3time2[0]) * 3600
                                                          + Convert.ToDouble(words3time2[1]) * 60 +
                                                          Convert.ToDouble(words3time2[2]);

                        objInputPointTrack.Freq = (float)Convert.ToDouble(words_rdm[7]);
                        objInputPointTrack.dFreq = 200;
                        objInputPointTrack._time = DateTime.Now;


                        //if (m2_rdm == 39)
                        //    m2_rdm = m2_rdm;


                        GlobalVarMapMain.objTracksDefinition.f_TracksDefinition(
                                                               // Входная отметка 
                                                                 objInputPointTrack
                                                                                 );

                        //GlobalVarMapMain.objTracksDefinition.f_TracksDefinition_H(
                        //                                         // Входная отметка 
                        //                                         objInputPointTrack
                        //                                                         );

                        //GlobalVarMapMain.objTracksDefinition.f_TracksDefinitionZad(
                        //                                         // Входная отметка 
                        //                                         objInputPointTrack
                        //                                                         );

                        // ..................................................................


                    } // FOR


                    // Change 16_12 Black
                    //GlobalVarMapMain.swK.Close();

                    // Файл отладочный после вызова f_DRM в TrackDefinition ----------------------------------
                    // Change 16_12 Black

/*
                    string dir2_my = "";
                    string sltlng2_my = "";
                    int m3_my = 0;

                    GlobalVarMapMain.lsttmp_dubl_my.Clear();

                    dir2_my = dir + "\\FILE_WRITE_FromRDM\\" + "Out_FromRDM.txt";

                    try
                    {
                        StreamReader sr2_my = new StreamReader(dir2_my);

                        for (m3_my = 0; m3_my < GlobalVarMapMain.ObjClassParametersGlobal.indstart; m3_my++)
                        {
                            sltlng2_my = sr2_my.ReadLine();
                        }
                        for (m3_my = 0; m3_my < (GlobalVarMapMain.ObjClassParametersGlobal.indstop - GlobalVarMapMain.ObjClassParametersGlobal.indstart); m3_my++)
                        {
                            sltlng2_my = sr2_my.ReadLine();
                            if (DecSep == '.')
                                sltlng2_my = sltlng2_my.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng2_my = sltlng2_my.Replace('.', ',');
                            string[] words2_my = sltlng2_my.Split(new char[] { ' ' });

                            // ...........................................................................

                            ClassTrackPoint objClassTrackPoint67_my = new ClassTrackPoint();
                            objClassTrackPoint67_my.Latitude = Convert.ToDouble(words2_my[0]);
                            objClassTrackPoint67_my.Longitude = Convert.ToDouble(words2_my[1]);
                            objClassTrackPoint67_my.Altitude = Convert.ToDouble(words2_my[2]);

                            // sec
                            //string[] words3time1 = words2[0].Split(new char[] { ':' });
                            //objClassTrackPoint67.timePoint = Convert.ToDouble(words3time1[0]) * 3600
                            //                                  + Convert.ToDouble(words3time1[1]) * 60 +
                            //                                  Convert.ToDouble(words3time1[2]);


                            if(
                                (objClassTrackPoint67_my.Latitude!=-1) 
                                &&(objClassTrackPoint67_my.Longitude!=-1) 
                                &&(objClassTrackPoint67_my.Altitude!=-1)
                                )
                            GlobalVarMapMain.lsttmp_dubl_my.Add(objClassTrackPoint67_my);
                            // ...........................................................................

                        }

                        sr2_my.Close();
                    }

                    catch
                    {

                    }
*/
                    //  ---------------------------------- Файл отладочный после вызова f_DRM в TrackDefinition







                    // ...........................................................................

                    sr_rdm.Close();


                    //double sss = ClassBearing.f_D_2Points(53.941501,27.632066,53.918933,27.632366,1);
                     //double sss = ClassBearing.f_D_2Points(57.19419, 27.057697, 57.196243, 27.056838, 1);
                     double sss = ClassBearing.f_D_2Points(53.932922, 27.636234, 53.933704, 27.636835, 1);

                    sss = sss;

                    // ___________________________________________________________________________________________
                    // Формирование входного листа отметок

                    int i1_1 = 0;
                    int i2_1 = 0;

                    // ...........................................................................................
                    // OLD

                    // rad
                    GlobalVarMapMain.latRLS_rad = GlobalVarMapMain.ObjClassParametersRLS.latRLS * Math.PI / 180;
                    GlobalVarMapMain.longRLS_rad = GlobalVarMapMain.ObjClassParametersRLS.longRLS * Math.PI / 180;

                    // Координаты RLS в ГЦСК
                    ClassGeoCalculator.f_BLH_XYZ_84_1
                                                     (

                                                      GlobalVarMapMain.ObjClassParametersRLS.latRLS,
                                                      GlobalVarMapMain.ObjClassParametersRLS.longRLS,
                                                      GlobalVarMapMain.ObjClassParametersRLS.HRLS,
                                                      ref GlobalVarMapMain.XRLS,
                                                      ref GlobalVarMapMain.YRLS,
                                                      ref GlobalVarMapMain.ZRLS
                                                      );

                    double XIRI = 0;
                    double YIRI = 0;
                    double ZIRI = 0;

                    for (i1_1 = 0; i1_1 < GlobalVarMapMain.lsttmp.Count; i1_1++)
                    {
                        // Координаты IRI в ГЦСК
                        ClassGeoCalculator.f_BLH_XYZ_84_1
                                                         (
                                                          GlobalVarMapMain.lsttmp[i1_1].Latitude,
                                                          GlobalVarMapMain.lsttmp[i1_1].Longitude,
                                                          GlobalVarMapMain.lsttmp[i1_1].Altitude,
                                                          ref XIRI,
                                                          ref YIRI,
                                                          ref ZIRI
                                                          );

                        ClassGeoCalculator.Geoc_MZSK_V(
                                                     GlobalVarMapMain.latRLS_rad,
                                                     GlobalVarMapMain.longRLS_rad,
                                                     GlobalVarMapMain.XRLS,
                                                     GlobalVarMapMain.YRLS,
                                                     GlobalVarMapMain.ZRLS,
                                                     XIRI,
                                                     YIRI,
                                                     ZIRI,
                                                     ref GlobalVarMapMain.lsttmp[i1_1].X,
                                                     ref GlobalVarMapMain.lsttmp[i1_1].Y,
                                                     ref GlobalVarMapMain.lsttmp[i1_1].Z
                                                    );


                    }
                    // ...........................................................................................

                    // ********************************************************************* Входной лист отметок

                    // Calculations *******************************************************************************

                    // --------------------------------------------------------------------------------------------
                    // DEFINE TRAJECTORIES

                    ClassTrajectory objClassTrajectory2 = new ClassTrajectory();
                    GlobalVarMapMain.lsttr = objClassTrajectory2.f_Trajectory(

                                             // Входной лист отметок
                                             GlobalVarMapMain.lsttmp,
                                             // Входные параметры
                                             GlobalVarMapMain.ObjClassParameters,
                                             // Флаг количества траекторий
                                             // =false -> одна траектория
                                             // =true -> несколько траекторий
                                             GlobalVarMapMain.ModeSeverTraj
                                            );


                    // *PavelEmulReal
                    //PointsFilter.Text= Convert.ToString(GlobalVarMapMain.lsttr.Count);
                    PointsFilter.Text = Convert.ToString(GlobalVarMapMain.listTrackAirObject.Count);

                    // --------------------------------------------------------------------------------------------


                    // Запись фильтра в файл *************************************************************

                    //FileKalman();

                    // ************************************************************* Запись высоты в файл

                    // Запись для Виктора ***************************************************************
                    // Выход фильтра,

                    //FileGeoFilter();
                    // *************************************************************** Запись для Виктора

                    // DRAW ******************************************************************************
                    // Отрисовка выбранных координат

                    Draw();
                    // Данные с фильтра Виктора (время, широта, долгота, высота)
                    // !!! Внутри Draw()
                    //DrawVictor();
                    //DrawStations();
                    // ****************************************************************************** DRAW

                    // ******************************************************************************* Calculations
                }
                // FILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILEFILE РАБОТА С ФАЙЛАМИ

                // Работа в реальном времени REALREALREALREALREALREALREALREALREALREALREALREALREALREALREALREAL

                else
                {
                    // ------------------------------------------------------------------------------------------
                    GlobalVarMapMain.lsttmp_aeroscope_1.Clear();
                    GlobalVarMapMain.lsttmp_stations.Clear();

                    GlobalVarMapMain.lsttmp_aeroscope.Clear();
                    GlobalVarMapMain.lsttmp_dubl.Clear();
                    GlobalVarMapMain.lsttr.Clear();
                    GlobalVarMapMain.lsttmp1.Clear();
                    GlobalVarMapMain.lsttmp.Clear();
                    // ------------------------------------------------------------------------------------------
                    ClassTrajectory objClassTrajectory = new ClassTrajectory();
                    // ------------------------------------------------------------------------------------------

                    int i = 0;
                    int j = 0;
                    // ------------------------------------------------------------------------------------------

                    // Входные параметры *************************************************************************

                    // Ввод с интерфейса
                    //LoadParameters();
                    // Запись в класс для подачи в DLL
                    //LoadClassParameters();

                    // Ввод с интерфейса
                    LoadParametersGlobal();
                    LoadParametersRLS();

                    // ************************************************************************* Входные параметры

                    // Входной лист отметок *********************************************************************

                    // Get current dir
                    string dir = "";
                    dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                    // ___________________________________________________________________________________________
                    // FILE_Stations

                    string dir3_3 = "";
                    string sltlng3_3 = "";
                    int m5_3 = 0;

                    dir3_3 = dir + "\\FILES\\" + "file_stations.txt";

                    try
                    {
                        StreamReader sr3_3 = new StreamReader(dir3_3);

                        do
                        {
                            sltlng3_3 = sr3_3.ReadLine();

                            if (DecSep == '.')
                                sltlng3_3 = sltlng3_3.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng3_3 = sltlng3_3.Replace('.', ',');

                            string[] words3_3 = sltlng3_3.Split(new char[] { ' ' });

                            ClassTrackPointDraw objClassTrackPoint69_3 = new ClassTrackPointDraw();
                            objClassTrackPoint69_3.Latitude = Convert.ToDouble(words3_3[0]);
                            objClassTrackPoint69_3.Longitude = Convert.ToDouble(words3_3[1]);
                            objClassTrackPoint69_3.Altitude = Convert.ToDouble(words3_3[2]);
                            GlobalVarMapMain.lsttmp_stations.Add(objClassTrackPoint69_3);
                        } while (sltlng3_3 != null);

                    }
                    catch
                    {

                    }
                    // ___________________________________________________________________________________________
                    // FILE_VICTOR: Данные с фильтра Виктора (время, широта, долгота, высота)

                    string dir3_1 = "";
                    string sltlng3_1 = "";
                    int m5_1 = 0;

                    dir3_1 = dir + "\\FILES\\" + "out.txt";

                    try
                    {
                        StreamReader sr3_1 = new StreamReader(dir3_1);

                        do
                        {
                            sltlng3_1 = sr3_1.ReadLine();

                            if (DecSep == '.')
                                sltlng3_1 = sltlng3_1.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng3_1 = sltlng3_1.Replace('.', ',');

                            string[] words3 = sltlng3_1.Split(new char[] { '\t' });

                            ClassTrackPoint objClassTrackPoint69_1 = new ClassTrackPoint();
                            objClassTrackPoint69_1.Latitude = Convert.ToDouble(words3[10]);
                            objClassTrackPoint69_1.Longitude = Convert.ToDouble(words3[11]);
                            objClassTrackPoint69_1.Altitude = Convert.ToDouble(words3[12]);
                            objClassTrackPoint69_1.timePoint = Convert.ToDouble(words3[0]); //sec
                            GlobalVarMapMain.lsttmp_aeroscope_1.Add(objClassTrackPoint69_1);
                        } while (sltlng3_1 != null);

                    }

                    catch
                    {

                    }
                    // ___________________________________________________________________________________________
                    // FILE_RDM

                    string dir1 = "";
                    string sltlng = "";
                    int m2 = 0;

                    dir1 = dir + "\\FILES\\" + "file.txt";

                    try
                    {
                        //using (StreamReader sr = new StreamReader(dir1))
                        StreamReader sr = new StreamReader(dir1);

                        do
                        {
                            sltlng = sr.ReadLine();
                            if (DecSep == '.')
                                sltlng = sltlng.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng = sltlng.Replace('.', ',');

                            string[] words = sltlng.Split(new char[] { ' ' });

                            ClassTrackPoint objClassTrackPoint66 = new ClassTrackPoint();
                            objClassTrackPoint66.Latitude = Convert.ToDouble(words[1]);
                            objClassTrackPoint66.Longitude = Convert.ToDouble(words[2]);
                            objClassTrackPoint66.Altitude = 175;
                            objClassTrackPoint66.timePoint = Convert.ToDouble(words[0]) / 1000; //sec
                            GlobalVarMapMain.lsttmp.Add(objClassTrackPoint66);

                        } while (sltlng != null);
                    }

                    catch
                    {

                    }
                    // ___________________________________________________________________________________________
                    // FILE_RDM_DUBL

                    string dir2 = "";
                    string sltlng2 = "";
                    int m3 = 0;

                    dir2 = dir + "\\FILES\\" + "file.txt";

                    try
                    {
                        StreamReader sr2 = new StreamReader(dir2);

                        do
                        {
                            sltlng2 = sr2.ReadLine();
                            if (DecSep == '.')
                                sltlng2 = sltlng2.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng2 = sltlng2.Replace('.', ',');

                            string[] words2 = sltlng2.Split(new char[] { ' ' });

                            ClassTrackPoint objClassTrackPoint67 = new ClassTrackPoint();
                            objClassTrackPoint67.Latitude = Convert.ToDouble(words2[1]);
                            objClassTrackPoint67.Longitude = Convert.ToDouble(words2[2]);
                            objClassTrackPoint67.Altitude = 175;
                            objClassTrackPoint67.timePoint = Convert.ToDouble(words2[0]) / 1000; //sec
                            GlobalVarMapMain.lsttmp_dubl.Add(objClassTrackPoint67);
                        } while (sltlng2 != null);
                    }

                    catch
                    {

                    }
                    // ___________________________________________________________________________________________
                    // FILE_AEROSCOPE

                    string dir3 = "";
                    string sltlng3 = "";
                    int m5 = 0;

                    dir3 = dir + "\\FILES\\" + "file_aeroscope.txt";

                    try
                    {
                        StreamReader sr3 = new StreamReader(dir3);

                        do
                        {
                            sltlng3 = sr3.ReadLine();
                            if (DecSep == '.')
                                sltlng3 = sltlng3.Replace(',', '.');
                            if (DecSep == ',')
                                sltlng3 = sltlng3.Replace('.', ',');

                            string[] words3 = sltlng3.Split(new char[] { ' ' });

                            ClassTrackPoint objClassTrackPoint69 = new ClassTrackPoint();
                            objClassTrackPoint69.Latitude = Convert.ToDouble(words3[1]);
                            objClassTrackPoint69.Longitude = Convert.ToDouble(words3[2]);
                            objClassTrackPoint69.Altitude = 175;
                            objClassTrackPoint69.timePoint = Convert.ToDouble(words3[0]) / 1000; //sec
                            GlobalVarMapMain.lsttmp_aeroscope.Add(objClassTrackPoint69);
                        } while (sltlng3 != null);
                    }

                    catch
                    {

                    }
                    // ___________________________________________________________________________________________

                    //GlobalVarMapMain.latRLS = 53.931193464;
                    //GlobalVarMapMain.longRLS = 27.635563616;
                    //GlobalVarMapMain.HRLS = 175;

                    // rad
                    GlobalVarMapMain.latRLS_rad = GlobalVarMapMain.latRLS * Math.PI / 180;
                    GlobalVarMapMain.longRLS_rad = GlobalVarMapMain.longRLS * Math.PI / 180;

                    // Координаты RLS в ГЦСК
                    ClassGeoCalculator.f_BLH_XYZ_84_1
                                                     (
                                                      GlobalVarMapMain.latRLS,
                                                      GlobalVarMapMain.longRLS,
                                                      GlobalVarMapMain.HRLS,
                                                      ref GlobalVarMapMain.XRLS,
                                                      ref GlobalVarMapMain.YRLS,
                                                      ref GlobalVarMapMain.ZRLS
                                                      );
                    // ___________________________________________________________________________________________


                    // ********************************************************************* Входной лист отметок

                    // Aeroscope ********************************************************************************
                    DrawAeroscope();
                    DrawStations();
                    DrawVictor();
                    // ******************************************************************************** Aeroscope

                    // TMR ***************************************************************************************
                    // Start Tmr

                    GlobalVarMapMain.shpoints = 0;

                    GlobalVarMapMain.dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                    //GlobalVarMapMain.dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                    GlobalVarMapMain.dispatcherTimer.Interval = TimeSpan.FromMilliseconds(3);

                    GlobalVarMapMain.dispatcherTimer.Start();
                    //  *************************************************************************************** TMR

                }
                // REALREALREALREALREALREALREALREALREALREALREALREALREALREALREALREAL Работа в реальном времени

            }  // Mode_Test
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& MODE_TEST


            // MODE_EMULATOR &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

            // *Pavel
            else if (GlobalVarMapMain.ModeEmulator == true)
            {

                // ...........................................................................................
                // Ввод с интерфейса
                LoadParametersGlobal();
                LoadParametersRLS();
                // !!! FOR OLD algorithm
                LoadClassParameters();
                GlobalVarMapMain.numbp = Convert.ToInt32(GlobalVarMapMain.objMainWindowG.TextBox12.Text);
                // ...........................................................................................

                // ...........................................................................................
                // OLD

                GlobalVarMapMain.objClassPointt.Clear();
                GlobalVarMapMain.list_tracks_emulRDM.Clear();

                // *Pavel
                //GlobalVarMapMain.listTrackSource.Clear();
                //GlobalVarMapMain.listTrackAirObject.Clear();


                //Aero
                istraj = true;
                // rdm
                istrajRDM = true;

                if (istraj)
                {
                    istrajRequest = true;
                    Task.Run(() => TrajRequest());
                }

                if (istrajRDM)
                {
                    istrajRequestRDM = true;
                    Task.Run(() => TrajRequestRDM());
                }
                // ...........................................................................................


            }  // ModeEmulator
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& MODE_EMULATOR

            // MODE_REAL &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

            else if (GlobalVarMapMain.ModeReal == true)
            {
                // Входные параметры
                // Ввод с интерфейса
                //LoadParameters();
                // Запись в класс для подачи в DLL
                //LoadClassParameters();

                // Ввод с интерфейса
                LoadParametersGlobal();
                LoadParametersRLS();


                // Свой IP
                string addr = ip2Box.Text;
                //string addr = "192.168.1.15";
                IPAddress address = IPAddress.Parse(addr);
                var myIp = address;

                // Свой порт
                //var myPort = 15001;
                var myPort = Convert.ToInt32(Port2Box.Text);

                // Удаленный IP
                string addr2 = ip2Box_1.Text;
                //string addr2 = "192.168.1.1";
                IPAddress address2 = IPAddress.Parse(addr2);
                var myIp2 = address2;
                var remoteIp2 = address2;

                // Удаленный порт
                var remotePort = 15000;
                //var remotePort = Convert.ToInt32(Port2Box_1.Text);

                byte addrSender = 0;
                byte addrRecipient = 0;

                uDPReceiver2 = new UDPReceiver.UDPReceiver(myIp, myPort, remoteIp2, remotePort, addrSender, addrRecipient);

                uDPReceiver2.OnGetCoordRDM += UDPReceiver2_OnGetCoordRDM;
                uDPReceiver2.OnGetCoordAero += UDPReceiver2_OnGetCoordAero;

                GlobalVarMapMain.objClassPointt1.Clear();
                GlobalVarMapMain.list_tracks_emulRDM1.Clear();

                uDPReceiver2.Connect();

            }  // ModeReal
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& MODE_REAL


        } // ButtonSTART
        // *************************************************************************************** TRACKS

        // Tmr ************************************************************************************************
        // !!! Получаем, обрабатываем и рисуем отфильтрованную траекторию : ТЕСТОВЫЙ РЕЖИМ

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {

            if (GlobalVarMapMain.flReal == true)
            {
                if (GlobalVarMapMain.shpoints >= GlobalVarMapMain.lsttmp.Count)
                    return;

                // --------------------------------------------------------------------------------
                // !!! Получаем отметку
                // Пока берем из файла lsttmp

                // --------------------------------------------------------------------------------

                int i1_1 = 0;
                int i2_1 = 0;
                double XIRI = 0;
                double YIRI = 0;
                double ZIRI = 0;

                // Координаты IRI в ГЦСК
                ClassGeoCalculator.f_BLH_XYZ_84_1
                                                 (
                                                  GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].Latitude,
                                                  GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].Longitude,
                                                  GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].Altitude,
                                                  ref XIRI,
                                                  ref YIRI,
                                                  ref ZIRI
                                                  );

                // #@#
                // Координаты IRI в МЗСК
                /*
                                ClassGeoCalculator.Geoc_MZSK(
                                                             GlobalVarMapMain.latRLS_rad,
                                                             GlobalVarMapMain.longRLS_rad,
                                                             GlobalVarMapMain.XRLS,
                                                             GlobalVarMapMain.YRLS,
                                                             GlobalVarMapMain.ZRLS,
                                                             XIRI,
                                                             YIRI,
                                                             ZIRI,
                                                             ref GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].X,
                                                             ref GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].Y,
                                                             ref GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].Z
                                                            );
                */
                ClassGeoCalculator.Geoc_MZSK_V(
                                             GlobalVarMapMain.latRLS_rad,
                                             GlobalVarMapMain.longRLS_rad,
                                             GlobalVarMapMain.XRLS,
                                             GlobalVarMapMain.YRLS,
                                             GlobalVarMapMain.ZRLS,
                                             XIRI,
                                             YIRI,
                                             ZIRI,
                                             ref GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].X,
                                             ref GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].Y,
                                             ref GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints].Z
                                            );

                // --------------------------------------------------------------------------------
                // Подаем отметку на обработку


                ClassTrajectory objClassTrajectory2 = new ClassTrajectory();
                //GlobalVarMapMain.lsttr = objClassTrajectory2.f_Trajectory1(
                objClassTrajectory2.f_Trajectory1(
                                      // Отметка
                                      GlobalVarMapMain.lsttmp[GlobalVarMapMain.shpoints],
                                      // Входные параметры
                                      GlobalVarMapMain.ObjClassParameters,
                                      ref GlobalVarMapMain.list_tracks
                                     );

                //TextBox12.Text = Convert.ToString(GlobalVarMapMain.lsttr.Count);

                int gg = 0;
                if (GlobalVarMapMain.list_tracks[0].listPointsTrajFiltr_I.Count != 0)
                    gg = 0;



                // --------------------------------------------------------------------------------
                // Рисуем отфильтрованную траекторию

                DrawFilter1();
                // --------------------------------------------------------------------------------
                GlobalVarMapMain.shpoints += 1;
                // --------------------------------------------------------------------------------
            }  // flReal==true

        }
        // ************************************************************************************************ Tmr


    } // Class
} // Namespace
