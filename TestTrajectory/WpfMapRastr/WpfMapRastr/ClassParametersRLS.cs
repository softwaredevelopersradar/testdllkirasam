﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastr
{
    // RLS coordinates
    class ClassParametersRLS
    {
        // Координаты станций в WGS84
        // 1
        public double latRLS = 0;
        public double longRLS = 0;
        public double latRLS_rad = 0;
        public double longRLS_rad = 0;
        public double HRLS = 0;
        // 2
        public double latRLS_2 = 0;
        public double longRLS_2 = 0;
        public double latRLS_rad_2 = 0;
        public double longRLS_rad_2 = 0;
        public double HRLS_2 = 0;
        // 3
        public double latRLS_3 = 0;
        public double longRLS_3 = 0;
        public double latRLS_rad_3 = 0;
        public double longRLS_rad_3 = 0;
        public double HRLS_3 = 0;
        // 4
        public double latRLS_4 = 0;
        public double longRLS_4 = 0;
        public double latRLS_rad_4 = 0;
        public double longRLS_rad_4 = 0;
        public double HRLS_4 = 0;

        // Координаты станций в геоцентрической СК
        // 1
        public double XRLS_Geoc = 0;
        public double YRLS_Geoc = 0;
        public double ZRLS_Geoc = 0;
        // 2
        public double XRLS_Geoc_2 = 0;
        public double YRLS_Geoc_2 = 0;
        public double ZRLS_Geoc_2 = 0;
        // 3
        public double XRLS_Geoc_3 = 0;
        public double YRLS_Geoc_3 = 0;
        public double ZRLS_Geoc_3 = 0;
        // 4
        public double XRLS_Geoc_4 = 0;
        public double YRLS_Geoc_4 = 0;
        public double ZRLS_Geoc_4 = 0;


        // Координаты станций в МЗСК (Victor)
        // 1
        public double XRLS_MZSK = 0;
        public double YRLS_MZSK = 0;
        public double ZRLS_MZSK = 0;
        // 2
        public double XRLS_MZSK_2 = 0;
        public double YRLS_MZSK_2 = 0;
        public double ZRLS_MZSK_2 = 0;
        // 3
        public double XRLS_MZSK_3 = 0;
        public double YRLS_MZSK_3 = 0;
        public double ZRLS_MZSK_3 = 0;
        // 4
        public double XRLS_MZSK_4 = 0;
        public double YRLS_MZSK_4 = 0;
        public double ZRLS_MZSK_4 = 0;

    } // Class
} // Namespace
