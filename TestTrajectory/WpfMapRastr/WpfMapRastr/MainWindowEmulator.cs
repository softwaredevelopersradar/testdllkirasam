﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

namespace WpfMapRastr
{
    // Обработка файлов в режиме эмулятора
    public partial class MainWindow
    {

        // AEROSCOPE *********************************************************************************

        public async void TrajRequest()

        {

            while (istrajRequest)
            {

                if (countAero == aeroScopes.Count)
                {
                    countAero = 0;
                }

                try
                {

                    float x = aeroScopes[countAero].latitude; //широта
                    float z = aeroScopes[countAero].longitude; //долгота
                    float y = (float)aeroScopes[countAero].altitude; // высота

                    if (GlobalVarMapMain.objClassPointt.Count < GlobalVarMapMain.numbp)
                    {
                        ClassPointt obb = new ClassPointt();

                        obb.Latitude = x;
                        obb.Longitude = z;
                        obb.Altitude = y;
                        GlobalVarMapMain.objClassPointt.Add(obb);
                    }
                    else
                    {
                        GlobalVarMapMain.objClassPointt.RemoveAt(0);

                        ClassPointt obb = new ClassPointt();

                        obb.Latitude = x;
                        obb.Longitude = z;
                        obb.Altitude = y;
                        GlobalVarMapMain.objClassPointt.Add(obb);
                    }

                    double ltvyx = 0;
                    double lngvyx = 0;
                    double altvyx = 0;

                    //отправить на карту
                    FucnPaintMap_Filtr(
                                       GlobalVarMapMain.objClassPointt,
                                       GlobalVarMapMain.list_tracks_emulRDM,
                                       ref ltvyx,
                                       ref lngvyx,
                                       ref altvyx
                                      );

                    //отправить через UDP
                    if (uDPReceiver != null)
                        uDPReceiver.SendAero(new UDPReceiver.Coord(x, z, y));
                    // ------------------------------------------------------------------------------

                    int Ti = countAero;
                    int Ti1 = countAero + 1;
                    if (Ti1 == aeroScopes.Count) Ti1 = 0;

                    int delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                    if (delay > 10000) delay = 5000;

                    DispatchIfNecessary(() =>
                    {
                        //if (timeCheckBox.IsChecked.Value)
                        //{
                        try
                        {

                            //if ((timeBox.Text.Contains(".")) && (DecSep == ','))
                            //    timeBox.Text = timeBox.Text.Replace(".", ",");
                            //if ((timeBox.Text.Contains(",")) && (DecSep == '.'))
                            //    timeBox.Text = timeBox.Text.Replace(",", ".");

                            //double coef = Convert.ToDouble(timeBox.Text);
                            //delay = (int)Math.Abs((delay * coef));

                        }
                        catch
                        {
                            delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                        }
                        //}
                    }); // DispatchIfNecessary

                    await Task.Delay(delay);

                    countAero++;


                } // try

                catch
                {
                    int g = 4;
                }
            } //  WHILE

        }
        // ********************************************************************************* AEROSCOPE

        // RDM ***************************************************************************************

        public async void TrajRequestRDM()

        {

            while (istrajRequestRDM)
            {
                if (countRDM == rdmList.Count)
                {
                    countRDM = 0;
                    GlobalVarMapMain.list_tracks_emulRDM.Clear();
                }

                try
                {

                    float x = rdmList[countRDM].latitude; //широта
                    float z = rdmList[countRDM].longitude; //долгота
                    float y = (float)rdmList[countRDM].altitude; // высота


                    // FILTER -----------------------------------------------------------

                    //public static List<ClassTrackPoint> lsttmp = new List<ClassTrackPoint>();
                    ClassTrackPoint objClassTrackPoint = new ClassTrackPoint();
                    objClassTrackPoint.Latitude = (double)rdmList[countRDM].latitude;
                    objClassTrackPoint.Longitude = (double)rdmList[countRDM].longitude;
                    objClassTrackPoint.Altitude = (double)rdmList[countRDM].altitude;
                    objClassTrackPoint.timePoint = (double)rdmList[countRDM].time / 1000;

                    f_Filter(
                             ref objClassTrackPoint,
                             ref GlobalVarMapMain.list_tracks_emulRDM
                            );


                    for (int i1 = 0; i1 < GlobalVarMapMain.list_tracks_emulRDM.Count; i1++)
                    {

                        for (int i2 = 1; i2 < GlobalVarMapMain.list_tracks_emulRDM[i1].listPointsTrajFiltr_I.Count; i2++)

                        {
                            if (GlobalVarMapMain.list_tracks_emulRDM[i1].listPointsTrajFiltr_I.Count > GlobalVarMapMain.numbp)
                            {
                                GlobalVarMapMain.list_tracks_emulRDM[i1].listPointsTrajFiltr_I.RemoveAt(0);
                            }

                        } // for2

                    } // for1

                    double ltvyx = 0;
                    double lngvyx = 0;
                    double altvyx = 0;

                    //отправить на карту
                    FucnPaintMap_Filtr(
                                       GlobalVarMapMain.objClassPointt,
                                       GlobalVarMapMain.list_tracks_emulRDM,
                                       ref ltvyx,
                                       ref lngvyx,
                                       ref altvyx
                                      );


                    //отправить через UDP
                    if (uDPReceiver != null)
                        uDPReceiver.SendRDM(new UDPReceiver.Coord(ltvyx, lngvyx, (float)altvyx));

                    int Ti = countRDM;
                    int Ti1 = countRDM + 1;
                    if (Ti1 == rdmList.Count) Ti1 = 0;

                    int delay = Math.Abs(rdmList[Ti1].time - rdmList[Ti].time);
                    if (delay > 10000) delay = 5000;

                    DispatchIfNecessary(() =>
                    {
                        //if (timeCheckBox.IsChecked.Value)
                        //{
                        try
                        {
                            //if ((timeBox.Text.Contains(".")) && (DecSep == ','))
                            //    timeBox.Text = timeBox.Text.Replace(".", ",");
                            //if ((timeBox.Text.Contains(",")) && (DecSep == '.'))
                            //    timeBox.Text = timeBox.Text.Replace(",", ".");

                            //double coef = Convert.ToDouble(timeBox.Text);
                            //delay = (int)Math.Abs((delay * coef));

                        }
                        catch
                        {
                            delay = Math.Abs(rdmList[Ti1].time - rdmList[Ti].time);
                        }
                        //}
                    });

                    await Task.Delay(delay);

                    countRDM++;
                }

                catch
                {
                    int g = 4;
                }
            }
        }

        // *************************************************************************************** RDM

        // -------------------------------------------------------------------------------------------
        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
        // -------------------------------------------------------------------------------------------



    } // Class
} // Namespace
