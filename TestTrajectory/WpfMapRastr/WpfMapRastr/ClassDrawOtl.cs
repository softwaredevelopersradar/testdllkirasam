﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

using YamlDotNet;
using YamlReverseExpertise;

using fDRM;

namespace WpfMapRastr
{
    public partial class MainWindow
    {

        // MAIN ****************************************************************************************

        public void FileTimeZad()
        {
            // Чтение входного файла ----------------------------------------------------------
            // Читаем входной файл аэроскопа в формате .txt:
            // t,LatLong,H
            // !!! Имя ввели перед Start по кнопке с диалогового окна

            string dir3time = "";
            string sltlng3time = "";
            // Get current dir
            string dirtime = "";
            dirtime = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            //dir3time = dirtime + "\\FILES\\" + "aeroscope_time_input.txt";
            dir3time = GlobalVarMapMain.DirAeroscope;

            GlobalVarMapMain.lsttmp_aeroscope_time.Clear();

            try
            {
                StreamReader sr3time = new StreamReader(dir3time);

                do
                {
                    sltlng3time = sr3time.ReadLine();

                    if (DecSep == '.')
                        sltlng3time = sltlng3time.Replace(',', '.');
                    if (DecSep == ',')
                        sltlng3time = sltlng3time.Replace('.', ',');

                    string[] words3time = sltlng3time.Split(new char[] { ' ' });

                    // ..................................................................

                    ClassTrackPoint objClassTrackPointtime = new ClassTrackPoint();
                    objClassTrackPointtime.Latitude = Convert.ToDouble(words3time[1]);
                    objClassTrackPointtime.Longitude = Convert.ToDouble(words3time[2]);
                    objClassTrackPointtime.Altitude = Convert.ToDouble(words3time[3]);

                    //objClassTrackPointtime.timePoint = Convert.ToDouble(words3time[0]) / 1000; //sec

                    string[] words3time_1 = words3time[0].Split(new char[] { ':' });
                    objClassTrackPointtime.timePoint = Convert.ToDouble(words3time_1[0]) * 3600 
                                                      + Convert.ToDouble(words3time_1[1])*60+ 
                                                      Convert.ToDouble(words3time_1[2]);

                    GlobalVarMapMain.lsttmp_aeroscope_time.Add(objClassTrackPointtime);
                    // ..................................................................

                } while (sltlng3time != null);

                sr3time.Close();
            }

            catch
            {

            }

            // ---------------------------------------------------------- Чтение входного файла

            // Выходной файл ------------------------------------------------------------------
            // Записываем выходной файл для аэроскопа в том же формате, но дополненный задержками в нс

            // .............................................................................
            string dirK = "";
            int i1 = 0;
            double t = 0;
            double Lat = 0;
            double Long = 0;
            double Alt = 0;
            double XX = 0;
            double YY = 0;
            double ZZ = 0;
            // .............................................................................
            dirK = dirtime + "\\FILES\\" + "aeroscope_time_output.txt";          
            StreamWriter swK = new StreamWriter(dirK, false, System.Text.Encoding.Default);

            // .............................................................................
            // FOR

            for (i1 = 0; i1 < GlobalVarMapMain.lsttmp_aeroscope_time.Count; i1++)
            {
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                t = GlobalVarMapMain.lsttmp_aeroscope_time[i1].timePoint;
                Lat = GlobalVarMapMain.lsttmp_aeroscope_time[i1].Latitude;
                Long = GlobalVarMapMain.lsttmp_aeroscope_time[i1].Longitude;
                Alt = GlobalVarMapMain.lsttmp_aeroscope_time[i1].Altitude;

                // 30_06
                // Lat = 53.933451;
                // Long = 27.636539;
                // Alt =70;

                // FIN1
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Формируем отметку MarkCoord (здесь же пересчет в декартовы идет)

                MarkCoord _MarkCoord = new MarkCoord(
                                                        Lat,
                                                        Long,
                                                        Alt,
                                                        GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Latitude,
                                                        GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Longitude,
                                                        GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Altitude
                                                     );
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                XX = _MarkCoord.X;
                YY = _MarkCoord.Y;
                ZZ = _MarkCoord.Z;

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Вычисление задержек

                var matrixCtime = new double[1, 3];
                matrixCtime=Calc_dt_time(XX, YY, ZZ);
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                double consec = t;
                int secparce = (int)consec;
                int hours = (secparce / 60) / 60;                   //Получаем ЧАСЫ из СЕКУНД
                double sec1 = consec - (hours * 60 * 60);            //Получаем Остаток СЕКУНД если вычесть ЧАСЫ
                int minuts = (int)sec1 / 60;                             //Получаем МИНУТЫ из Оставшихся СЕКУНД
                double seconds = sec1 - (minuts * 60);                 //Получаем СЕКУНДЫ от Остатка СЕКУНД после вычета ЧАСА и МИНУТ

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // Запись в файл

               string sfilter = "";

                double secd = 0;
                long secl = 0;

                double dt1d = 0;
                long dt1l = 0;
                double dt2d = 0;
                long dt2l = 0;
                double dt3d = 0;
                long dt3l = 0;

                secl = (long)(seconds * 1000);
                secd = ((double)secl) / 1000;
                dt1l = (long)(matrixCtime[0, 0] * 1000000000 * 1000);
                dt1d = ((double)dt1l) / 1000;
                dt2l = (long)(matrixCtime[0, 1] * 1000000000 * 1000);
                dt2d = ((double)dt2l) / 1000;
                dt3l = (long)(matrixCtime[0, 2] * 1000000000 * 1000);
                dt3d = ((double)dt3l) / 1000;

                sfilter = //Convert.ToString(t) +
                              Convert.ToString(hours)+":"+ Convert.ToString(minuts) + ":"+
                              Convert.ToString(secd) + 
                              " " + Convert.ToString(Lat) + " " +
                              Convert.ToString(Long) + 
                              " " + Convert.ToString(Alt) +
                              " " + Convert.ToString(dt1d) +
                              " " + Convert.ToString(dt2d) +
                              " " + Convert.ToString(dt3d);

               swK.WriteLine(sfilter);
          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            } // FOR1
            // ...........................................................................
            swK.Close();
            // ...........................................................................

            // ------------------------------------------------------------------ Выходной файл

            // Файл измеренных задержек и координат от Павла ----------------------------------
            // Pavel : задержки +RDM
            // Имя заранее ввели по кнопке с диалогового окна

            Yaml yaml = new Yaml();
            //var load = yaml.YamlLoad<ReverseExpertise>(dirtime + "\\FILES\\" + "ReverseExpertise.yaml");
            var load = yaml.YamlLoad<ReverseExpertise>(GlobalVarMapMain.DirRDM);

            // ---------------------------------- Файл измеренных задержек и координат от Павла

            // Создание файла координат по задержкам Павла --------------------------------------------- 

/*
            string dirK1 = "";
            DateTime t1;
            double Lat1 = 0;
            double Long1 = 0;
            double Alt1 = 0;
            double XX1 = 0;
            double YY1 = 0;
            double ZZ1 = 0;
            string sfilter1 = "";

            // Открыть файл для записи >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            dirK1 = dirtime + "\\FILES\\" + "aeroscope_time_output_OnReal.txt";
            StreamWriter swK1 = new StreamWriter(dirK1, false, System.Text.Encoding.Default);

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Открыть файл для записи

            // FOR2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            int i2 = 0;


            GlobalVarMapMain.sh1RDM = 0;
            GlobalVarMapMain.sh2RDM = 0;

            for (i2 = 0; i2 < load.Expertises.Length; i2++)
            {

                // Входной лист для подачи в функцию ....................................................
                List<ClassObject> LstClassObject1 = new List<ClassObject>();


                // SP1 (base)
                ClassObject objClassObject1 = new ClassObject();
                objClassObject1.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Latitude;
                objClassObject1.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Longitude;
                objClassObject1.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Altitude;
                objClassObject1.tau = 0;
                objClassObject1.BaseStation = true;
                objClassObject1.IndexStation = 1;
                LstClassObject1.Add(objClassObject1);


                // FIN1
                // SP2
                ClassObject objClassObject2 = new ClassObject();
                objClassObject2.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Latitude;
                objClassObject2.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Longitude;
                objClassObject2.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Altitude;

               objClassObject2.tau = load.Expertises[i2].Tau1;
                //objClassObject2.tau = 2.08E-07;

                objClassObject2.BaseStation = false;
                objClassObject2.IndexStation = 2;
                LstClassObject1.Add(objClassObject2);

                // SP3
                ClassObject objClassObject3 = new ClassObject();
                objClassObject3.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Latitude;
                objClassObject3.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Longitude;
                objClassObject3.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Altitude;

                objClassObject3.tau = load.Expertises[i2].Tau2;
                //objClassObject3.tau = -1.1004E-07;

                objClassObject3.BaseStation = false;
                objClassObject3.IndexStation = 3;
                LstClassObject1.Add(objClassObject3);

                // SP4
                ClassObject objClassObject4 = new ClassObject();
                objClassObject4.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Latitude;
                objClassObject4.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Longitude;
                objClassObject4.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Altitude;

                objClassObject4.tau = load.Expertises[i2].Tau3;
                //objClassObject4.tau = -8.0E-08;
            
                objClassObject4.BaseStation = false;
                objClassObject4.IndexStation = 4;
                LstClassObject1.Add(objClassObject4);
                // .................................................... Входной лист для подачи в функцию

                // Расчет координат по задержкам ........................................................

                ClassDRM_dll objClassDRM_dll = new ClassDRM_dll();
                ClassObjectTmp ObjClassObjectTmp1 = new ClassObjectTmp();
                ObjClassObjectTmp1 = objClassDRM_dll.f_DRM(
                                                          LstClassObject1
                                                          //ref GlobalVarMapMain.sh1RDM ,
                                                          //ref GlobalVarMapMain.sh2RDM
                                                         );



                // FIN2
                ObjClassObjectTmp1.Latitude = load.Expertises[i2].Lat;
                ObjClassObjectTmp1.Longitude = load.Expertises[i2].Lon;
                ObjClassObjectTmp1.Altitude = load.Expertises[i2].Alt;

                // ........................................................ Расчет координат по задержкам

                // Запись в файл ........................................................................

                t1 = load.Expertises[i2].Time;
                Lat1 = ObjClassObjectTmp1.Latitude;
                Long1 = ObjClassObjectTmp1.Longitude;
                Alt1 = ObjClassObjectTmp1.Altitude;

                double dt1d_1 = 0;
                long dt1l_1 = 0;
                double dt2d_1 = 0;
                long dt2l_1 = 0;
                double dt3d_1 = 0;
                long dt3l_1 = 0;
                dt1l_1 = (long)(load.Expertises[i2].Tau1 * 1000000000 * 1000);
                dt1d_1 = ((double)dt1l_1) / 1000;
                dt2l_1 = (long)(load.Expertises[i2].Tau2 * 1000000000 * 1000);
                dt2d_1 = ((double)dt2l_1) / 1000;
                dt3l_1 = (long)(load.Expertises[i2].Tau3 * 1000000000 * 1000);
                dt3d_1 = ((double)dt3l_1) / 1000;

                sfilter1 = Convert.ToString(t1.TimeOfDay) + " " +
                              Convert.ToString(Lat1) + " " +
                              Convert.ToString(Long1) + " " +
                              Convert.ToString(Alt1) + " " +
                              Convert.ToString(dt1d_1) + " " +
                              Convert.ToString(dt2d_1) + " " +
                              Convert.ToString(dt3d_1);

                swK1.WriteLine(sfilter1);
                // ........................................................................ Запись в файл


            } // FOR2
              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR2

            swK1.Close();
*/
            // ---------------------------------------------  Создание файла координат по задержкам Павла


            // Создание файла координат по (задержкам Павла)*K ------------------------------------------
            // !!! Это будет входной файл в фильтр
            // !!! Теперь координаты по задержкам не пересчитываем, а просто берем RDM из файла Павла

            string dirK1_inv = "";
            DateTime t1_inv;
            double Lat1_inv = 0;
            double Long1_inv = 0;
            double Alt1_inv = 0;
            double XX1_inv = 0;
            double YY1_inv = 0;
            double ZZ1_inv = 0;
            string sfilter1_inv = "";

            // Открыть файл для записи >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            dirK1_inv = dirtime + "\\FILES\\" + "aeroscope_time_output_OnRealInv.txt";
            StreamWriter swK1_inv = new StreamWriter(dirK1_inv, false, System.Text.Encoding.Default);

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Открыть файл для записи

            // FOR2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            int i2_inv = 0;

            for (i2_inv = 0; i2_inv < load.Expertises.Length; i2_inv++)
            {

                // Входной лист для подачи в функцию ....................................................
                List<ClassObject> LstClassObject1_inv = new List<ClassObject>();

                // SP1 (base)
                ClassObject objClassObject1_inv = new ClassObject();
                objClassObject1_inv.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Latitude;
                objClassObject1_inv.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Longitude;
                objClassObject1_inv.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Altitude;
                objClassObject1_inv.tau = 0;
                objClassObject1_inv.BaseStation = true;
                objClassObject1_inv.IndexStation = 1;
                LstClassObject1_inv.Add(objClassObject1_inv);

                // SP2
                ClassObject objClassObject2_inv = new ClassObject();
                objClassObject2_inv.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Latitude;
                objClassObject2_inv.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Longitude;
                objClassObject2_inv.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Altitude;
                
                objClassObject2_inv.tau = load.Expertises[i2_inv].Tau1 * GlobalVarMapMain.ObjClassParametersGlobal.KDT;

                objClassObject2_inv.BaseStation = false;
                objClassObject2_inv.IndexStation = 2;
                LstClassObject1_inv.Add(objClassObject2_inv);

                // SP3
                ClassObject objClassObject3_inv = new ClassObject();
                objClassObject3_inv.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Latitude;
                objClassObject3_inv.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Longitude;
                objClassObject3_inv.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Altitude;

                objClassObject3_inv.tau = load.Expertises[i2_inv].Tau2 * GlobalVarMapMain.ObjClassParametersGlobal.KDT;

                objClassObject3_inv.BaseStation = false;
                objClassObject3_inv.IndexStation = 3;
                LstClassObject1_inv.Add(objClassObject3_inv);

                // SP4
                ClassObject objClassObject4_inv = new ClassObject();
                objClassObject4_inv.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Latitude;
                objClassObject4_inv.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Longitude;
                objClassObject4_inv.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Altitude;
                
                objClassObject4_inv.tau = load.Expertises[i2_inv].Tau3 * GlobalVarMapMain.ObjClassParametersGlobal.KDT;

                objClassObject4_inv.BaseStation = false;
                objClassObject4_inv.IndexStation = 4;
                LstClassObject1_inv.Add(objClassObject4_inv);

                // .................................................... Входной лист для подачи в функцию

                // Расчет координат по задержкам ........................................................

                ClassDRM_dll objClassDRM_dll_inv = new ClassDRM_dll();
                ClassObjectTmp ObjClassObjectTmp1_inv = new ClassObjectTmp();

                //ObjClassObjectTmp1_inv = objClassDRM_dll_inv.f_DRM(
                //                                                   LstClassObject1_inv
                //                                          // Otl
                //                                          //ref GlobalVarMapMain.sh1RDM,
                //                                          //ref GlobalVarMapMain.sh2RDM
                //                                                   );

                // FIN2
                ObjClassObjectTmp1_inv.Latitude = load.Expertises[i2_inv].Lat;
                ObjClassObjectTmp1_inv.Longitude = load.Expertises[i2_inv].Lon;
                ObjClassObjectTmp1_inv.Altitude = load.Expertises[i2_inv].Alt;

                // ........................................................ Расчет координат по задержкам

                // Запись в файл ........................................................................

                t1_inv = load.Expertises[i2_inv].Time;
                Lat1_inv = ObjClassObjectTmp1_inv.Latitude;
                Long1_inv = ObjClassObjectTmp1_inv.Longitude;
                Alt1_inv = ObjClassObjectTmp1_inv.Altitude;

                double dt1d_2 = 0;
                long dt1l_2 = 0;
                double dt2d_2 = 0;
                long dt2l_2 = 0;
                double dt3d_2 = 0;
                long dt3l_2 = 0;

                double dtt1 = 0;
                double dtt2 = 0;
                double dtt3 = 0;

                double fq = 0;

                fq = load.Expertises[i2_inv].FreqkHz;

                // .....................................................
                dt1l_2 = (long)(load.Expertises[i2_inv].Tau1 * GlobalVarMapMain.ObjClassParametersGlobal.KDT * 1000000000 * 1000);

                dt1d_2 = ((double)dt1l_2) / 1000;
                // .....................................................

                dt2l_2 = (long)(load.Expertises[i2_inv].Tau2 * GlobalVarMapMain.ObjClassParametersGlobal.KDT * 1000000000 * 1000);

                dt2d_2 = ((double)dt2l_2) / 1000;
                // .....................................................

                dt3l_2 = (long)(load.Expertises[i2_inv].Tau3 * GlobalVarMapMain.ObjClassParametersGlobal.KDT * 1000000000 * 1000);
                
                dt3d_2 = ((double)dt3l_2) / 1000;
                // .....................................................


                sfilter1_inv = Convert.ToString(t1_inv.TimeOfDay) + " " +
                              Convert.ToString(Lat1_inv) + " " +
                              Convert.ToString(Long1_inv) + " " +
                              Convert.ToString(Alt1_inv)+ " " +
                              Convert.ToString(dt1d_2) + " " +
                              Convert.ToString(dt2d_2) + " " +
                              Convert.ToString(dt3d_2) + " " +
                              Convert.ToString(fq);

                swK1_inv.WriteLine(sfilter1_inv);
                // ........................................................................ Запись в файл


            } // FOR2
              // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR2

            swK1_inv.Close();

            // ------------------------------------------ Создание файла координат по (задержкам Павла)*K


        }

        // **************************************************************************************** MAIN






        // Calc_dt ********************************************************************************
        // Расчет временных задержек

        public double[,] Calc_dt_time(
                                // координаты обэекта в МЗСК
                                double X,
                                double Y,
                                double Z
                               )
        {
            // ....................................................................................
            double RCC = 0;

            // Расстояние между ППi и ИРИ
            double R0 = 0;
            double R1 = 0;
            double R2 = 0;
            double R3 = 0;

            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3
            double dr01 = 0;
            double dr02 = 0;
            double dr03 = 0;

            //double cc = 299792458;
            double cc = 299796459.2;

            var matrixC = new double[1, 3];
            // ....................................................................................

            RCC = Math.Sqrt(X * X + Y * Y + Z * Z);
            R0 = RCC;

            R1 = Math.Sqrt((X - GlobalVarMapMain.objTracksDefinition.listStations[1].Position.X) * (X - GlobalVarMapMain.objTracksDefinition.listStations[1].Position.X)
                           + (Y - GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Y) * (Y - GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Y) +
                             (Z - GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Z) * (Z - GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Z));

            R2 = Math.Sqrt((X - GlobalVarMapMain.objTracksDefinition.listStations[2].Position.X) * (X - GlobalVarMapMain.objTracksDefinition.listStations[2].Position.X)
                           + (Y - GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Y) * (Y - GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Y) +
                             (Z - GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Z) * (Z - GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Z));

            R3 = Math.Sqrt((X - GlobalVarMapMain.objTracksDefinition.listStations[3].Position.X) * (X - GlobalVarMapMain.objTracksDefinition.listStations[3].Position.X)
                           + (Y - GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Y) * (Y - GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Y) +
                             (Z - GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Z) * (Z - GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Z));

            // ....................................................................................
            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3

            dr01 = R1 - R0;
            dr02 = R2 - R0;
            dr03 = R3 - R0;

            matrixC[0, 0] = dr01 / cc;
            matrixC[0, 1] = dr02 / cc;
            matrixC[0, 2] = dr03 / cc;
            // .......................................................................................

            return matrixC;
        }
        // ****************************************************************************************

        // FileForVictor ********************************************************************
        // Запись выхода фильтра: t,Latitude,Longitude,H
        // GeoCoordTxt

        public void FileGeoFilter()
     {
            int i1 = 0;
            int i2 = 0;
            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double ltt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;

            double t = 0;
            double Lat = 0;
            double Long = 0;
            double Alt = 0;
            // ..........................................................................
            // Для записи файла GeoCoord.txt

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dirK = "";
            dirK = dir + "\\FILES\\" + "GeoCoord.txt";
            StreamWriter swK = new StreamWriter(dirK, false, System.Text.Encoding.Default);
            // ...........................................................................
            for (i1 = 0; i1 < GlobalVarMapMain.lsttr.Count; i1++)
            {

                for (i2 = 1; i2 < GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I.Count; i2++)
                {
                    // #@#
                    /*
                                        ClassGeoCalculator.MZSK_Geoc(
                                                                  GlobalVarMapMain.latRLS_rad,
                                                                  GlobalVarMapMain.longRLS_rad,
                                                                  GlobalVarMapMain.XRLS,
                                                                  GlobalVarMapMain.YRLS,
                                                                  GlobalVarMapMain.ZRLS,
                                                                  GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].X,
                                                                  GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Y,
                                                                  GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Z,
                                                                  ref xxx13,
                                                                  ref yyy13,
                                                                  ref zzz13
                                                                );
                    */

                    ClassGeoCalculator.MZSK_Geoc_V(
                                              GlobalVarMapMain.latRLS_rad,
                                              GlobalVarMapMain.longRLS_rad,
                                              GlobalVarMapMain.XRLS,
                                              GlobalVarMapMain.YRLS,
                                              GlobalVarMapMain.ZRLS,
                                              GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].X,
                                              GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Y,
                                              GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Z,
                                              ref xxx13,
                                              ref yyy13,
                                              ref zzz13
                                            );

                    ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13, yyy13, zzz13, ref ltt23, ref lng23, ref hhh23);

                    // Фильтр[1] соответствует входной отметке[3] (по первым трем фильтр инициализируется, и это фильтр[0])
                    t = GlobalVarMapMain.lsttr[i1].listPointsTraj_I[i2 + 2].timePoint;
                    Lat = ltt23;
                    Long = lng23;
                    Alt = hhh23;

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Запись в файл

                    string sfilter = "";

                    // #@#
                    sfilter = Convert.ToString(t) + " " + Convert.ToString(Lat) + " " +
                              Convert.ToString(Long) + " " + Convert.ToString(Alt)+
                              " "+ Convert.ToString(GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].X)+
                              " "+ Convert.ToString(GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Y) +
                              " " + Convert.ToString(GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Z) ;

                    swK.WriteLine(sfilter);
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                } // for2



            } // for1
            // ...........................................................................
            swK.Close();
            // ...........................................................................

        }
        // ******************************************************************** FileForVictor

        // DrawVictor ************************************************************************************
        // Данные с фильтра Виктора (время, широта, долгота, высота)

        public void DrawVictor()
        {

            string s6 = "";
            int i1 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

            for (i1 = 0; i1 < GlobalVarMapMain.lsttmp_aeroscope_1.Count; i1++)
            {
                var p = Mercator.FromLonLat(GlobalVarMapMain.lsttmp_aeroscope_1[i1].Longitude, GlobalVarMapMain.lsttmp_aeroscope_1[i1].Latitude);
                xcntr1 = p.X;
                ycntr1 = p.Y;

                pointPel.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));

            } // for1

            s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                // points.X=X, points.Y=Y -> for Mercator map
                                                pointPel,
                                                // Color -> Red
                                                100,
                                                0,
                                                0,
                                                255
                                               );

        }
        // ************************************************************************************ DrawVictor


    } // Class
} // namespace
