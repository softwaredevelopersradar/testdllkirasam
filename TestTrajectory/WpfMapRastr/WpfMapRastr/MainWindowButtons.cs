﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

using fDRM;


namespace WpfMapRastr
{
    public partial class MainWindow
    {
        // ButtonsModes ****************************************************************************
        private void RadioButton1_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.ModeTest = true;
            //GlobalVarMapMain.ModeEmulator = false;
            //GlobalVarMapMain.ModeReal = false;

            PanelTest.IsEnabled = true;
            //PanelReal.IsEnabled = false;
            //PanelEmulator.IsEnabled = false;
        }

        private void RadioButton2_Checked(object sender, RoutedEventArgs e)
        {

            GlobalVarMapMain.ModeTest = false;
            GlobalVarMapMain.ModeEmulator = true;
            GlobalVarMapMain.ModeReal = false;

            PanelTest.IsEnabled = false;
            //PanelReal.IsEnabled = false;
            PanelEmulator.IsEnabled = true;

        }

        private void RadioButton3_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.ModeTest = false;
            GlobalVarMapMain.ModeEmulator = false;
            GlobalVarMapMain.ModeReal = true;

            PanelTest.IsEnabled = false;
            //PanelReal.IsEnabled = true;
            PanelEmulator.IsEnabled = false;

        }
        // **************************************************************************** ButtonsModes

        // NumberTraaj ****************************************************************************
        private void RadioButton1_1_Checked(object sender, RoutedEventArgs e)
        {
            //GlobalVarMapMain.ModeOneTraj = true;
            GlobalVarMapMain.ModeSeverTraj = false;
        }

        private void RadioButton2_1_Checked(object sender, RoutedEventArgs e)
        {
            //GlobalVarMapMain.ModeOneTraj = false;
            GlobalVarMapMain.ModeSeverTraj = true;
        }
        // **************************************************************************** NumberTraj

        // checkedBoxALL_Checked ********************************************************************************
/*
        private void checkBoxALL_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flALL = true;
            Draw();
        }
*/
        // ******************************************************************************** checkedBoxALL_Checked

        // checkedBoxALL_Unchecked ******************************************************************************
/*
        private void checkBoxALL_Unchecked(object sender, RoutedEventArgs e)
        {

            GlobalVarMapMain.flALL = false;
            Draw();
        }
*/
        // ****************************************************************************** checkedBoxALL_Unchecked

        // checkedBoxAeroscope_Checked **************************************************************************
        private void checkBoxAeroscope_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flAeroscope = true;
            Draw();
        }
        // ************************************************************************** checkedBoxAeroscope_Checked

        // checkedBoxAeroscope_Unchecked ************************************************************************

        private void checkBoxAeroscope_Unchecked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flAeroscope = false;
            Draw();
        }
        // ************************************************************************ checkedBoxAeroscope_Unchecked

        // checkedBoxRDM_Checked ********************************************************************************
        private void checkBoxRDM_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flRDM = true;
            Draw();
        }
        // ******************************************************************************** checkedBoxRDM_Checked

        // checkedBoxRDM_Unchecked ******************************************************************************

        private void checkBoxRDM_Unchecked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flRDM = false;
            Draw();
        }
        // ****************************************************************************** checkedBoxRDM_Unchecked

        // checkedBoxFilter_Checked *****************************************************************************
        private void checkBoxFilter_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flFilter = true;
            Draw();
        }
        // ***************************************************************************** checkedBoxFilter_Checked

        // checkedBoxFilter_Unchecked ***************************************************************************

        private void checkBoxFilter_Unchecked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flFilter = false;
            Draw();
        }
        // ************************************************************************ checkedBoxFilter_Unchecked

        // checkedBoxWorkFile_Checked ************************************************************************
        private void checkBoxWorkFile_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flWorkFile = true;
            checkBoxReal.IsEnabled = false;

            ClearAll();
        }
        // ************************************************************************* checkedBoxWorkFile_Checked

        // checkedBoxWorkFile_Unchecked ***********************************************************************

        private void checkBoxWorkFile_Unchecked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flWorkFile = false;
            checkBoxReal.IsEnabled = true;

            ClearAll();

        }
        // ******************************************************************** checkedBoxWorkFile_Unchecked

        // checkedBoxReal_Checked **************************************************************************
        private void checkBoxReal_Checked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flReal = true;
            checkBoxWorkFile.IsEnabled = false;

            ClearAll();

        }
        // ************************************************************************** checkedBoxReal_Checked

        // checkedBoxReal_Unchecked ************************************************************************

        private void checkBoxReal_Unchecked(object sender, RoutedEventArgs e)
        {
            GlobalVarMapMain.flReal = false;
            checkBoxWorkFile.IsEnabled = true;

            ClearAll();

        }
        // ********************************************************************* checkedBoxReal_Unchecked

        // Clear *********************************************************************************************
        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {

            //GlobalVarMapMain.objClassPointt.Clear();
            //GlobalVarMapMain.list_tracks_emulRDM.Clear();

            //GlobalVarMapMain.objClassPointt1.Clear();
            //GlobalVarMapMain.list_tracks_emulRDM1.Clear();

            // ModeTest &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            if (GlobalVarMapMain.ModeTest == true)
            {
                ClearAll();
            }
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ModeTest

            // ModeEmulator &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            else if (GlobalVarMapMain.ModeEmulator == true)
            {
                // aero
                GlobalVarMapMain.objClassPointt.Clear();
                // rdm
                GlobalVarMapMain.list_tracks_emulRDM.Clear();

                GlobalVarMapMain.lsttmp_stations.Clear();

                // DelAll >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                mapCtrl.objClassMapRastrReDraw.DelAll();
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DelAll

            }
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ModeEmulator

            // ModeReal &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            else if (GlobalVarMapMain.ModeReal == true)
            {
                GlobalVarMapMain.objClassPointt1.Clear();
                GlobalVarMapMain.list_tracks_emulRDM1.Clear();

                GlobalVarMapMain.lsttmp_stations.Clear();

                // DelAll >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                mapCtrl.objClassMapRastrReDraw.DelAll();
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DelAll

            }
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ModeReal


        }
        // ********************************************************************************************* Clear


        // LoadFiles *****************************************************************************************

        // Aeroscope
        private void ButtonLoadA_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстовый файл. Файл формата  .txt|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                GlobalVarMapMain.DirAeroscope = openFileDialog.FileName;
            }

        }

        // RDM
        private void ButtonLoadRDM_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстовый файл. Файл формата  .yaml|*.yaml";
            if (openFileDialog.ShowDialog() == true)
            {
                GlobalVarMapMain.DirRDM = openFileDialog.FileName;
            }

        }

        // VI
        private void ButtonLoadVI_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстовый файл. Файл формата  .txt|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                GlobalVarMapMain.DirVI = openFileDialog.FileName;
            }

        }

        // LoadFiles *****************************************************************************************




        // Button: Load File Aeroscope ***********************************************************************
        // Загрузка файла эмулятора аэроскопа

        private void Button3D_Click(object sender, RoutedEventArgs e)
        {

            //string strFile = "AeroScopeSamples";
            //LoadAeroScopeEmulator(strFile);


            double lt = 0;
            double lng = 0;
            double hh = 0;
            double XX = 0;
            double YY = 0;
            double ZZ = 0;

            if (DecSep == '.')
                ip1Box.Text = ip1Box.Text.Replace(',', '.');
            if (DecSep == ',')
                ip1Box.Text = ip1Box.Text.Replace('.', ',');
            lt = Convert.ToDouble(ip1Box.Text);

            if (DecSep == '.')
                Port1Box.Text = Port1Box.Text.Replace(',', '.');
            if (DecSep == ',')
                Port1Box.Text = Port1Box.Text.Replace('.', ',');
            lng = Convert.ToDouble(Port1Box.Text);

            if (DecSep == '.')
                ip1Box_1.Text = ip1Box_1.Text.Replace(',', '.');
            if (DecSep == ',')
                ip1Box_1.Text = ip1Box_1.Text.Replace('.', ',');
            hh = Convert.ToDouble(ip1Box_1.Text);


            MarkCoord _MarkCoord = new MarkCoord(
                                                    lt,
                                                    lng,
                                                    hh,
                                                    GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Latitude,
                                                    GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Longitude,
                                                    GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Altitude
                                                 );

            XX = _MarkCoord.X;
            YY = _MarkCoord.Y;
            ZZ = _MarkCoord.Z;

            var matrixCtime = new double[1, 3];
            matrixCtime = Calc_dt_time(XX, YY, ZZ);

            ip2Box.Text = Convert.ToString(matrixCtime[0,0]*1E09);
            Port2Box.Text = Convert.ToString(matrixCtime[0, 1] * 1E09);
            ip2Box_1.Text = Convert.ToString(matrixCtime[0, 2] * 1E09);


        }

        private void LoadAeroScopeEmulator(string name)
        {

            string temp = "";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстовый файл. Файл формата  .txt|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                temp = openFileDialog.FileName;
            }

            aeroScopes = AeroScopeEmulator.Emulator.GetSamples(temp);

            fl1Box.Text = temp;
        }

        // *********************************************************************** Button: Load File Aeroscope

        // Button: Load File RDM ***********************************************************************
        // Загрузка файла эмулятора аэроскопа

        private void Button3D1_Click(object sender, RoutedEventArgs e)
        {
            //string strFile = "AeroRDMSamples";
            //LoadRDMEmulator(strFile);


            double dt1 = 0;
            double dt2 = 0;
            double dt3 = 0;

            if (DecSep == '.')
                ip1Box.Text = ip1Box.Text.Replace(',', '.');
            if (DecSep == ',')
                ip1Box.Text = ip1Box.Text.Replace('.', ',');
            dt1 = Convert.ToDouble(ip1Box.Text);

            if (DecSep == '.')
                Port1Box.Text = Port1Box.Text.Replace(',', '.');
            if (DecSep == ',')
                Port1Box.Text = Port1Box.Text.Replace('.', ',');
            dt2 = Convert.ToDouble(Port1Box.Text);

            if (DecSep == '.')
                ip1Box_1.Text = ip1Box_1.Text.Replace(',', '.');
            if (DecSep == ',')
                ip1Box_1.Text = ip1Box_1.Text.Replace('.', ',');
            dt3 = Convert.ToDouble(ip1Box_1.Text);


            List<ClassObject> LstClassObject1 = new List<ClassObject>();


            // SP1 (base)
            ClassObject objClassObject1 = new ClassObject();
            objClassObject1.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Latitude;
            objClassObject1.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Longitude;
            objClassObject1.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[0].Position.Altitude;
            objClassObject1.tau = 0;
            objClassObject1.BaseStation = true;
            objClassObject1.IndexStation = 1;
            LstClassObject1.Add(objClassObject1);


            // FIN1
            // SP2
            ClassObject objClassObject2 = new ClassObject();
            objClassObject2.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Latitude;
            objClassObject2.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Longitude;
            objClassObject2.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[1].Position.Altitude;

            objClassObject2.tau = dt1*1E-09;
            //objClassObject2.tau = 2.08E-07;

            objClassObject2.BaseStation = false;
            objClassObject2.IndexStation = 2;
            LstClassObject1.Add(objClassObject2);

            // SP3
            ClassObject objClassObject3 = new ClassObject();
            objClassObject3.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Latitude;
            objClassObject3.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Longitude;
            objClassObject3.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[2].Position.Altitude;

            objClassObject3.tau = dt2*1E-09;
            //objClassObject3.tau = -1.1004E-07;

            objClassObject3.BaseStation = false;
            objClassObject3.IndexStation = 3;
            LstClassObject1.Add(objClassObject3);

            // SP4
            ClassObject objClassObject4 = new ClassObject();
            objClassObject4.Latitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Latitude;
            objClassObject4.Longitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Longitude;
            objClassObject4.Altitude = GlobalVarMapMain.objTracksDefinition.listStations[3].Position.Altitude;

            objClassObject4.tau = dt3*1E-09;
            //objClassObject4.tau = -8.0E-08;

            objClassObject4.BaseStation = false;
            objClassObject4.IndexStation = 4;
            LstClassObject1.Add(objClassObject4);

            //LstClassObject1[1].tau = 88/1000000000;
            //LstClassObject1[2].tau = -88 / 1000000000;
            //LstClassObject1[3].tau = -380 / 1000000000;

            // .................................................... Входной лист для подачи в функцию

            // Расчет координат по задержкам ........................................................

            ClassDRM_dll objClassDRM_dll = new ClassDRM_dll();
            ClassObjectTmp ObjClassObjectTmp1 = new ClassObjectTmp();
            ObjClassObjectTmp1 = objClassDRM_dll.f_DRM(
                                                      LstClassObject1,
                                                      GlobalVarMapMain.ObjClassParametersGlobal.flMnimPoints,
                                                      GlobalVarMapMain.objTracksDefinition.HInput,
                                                      true,
                                                      true,
                                                      true
                                                     //ref GlobalVarMapMain.sh1RDM ,
                                                     //ref GlobalVarMapMain.sh2RDM
                                                     );


            ip2Box.Text = Convert.ToString(ObjClassObjectTmp1.Latitude);
            Port2Box.Text = Convert.ToString(ObjClassObjectTmp1.Longitude);
            ip2Box_1.Text = Convert.ToString(ObjClassObjectTmp1.Altitude);




        }

        private void LoadRDMEmulator(string name)
        {
            string temp = "";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстовый файл. Файл формата  .txt|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                temp = openFileDialog.FileName;
            }
            rdmList = AeroScopeEmulator.Emulator.GetSamples(temp);
            fl2Box.Text = temp;

        }

        // *********************************************************************** Button: Load File RDM

        // Button: 3D ****************************************************************************************
        private void ButtonUDP_Click(object sender, RoutedEventArgs e)
        {

            //string addr = "127.0.0.1";
            string addr = ip1Box.Text;
            IPAddress address = IPAddress.Parse(addr);
            var myIp = address;

            //var myPort = 15000;
            var myPort = Convert.ToInt32(Port1Box.Text);

            string addr2 = ip1Box_1.Text;
            IPAddress address2 = IPAddress.Parse(addr2);
            var myIp2 = address2;

            var remoteIp2 = address2;

            //var remotePort = 15001;
           // var remotePort = Convert.ToInt32(Port1Box_1.Text);

            byte addrSender = 0;
            byte addrRecipient = 0;

            //uDPReceiver = new UDPReceiver.UDPReceiver(myIp, myPort, remoteIp2, remotePort, addrSender, addrRecipient);

            //GlobalVarMapMain.objClassPointt.Clear();
            //GlobalVarMapMain.list_tracks_emulRDM.Clear();

            uDPReceiver.Connect();

        }
        // **************************************************************************************** Button: 3D

        // Stop *********************************************************************************************
        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            // ModeTest &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            if (GlobalVarMapMain.ModeTest == true)
            {
                GlobalVarMapMain.dispatcherTimer.Stop();
            }
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ModeTest

            // ModeEmulator &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            else if (GlobalVarMapMain.ModeEmulator == true)
            {
                istraj = false;
                istrajRDM = false;
                istrajRequest = false;
                istrajRequestRDM = false;

                // aero
                GlobalVarMapMain.objClassPointt.Clear();
                // rdm
                GlobalVarMapMain.list_tracks_emulRDM.Clear();

                // 3D
                if (uDPReceiver != null)
                    uDPReceiver.Disconnect();

            }
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ModeEmulator

            // ModeReal &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            else if (GlobalVarMapMain.ModeReal == true)
            {
                GlobalVarMapMain.objClassPointt1.Clear();
                GlobalVarMapMain.list_tracks_emulRDM1.Clear();

                if (uDPReceiver != null)
                    uDPReceiver.Disconnect();
                if (uDPReceiver2 != null)
                    uDPReceiver2.Disconnect();

            }
            // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ModeReal


        }
        // ********************************************************************************************* Stop

    } // Class
} // Namespace
