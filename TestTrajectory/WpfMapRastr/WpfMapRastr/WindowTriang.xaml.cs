﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfMapRastr
{
    /// <summary>
    /// Interaction logic for WindowTriang.xaml
    /// </summary>
    public partial class WindowTriang : Window
    {
        public WindowTriang()
        {
            InitializeComponent();

            this.Closing += WndTriang_Closing;

        }

        // Closing ************************************************************************
        private void WndTriang_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int ii = 0;

            GlobalVarMapMain.flTriang = false;
            /*
                        GlobalVarMapMain.flPeleng1 = false;
                        GlobalVarMapMain.flPeleng2 = false;

                        for (ii = GlobalVarMapMain.listPeleng1.Count - 1; ii >= 0; ii--)
                        {
                            GlobalVarMapMain.listPeleng1.RemoveAt(ii);
                        }
                        for (ii = GlobalVarMapMain.listPeleng2.Count - 1; ii >= 0; ii--)
                        {
                            GlobalVarMapMain.listPeleng2.RemoveAt(ii);
                        }
            */

            // Hide (спрятать) window, but NOT delete
            e.Cancel = true;
            Hide();

        }
        // ************************************************************************ Closing

    } // Class
} // Namespace
