﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastr
{
    public class Mark 
    {
        public int ID { get; set; }
        public double Time { get; set; }
        // Real Coordinates
        public TimeMarkCoord CoordReal { get; set; }
        // Coordinates from filter
        public TimeMarkCoord CoordFilter { get; set; }
        // Extrapolated coordinates
        public TimeMarkCoord CoordExtr { get; set; }
        // Lat,long,H
        public TimeMarkCoord CoordPaint { get; set; }
        // Strobe
        public double Strobe { get; set; }
        // Point skip sign
        public bool Skip { get; set; }

        // Air object
        //public double Latitude { get; set; } // WGS84, degree
        //public double Longitude { get; set; } // WGS84, degree
        //public double Altitude { get; set; } // m

        // Конструктор *********************************************************************
        public Mark()
        {

        }
        // ********************************************************************* Конструктор



    }  // Class
} // Namespace
