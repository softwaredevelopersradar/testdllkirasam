﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

namespace WpfMapRastr
{

    // ClearAll+FILTER
    public partial class MainWindow 
    {


        // ClearAll ************************************************************************************
        // Для тестового режима

        public void ClearAll()
        {
            GlobalVarMapMain.lsttmp_stations.Clear();

            GlobalVarMapMain.lsttmp_aeroscope.Clear();
            GlobalVarMapMain.lsttmp_aeroscope_1.Clear();
            GlobalVarMapMain.lsttmp_dubl.Clear();
            GlobalVarMapMain.lsttr.Clear();
            GlobalVarMapMain.lsttmp1.Clear();
            GlobalVarMapMain.lsttmp.Clear();
            GlobalVarMapMain.list_tracks.Clear();
            GlobalVarMapMain.dispatcherTimer.Stop();


            // DelAll >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            mapCtrl.objClassMapRastrReDraw.DelAll();
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DelAll

        }
        // ************************************************************************************* ClearAll


        // FILTER ***************************************************************************************
        // Фильтрование данных RDM

        public void f_Filter(
                              // Отметка
                              ref ClassTrackPoint objClassTrackPoint,
                              // Лист сформированных траекторий
                              ref List<ClassTraj> list_tracks_emulRDMPP
                             )
        {
            // ------------------------------------------------------------------------------------------
            // Base RLS 

            // rad
            GlobalVarMapMain.latRLS_rad = GlobalVarMapMain.latRLS * Math.PI / 180;
            GlobalVarMapMain.longRLS_rad = GlobalVarMapMain.longRLS * Math.PI / 180;

            // Координаты RLS в ГЦСК
            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              GlobalVarMapMain.latRLS,
                                              GlobalVarMapMain.longRLS,
                                              GlobalVarMapMain.HRLS,
                                              ref GlobalVarMapMain.XRLS,
                                              ref GlobalVarMapMain.YRLS,
                                              ref GlobalVarMapMain.ZRLS
                                              );
            // ------------------------------------------------------------------------------------------
            // Отметка

            int i1_1 = 0;
            int i2_1 = 0;
            double XIRI = 0;
            double YIRI = 0;
            double ZIRI = 0;

            // Координаты IRI в ГЦСК
            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              objClassTrackPoint.Latitude,
                                              objClassTrackPoint.Longitude,
                                              objClassTrackPoint.Altitude,
                                              ref XIRI,
                                              ref YIRI,
                                              ref ZIRI
                                              );

            // #@#
            // Координаты IRI в МЗСК
            /*
                        ClassGeoCalculator.Geoc_MZSK(
                                                     GlobalVarMapMain.latRLS_rad,
                                                     GlobalVarMapMain.longRLS_rad,
                                                     GlobalVarMapMain.XRLS,
                                                     GlobalVarMapMain.YRLS,
                                                     GlobalVarMapMain.ZRLS,
                                                     XIRI, // геоц
                                                     YIRI,
                                                     ZIRI,
                                                     ref objClassTrackPoint.X,
                                                     ref objClassTrackPoint.Y,
                                                     ref objClassTrackPoint.Z
                                                    );
            */
            ClassGeoCalculator.Geoc_MZSK_V(
                                         GlobalVarMapMain.latRLS_rad,
                                         GlobalVarMapMain.longRLS_rad,
                                         GlobalVarMapMain.XRLS,
                                         GlobalVarMapMain.YRLS,
                                         GlobalVarMapMain.ZRLS,
                                         XIRI, // геоц
                                         YIRI,
                                         ZIRI,
                                         ref objClassTrackPoint.X,
                                         ref objClassTrackPoint.Y,
                                         ref objClassTrackPoint.Z
                                        );

            // --------------------------------------------------------------------------------
            // Подаем отметку на обработку


            ClassTrajectory objClassTrajectory2 = new ClassTrajectory();
            objClassTrajectory2.f_Trajectory1(
                                  // Отметка
                                  objClassTrackPoint,
                                             /*
                                                                               // Максимально допустимое время ожидания отметки
                                                                               GlobalVarMapMain.twaitPoint,
                                                                               // Допустимая скорость
                                                                               GlobalVarMapMain.V_Dop,
                                                                               // СКО ускорения на плоскости
                                                                               GlobalVarMapMain.CKOA_Pl,
                                                                               // СКО ускорения по высоте
                                                                               GlobalVarMapMain.CKOA_H,

                                                                               GlobalVarMapMain.CKO_X,
                                                                               GlobalVarMapMain.CKO_Y,
                                                                               GlobalVarMapMain.CKO_Z,
                                                                               GlobalVarMapMain.strob,
                                             */
                                  GlobalVarMapMain.ObjClassParameters,
                                  ref list_tracks_emulRDMPP
                                 );
            // ------------------------------------------------------------------------------------------
        }
        // *************************************************************************************** FILTER


    } // Class
} // Namespace
