﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastr
{
    // Class for point drawing
    public class ClassTrackPointDraw
    {
        public double Latitude = 0;
        public double Longitude = 0;
        public double Altitude = 0;

    } // Class
} // Namespace
