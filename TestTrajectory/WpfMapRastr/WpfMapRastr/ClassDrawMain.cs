﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

namespace WpfMapRastr
{
    // Отрисовка аэроскопа и RDM(отфильтрованных)
    // Отрисовка в тестовом режиме в зависимости от выбора в CheckBox-ах
    // Вывод данных аэроскопа (тестовый режим)
    // Вывод данных RDM (тестовый режим)

    // Тестовый режим
    // Вывод отфильтрованных RDM (всех сразу из файла)
    // Фильтрованные и пересчитанные в долготу, широту

    // Тестовый режим
    // Вывод отфильтрованных RDM (по одной отметке из файла )
    // Фильтрованные и пересчитанные в долготу, широту
    // Для работы в реальном времени

    // Draw stations

    public partial class MainWindow
    {
        // DrawAeroscope+RDM  ***************************************************************************
        // Отрисовка аэроскопа и RDM(отфильтрованных)

        public void FucnPaintMap_Filtr(
                                       List<ClassPointt> objClassPointtPP,
                                       List<ClassTraj> list_tracks_emulRDMPP,
                                       ref double ltvyx,
                                       ref double lngvyx,
                                       ref double altvyx
                                      )
        {
            // ----------------------------------------------------------------------------------
            mapCtrl.objClassMapRastrReDraw.DelAll();
            // ----------------------------------------------------------------------------------
            // aeroscope

            List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();

            double xcntr1 = 0;
            double ycntr1 = 0;
            string s6 = "";

            for (int ii = 0; ii < objClassPointtPP.Count; ii++)
            {
                var p2 = Mercator.FromLonLat(objClassPointtPP[ii].Longitude, objClassPointtPP[ii].Latitude);

                xcntr1 = p2.X;
                ycntr1 = p2.Y;

                pointPel2.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));
            }

            s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                // points.X=X, points.Y=Y -> for Mercator map
                                                pointPel2,
                                                // Color-> red
                                                160,
                                                255,
                                                0,
                                                0
                                               );
            // ----------------------------------------------------------------------------------
            // Filter

            string s6_1 = "";
            int i1 = 0;
            int i2 = 0;
            double xcntr1_1 = 0;
            double ycntr1_1 = 0;

            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double ltt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;
            // ....................................................................................
            // base RLS

            // rad
            GlobalVarMapMain.latRLS_rad = GlobalVarMapMain.latRLS * Math.PI / 180;
            GlobalVarMapMain.longRLS_rad = GlobalVarMapMain.longRLS * Math.PI / 180;

            // Координаты RLS в ГЦСК
            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              GlobalVarMapMain.latRLS,
                                              GlobalVarMapMain.longRLS,
                                              GlobalVarMapMain.HRLS,
                                              ref GlobalVarMapMain.XRLS,
                                              ref GlobalVarMapMain.YRLS,
                                              ref GlobalVarMapMain.ZRLS
                                              );
            // ....................................................................................
            // Trajectories

            for (i1 = 0; i1 < list_tracks_emulRDMPP.Count; i1++)
            {
                List<Mapsui.Geometries.Point> pointPel2_1 = new List<Mapsui.Geometries.Point>();

                for (i2 = 1; i2 < list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I.Count; i2++)

                {
                    // #@#
                    /*
                                        ClassGeoCalculator.MZSK_Geoc(
                                                                  GlobalVarMapMain.latRLS_rad,
                                                                  GlobalVarMapMain.longRLS_rad,
                                                                  GlobalVarMapMain.XRLS,
                                                                  GlobalVarMapMain.YRLS,
                                                                  GlobalVarMapMain.ZRLS,
                                                                  list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I[i2].X,
                                                                  list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I[i2].Y,
                                                                  list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I[i2].Z,
                                                                  ref xxx13,
                                                                  ref yyy13,
                                                                  ref zzz13
                                                                );
                    */
                    ClassGeoCalculator.MZSK_Geoc_V(
                                              GlobalVarMapMain.latRLS_rad,
                                              GlobalVarMapMain.longRLS_rad,
                                              GlobalVarMapMain.XRLS,
                                              GlobalVarMapMain.YRLS,
                                              GlobalVarMapMain.ZRLS,
                                              list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I[i2].X,
                                              list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I[i2].Y,
                                              list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I[i2].Z,
                                              ref xxx13,
                                              ref yyy13,
                                              ref zzz13
                                            );


                    ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13, yyy13, zzz13, ref ltt23, ref lng23, ref hhh23);

                    if (
                       (i2 == list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I.Count - 1) &&
                       (list_tracks_emulRDMPP[i1].listPointsTrajFiltr_I.Count != 0)
                       )
                    {
                        ltvyx = ltt23;
                        lngvyx = lng23;
                        altvyx = hhh23;
                    }

                    var p2_1 = Mercator.FromLonLat(lng23, ltt23);
                    xcntr1_1 = p2_1.X;
                    ycntr1_1 = p2_1.Y;

                    pointPel2_1.Add(new Mapsui.Geometries.Point(xcntr1_1, ycntr1_1));
                } // for2

                s6_1 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                    // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                    // points.X=X, points.Y=Y -> for Mercator map
                                                    pointPel2_1,
                                                    // Color-> Blue
                                                    160,
                                                    0,
                                                    0,
                                                    255
                                                   );
            } // for1
            // ----------------------------------------------------------------------------------

        }
        //  *************************************************************************** DrawAeroscope+RDM

        // DrawMain *************************************************************************************
        // Отрисовка в тестовом режиме в зависимости от выбора в CheckBox-ах
        // *PavelEmulReal

        public void Draw()
        {
            if (GlobalVarMapMain.flWorkFile == true)
            {
                // DelAll >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                mapCtrl.objClassMapRastrReDraw.DelAll();
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DelAll

                // Данные с фильтра Виктора (время, широта, долгота, высота)
                //DrawVictor();

                DrawStations();
                // Draw coordinates
                //DrawCoordinates();


                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                if (GlobalVarMapMain.flAeroscope == true)
                {
                    DrawAeroscope();
                    // От Валентина Ивановича
                    DrawAeroscopeVI();

                }

                if (GlobalVarMapMain.flRDM == true)
                {
                    DrawRDM();

                    // Change 16_12 Black
                    //DrawRDM_Otl();

                }

                if (GlobalVarMapMain.flFilter == true)
                {
                    // !!! Старый головной алгоритм
                    //DrawFilter();

                    // *PavelEmulReal
                    DrawFilterPavel();


                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // *PavelEmulReal

                //DrawFilterPavel();
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            }
        }
        // ************************************************************************************* DrawMain


        // DrawAeroscope *********************************************************************************
        // Вывод данных аэроскопа (тестовый режим)

        public void DrawAeroscope()
        {
            string s6 = "";
            int i1 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

            for (i1 = 0; i1 < GlobalVarMapMain.lsttmp_aeroscope.Count; i1++)
            {
                var p = Mercator.FromLonLat(GlobalVarMapMain.lsttmp_aeroscope[i1].Longitude, GlobalVarMapMain.lsttmp_aeroscope[i1].Latitude);
                xcntr1 = p.X;
                ycntr1 = p.Y;

                pointPel.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));

/*
                // Треугольник для точки
                s6 = mapCtrl.objClassInterfaceMap.DrawImage(
                                         ycntr1,
                                         xcntr1,
                                         // Path to image
                                        // dir + "\\Images\\OtherObject\\" + "SquareGR.png",
                                         dir + "\\Images\\OtherObject\\" + "tr10.png",
                                         // Text
                                         "",
                                         // Scale
                                         //1
                                         0.7
                              );
*/
            
            } // for1

            // Линия для траектории

            s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                // points.X=X, points.Y=Y -> for Mercator map
                                                pointPel,
                                                // Color -> Red
                                                100,
                                                255,
                                                0,
                                                0
                                               );

        }

        // ********************************************************************************* DrawAeroscope


        // DrawAeroscope *********************************************************************************
        // От Валентина Ивановича

        public void DrawAeroscopeVI()
        {
            string s6 = "";
            int i1 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

            for (i1 = 0; i1 < GlobalVarMapMain.lsttmp_aeroscopeVI.Count; i1++)
            {
                var p = Mercator.FromLonLat(GlobalVarMapMain.lsttmp_aeroscopeVI[i1].Longitude, GlobalVarMapMain.lsttmp_aeroscopeVI[i1].Latitude);
                xcntr1 = p.X;
                ycntr1 = p.Y;

                pointPel.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));

                s6 = mapCtrl.objClassInterfaceMap.DrawImage(
                                         ycntr1,
                                         xcntr1,
                                         // Path to image
                                         // dir + "\\Images\\OtherObject\\" + "SquareGR.png",
                                         dir + "\\Images\\OtherObject\\" + "tr11.png",
                                         // Text
                                         "",
                                         // Scale
                                         //1
                                         0.5
                              );


            } // for1

            /*
                        s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                            // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                            // points.X=X, points.Y=Y -> for Mercator map
                                                            pointPel,
                                                            // Color -> Red
                                                            100,
                                                            255,
                                                            0,
                                                            0
                                                           );
            */
        }
        // ********************************************************************************* DrawAeroscope

        // DrawRDM ***************************************************************************************
        // Вывод данных RDM (тестовый режим)

        public void DrawRDM()
        {
            string s6 = "";
            int i1 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            List<Mapsui.Geometries.Point> pointPel1 = new List<Mapsui.Geometries.Point>();

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


            for (i1 = 0; i1 < GlobalVarMapMain.lsttmp_dubl.Count; i1++)
            {
                var p1 = Mercator.FromLonLat(GlobalVarMapMain.lsttmp_dubl[i1].Longitude, GlobalVarMapMain.lsttmp_dubl[i1].Latitude);
                xcntr1 = p1.X;
                ycntr1 = p1.Y;

                pointPel1.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));

                s6 = mapCtrl.objClassInterfaceMap.DrawImage(
                                         ycntr1,
                                         xcntr1,
                                         // Path to image
                                         dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                         // Text
                                         "",
                                         // Scale
                                         0.2
                              );

            } // for1
              /*
                          s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                              // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                              // points.X=X, points.Y=Y -> for Mercator map
                                                              pointPel1,
                                                              // Color -> Green
                                                              100,
                                                              0,
                                                              255,
                                                              0
                                                             );
              */
        }

        // *************************************************************************************** DrawRDM



        // DrawRDM ***************************************************************************************
        // Вывод данных RDM (тестовый режим)
        // Вывод с отладочного файла (который создается моей f_DRM)

        public void DrawRDM_Otl()
        {
            string s6 = "";
            int i1 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            List<Mapsui.Geometries.Point> pointPel1 = new List<Mapsui.Geometries.Point>();

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


            for (i1 = 0; i1 < GlobalVarMapMain.lsttmp_dubl_my.Count; i1++)
            {
                var p1 = Mercator.FromLonLat(GlobalVarMapMain.lsttmp_dubl_my[i1].Longitude, GlobalVarMapMain.lsttmp_dubl_my[i1].Latitude);
                xcntr1 = p1.X;
                ycntr1 = p1.Y;

                pointPel1.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));

                s6 = mapCtrl.objClassInterfaceMap.DrawImage(
                                         ycntr1,
                                         xcntr1,
                                         // Path to image
                                         dir + "\\Images\\OtherObject\\" + "SquareBlsck.png",
                                         // Text
                                         "",
                                         // Scale
                                         0.2
                              );

            } // for1
            /*
                        s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                            // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                            // points.X=X, points.Y=Y -> for Mercator map
                                                            pointPel1,
                                                            // Color -> Green
                                                            100,
                                                            0,
                                                            255,
                                                            0
                                                           );
            */
        }

        // *************************************************************************************** DrawRDM






        // DrawFilter ************************************************************************************
        // Тестовый режим
        // Вывод отфильтрованных RDM (всех сразу из файла)
        // Фильтрованные и пересчитанные в долготу, широту

        public void DrawFilter()
        {
            string s6 = "";
            int i1 = 0;
            int i2 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double ltt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;

            //Random rand = new Random();
            //Random rand1 = new Random();
            //Random rand2 = new Random();
            //int col1 = 0;
            //int col2 = 0;
            //int col3 = 0;
            int indcol = 0;


            indcol = 0;
            for (i1 = 0; i1 < GlobalVarMapMain.lsttr.Count; i1++)
            {
                List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();


                // *&
                for (i2 = 1; i2 < GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I.Count; i2++)
                {
                    // #@#
                    /*
                                        ClassGeoCalculator.MZSK_Geoc(
                                                                  GlobalVarMapMain.latRLS_rad,
                                                                  GlobalVarMapMain.longRLS_rad,
                                                                  GlobalVarMapMain.XRLS,
                                                                  GlobalVarMapMain.YRLS,
                                                                  GlobalVarMapMain.ZRLS,
                                                                  GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].X,
                                                                  GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Y,
                                                                  GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Z,
                                                                  ref xxx13,
                                                                  ref yyy13,
                                                                  ref zzz13
                                                                );
                    */
                    ClassGeoCalculator.MZSK_Geoc_V(
                                              GlobalVarMapMain.latRLS_rad,
                                              GlobalVarMapMain.longRLS_rad,
                                              GlobalVarMapMain.XRLS,
                                              GlobalVarMapMain.YRLS,
                                              GlobalVarMapMain.ZRLS,
                                              GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].X,
                                              GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Y,
                                              GlobalVarMapMain.lsttr[i1].listPointsTrajFiltr_I[i2].Z,
                                              ref xxx13,
                                              ref yyy13,
                                              ref zzz13
                                            );

                    ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13, yyy13, zzz13, ref ltt23, ref lng23, ref hhh23);


                    var p2 = Mercator.FromLonLat(lng23, ltt23);
                    xcntr1 = p2.X;
                    ycntr1 = p2.Y;

                    pointPel2.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));
                } // for2


                // Color
                //col1 = 255;-> сиреневый
                //col2 = 0;
                //col3 = 255;
                //col1 = rand.Next(0, 255);
                //col2 = rand1.Next(0, 255);
                //col3 = rand2.Next(0, 255);
                s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                    // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                    // points.X=X, points.Y=Y -> for Mercator map
                                                    pointPel2,
                                                    // Color-> сиреневый
                                                    100,
                                                    //(byte)col1,
                                                    //(byte)col2,
                                                    //(byte)col3
                                                    GlobalVarMapMain.lstClassColor[indcol].Color1,
                                                    GlobalVarMapMain.lstClassColor[indcol].Color2,
                                                    GlobalVarMapMain.lstClassColor[indcol].Color3

                                                   );

                indcol += 1;
                if (indcol == GlobalVarMapMain.lstClassColor.Count)
                    indcol = 0;

            } // for1

        }
        // ************************************************************************************ DrawFilter

        // DrawFilterPavel *******************************************************************************
        // Тестовый режим
        // Вывод отфильтрованных RDM (всех сразу из файла)
        // Фильтрованные и пересчитанные в долготу, широту

        // *PavelEmulNew

        public void DrawFilterPavel()
        {
            string s6 = "";
            int i1 = 0;
            int i2 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double ltt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;

            int indcol = 0;


            // XYZ RLSBase,m (in geocentric CS)
            double XRLS = 0;
            double YRLS = 0;
            double ZRLS = 0;

            indcol = 1;

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dirK = "";
            dirK = dir + "\\FILES\\" + "Pavel.txt";
            StreamWriter swK = new StreamWriter(dirK, false, System.Text.Encoding.Default);

            // Лист траекторий ВО
            for (i1 = 0; i1 < GlobalVarMapMain.listTrackAirObject.Count; i1++)
            {
                List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();

                // Лист отметок i-й траектории
                for (i2 = 1; i2 < GlobalVarMapMain.listTrackAirObject[i1].Marks.Count; i2++)
                {
                    if (GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].Skip == false)
                    {
                        /*
                                                // base
                                                double latRLS_rad = GlobalVarMapMain.listTrackAirObject[i1].stations[0].Position.Latitude * Math.PI / 180;
                                                double longRLS_rad = GlobalVarMapMain.listTrackAirObject[i1].stations[0].Position.Longitude * Math.PI / 180;

                                                // XYZ RLSBase,m (in geocentric CS)
                                                ClassGeoCalculator.f_BLH_XYZ_84_1
                                                                                 (
                                                                                  GlobalVarMapMain.listTrackAirObject[i1].stations[0].Position.Latitude,
                                                                                  GlobalVarMapMain.listTrackAirObject[i1].stations[0].Position.Longitude,
                                                                                  GlobalVarMapMain.listTrackAirObject[i1].stations[0].Position.Altitude,
                                                                                  ref XRLS,
                                                                                  ref YRLS,
                                                                                  ref ZRLS
                                                                                  );


                                                ClassGeoCalculator.MZSK_Geoc_V(
                                                                         // Base
                                                                         latRLS_rad,
                                                                         longRLS_rad,
                                                                         XRLS, // BaseGeoc
                                                                         YRLS,
                                                                         ZRLS,

                                                                          // Object, MZSK,Filtr
                                                                          GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordFilter.X,
                                                                          GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordFilter.Y,
                                                                          GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordFilter.Z,
                                                                          // Object,Geoc,m
                                                                          ref xxx13,
                                                                          ref yyy13,
                                                                          ref zzz13
                                                                        );



                                                ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13, yyy13, zzz13, ref ltt23, ref lng23, ref hhh23);
                        */

                        //var p2 = Mercator.FromLonLat(lng23, ltt23);
                        var p2 = Mercator.FromLonLat(GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordPaint.Longitude,
                                                     GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordPaint.Latitude);
                        xcntr1 = p2.X;
                        ycntr1 = p2.Y;

                        pointPel2.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));



                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Запись в файл

                        string sfilter = "";

                        double t = 0;
                        double Lat = 0;
                        double Long = 0;
                        double hn = 0;
                        long lLat = 0;
                        long lLong = 0;
                        long lhn = 0;

                        //Llonsec = (long)(dlonsec * 100);
                        //dlonsec = ((double)Llonsec) / 100;


                        lLat = (long)(GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordPaint.Latitude * 1000000);
                        Lat = ((double)lLat) / 1000000;

                        lLong = (long)(GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordPaint.Longitude * 1000000);
                        Long = ((double)lLong) / 1000000;

                        //Lat = GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordPaint.Latitude;
                        //Long = GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordPaint.Longitude;
                        t = GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].Time;

                        lhn = (long)(GlobalVarMapMain.listTrackAirObject[i1].Marks[i2].CoordPaint.Altitude * 10);
                        hn = ((double)lhn) / 10;

                        sfilter = Convert.ToString(t) + " " + Convert.ToString(Lat) + " " +
                                  Convert.ToString(Long) + " " + Convert.ToString(hn);

                        swK.WriteLine(sfilter);
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    }


                } // for2

                s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                    // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                    // points.X=X, points.Y=Y -> for Mercator map
                                                    pointPel2,
                                                    // Color
                                                    100,
                                                    GlobalVarMapMain.lstClassColor[indcol].Color1,
                                                    GlobalVarMapMain.lstClassColor[indcol].Color2,
                                                    GlobalVarMapMain.lstClassColor[indcol].Color3

                                                   );

                indcol += 1;
                if (indcol == GlobalVarMapMain.lstClassColor.Count)
                    indcol = 0;

            } // for1
            swK.Close();

        }
        // ******************************************************************************* DrawFilterPavel

        // DrawFilter1 ***********************************************************************************
        // Тестовый режим
        // Вывод отфильтрованных RDM (по одной отметке из файла )
        // Фильтрованные и пересчитанные в долготу, широту
        // Для работы в реальном времени

        public void DrawFilter1()
        {
            string s6 = "";
            int i1 = 0;
            int i2 = 0;
            double xcntr1 = 0;
            double ycntr1 = 0;

            double xxx13 = 0;
            double yyy13 = 0;
            double zzz13 = 0;
            double ltt23 = 0;
            double lng23 = 0;
            double hhh23 = 0;

            for (i1 = 0; i1 < GlobalVarMapMain.list_tracks.Count; i1++)
            {
                List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();

                for (i2 = 1; i2 < GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I.Count; i2++)
                {
                    // #@#
                    /*
                                        ClassGeoCalculator.MZSK_Geoc(
                                                                  GlobalVarMapMain.latRLS_rad,
                                                                  GlobalVarMapMain.longRLS_rad,
                                                                  GlobalVarMapMain.XRLS,
                                                                  GlobalVarMapMain.YRLS,
                                                                  GlobalVarMapMain.ZRLS,
                                                                  GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].X,
                                                                  GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].Y,
                                                                  GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].Z,
                                                                  ref xxx13,
                                                                  ref yyy13,
                                                                  ref zzz13
                                                                );
                    */

                    ClassGeoCalculator.MZSK_Geoc_V(
                                              GlobalVarMapMain.latRLS_rad,
                                              GlobalVarMapMain.longRLS_rad,
                                              GlobalVarMapMain.XRLS,
                                              GlobalVarMapMain.YRLS,
                                              GlobalVarMapMain.ZRLS,
                                              GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].X,
                                              GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].Y,
                                              GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].Z,
                                              ref xxx13,
                                              ref yyy13,
                                              ref zzz13
                                            );

                    ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13, yyy13, zzz13, ref ltt23, ref lng23, ref hhh23);

                    var p2 = Mercator.FromLonLat(lng23, ltt23);
                    xcntr1 = p2.X;
                    ycntr1 = p2.Y;

                    pointPel2.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));
                } // for2

                s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                    // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                    // points.X=X, points.Y=Y -> for Mercator map
                                                    pointPel2,
                                                    // Color-> сиреневый
                                                    100,
                                                    255,
                                                    0,
                                                    255
                                                   );
            } // for1

            /*
                        // FOR1_1
                        for (i1 = 0; i1 < GlobalVarMapMain.list_tracks.Count; i1++)
                        {
                            List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();

                            double xstr = 0;
                            double ystr = 0;
                            double zstr = 0;
                            double str = 0;

                            //str= Math.Sqrt((GlobalVarMapMain.XRLS-) +);


                            for (i2 = 1; i2 < 10; i2++)
                            {
                                ClassGeoCalculator.MZSK_Geoc(
                                                          GlobalVarMapMain.latRLS_rad,
                                                          GlobalVarMapMain.longRLS_rad,
                                                          GlobalVarMapMain.XRLS,
                                                          GlobalVarMapMain.YRLS,
                                                          GlobalVarMapMain.ZRLS,
                                                          GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].X,
                                                          GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].Y,
                                                          GlobalVarMapMain.list_tracks[i1].listPointsTrajFiltr_I[i2].Z,
                                                          ref xxx13,
                                                          ref yyy13,
                                                          ref zzz13
                                                        );

                                ClassGeoCalculator.f_XYZ_BLH_84_1(xxx13, yyy13, zzz13, ref ltt23, ref lng23, ref hhh23);

                                var p2 = Mercator.FromLonLat(lng23, ltt23);
                                xcntr1 = p2.X;
                                ycntr1 = p2.Y;

                                pointPel2.Add(new Mapsui.Geometries.Point(xcntr1, ycntr1));
                            } // for2

                            s6 = mapCtrl.objClassInterfaceMap.DrawLinesLatLong(
                                                                // Draw Lines: points.X=Long, points.Y=Lat -> degree(for geographic map)
                                                                // points.X=X, points.Y=Y -> for Mercator map
                                                                pointPel2,
                                                                // Color-> сиреневый
                                                                100,
                                                                255,
                                                                0,
                                                                255
                                                               );


                        } // for1
            */



        }
        // *********************************************************************************** DrawFilter1

        // DrawStations **********************************************************************************

        public void DrawStations()
        {
            string s = "";
            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            for (int i = 0; i < GlobalVarMapMain.lsttmp_stations.Count; i++)
            {
                // .............................................................................................
                double xcntr = 0;
                double ycntr = 0;
                var p = Mercator.FromLonLat(GlobalVarMapMain.lsttmp_stations[i].Longitude, GlobalVarMapMain.lsttmp_stations[i].Latitude);
                xcntr = p.X;
                ycntr = p.Y;

                s = mapCtrl.objClassInterfaceMap.DrawImage(
                                         ycntr,
                                         xcntr,
                                         // Path to image
                                         dir + "\\Images\\Jammer\\" + "1.png",
                                         // Text
                                         //"",
                                         Convert.ToString(i + 1),
                                         // Scale
                                         2

                              );

                // .............................................................................................

            } // for
              // -------------------------------------------------------------------------------------------------


        }
        // ********************************************************************************** DrawStations

        // DrawStations **********************************************************************************
        // UBR1
        // Для отрисовки других объектов

/*
        public void DrawStations1()
        {
            string s = "";
            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            for (int i = 0; i < GlobalVarMapMain.lsttmp_stations1.Count; i++)
            {
                // .............................................................................................
                double xcntr = 0;
                double ycntr = 0;
                var p = Mercator.FromLonLat(GlobalVarMapMain.lsttmp_stations1[i].Longitude, GlobalVarMapMain.lsttmp_stations1[i].Latitude);
                xcntr = p.X;
                ycntr = p.Y;

                s = mapCtrl.objClassInterfaceMap.DrawImage(
                                         ycntr,
                                         xcntr,
                                         // Path to image
                                         dir + "\\Images\\Jammer\\" + "5.png",
                                         // Text
                                         //"",
                                         Convert.ToString(i + 1),
                                         // Scale
                                         2

                              );

                // .............................................................................................

            } // for
              // -------------------------------------------------------------------------------------------------


        }
        // ********************************************************************************** DrawStations
*/


    } // Class
} // Namespace
