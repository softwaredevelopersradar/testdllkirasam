﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;

using InheritorsEventArgs;
using ClassLibraryIniFiles;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using Bearing;
using GeoCalculator;

namespace WpfMapRastr
{
    // Class: Coordinates of mark
    public class MarkCoord: Mark
    {

        // Air object
        public double Latitude  { get; set; } // WGS84, degree
        public double Longitude { get; set; } // WGS84, degree
        public double Altitude  { get; set; } // m

        // Base RLS 
        public double LatitudeRLSBase { get; set; } // WGS84, degree
        public double LongitudeRLSBase { get; set; } // WGS84, degree
        public double AltitudeRLSBase { get; set; } // m

        public List<Station> stations = new List<Station>();

        private double _x; // m
        private double _y; // m
        private double _z; // m

        // Local earth coordinate system, m
        public double X { get { return _x; } set { _x = value; } }
        public double Y { get { return _y; } set { _y = value; } }
        public double Z { get { return _z; } set { _z = value; } }

        public double VX { get; set; }
        public double VY { get; set; }
        public double VZ { get; set; }


        // Конструктор *********************************************************************
        public MarkCoord(
                         // Air object
                         double Lat,  // WGS84
                         double Long, // WGS84
                         double Alt,

                         // Base RLS                         
                         //double LatRLSBase,
                         //double LongRLSBase,
                         //double AltRLSBase// m

                         List<Station> _stations

                         )
        {
            
            // Air object
            Latitude = Lat;
            Longitude = Long;
            Altitude = Alt;

            stations = _stations;

            LatitudeRLSBase = stations[0].Position.Latitude;
            LongitudeRLSBase = stations[0].Position.Longitude;
            AltitudeRLSBase = stations[0].Position.Altitude;

           ConvertWGS84ToDecart();

        }

        public MarkCoord(
                         // Air object
                         double Lat,  // WGS84
                         double Long, // WGS84
                         double Alt,

                         // Base RLS                         
                         double LatRLSBase,
                         double LongRLSBase,
                         double AltRLSBase// m

                         )
        {

            // Air object
            Latitude = Lat;
            Longitude = Long;
            Altitude = Alt;

            //stations = _stations;

            LatitudeRLSBase = LatRLSBase;
            LongitudeRLSBase = LongRLSBase;
            AltitudeRLSBase = AltRLSBase;

            ConvertWGS84ToDecart();

        }

        // ********************************************************************* Конструктор

        // WGS84->XYZ **********************************************************************

        private void ConvertWGS84ToDecart()
         {

            //double LatitudeRLSBase = stations[0].Position.Latitude;
            //double LongitudeRLSBase = stations[0].Position.Longitude;
            //double AltitudeRLSBase = stations[0].Position.Altitude;

            // ............................................................................
            // Lat,Long,H RLSBase in rad
            double latRLS_rad = 0;
            double longRLS_rad = 0;

            // XYZ RLSBase,m (in geocentric CS)
            double XRLS = 0;
            double YRLS = 0;
            double ZRLS = 0;

            // XYZ air object,m (in geocentric CS)
            double XIRI = 0;
            double YIRI = 0;
            double ZIRI = 0;

            double XZSK = 0;
            double YZSK = 0;
            double ZZSK = 0;

            // ..............................................................................
            // RLSBase,rad

            latRLS_rad = LatitudeRLSBase * Math.PI / 180;
            longRLS_rad = LongitudeRLSBase * Math.PI / 180;
            // ..............................................................................
            // XYZ RLSBase,m (in geocentric CS)

            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              LatitudeRLSBase,
                                              LongitudeRLSBase,
                                              AltitudeRLSBase,
                                              ref XRLS,
                                              ref YRLS,
                                              ref ZRLS
                                              );

            // ..............................................................................
            // Coordinates of air object  in geocentric CS

            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              Latitude,  // Air objectWGS84
                                              Longitude,
                                              Altitude,

                                              ref XIRI,
                                              ref YIRI,
                                              ref ZIRI
                                              );
            // ..............................................................................
            // XYZ air object,m (in local earth CS)

            ClassGeoCalculator.Geoc_MZSK_V(
                                         // Base RLS
                                         latRLS_rad,  // WGS84
                                         longRLS_rad,
                                         XRLS,        // geoc
                                         YRLS,
                                         ZRLS,

                                         // Air object
                                         XIRI,
                                         YIRI,
                                         ZIRI,
                                         ref _x,
                                         ref _y,
                                         ref _z
                                        );
            // ..............................................................................

        }
        // ********************************************************************** WGS84->XYZ

    } // Class
} // Namespace
