﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ClassLibraryTrajectory;

using ModelsTablesDBLib;
using ClientDataBase;

using fDRM;

namespace WpfMapRastr
{
    class GlobalVarMapMain
    {

        // Form ********************************************************************************
        // Объекты форм для использования в других формах (Инициализируются в Main)

        public static MainWindow objMainWindowG;
        public static WindowAzimuth objWindowAzimuthG;
        public static WindowTriang objWindowTriangG;

        // ******************************************************************************** Form

        // MAP *********************************************************************************
        // !!! Сюда заносятся координаты клика мыши на карте (или при движении мыши)

        // Mercator
        public static double X_Rastr = 0;
        public static double Y_Rastr = 0;
        public static string Str_Rastr = "";

        public static double LAT_Rastr = 0;
        public static double LONG_Rastr = 0;
        public static double H_Rastr = 0;

        // Для оценки изменения местоположения
        public static double deltaS = 250; // m
        // ********************************************************************************* MAP

        // From YAML/PropertyGrid **************************************************************

        //public static string IP_DB = "";
        //public static int NumbPort_DB = 0;
        // 0 - WGS84degree
        // 1 - SK42degree
        // 2 - SK42_XY
        // 3 - Mercator
        //public static byte SysCoord = 0;
        // 0 - DD.dddddd
        // 1 - DD MM.mmmmmm
        // 2-  DD MM SS.ssssss
        //public static byte FormatCoord = 0;
        // ************************************************************** From YAML/PropertyGrid

        // JS **********************************************************************************

        //public static List<TableASP> listJS = new List<TableASP>();

        // ********************************************************************************** JS

        // Azimuth ******************************************************************************
        //public static List<double> listAzimuth = new List<double>();
        //public static bool flAzimuth = false;
        //public static bool CheckShowAzimuth = false;
        // ****************************************************************************** Azimuth

        // Triang ******************************************************************************
        public static List<double> listPeleng1 = new List<double>();
        public static List<double> listPeleng2 = new List<double>();

        public static double LatPel1 = 0;
        public static double LonPel1 = 0;
        public static double LatPel2 = 0;
        public static double LonPel2 = 0;
        public static double Pel1Grad = 0;
        public static double Pel2Grad = 0;

        public static bool flTriang = false;
        public static bool flPeleng1 = false;
        public static bool flPeleng2 = false;

        public static double distance = 100000; // m distance of calculation
        public static uint numberofdots = 1000;  // number of points
        public static double distance_pel = 100000; // m bearing visibility

        public static double LatTriang = 0;
        public static double LonTriang = 0;

        // ****************************************************************************** Triang

        // AirPlanes ***************************************************************************
        public static List<TempADSB> listTableADSB = new List<TempADSB>();
        public static int fl_AirPlane = 0;
        public static int numbtr = 50;
        public static double Smax_traj = 50000; // 50km
        public static int fltmr_air = 0;
        public static System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public static double ltmin_air = 50.757331;
        public static double ltmax_air = 56.5699;
        public static double lnmin_air = 22.497829;
        public static double lnmax_air = 33.71487;

        public static bool fltbl = false;

        //&&&
        public static bool flairone = false;
        public static string ICAO_LAST = "";

        public static List<string> sICAO = new List<string>();

        // *************************************************************************** AirPlanes

        // Flags ********************************************************************************
        public static bool ButtonSettingsOn = true;

        // ******************************************************************************** Flags

        //  GNSS ********************************************************************************
        public static double GNSS_Lat = -1;
        public static double GNSS_Long = -1;
        public static string GNSS_time = "";
        public static int GNSS_Numb_Sat = 0;
        //  ******************************************************************************** GNSS

        // Otl **********************************************************************************

        // Belarus
        public static int lat1_temp1 = 5386500;
        public static int lat2_temp1 = 5397332;
        public static int lon1_temp1 = 2747061;
        public static int lon2_temp1 = 2768947;


/*
        // Emirat
        public static int lat1_temp1 = 2163350;
        public static int lat2_temp1 = 2337590;
        public static int lon1_temp1 = 5162403;
        public static int lon2_temp1 = 5707373;
*/
        public static int sh1=0;
        // ********************************************************************************** Otl

        // Trajectory ****************************************************************************
        // KIRASA

        // Общий лист отметок - это вход DLL
        public static List<ClassTrackPoint> lstTrajectory = new List<ClassTrackPoint>();
        //public static List<List<ClassTrackPoint>> lstTracks = new List<List<ClassTrackPoint>>();
        // Лист сформированных траекторий - это выход DLL
        public static List<ClassTraj> lstTracks = new List<ClassTraj>();

        // Время ожидания отметки
        public static double twaitPoint=0;
        // Допустимая скорость
        public static double V_Dop=0;
        // СКО ускорений по плоскостным координатам
        public static double CKOA_Pl = 0;
        // СКО ускорений по H
        public static double CKOA_H = 0;

        // СКО по координатам
        public static double CKO_X = 0;
        public static double CKO_Y = 0;
        public static double CKO_Z = 0;

        public static int numbp = 0;

        public static double strob = 0;

        // Ошибки измерения разности времени приема сигналов
        public static double Error_dt = 0;
        //public static double Error_dt2 = 0;
        //public static double Error_dt3 = 0;

        // Начальный индекс чтения файла
        public static int indstart = 0;
        // Конечный индекс чтения файла
        public static int indstop = 0;

        public static bool flALL = false;
        public static bool flAeroscope = false;
        public static bool flRDM = false;
        public static bool flFilter = false;

        // Данные с аэроскопа
        public static List<ClassTrackPoint> lsttmp_aeroscope = new List<ClassTrackPoint>();

        public static List<ClassTrackPoint> lsttmp_aeroscopeVI = new List<ClassTrackPoint>();


        // Для расчета временных задержек
        public static List<ClassTrackPoint> lsttmp_aeroscope_time = new List<ClassTrackPoint>();
        // Данные с фильтра Виктора (время, широта, долгота, высота)
        public static List<ClassTrackPoint> lsttmp_aeroscope_1 = new List<ClassTrackPoint>();
        // Данные с RDM -> данные RDM для вывода на экран
        public static List<ClassTrackPoint> lsttmp1 = new List<ClassTrackPoint>();
        // !!! Переворачиваем точки наоборот
        public static List<ClassTrackPoint> lsttmp = new List<ClassTrackPoint>();
        // Дубликат данных с RDM
        public static List<ClassTrackPoint> lsttmp_dubl = new List<ClassTrackPoint>();


        // UBR
        // Для отображения 4-х позиций аэроскопа
        public static List<ClassTrackPoint> lsttmp_dubl1 = new List<ClassTrackPoint>();
        public static List<ClassTrackPoint> lsttmp_dubl2 = new List<ClassTrackPoint>();
        public static List<ClassTrackPoint> lsttmp_dubl3 = new List<ClassTrackPoint>();


        // Выходной лист траекторий
        public static List<ClassTraj> lsttr = new List<ClassTraj>();
        // SP
        //public static List<ClassTrackPoint> lsttmp_stations = new List<ClassTrackPoint>();

        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        // RLS

        // BASE
        public static double latRLS = 0;
        public static double longRLS = 0;
        public static double latRLS_rad = 0;
        public static double longRLS_rad = 0;
        public static double HRLS = 0;
        public static double XRLS = 0;
        public static double YRLS = 0;
        public static double ZRLS = 0;
        public static double XRLS_MZSK = 0;
        public static double YRLS_MZSK = 0;
        public static double ZRLS_MZSK = 0;

        // 2
        public static double latRLS_2 = 0;
        public static double longRLS_2 = 0;
        public static double latRLS_rad_2 = 0;
        public static double longRLS_rad_2 = 0;
        public static double HRLS_2 = 0;
        public static double XRLS_2 = 0;
        public static double YRLS_2 = 0;
        public static double ZRLS_2 = 0;
        public static double XRLS_MZSK_2 = 0;
        public static double YRLS_MZSK_2 = 0;
        public static double ZRLS_MZSK_2 = 0;

        // 3
        public static double latRLS_3 = 0;
        public static double longRLS_3 = 0;
        public static double latRLS_rad_3 = 0;
        public static double longRLS_rad_3 = 0;
        public static double HRLS_3 = 0;
        public static double XRLS_3 = 0;
        public static double YRLS_3 = 0;
        public static double ZRLS_3 = 0;
        public static double XRLS_MZSK_3 = 0;
        public static double YRLS_MZSK_3 = 0;
        public static double ZRLS_MZSK_3 = 0;

        // 4
        public static double latRLS_4 = 0;
        public static double longRLS_4 = 0;
        public static double latRLS_rad_4 = 0;
        public static double longRLS_rad_4 = 0;
        public static double HRLS_4 = 0;
        public static double XRLS_4 = 0;
        public static double YRLS_4 = 0;
        public static double ZRLS_4 = 0;
        public static double XRLS_MZSK_4 = 0;
        public static double YRLS_MZSK_4 = 0;
        public static double ZRLS_MZSK_4 = 0;
        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        // Общий счетчик отметок при работе в реальном времени
        public static int shpoints = 0;

        public static List<ClassTraj> list_tracks = new List<ClassTraj>();

        public static bool flWorkFile = false;
        public static bool flReal = false;


        // Эмулятор аэроскопа: вывод из файла по кнопке "Trajectory"
        public static List<ClassPointt> objClassPointt = new List<ClassPointt>();
        // Эмулятор RDM: вывод из файла по кнопке "Trajectory" (файл сформированных траекторий)
        public static List<ClassTraj> list_tracks_emulRDM = new List<ClassTraj>();

        // Полученные данные от аэроскопа при работе в режиме реального времени (Egor)
        // Прием - в прерывании
        public static List<ClassPointt> objClassPointt1 = new List<ClassPointt>();
        // Полученные данные от RDM при работе в режиме реального времени (Egor)
        // Файл сформированных траекторий
        // Прием - в прерывании
        public static List<ClassTraj> list_tracks_emulRDM1 = new List<ClassTraj>();

        // Modes
        public static bool ModeTest = false;
        public static bool ModeEmulator = false;
        public static bool ModeReal = false;

        // Number of trajectories
        //public static bool ModeOneTraj = false;
        // =false -> одна траектория
        // =true -> несколько траекторий
        public static bool ModeSeverTraj = false;

        public static ClassParameters ObjClassParameters = new ClassParameters();

        // Число точек для обработки фильтром
        public static int NumberPointsFilter = 0;

        // Цвета для траекторий
        public static List<ClassColor> lstClassColor = new List<ClassColor>();


        // NEW_TRAJECTORY >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        // SP
        public static List<ClassTrackPointDraw> lsttmp_stations = new List<ClassTrackPointDraw>();

        // UBR1
        // Для отрисовки других объектов
        public static List<ClassTrackPointDraw> lsttmp_stations1 = new List<ClassTrackPointDraw>();



        // Данные с аэроскопа
        public static List<Mark> ListAeroscope = new List<Mark>();
        // Данные с фильтра Виктора
        public static List<Mark> ListVictor = new List<Mark>();
        // Данные РДМ
        public static List<Mark> ListRDM = new List<Mark>();
        // Данные РДМ
        // !!! Переворачиваем точки наоборот -> Это будет лист входных отметок на вход фильтра
        public static List<Mark> ListRDMObr = new List<Mark>();
        // Данные РДМ (дубликат)
        public static List<Mark> ListRDMDubl = new List<Mark>();

        // Лист входных отметок
        public static List<Mark> ListInput = new List<Mark>();

        // ---------------------------------------------------------------------------------------
        // FOR Pavel

        // Class global parameters
        public static ClassParametersGlobal ObjClassParametersGlobal = new ClassParametersGlobal();
        // Class RLS coordinates
        public static ClassParametersRLS ObjClassParametersRLS = new ClassParametersRLS();

        // Лист станций
        public static List<Station> listStations = new List<Station>();

        // Лист трасс источников
        //public static List<TrackSource> listTrackSource = new List<TrackSource>();

        // Список трасс ВО
        public static List<TrackAirObject> listTrackAirObject = new List<TrackAirObject>();


        // Параметры с интерфейса
        public static Parameters ObjParameters = new Parameters();

        // Класс основного алгоритма
        public static TracksDefinition objTracksDefinition;

        // Для DLL fDRM расчета координат по задержкам для сравнения с файлом Павла
        public static List<ClassObject> LstClassObject = new List<ClassObject>();
        public static ClassObjectTmp ObjClassObjectTmp = new ClassObjectTmp();

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NEW_TRAJECTORY


        // LoadFiles >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        public static string GeneralDir = "";
        public static string DirAeroscope = "";
        public static string DirRDM = "";
        public static string DirVI = "";


        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> LoadFiles


        // Otladka ------------------------------------------------------------------------------

        public static int sh3 = 0;

        public static int sh1RDM = 0;
        public static int sh2RDM = 0;

        // Change 16_12
        // Для записи файла после обработки f_DRM в функции TrackDefinition в тесте
        public static string dirwork = "";
        public static string dirKwork = "";
        public static StreamWriter swK;

        // Change 16_12
        public static List<ClassTrackPoint> lsttmp_dubl_my = new List<ClassTrackPoint>();
        // ------------------------------------------------------------------------------ Otladka


        // **************************************************************************** Trajectory


    } // Class
} // NameSpace
