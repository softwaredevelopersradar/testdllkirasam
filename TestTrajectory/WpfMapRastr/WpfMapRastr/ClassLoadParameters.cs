﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

namespace WpfMapRastr
{
    // Class: Load parameters
    public partial class MainWindow
    {
        // LoadParameters *******************************************************************************
        // Ввод входных параметров для алгоритма формирования траекторий

        public void LoadParametersGlobal()
        {
            // Время ожидания отметки источника
            if ((TextBox1.Text.Contains(".")) && (DecSep == ','))
                TextBox1.Text = TextBox1.Text.Replace(".", ",");
            if ((TextBox1.Text.Contains(",")) && (DecSep == '.'))
                TextBox1.Text = TextBox1.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.twait= Convert.ToDouble(TextBox1.Text);

            // Время ожидания отметки ВО
            if ((TextBox101.Text.Contains(".")) && (DecSep == ','))
                TextBox101.Text = TextBox101.Text.Replace(".", ",");
            if ((TextBox101.Text.Contains(",")) && (DecSep == '.'))
                TextBox101.Text = TextBox1.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.twaitAirObject = Convert.ToDouble(TextBox101.Text);

            // Допустимая скорость
            if ((TextBox2.Text.Contains(".")) && (DecSep == ','))
                TextBox2.Text = TextBox2.Text.Replace(".", ",");
            if ((TextBox2.Text.Contains(",")) && (DecSep == '.'))
                TextBox2.Text = TextBox2.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.Vdop= Convert.ToDouble(TextBox2.Text);

            // СКО ускорений по плоскостным координатам
            if ((TextBox3.Text.Contains(".")) && (DecSep == ','))
                TextBox3.Text = TextBox3.Text.Replace(".", ",");
            if ((TextBox3.Text.Contains(",")) && (DecSep == '.'))
                TextBox3.Text = TextBox3.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.CKOA_pl= Convert.ToDouble(TextBox3.Text);

            // СКО ускорений по H
            if ((TextBox4.Text.Contains(".")) && (DecSep == ','))
                TextBox4.Text = TextBox4.Text.Replace(".", ",");
            if ((TextBox4.Text.Contains(",")) && (DecSep == '.'))
                TextBox4.Text = TextBox4.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.CKOA_H= Convert.ToDouble(TextBox4.Text);

            // CKO по координатам для формирования матрицы D ошибок 
            // измерения в данный момент времени
            if ((TextBox6_1.Text.Contains(".")) && (DecSep == ','))
                TextBox6_1.Text = TextBox6_1.Text.Replace(".", ",");
            if ((TextBox6_1.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_1.Text = TextBox6_1.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.CKO_X= Convert.ToDouble(TextBox6_1.Text);
            GlobalVarMapMain.ObjClassParametersGlobal.CKO_Z = Convert.ToDouble(TextBox6_1.Text);

            if ((TextBox6_2.Text.Contains(".")) && (DecSep == ','))
                TextBox6_2.Text = TextBox6_2.Text.Replace(".", ",");
            if ((TextBox6_2.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_2.Text = TextBox6_2.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.CKO_Y= Convert.ToDouble(TextBox6_2.Text);

            // ?????????????????????
            // Не Нужен
            GlobalVarMapMain.ObjClassParametersGlobal.strob = 20;

            // Начальный и конечный индекс отметок при выводе из файла
            GlobalVarMapMain.ObjClassParametersGlobal.indstart= Convert.ToInt32(TextBox10.Text);
            GlobalVarMapMain.ObjClassParametersGlobal.indstop= Convert.ToInt32(TextBox11.Text);

            // Количество точек для отображения в реальном режиме
            GlobalVarMapMain.ObjClassParametersGlobal.numbp= Convert.ToInt32(GlobalVarMapMain.objMainWindowG.TextBox12.Text);

            double jjd = 0;

            // Ошибки измерения разности времени приема сигналов RLSi в нс
            //if ((TextBox6_5.Text.Contains(".")) && (DecSep == ','))
            //    TextBox6_5.Text = TextBox6_5.Text.Replace(".", ",");
            //if ((TextBox6_5.Text.Contains(",")) && (DecSep == '.'))
            //    TextBox6_5.Text = TextBox6_5.Text.Replace(",", ".");
            //jjd = Convert.ToDouble(TextBox6_5.Text);
            //GlobalVarMapMain.ObjClassParametersGlobal.Error_dt1 = jjd/1000000000; // sec

            if ((TextBox6_6.Text.Contains(".")) && (DecSep == ','))
                TextBox6_6.Text = TextBox6_6.Text.Replace(".", ",");
            if ((TextBox6_6.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_6.Text = TextBox6_6.Text.Replace(",", ".");
            jjd = Convert.ToDouble(TextBox6_6.Text);
            GlobalVarMapMain.ObjClassParametersGlobal.Error_dt = jjd / 1000000000; // sec

            if ((TextBox6_7.Text.Contains(".")) && (DecSep == ','))
                TextBox6_7.Text = TextBox6_7.Text.Replace(".", ",");
            if ((TextBox6_7.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_7.Text = TextBox6_7.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.KDT = Convert.ToDouble(TextBox6_7.Text);

            if ((TextBox6_8.Text.Contains(".")) && (DecSep == ','))
                TextBox6_8.Text = TextBox6_8.Text.Replace(".", ",");
            if ((TextBox6_8.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_8.Text = TextBox6_8.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.Hmax = Convert.ToDouble(TextBox6_8.Text);


            //if ((TextBox6_7.Text.Contains(".")) && (DecSep == ','))
            //    TextBox6_7.Text = TextBox6_7.Text.Replace(".", ",");
            //if ((TextBox6_7.Text.Contains(",")) && (DecSep == '.'))
            //    TextBox6_7.Text = TextBox6_7.Text.Replace(",", ".");
            //jjd = Convert.ToDouble(TextBox6_7.Text);
            //GlobalVarMapMain.ObjClassParametersGlobal.Error_dt3 = jjd / 1000000000; // sec

            // Количество точек для инициализации фильтра
            if ((TextBox6_3.Text.Contains(".")) && (DecSep == ','))
                TextBox6_3.Text = TextBox6_3.Text.Replace(".", ",");
            if ((TextBox6_3.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_3.Text = TextBox6_3.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.numbInit = Convert.ToInt32(TextBox6_3.Text);

            int jj = 0;
            // =true -> использовать динамическую матрицу ошибок измерений
            if ((TextBox6_4.Text.Contains(".")) && (DecSep == ','))
                TextBox6_4.Text = TextBox6_4.Text.Replace(".", ",");
            if ((TextBox6_4.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_4.Text = TextBox6_4.Text.Replace(",", ".");
            jj = Convert.ToInt32(TextBox6_4.Text);
            if (jj == 1)
                GlobalVarMapMain.ObjClassParametersGlobal.flDynStat = true;
            else
                GlobalVarMapMain.ObjClassParametersGlobal.flDynStat = false;

            // =true -> использовать динамическую матрицу ошибок измерений
            // Для инициализации фильтра
            if ((TextBox6_5.Text.Contains(".")) && (DecSep == ','))
                TextBox6_5.Text = TextBox6_5.Text.Replace(".", ",");
            if ((TextBox6_5.Text.Contains(",")) && (DecSep == '.'))
                TextBox6_5.Text = TextBox6_5.Text.Replace(",", ".");
            jj = Convert.ToInt32(TextBox6_5.Text);
            if (jj == 1)
                GlobalVarMapMain.ObjClassParametersGlobal.flDynStatInit = true;
            else
                GlobalVarMapMain.ObjClassParametersGlobal.flDynStatInit = false;

            // =false->инверсия мнимых точек
            // =true->отбрасывание мнимых точек
            if ((TextBox12_mnim.Text.Contains(".")) && (DecSep == ','))
                TextBox12_mnim.Text = TextBox12_mnim.Text.Replace(".", ",");
            if ((TextBox12_mnim.Text.Contains(",")) && (DecSep == '.'))
                TextBox12_mnim.Text = TextBox12_mnim.Text.Replace(",", ".");
            jj = Convert.ToInt32(TextBox12_mnim.Text);
            if (jj == 1)
                GlobalVarMapMain.ObjClassParametersGlobal.flMnimPoints = true;
            else
                GlobalVarMapMain.ObjClassParametersGlobal.flMnimPoints = false;

            // Замена рассчитанной высоты введенной
            if ((TextBox12_HInp.Text.Contains(".")) && (DecSep == ','))
                TextBox12_HInp.Text = TextBox12_HInp.Text.Replace(".", ",");
            if ((TextBox12_HInp.Text.Contains(",")) && (DecSep == '.'))
                TextBox12_HInp.Text = TextBox12_HInp.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersGlobal.HInput = Convert.ToDouble(TextBox12_HInp.Text);


        }
        // ******************************************************************************* LoadParameters

        // LoadParametersRLS ****************************************************************************

        public void LoadParametersRLS()
        {
            // -------------------------------------------------------------------
            // Base RLS

            // Lat,Long,H
            if ((latRLS.Text.Contains(".")) && (DecSep == ','))
                latRLS.Text = latRLS.Text.Replace(".", ",");
            if ((latRLS.Text.Contains(",")) && (DecSep == '.'))
                latRLS.Text = latRLS.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersRLS.latRLS = Convert.ToDouble(latRLS.Text);

            if ((longRLS.Text.Contains(".")) && (DecSep == ','))
                longRLS.Text = longRLS.Text.Replace(".", ",");
            if ((longRLS.Text.Contains(",")) && (DecSep == '.'))
                longRLS.Text = longRLS.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersRLS.longRLS = Convert.ToDouble(longRLS.Text);

            if ((HRLS.Text.Contains(".")) && (DecSep == ','))
                HRLS.Text = HRLS.Text.Replace(".", ",");
            if ((HRLS.Text.Contains(",")) && (DecSep == '.'))
                HRLS.Text = HRLS.Text.Replace(",", ".");
            GlobalVarMapMain.ObjClassParametersRLS.HRLS = Convert.ToDouble(HRLS.Text);

            // rad
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_rad = GlobalVarMapMain.ObjClassParametersRLS.latRLS * Math.PI / 180;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_rad = GlobalVarMapMain.ObjClassParametersRLS.longRLS * Math.PI / 180;

            // Координаты RLS в ГЦСК
            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              GlobalVarMapMain.ObjClassParametersRLS.latRLS,
                                              GlobalVarMapMain.ObjClassParametersRLS.longRLS,
                                              GlobalVarMapMain.ObjClassParametersRLS.HRLS,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc
                                              );
            // MZSK (Victor)
            GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK = 0;
            GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK = 0;
            GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK = 0;

            // -------------------------------------------------------------------
            // RLS2

            // ?????????????????????? Заменить с интерфейса
            // Lat,Long,H
            // old
            //GlobalVarMapMain.ObjClassParametersRLS.latRLS_2 = 53.930997222;
            //GlobalVarMapMain.ObjClassParametersRLS.longRLS_2 = 27.6349;
            //GlobalVarMapMain.ObjClassParametersRLS.HRLS_2 = 21.235;
            // new
            //GlobalVarMapMain.ObjClassParametersRLS.latRLS_2 = 53.93099722;
            //GlobalVarMapMain.ObjClassParametersRLS.longRLS_2 = 27.6349;
            //GlobalVarMapMain.ObjClassParametersRLS.HRLS_2 = 21.2;
            // new1
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_2 = 53.93100255;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_2 = 27.63489395;
            GlobalVarMapMain.ObjClassParametersRLS.HRLS_2 = 21.12;
            // 2021_04_12
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_2 = 53.9309797;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_2 = 27.63490256;
            GlobalVarMapMain.ObjClassParametersRLS.HRLS_2 = 21.133;


            // rad
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_rad_2 = GlobalVarMapMain.ObjClassParametersRLS.latRLS_2 * Math.PI / 180;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_rad_2 = GlobalVarMapMain.ObjClassParametersRLS.longRLS_2 * Math.PI / 180;

            // Координаты RLS в ГЦСК
            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              GlobalVarMapMain.ObjClassParametersRLS.latRLS_2,
                                              GlobalVarMapMain.ObjClassParametersRLS.longRLS_2,
                                              GlobalVarMapMain.ObjClassParametersRLS.HRLS_2,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc_2,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc_2,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc_2
                                              );

            // Координаты RLSi в МЗСК (Victor)
            ClassGeoCalculator.Geoc_MZSK_V(
                                         // BaseRLS
                                         GlobalVarMapMain.ObjClassParametersRLS.latRLS_rad,
                                         GlobalVarMapMain.ObjClassParametersRLS.longRLS_rad,
                                         GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc,
                                         GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc,
                                         GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc,

                                         // RLS2
                                         GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc_2, // геоц
                                         GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc_2,
                                         GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc_2,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK_2,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK_2,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK_2
                                        );
            // -------------------------------------------------------------------
            // RLS3

            // ?????????????????????? Заменить с интерфейса
            // Lat,Long,H
            // old
            //GlobalVarMapMain.ObjClassParametersRLS.latRLS_3 = 53.931444444;
            //GlobalVarMapMain.ObjClassParametersRLS.longRLS_3 = 27.636638889;
            //GlobalVarMapMain.ObjClassParametersRLS.HRLS_3 = 20.947;
            // new
            //GlobalVarMapMain.ObjClassParametersRLS.latRLS_3 = 53.931933;
            //GlobalVarMapMain.ObjClassParametersRLS.longRLS_3 = 27.636481;
            //GlobalVarMapMain.ObjClassParametersRLS.HRLS_3 = 5.6;
            // new
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_3 = 53.9319363;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_3 = 27.63647759;
            GlobalVarMapMain.ObjClassParametersRLS.HRLS_3 = 6.04;
            // 2021_04_12
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_3 = 53.93191408;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_3 = 27.6364854;
            GlobalVarMapMain.ObjClassParametersRLS.HRLS_3 = 5.174;


            // rad
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_rad_3 = GlobalVarMapMain.ObjClassParametersRLS.latRLS_3 * Math.PI / 180;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_rad_3 = GlobalVarMapMain.ObjClassParametersRLS.longRLS_3 * Math.PI / 180;

            // Координаты RLS в ГЦСК
            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              GlobalVarMapMain.ObjClassParametersRLS.latRLS_3,
                                              GlobalVarMapMain.ObjClassParametersRLS.longRLS_3,
                                              GlobalVarMapMain.ObjClassParametersRLS.HRLS_3,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc_3,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc_3,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc_3
                                              );

            // Координаты RLSi в МЗСК (Victor)
            ClassGeoCalculator.Geoc_MZSK_V(
                                         // BaseRLS
                                         GlobalVarMapMain.ObjClassParametersRLS.latRLS_rad,
                                         GlobalVarMapMain.ObjClassParametersRLS.longRLS_rad,
                                         GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc,
                                         GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc,
                                         GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc,

                                         // RLS3
                                         GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc_3, // геоц
                                         GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc_3,
                                         GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc_3,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK_3,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK_3,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK_3
                                        );
            // -------------------------------------------------------------------
            // RLS4

            // ?????????????????????? Заменить с интерфейса
            // Lat,Long,H
            // old
            //GlobalVarMapMain.ObjClassParametersRLS.latRLS_4 = 53.932327778;
            //GlobalVarMapMain.ObjClassParametersRLS.longRLS_4 = 27.63485;
            //GlobalVarMapMain.ObjClassParametersRLS.HRLS_4 = 13.237;
            // new
           // GlobalVarMapMain.ObjClassParametersRLS.latRLS_4 = 53.93232778;
            //GlobalVarMapMain.ObjClassParametersRLS.longRLS_4 = 27.63485;
            //GlobalVarMapMain.ObjClassParametersRLS.HRLS_4 = 13.3;
            // new1
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_4 = 53.93233125;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_4 = 27.63485056;
            GlobalVarMapMain.ObjClassParametersRLS.HRLS_4 = 13.04;
            // 2021_04_12
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_4 = 53.93230937;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_4 = 27.6348569;
            GlobalVarMapMain.ObjClassParametersRLS.HRLS_4 = 13.064;

            // rad
            GlobalVarMapMain.ObjClassParametersRLS.latRLS_rad_4 = GlobalVarMapMain.ObjClassParametersRLS.latRLS_4 * Math.PI / 180;
            GlobalVarMapMain.ObjClassParametersRLS.longRLS_rad_4 = GlobalVarMapMain.ObjClassParametersRLS.longRLS_4 * Math.PI / 180;

            // Координаты RLS в ГЦСК
            ClassGeoCalculator.f_BLH_XYZ_84_1
                                             (
                                              GlobalVarMapMain.ObjClassParametersRLS.latRLS_4,
                                              GlobalVarMapMain.ObjClassParametersRLS.longRLS_4,
                                              GlobalVarMapMain.ObjClassParametersRLS.HRLS_4,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc_4,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc_4,
                                              ref GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc_4
                                              );

            // Координаты RLSi в МЗСК (Victor)
            ClassGeoCalculator.Geoc_MZSK_V(
                                         // BaseRLS
                                         GlobalVarMapMain.ObjClassParametersRLS.latRLS_rad,
                                         GlobalVarMapMain.ObjClassParametersRLS.longRLS_rad,
                                         GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc,
                                         GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc,
                                         GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc,

                                         // RLS4
                                         GlobalVarMapMain.ObjClassParametersRLS.XRLS_Geoc_4, // геоц
                                         GlobalVarMapMain.ObjClassParametersRLS.YRLS_Geoc_4,
                                         GlobalVarMapMain.ObjClassParametersRLS.ZRLS_Geoc_4,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK_4,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK_4,
                                         ref GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK_4
                                        );
            // -------------------------------------------------------------------

        }
        // **************************************************************************** LoadParametersRLS

        // LoadParametersOld ****************************************************************************
        // !!! FOR OLD algorithm

        // Ввод входных параметров для алгоритма формирования траекторий
        // Запись входных параметров в класс параметров для подачи в DLL формирования траекторий

        public void LoadClassParameters()
        {
            // Время ожидания отметки
            GlobalVarMapMain.ObjClassParameters.twait = GlobalVarMapMain.ObjClassParametersGlobal.twait;

            // Допустимая скорость
            GlobalVarMapMain.ObjClassParameters.Vdop = GlobalVarMapMain.ObjClassParametersGlobal.Vdop;

            // СКО ускорений по плоскостным координатам
            GlobalVarMapMain.ObjClassParameters.CKOA_pl = GlobalVarMapMain.ObjClassParametersGlobal.CKOA_pl;

            // СКО ускорений по H
            GlobalVarMapMain.ObjClassParameters.CKOA_H = GlobalVarMapMain.ObjClassParametersGlobal.CKOA_H;

            // CKO по координатам для формирования матрицы D ошибок 
            // измерения в данный момент времени
            GlobalVarMapMain.ObjClassParameters.CKO_X = GlobalVarMapMain.ObjClassParametersGlobal.CKO_X;
            GlobalVarMapMain.ObjClassParameters.CKO_Y = GlobalVarMapMain.ObjClassParametersGlobal.CKO_Y;
            GlobalVarMapMain.ObjClassParameters.CKO_Z = GlobalVarMapMain.ObjClassParametersGlobal.CKO_Z;

            // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            // RLS

            // 1
            GlobalVarMapMain.ObjClassParameters.XRLS_MZSK = GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK;
            GlobalVarMapMain.ObjClassParameters.YRLS_MZSK = GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK;
            GlobalVarMapMain.ObjClassParameters.ZRLS_MZSK = GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK;

            // 2
            GlobalVarMapMain.ObjClassParameters.XRLS_MZSK_2 = GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK_2;
            GlobalVarMapMain.ObjClassParameters.YRLS_MZSK_2 = GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK_2;
            GlobalVarMapMain.ObjClassParameters.ZRLS_MZSK_2 = GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK_2;

            // 3
            GlobalVarMapMain.ObjClassParameters.XRLS_MZSK_3 = GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK_3;
            GlobalVarMapMain.ObjClassParameters.YRLS_MZSK_3 = GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK_3;
            GlobalVarMapMain.ObjClassParameters.ZRLS_MZSK_3 = GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK_3;

            // 4
            GlobalVarMapMain.ObjClassParameters.XRLS_MZSK_4 = GlobalVarMapMain.ObjClassParametersRLS.XRLS_MZSK_4;
            GlobalVarMapMain.ObjClassParameters.YRLS_MZSK_4 = GlobalVarMapMain.ObjClassParametersRLS.YRLS_MZSK_4;
            GlobalVarMapMain.ObjClassParameters.ZRLS_MZSK_4 = GlobalVarMapMain.ObjClassParametersRLS.ZRLS_MZSK_4;

            // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


            // Величина строба (расстояние от предыдущей отметки)
            // для идентификации отметки
            GlobalVarMapMain.strob = 20;
            GlobalVarMapMain.ObjClassParameters.strob = 20;

            // Ошибки измерения разности времени приема сигналов
            GlobalVarMapMain.ObjClassParameters.Error_dt1 = GlobalVarMapMain.ObjClassParametersGlobal.Error_dt;
            GlobalVarMapMain.ObjClassParameters.Error_dt2 = GlobalVarMapMain.ObjClassParametersGlobal.Error_dt;
            GlobalVarMapMain.ObjClassParameters.Error_dt3 = GlobalVarMapMain.ObjClassParametersGlobal.Error_dt;

        }
        // **************************************************************************** LoadParametersOld




    } // Class
} // Namespace
