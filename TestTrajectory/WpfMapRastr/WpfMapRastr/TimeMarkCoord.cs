﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMapRastr
{
    public class TimeMarkCoord: MarkCoord
    {

        // Time of mark's getting
        public double Time { get; set; }
        // Частота, КГц
        public float Freq { get; set; }
        // Ширина полосы, КГц
        public float dFreq { get; set; }
        //
        public DateTime _time { get; set; }


        // Matrix of measurement errors
        public double[,] Error = new double[3, 3];
        // Matrix of measurement errors static
        public double[,] ErrorStat = new double[3, 3];

        public List<Station> StationComplex = new List<Station>();

        public double Xe; 
        public double Ye; 
        public double Ze;
        public double VXe;
        public double VYe;
        public double VZe;

        // 07_04
        public double dt2e;
        public double dt3e;
        public double dt4e;

        // Constructor *****************************************************************
        public TimeMarkCoord(
                         // Air object
                         double Lat,  // WGS84
                         double Long, // WGS84
                         double Alt,

                         List<Station> _stations,

                         double time,
                         float freq,
                         float dfreq,
                         DateTime _timeinp

                             ): base(
                         // Air object
                         Lat,  // WGS84
                         Long, // WGS84
                         Alt,

                         _stations
                                   )

        {

            StationComplex = _stations;
            Time = time;
            Freq = freq;
            dFreq = dfreq;
            _time = _timeinp;
        }
        // ***************************************************************** Constructor

        // ErrorMatrix ****************************************************************************
        public double[,] ErrorMeasurement(
                                         List<Station> StationComplex,
                                         // Air object
                                         double Lat,  // WGS84
                                         double Long, // WGS84
                                         double Alt,
                                         double time,
                                         double Xe,
                                         double Ye,
                                         double Ze
                                         )
        {
            // --------------------------------------------------------------------------------
            // !!! Для источников

            //Xe = X;
            //Ye = Y;
            //Ze = Z;
            // --------------------------------------------------------------------------------
            // Массив временных задержек 1*3
            var matrdt = new double[1, 3];

            //матрица частных производных
            var Qa1 = new double[3, 15];
            var Qa1T = new double[15, 3];

            // Ковариационная матрица ошибок наблюдения 15x15
            var matrixK = new double[15, 15];

            // Матрица ошибок измерения  3x3
            var res = new double[3, 3];

            Matrix objMatrix1 = new Matrix();
            // --------------------------------------------------------------------------------
            // Определение массива временных задержек

            matrdt = Calc_dt();
            // --------------------------------------------------------------------------------
            // Определение  TipRes

            // Тип решения +/-
            byte TipRes = 0;

            // Рассчитанные координаты ВО
            var XYZres = new double[2, 4];
            // Экстраполированные координаты ВО
            var VO = new double[1, 3];

            VO[0, 0] = Xe;
            VO[0, 1] = Ze;
            VO[0, 2] = Ye;

            XYZres = Func_XYZ_3RDM(
                                  // Массив временных задержек 1*3
                                  matrdt
                                 );

            if ((XYZres[0, 2] - VO[0, 2]) <= 1)
                TipRes = 0;
            else
                TipRes = 1;
            // --------------------------------------------------------------------------------
            // Ковариационная матрица ошибок наблюдения 15x15

            matrixK = Calc_K();
            // --------------------------------------------------------------------------------
            // Расчет частных производных

            // Матрица частных производных
            Qa1 = Func_Qa3RDM(
                              // Массив временных задержек 1*3
                              matrdt,
                              // Тип решения +/-
                              TipRes
                             );
            // Транспонированная матрица частных производных
            objMatrix1.MTransp(
                               3,
                               15,
                               Qa1,
                               ref Qa1T
                              );
            // --------------------------------------------------------------------------------
            // Матрица ошибок измерения  3x3

            res = objMatrix1.MatrixMultiplication(objMatrix1.MatrixMultiplication(Qa1, matrixK), Qa1T);
            // --------------------------------------------------------------------------------

            return res;
        }
        // ************************************************************************** ErrorMatrix

        // ErrorMatrixStat **********************************************************************
        // Расчет матрицы 3x3 ошибок измерения координат в момент времени Ti

        public double[,] ErrorMeasurementStat(
                               double CKOX,
                               double CKOY,
                               double CKOZ
                                         )
        {
            double[,] MError = new double[3, 3];


            MError[0, 0] = CKOX * CKOX;
            MError[1, 1] = CKOZ * CKOZ;
            MError[2, 2] = CKOY * CKOY;

            return MError;
        }
        // ************************************************************************ ErrorMatrixStat

        // Calc_dt ********************************************************************************

        public double[,] Calc_dt()
        {
            // ....................................................................................
            double RCC = 0;

            // Расстояние между ППi и ИРИ
            double R0 = 0;
            double R1 = 0;
            double R2 = 0;
            double R3 = 0;

            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3
            double dr01 = 0;
            double dr02 = 0;
            double dr03 = 0;

            //double cc = 299792458;
            double cc = 299796459.2;

            var matrixC = new double[1, 3];
            // ....................................................................................

            RCC = Math.Sqrt(X * X + Y * Y + Z * Z);
            R0 = RCC;

            R1 = Math.Sqrt((X - StationComplex[1].Position.X) * (X - StationComplex[1].Position.X)
                           + (Y - StationComplex[1].Position.Y) * (Y - StationComplex[1].Position.Y) +
                             (Z - StationComplex[1].Position.Z) * (Z - StationComplex[1].Position.Z));

            R2 = Math.Sqrt((X - StationComplex[2].Position.X) * (X - StationComplex[2].Position.X)
                           + (Y - StationComplex[2].Position.Y) * (Y - StationComplex[2].Position.Y) +
                             (Z - StationComplex[2].Position.Z) * (Z - StationComplex[2].Position.Z));

            R3 = Math.Sqrt((X - StationComplex[3].Position.X) * (X - StationComplex[3].Position.X)
                           + (Y - StationComplex[3].Position.Y) * (Y-StationComplex[3].Position.Y) +
                             (Z - StationComplex[3].Position.Z) * (Z - StationComplex[3].Position.Z));

            // ....................................................................................
            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3

            dr01 = R1 - R0;
            dr02 = R2 - R0;
            dr03 = R3 - R0;

            matrixC[0, 0] = dr01 / cc;
            matrixC[0, 1] = dr02 / cc;
            matrixC[0, 2] = dr03 / cc;
            // .......................................................................................

            return matrixC;
        }
        // ****************************************************************************************

        // K **************************************************************************************
        // Расчет ковариационной матрицы ошибок наблюдения D_Omega 15x15

        // По главной диагонали стоят квадраты априорных ошибок измерения разности времени приема сигналов и топопривязки
        // ошибки топопривязки имеют нулевые значения)

        // В файле "методика расчета ошибок местоопределения приложение к ПЗ траекторной обработки.doc"
        // эта переменная описана применительно к формуле (2.6) и имеет наименование D с индексом омега

        // ??????????????????????????????????
        public double[,] Calc_K()
        {

            var matrixK = new double[15, 15];
            int i = 0;
            int j = 0;
            // ...................................................................................
            matrixK[0, 0] = Math.Pow(StationComplex[1].ErrorTime,2);
            for (i = 1; i < 15; i++)
                matrixK[0, i] = 0;

            matrixK[1, 0] = 0;
            matrixK[1, 1] = Math.Pow(StationComplex[2].ErrorTime, 2);
            for (i = 1; i < 15; i++)
                matrixK[0, i] = 0;

            matrixK[2, 0] = 0;
            matrixK[2, 1] = 0;
            matrixK[2, 2] = Math.Pow(StationComplex[3].ErrorTime, 2);
            for (i = 3; i < 15; i++)
                matrixK[0, i] = 0;
            // ...................................................................................
            for (i = 3; i < 15; i++)
            {
                for (j = 0; j < 15; j++)
                {
                    matrixK[i, j] = 0;
                }
            }
            // ....................................................................................

            return matrixK;
        }
        // ************************************************************************************** K

        // Func_XYZ_3RDM **************************************************************************
        // Функция расчета координат ИРИ для 3 РДМ

        public double[,] Func_XYZ_3RDM(
                                         // Массив временных задержек 1*3
                                         double[,] matrdt
                                        )
        {
            // ------------------------------------------------------------------------------------
            var A1 = new double[1, 15];

            int i = 0;
            double mk1 = 0;
            double mk2 = 0;
            double mk3 = 0;
            double V = 0;
            double U1 = 0;
            double U2 = 0;
            double U3 = 0;
            double U4 = 0;
            double Xs = 0;
            double Z0 = 0;
            double Ys = 0;
            double Zs = 0;
            double Z1 = 0;
            double Y0 = 0;
            double Y1 = 0;

            double b1 = 0;
            double b2 = 0;
            double b3 = 0;
            double b4 = 0;
            double b5 = 0;
            double AA = 0;
            double BB = 0;
            double CC = 0;

            //double C = 299792458;
            double C = 299796459.2;

            var a = new double[2, 4];
            var res = new double[2, 4];
            // ------------------------------------------------------------------------------------
            // A1

            // dt
            A1[0, 0] = matrdt[0, 0];
            A1[0, 1] = matrdt[0, 1];
            A1[0, 2] = matrdt[0, 2];

            // RLS1
            A1[0, 3] = StationComplex[0].Position.X;  // X
            A1[0, 4] = StationComplex[0].Position.Z;  // Z
            A1[0, 5] = StationComplex[0].Position.Y;  // Y
            // RLS2
            A1[0, 6] = StationComplex[1].Position.X;  // X
            A1[0, 7] = StationComplex[1].Position.Z;  // Z
            A1[0, 8] = StationComplex[1].Position.Y;  // Y
            // RLS3
            A1[0, 9] = StationComplex[2].Position.X;  // X
            A1[0, 10] = StationComplex[2].Position.Z;  // Z
            A1[0, 11] = StationComplex[2].Position.Y;  // Y
            // RLS4
            A1[0, 12] = StationComplex[3].Position.X;  // X
            A1[0, 13] = StationComplex[3].Position.Z;  // Z
            A1[0, 14] = StationComplex[3].Position.Y;  // Y

            // ------------------------------------------------------------------------------------
            mk1 = ((Math.Pow(A1[0, 6], 2) + Math.Pow(A1[0, 7], 2) + Math.Pow(A1[0, 8], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow((C * A1[0, 0]), 2)) / 2;
            mk2 = ((Math.Pow(A1[0, 9], 2) + Math.Pow(A1[0, 10], 2) + Math.Pow(A1[0, 11], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow((C * A1[0, 1]), 2)) / 2;
            mk3 = ((Math.Pow(A1[0, 12], 2) + Math.Pow(A1[0, 13], 2) + Math.Pow(A1[0, 14], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow((C * A1[0, 2]), 2)) / 2;
            // ------------------------------------------------------------------------------------
            a[0, 0] = A1[0, 1] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 9]);
            a[0, 1] = A1[0, 1] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 10]);
            a[0, 2] = A1[0, 1] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 11]);
            a[0, 3] = A1[0, 1] * mk1 - A1[0, 0] * mk2;

            a[1, 0] = A1[0, 2] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 12]);
            a[1, 1] = A1[0, 2] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 13]);
            a[1, 2] = A1[0, 2] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 14]);
            a[1, 3] = A1[0, 2] * mk1 - A1[0, 0] * mk3;
            // ------------------------------------------------------------------------------------
            V = a[0, 0] * a[1, 1] - a[0, 1] * a[1, 0];
            // ------------------------------------------------------------------------------------
            // V!=0

            if (V != 0)
            {
                U1 = a[0, 1] * a[1, 3] - a[0, 3] * a[1, 1];
                U2 = a[0, 1] * a[1, 2] - a[0, 2] * a[1, 1];
                U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                U4 = a[1, 0] * a[0, 2] - a[0, 0] * a[1, 2];
                Xs = U1 / V;
                Z0 = U2 / V;
                Ys = U3 / V;
                Z1 = U4 / V;

                b1 = (A1[0, 5] - A1[0, 14]) + Z0 * (A1[0, 3] - A1[0, 12]) + Z1 * (A1[0, 4] - A1[0, 13]);
                b2 = Xs * (A1[0, 3] - A1[0, 12]) + Ys * (A1[0, 4] - A1[0, 13]) + mk3;
                b3 = 1 + Math.Pow(Z0, 2) + Math.Pow(Z1, 2);
                b4 = 2 * (Z0 * (Xs - A1[0, 3]) + Z1 * (Ys - A1[0, 4]) - A1[0, 5]);
                b5 = Math.Pow(A1[0, 5], 2) + Math.Pow((Xs - A1[0, 3]), 2) + Math.Pow((Ys - A1[0, 4]), 2);

                AA = Math.Pow(b1, 2) - Math.Pow((C * A1[0, 2]), 2) * b3;
                BB = 2 * b1 * b2 - Math.Pow((C * A1[0, 2]), 2) * b4;
                CC = Math.Pow(b2, 2) - Math.Pow((C * A1[0, 2]), 2) * b5;

                // возможно перепутываание индексации
                res[0, 2] = (-BB - Math.Sqrt(Math.Pow(BB, 2) - 4 * AA * CC)) / (2 * AA);
                res[1, 2] = (-BB + Math.Sqrt(Math.Pow(BB, 2) - 4 * AA * CC)) / (2 * AA);
                res[0, 0] = Xs + res[0, 1] * Z0;
                res[1, 0] = Xs + res[1, 1] * Z0;
                res[0, 1] = Ys + res[0, 1] * Z1;
                res[1, 1] = Ys + res[1, 1] * Z1;

                res[0, 3] = 1;     // варинат решения № 1
                res[1, 3] = 1;

            } // V!=0
              // ------------------------------------------------------------------------------------
              // V==0

            else
            {
                V = a[0, 0] * a[1, 2] - a[0, 2] * a[1, 0];

                U1 = a[0, 2] * a[1, 3] - a[0, 3] * a[1, 2];
                U2 = a[0, 2] * a[1, 1] - a[0, 1] * a[1, 2];
                U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                U4 = a[1, 0] * a[0, 1] - a[0, 0] * a[1, 1];
                Xs = U1 / V;
                Y0 = U2 / V;
                Zs = U3 / V;
                Y1 = U4 / V;

                b1 = (A1[0, 4] - A1[0, 13]) + Y0 * (A1[0, 3] - A1[0, 12]) + Y1 * (A1[0, 4] - A1[0, 13]);
                b2 = Xs * (A1[0, 3] - A1[0, 12]) + Zs * (A1[0, 5] - A1[0, 14]) + mk3;
                b3 = 1 + Math.Pow(Y0, 2) + Math.Pow(Y1, 2);
                b4 = 2 * (Y0 * (Xs - A1[0, 3]) + Y1 * (Zs - A1[0, 4]) - A1[0, 5]);
                b5 = Math.Pow(A1[0, 4], 2) + Math.Pow((Xs - A1[0, 3]), 2) + Math.Pow((Zs - A1[0, 5]), 2);

                AA = Math.Pow(b1, 2) - Math.Pow((C * A1[0, 2]), 2) * b3;
                BB = 2 * b1 * b2 - Math.Pow((C * A1[0, 2]), 2) * b4;
                CC = Math.Pow(b2, 2) - Math.Pow((C * A1[0, 2]), 2) * b5;

                // возможно перепутываание индексации
                res[0, 2] = (-BB - Math.Sqrt(Math.Pow(BB, 2) - 4 * AA * CC)) / (2 * AA);
                res[1, 2] = (-BB + Math.Sqrt(Math.Pow(BB, 2) - 4 * AA * CC)) / (2 * AA);
                res[0, 0] = Xs + res[0, 2] * Y0;
                res[1, 0] = Xs + res[1, 2] * Y0;
                res[0, 1] = Zs + res[0, 2] * Y1;
                res[1, 1] = Zs + res[1, 2] * Y1;

                res[0, 3] = 2;     // варинат решения № 2
                res[1, 3] = 2;

            } // V==0
              // ------------------------------------------------------------------------------------
            return res;
            // ------------------------------------------------------------------------------------
        }
        // ************************************************************************** Func_XYZ_3RDM

        // Func_Qa3RDM ****************************************************************************
        //расчет частных производных первого  порядка  для 3 РДМ комплекса

        public double[,] Func_Qa3RDM(
                                         // Массив временных задержек 1*3
                                         double[,] matrdt,

                                         // Тип решения +/-
                                         byte TipRes
                                    )
        {

            // ------------------------------------------------------------------------------------
            var A1 = new double[1, 15];

            int i = 0;
            double mk1 = 0;
            double mk2 = 0;
            double mk3 = 0;
            double V = 0;
            double U1 = 0;
            double U2 = 0;
            double U3 = 0;
            double U4 = 0;
            double Xs = 0;
            double Z0 = 0;
            double Ys = 0;
            double Zs = 0;
            double Z1 = 0;
            double Y0 = 0;
            double Y1 = 0;

            double b1 = 0;
            double b2 = 0;
            double b3 = 0;
            double b4 = 0;
            double b5 = 0;
            double AA = 0;
            double BB = 0;
            double CC = 0;
            double S = 0;
            double X = 0;
            double Y = 0;
            double Z = 0;

            //double C = 299792458;
            double C = 299796459.2;

            var a = new double[2, 4];
            var res = new double[3, 15];

            //dA1, da: matrix;
            var da = new double[2, 4];

            double dmk1 = 0;
            double dmk2 = 0;
            double dmk3 = 0;

            double dV = 0;
            double dU1 = 0;
            double dU2 = 0;
            double dU3 = 0;
            double dU4 = 0;
            double dXs = 0;
            double dZ0 = 0;
            double dYs = 0;
            double dZs = 0;
            double dZ1 = 0;
            double dY0 = 0;
            double dY1 = 0;

            double db1 = 0;
            double db2 = 0;
            double db3 = 0;
            double db4 = 0;
            double db5 = 0;
            double dAA = 0;
            double dBB = 0;
            double dCC = 0;
            double dS = 0;
            double dY = 0;
            double dX = 0;
            double dZ = 0;
            // ------------------------------------------------------------------------------------
            // A1

            // dt
            A1[0, 0] = matrdt[0, 0];
            A1[0, 1] = matrdt[0, 1];
            A1[0, 2] = matrdt[0, 2];

            // RLS1
            A1[0, 3] = StationComplex[0].Position.X;  // X
            A1[0, 4] = StationComplex[0].Position.Z;  // Z
            A1[0, 5] = StationComplex[0].Position.Y;  // Y
            // RLS2
            A1[0, 6] = StationComplex[1].Position.X;  // X
            A1[0, 7] = StationComplex[1].Position.Z;  // Z
            A1[0, 8] = StationComplex[1].Position.Y;  // Y
            // RLS3
            A1[0, 9] = StationComplex[2].Position.X;  // X
            A1[0, 10] = StationComplex[2].Position.Z;  // Z
            A1[0, 11] = StationComplex[2].Position.Y;  // Y
            // RLS4
            A1[0, 12] = StationComplex[3].Position.X;  // X
            A1[0, 13] = StationComplex[3].Position.Z;  // Z
            A1[0, 14] = StationComplex[3].Position.Y;  // Y

            // ------------------------------------------------------------------------------------
            mk1 = ((Math.Pow(A1[0, 6], 2) + Math.Pow(A1[0, 7], 2) + Math.Pow(A1[0, 8], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow(C * A1[0, 0], 2)) / 2;
            mk2 = ((Math.Pow(A1[0, 9], 2) + Math.Pow(A1[0, 10], 2) + Math.Pow(A1[0, 11], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow(C * A1[0, 1], 2)) / 2;
            mk3 = ((Math.Pow(A1[0, 12], 2) + Math.Pow(A1[0, 13], 2) + Math.Pow(A1[0, 14], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow(C * A1[0, 2], 2)) / 2;
            // ------------------------------------------------------------------------------------
            a[0, 0] = A1[0, 1] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 9]);
            a[0, 1] = A1[0, 1] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 10]);
            a[0, 2] = A1[0, 1] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 11]);
            a[0, 3] = A1[0, 1] * mk1 - A1[0, 0] * mk2;

            a[1, 0] = A1[0, 2] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 12]);
            a[1, 1] = A1[0, 2] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 13]);
            a[1, 2] = A1[0, 2] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 14]);
            a[1, 3] = A1[0, 2] * mk1 - A1[0, 0] * mk3;
            // ------------------------------------------------------------------------------------
            V = a[0, 0] * a[1, 1] - a[0, 1] * a[1, 0];
            // ------------------------------------------------------------------------------------
            // V!=0

            if (V != 0)
            {
                U1 = a[0, 1] * a[1, 3] - a[0, 3] * a[1, 1];
                U2 = a[0, 1] * a[1, 2] - a[0, 2] * a[1, 1];
                U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                U4 = a[1, 0] * a[0, 2] - a[0, 0] * a[1, 2];
                Xs = U1 / V;
                Z0 = U2 / V;
                Ys = U3 / V;
                Z1 = U4 / V;

                b1 = (A1[0, 5] - A1[0, 14]) + Z0 * (A1[0, 3] - A1[0, 12]) + Z1 * (A1[0, 4] - A1[0, 13]);
                b2 = Xs * (A1[0, 3] - A1[0, 12]) + Ys * (A1[0, 4] - A1[0, 13]) + mk3;
                b3 = 1 + Math.Pow(Z0, 2) + Math.Pow(Z1, 2);
                b4 = 2 * (Z0 * (Xs - A1[0, 3]) + Z1 * (Ys - A1[0, 4]) - A1[0, 5]);
                b5 = Math.Pow(A1[0, 5], 2) + Math.Pow((Xs - A1[0, 3]), 2) + Math.Pow((Ys - A1[0, 4]), 2);

                AA = Math.Pow(b1, 2) - Math.Pow((C * A1[0, 2]), 2) * b3;
                BB = 2 * b1 * b2 - Math.Pow((C * A1[0, 2]), 2) * b4;
                CC = Math.Pow(b2, 2) - Math.Pow((C * A1[0, 2]), 2) * b5;
                S = Math.Sqrt(Math.Pow(BB, 2) - 4 * AA * CC);
                // возможно перепутываание индексации
                if (TipRes == 0)
                    Y = (-BB - S) / (2 * AA);
                else
                    Y = (-BB + S) / (2 * AA);

                X = Xs + Y * Z0;
                Z = Ys + Y * Z1;

            } // V!=0
            // ------------------------------------------------------------------------------------
            // V==0

            else
            {
                V = a[0, 0] * a[1, 2] - a[0, 2] * a[1, 0];

                U1 = a[0, 2] * a[1, 3] - a[0, 3] * a[1, 2];
                U2 = a[0, 2] * a[1, 1] - a[0, 1] * a[1, 2];
                U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                U4 = a[1, 0] * a[0, 1] - a[0, 0] * a[1, 1];
                Xs = U1 / V;
                Y0 = U2 / V;
                Ys = U3 / V;
                Y1 = U4 / V;

                b1 = (A1[0, 4] - A1[0, 13]) + Y0 * (A1[0, 3] - A1[0, 12]) + Y1 * (A1[0, 4] - A1[0, 13]);
                b2 = Xs * (A1[0, 3] - A1[0, 12]) + Zs * (A1[0, 5] - A1[0, 14]) + mk3;
                b3 = 1 + Math.Pow(Y0, 2) + Math.Pow(Y1, 2);
                b4 = 2 * (Y0 * (Xs - A1[0, 3]) + Y1 * (Zs - A1[0, 4]) - A1[0, 5]);
                b5 = Math.Pow(A1[0, 4], 2) + Math.Pow((Xs - A1[0, 3]), 2) + Math.Pow((Zs - A1[0, 5]), 2);

                AA = Math.Pow(b1, 2) - Math.Pow((C * A1[0, 2]), 2) * b3;
                BB = 2 * b1 * b2 - Math.Pow((C * A1[0, 2]), 2) * b4;
                CC = Math.Pow(b2, 2) - Math.Pow((C * A1[0, 2]), 2) * b5;

                S = Math.Sqrt(Math.Pow(BB, 2) - 4 * AA * CC);

                // возможно перепутываание индексации
                if (TipRes == 0)
                    Z = (-BB - S) / (2 * AA);
                else
                    Z = (-BB + S) / (2 * AA);

                X = Xs + Z * Y0;
                Y = Zs + Z * Y1;

            } // V==0
              // ------------------------------------------------------------------------------------
              // Единичная матрица 15x15

            var dA1 = new byte[15, 15];
            Matrix objMatrix = new Matrix();
            dA1 = objMatrix.MatrixOne(15, 15);
            // ------------------------------------------------------------------------------------

            // FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1F
            // FOR1

            for (i = 0; i <= 14; i++)
            {
                // ...................................................................................
                dmk1 = (A1[0, 6] * dA1[i, 6] + A1[0, 7] * dA1[i, 7] + A1[0, 8] * dA1[i, 8]) -
                       (A1[0, 3] * dA1[i, 3] + A1[0, 4] * dA1[i, 4] + A1[0, 5] * dA1[i, 5]) -
                       Math.Pow(C, 2) * A1[0, 0] * dA1[i, 0];

                dmk2 = (A1[0, 9] * dA1[i, 9] + A1[0, 10] * dA1[i, 10] + A1[0, 11] * dA1[i, 11]) -
                       (A1[0, 3] * dA1[i, 3] + A1[0, 4] * dA1[i, 4] + A1[0, 5] * dA1[i, 5]) -
                       Math.Pow(C, 2) * A1[0, 1] * dA1[i, 1];

                dmk3 = (A1[0, 12] * dA1[i, 12] + A1[0, 13] * dA1[i, 13] + A1[0, 14] * dA1[i, 14]) -
                       (A1[0, 3] * dA1[i, 3] + A1[0, 4] * dA1[i, 4] + A1[0, 5] * dA1[i, 5]) -
                       Math.Pow(C, 2) * A1[0, 2] * dA1[i, 2];
                // ...................................................................................
                da[0, 0] = (A1[0, 3] - A1[0, 6]) * dA1[i, 1] + (dA1[i, 3] - dA1[i, 6]) * A1[0, 1] -
                           ((A1[0, 3] - A1[0, 9]) * dA1[i, 0] + (dA1[i, 3] - dA1[i, 9]) * A1[0, 0]);
                da[0, 1] = (A1[0, 4] - A1[0, 7]) * dA1[i, 1] + (dA1[i, 4] - dA1[i, 4]) * A1[0, 1] -
                           ((A1[0, 4] - A1[0, 10]) * dA1[i, 0] + (dA1[i, 4] - dA1[i, 10]) * A1[0, 0]);
                da[0, 2] = (A1[0, 5] - A1[0, 8]) * dA1[i, 1] + (dA1[i, 5] - dA1[i, 8]) * A1[0, 1] -
                           ((A1[0, 5] - A1[0, 11]) * dA1[i, 0] + (dA1[i, 5] - dA1[i, 11]) * A1[0, 0]);
                da[0, 3] = mk1 * dA1[i, 1] + dmk1 * A1[0, 1] - (mk2 * dA1[i, 0] + dmk2 * A1[0, 0]);


                da[1, 0] = (A1[0, 3] - A1[0, 6]) * dA1[i, 2] + (dA1[i, 3] - dA1[i, 6]) * A1[0, 2] -
                           ((A1[0, 3] - A1[0, 12]) * dA1[i, 0] + (dA1[i, 3] - dA1[i, 12]) * A1[0, 0]);
                da[1, 1] = (A1[0, 4] - A1[0, 7]) * dA1[i, 2] + (dA1[i, 4] - dA1[i, 4]) * A1[0, 2] -
                           ((A1[0, 4] - A1[0, 13]) * dA1[i, 0] + (dA1[i, 4] - dA1[i, 13]) * A1[0, 0]);
                da[1, 2] = (A1[0, 5] - A1[0, 8]) * dA1[i, 2] + (dA1[i, 5] - dA1[i, 8]) * A1[0, 2] -
                           ((A1[0, 5] - A1[0, 14]) * dA1[i, 0] + (dA1[i, 5] - dA1[i, 14]) * A1[0, 0]);
                da[1, 3] = mk1 * dA1[i, 2] + dmk1 * A1[0, 2] - (mk3 * dA1[i, 0] + dmk3 * A1[0, 0]);
                // ...................................................................................
                // V!=0

                if (V != 0)
                {
                    dV = (a[0, 0] * da[1, 1] + a[1, 1] * da[0, 0]) - (a[0, 1] * da[1, 0] + a[1, 0] * da[0, 1]);
                    dU1 = (a[0, 1] * da[1, 3] + a[1, 3] * da[0, 1]) - (a[0, 3] * da[1, 1] + a[1, 1] * da[0, 3]);
                    dU2 = (a[0, 1] * da[1, 2] + a[1, 2] * da[0, 1]) - (a[0, 2] * da[1, 1] + a[1, 1] * da[0, 2]);
                    dU3 = (a[1, 0] * da[0, 3] + a[0, 3] * da[1, 0]) - (a[0, 0] * da[1, 3] + a[1, 3] * da[0, 0]);
                    dU4 = (a[1, 0] * da[0, 2] + a[0, 2] * da[1, 0]) - (a[0, 0] * da[1, 2] + a[1, 2] * da[0, 0]);

                    dXs = (dU1 * V - U1 * dV) / Math.Pow(V, 2);
                    dZ0 = (dU2 * V - U2 * dV) / Math.Pow(V, 2);
                    dYs = (dU3 * V - U3 * dV) / Math.Pow(V, 2);
                    dZ1 = (dU4 * V - U4 * dV) / Math.Pow(V, 2);

                    db1 = dA1[i, 5] - dA1[i, 14] + dZ0 * (A1[0, 3] - A1[0, 12]) + Z0 * (dA1[i, 3] - dA1[i, 12]) +
                          dZ1 * (A1[0, 4] - A1[0, 13]) + Z1 * (dA1[i, 4] - dA1[i, 13]);
                    db2 = dXs * (A1[0, 3] - A1[0, 12]) + Xs * (dA1[i, 3] - dA1[i, 12]) +
                         dYs * (A1[0, 4] - A1[0, 13]) + Ys * (dA1[i, 4] - dA1[i, 13]) + dmk3;
                    db3 = 2 * Z0 * dZ0 + 2 * Z1 * dZ1;
                    db4 = 2 * (dZ0 * (Xs - A1[0, 3]) + Z0 * (dXs - dA1[i, 3]) + dZ1 * (Ys - A1[0, 4]) + Z1 * (dYs - dA1[i, 4]) - dA1[i, 5]);
                    db5 = 2 * (A1[0, 5] * dA1[i, 5] + (Xs - A1[0, 3]) * (dXs - dA1[i, 3]) + (Ys - A1[0, 4]) * (dYs - dA1[i, 4]));

                    dAA = 2 * b1 * db1 - 2 * Math.Pow(C, 2) * dA1[i, 2] * A1[0, 2] * b3 - Math.Pow((C * A1[0, 2]), 2) * db3;
                    dBB = 2 * b1 * db2 + 2 * b2 * db1 - 2 * Math.Pow(C, 2) * dA1[i, 2] * A1[0, 2] * b4 - Math.Pow((C * A1[0, 2]), 2) * db4;
                    dCC = 2 * b2 * db2 - 2 * Math.Pow(C, 2) * dA1[i, 2] * A1[0, 2] * b5 - Math.Pow((C * A1[0, 2]), 2) * db5;

                    dS = (BB * dBB - 2 * CC * dAA - 2 * AA * dCC) / S;

                    if (TipRes == 0)
                        dY = (-(AA * dBB - BB * dAA) - (AA * dS - S * dAA)) / (2 * Math.Pow(AA, 2));
                    else
                        dY = (-(AA * dBB - BB * dAA) + (AA * dS - S * dAA)) / (2 * Math.Pow(AA, 2));
                    dX = dXs + dZ0 * Y + Z0 * dY;
                    dZ = dYs + dZ1 * Y + Z1 * dY;

                    res[0, i] = dX;
                    res[1, i] = dZ;
                    res[2, i] = dY;

                } // V!=0
                // ...................................................................................
                // V==0

                else
                {
                    dV = (a[0, 0] * da[1, 2] + a[1, 2] * da[0, 0]) - (a[0, 2] * da[1, 0] + a[1, 0] * da[0, 2]);
                    dU1 = (a[0, 2] * da[1, 3] + a[1, 3] * da[0, 2]) - (a[0, 3] * da[1, 2] + a[1, 2] * da[0, 3]);
                    dU2 = (a[0, 2] * da[1, 1] + a[1, 1] * da[0, 2]) - (a[0, 1] * da[1, 2] + a[1, 2] * da[0, 1]);
                    dU3 = (a[0, 3] * da[1, 0] + a[1, 0] * da[0, 3]) - (a[0, 0] * da[1, 3] + a[1, 3] * da[0, 0]);
                    dU4 = (a[0, 1] * da[1, 0] + a[1, 0] * da[0, 1]) - (a[0, 0] * da[1, 1] + a[1, 1] * da[0, 0]);

                    dXs = (dU1 * V - U1 * dV) / Math.Pow(V, 2);
                    dY0 = (dU2 * V - U2 * dV) / Math.Pow(V, 2);
                    dYs = (dU3 * V - U3 * dV) / Math.Pow(V, 2);
                    dY1 = (dU4 * V - U4 * dV) / Math.Pow(V, 2);

                    db1 = dA1[i, 4] - dA1[i, 13] + dY0 * (A1[0, 3] - A1[0, 12]) + Y0 * (dA1[i, 3] - dA1[i, 12]) +
                         dY1 * (A1[0, 5] - A1[0, 14]) + Y1 * (dA1[i, 5] - dA1[i, 14]);
                    db2 = dXs * (A1[0, 3] - A1[0, 12]) + Xs * (dA1[i, 3] - dA1[i, 12]) +
                        dZs * (A1[0, 5] - A1[0, 14]) + Zs * (dA1[i, 5] - dA1[i, 14]) + dmk3;
                    db3 = 2 * Y0 * dY0 + 2 * Y1 * dY1;
                    db4 = 2 * (dY0 * (Xs - A1[0, 3]) + Y0 * (dXs - dA1[i, 3]) + dY1 * (Zs - A1[0, 5]) + Y1 * (dZs - dA1[i, 5]) - dA1[i, 5]);
                    db5 = 2 * (A1[0, 4] * dA1[i, 4] + (Xs - A1[0, 3]) * (dXs - dA1[i, 3]) + (Zs - A1[0, 5]) * (dZs - dA1[i, 5]));

                    dAA = 2 * b1 * db1 - 2 * Math.Pow(C, 2) * dA1[i, 2] * A1[0, 2] * b3 - Math.Pow((C * A1[0, 2]), 2) * db3;
                    dBB = 2 * b1 * db2 + 2 * b2 * db1 - 2 * Math.Pow(C, 2) * dA1[i, 2] * A1[0, 2] * b4 - Math.Pow((C * A1[0, 2]), 2) * db4;
                    dCC = 2 * b2 * db2 - 2 * Math.Pow(C, 2) * dA1[i, 2] * A1[0, 2] * b5 - Math.Pow((C * A1[0, 2]), 2) * db5;

                    dS = (BB * dBB - 2 * CC * dAA - 2 * AA * dCC) / S;

                    if (TipRes == 0)
                        dY = (-(AA * dBB - BB * dAA) - (AA * dS - S * dAA)) / (2 * Math.Pow(AA, 2));
                    else
                        dY = (-(AA * dBB - BB * dAA) + (AA * dS - S * dAA)) / (2 * Math.Pow(AA, 2));
                    dX = dXs + dZ0 * Y + Z0 * dY;
                    dZ = dYs + dZ1 * Y + Z1 * dY;

                    res[0, i] = dX;
                    res[1, i] = dY;
                    res[2, i] = dZ;

                } // V==0
                // ...................................................................................

            } // FOR1
            // FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1F

            return res;
        }
        // **************************************************************************** Func_Qa3RDM




    } // Class
} // Namespace
