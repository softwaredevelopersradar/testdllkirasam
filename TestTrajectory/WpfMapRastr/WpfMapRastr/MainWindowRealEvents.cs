﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;

using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

namespace WpfMapRastr
{
    // Events по принятию и посылке данных в реальном режиме от Егора
    public partial class MainWindow
    {

        // EVENT: get Coordinates from aeroscope *****************************************************
        // Подписка - в обработчике кнопки Source

        private void UDPReceiver2_OnGetCoordAero(object sender, UDPReceiver.Coord e)
        {
            //throw new NotImplementedException();

            //отправить через UDP на 3D
            if (uDPReceiver != null)
                uDPReceiver.SendAero(e);
            //uDPReceiver.

            // Пишем в лист кол-во последних точек, указанное в интерфейсе
            if (GlobalVarMapMain.objClassPointt1.Count < GlobalVarMapMain.numbp)
            {
                ClassPointt obb = new ClassPointt();

                obb.Latitude = e.latitude;
                obb.Longitude = e.longitude;
                obb.Altitude = e.altitude;
                GlobalVarMapMain.objClassPointt1.Add(obb);
            }
            else
            {
                GlobalVarMapMain.objClassPointt1.RemoveAt(0);

                ClassPointt obb = new ClassPointt();

                obb.Latitude = e.latitude;
                obb.Longitude = e.longitude;
                obb.Altitude = e.altitude;
                GlobalVarMapMain.objClassPointt1.Add(obb);
            }

            double ltvyx = 0;
            double lngvyx = 0;
            double altvyx = 0;
            //отправить на карту
            FucnPaintMap_Filtr(
                               GlobalVarMapMain.objClassPointt1,
                               GlobalVarMapMain.list_tracks_emulRDM1,
                               ref ltvyx,
                               ref lngvyx,
                               ref altvyx
                              );


        }
        // ***************************************************** EVENT: get Coordinates from aeroscope

        // EVENT: getCoordinates from RDM ************************************************************

        private void UDPReceiver2_OnGetCoordRDM(object sender, UDPReceiver.Coord e)
        {

            ClassTrackPoint objClassTrackPoint = new ClassTrackPoint();
            objClassTrackPoint.Latitude = (double)e.latitude;
            objClassTrackPoint.Longitude = (double)e.longitude;
            objClassTrackPoint.Altitude = (double)e.altitude;
            //objClassTrackPoint.timePoint = (double)e. / 1000;

            var pp = DateTime.Now;
            double time = (double)(pp.Second + pp.Minute * 60 + pp.Hour * 60 * 60);
            objClassTrackPoint.timePoint = time;

            f_Filter(
                     ref objClassTrackPoint,
                     ref GlobalVarMapMain.list_tracks_emulRDM1
                    );

            for (int i1 = 0; i1 < GlobalVarMapMain.list_tracks_emulRDM1.Count; i1++)
            {
                for (int i2 = 1; i2 < GlobalVarMapMain.list_tracks_emulRDM1[i1].listPointsTrajFiltr_I.Count; i2++)

                {
                    if (GlobalVarMapMain.list_tracks_emulRDM1[i1].listPointsTrajFiltr_I.Count > GlobalVarMapMain.numbp)
                    {
                        GlobalVarMapMain.list_tracks_emulRDM1[i1].listPointsTrajFiltr_I.RemoveAt(0);
                    }

                } // for2

            } // for1

            double ltvyx = 0;
            double lngvyx = 0;
            double altvyx = 0;
            //отправить на карту
            FucnPaintMap_Filtr(
                               GlobalVarMapMain.objClassPointt1,
                               GlobalVarMapMain.list_tracks_emulRDM1,
                               ref ltvyx,
                               ref lngvyx,
                               ref altvyx
                              );


            //отправить через UDP
            if (uDPReceiver != null)
                uDPReceiver.SendRDM(new UDPReceiver.Coord(ltvyx, lngvyx, (float)altvyx));


        }
        // ************************************************************ EVENT: getCoordinates from RDM

    } // Class
} // Namespace
