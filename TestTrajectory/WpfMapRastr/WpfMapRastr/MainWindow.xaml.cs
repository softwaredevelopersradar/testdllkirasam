﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientDataBase;

using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using System.IO;

using System.Windows.Data;

using System.Windows.Threading;
using System.Threading.Tasks;

// Otl
// Для YAML реализации
using YamlDotNet.Serialization;
// Для JSON реализации
//using System.Runtime.Serialization.Json;
//using System.Runtime.Serialization;
// Для реализации XML
//using System.Xml;
//using System.Xml.Linq;
//using ClassLibraryIniFiles;

using WpfMapRastrControl;
using WpfTasksControl;
using WpfAzimuthControl;
using ModelsTablesDBLib;
using Bearing;
using GeoCalculator;
using InheritorsEventArgs;
using ClassLibraryIniFiles;
using ControlSettingsMap;
using ADSBControl;
using AzimutTask;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassLibraryTrajectory;

// Semen
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;



using AeroScopeEmulator;
using Microsoft.Win32;
using System.Net;

using System.Globalization;

using YamlDotNet.Serialization;
using YamlDotNet;
using YamlReverseExpertise;

using fDRM;

namespace WpfMapRastr
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public WindowAzimuth objWindowAzimuth;
        public WindowTriang objWindowTriang;

        public ClassFunctionsMain objClassFunctionsMain;

        List<AeroScopeEmulator.Emulator.AeroScope> aeroScopes = new List<AeroScopeEmulator.Emulator.AeroScope>();
        List<AeroScopeEmulator.Emulator.AeroScope> rdmList = new List<AeroScopeEmulator.Emulator.AeroScope>();

        // ---------------------------------------------------------------------------------------------
        // Azimuth*
        // Для азимута (нажатие/отжатие птички)

        private bool isShowProp;
        public bool IsShowProp
        {
            get => isShowProp;
            set
            {
                if (value == isShowProp) return;
                isShowProp = value;
                OnPropertyChanged();
            }
        }

        // !!! Обработчик смотри в блоке азимута
        public event PropertyChangedEventHandler PropertyChanged;
        // ---------------------------------------------------------------------------------------------
        // Kirasa

        // Для отправки на 3D
        public UDPReceiver.UDPReceiver uDPReceiver;

        // Для получения данных в реальном режиме работы
        public UDPReceiver.UDPReceiver uDPReceiver2;

        // NationalCulture -> Получить разделитель дробных чисел
        public Char DecSep ;


        public bool istraj;
        public bool istrajRequest;
        public bool istrajRDM;
        public bool istrajRequestRDM;
        public int countAero;
        public int countRDM;


        // Constructor ********************************************************************************
        public MainWindow()
        {

            // ----------------------------------------------------------------------------------------
            InitializeComponent();
            // ----------------------------------------------------------------------------------------
            this.Loaded += MainWindow_Loaded;
            this.Closing += MainWindow_Closing;
            //this.Closed += MainWindow_Closed;
            // ----------------------------------------------------------------------------------------
            // Other windows

            objWindowAzimuth = new WindowAzimuth();
            GlobalVarMapMain.objWindowAzimuthG = objWindowAzimuth;

            objWindowTriang = new WindowTriang();
            GlobalVarMapMain.objWindowTriangG = objWindowTriang;

            objClassFunctionsMain = new ClassFunctionsMain();
            // ----------------------------------------------------------------------------------------
            // EVENTS

            // Click Button of Azimuth task (WpfTasksControl)
            //tasksCtrl.OnTaskAzimuth += ButtonAz;
            //tasksCtrl.OnTaskTriang += ButtonTriang;

            // Click Button "Calc" in Triangulation Control
            //objWindowTriang.tasksTriang.OnCalcTriang += ButtonCalcTriang;

            // Click Button "Clear" in Azimuth Control
            //objWindowAzimuth.tasksAzimuth.OnClearAzimuth += ButtonClearAzimuth;
            // Show azimuth title  near station
            //objWindowAzimuth.tasksAzimuth.OnCheckedShowAzimuth += CheckShowAzimuth;
            //objWindowAzimuth.tasksAzimuth.OnUnCheckedShowAzimuth += UnCheckShowAzimuth;

            // Mouse Click On the map
            mapCtrl.OnMouseCl += MapCtrl_OnMouseCl;
            ADSBCleanerEvent.OnCleanADSBTable += ADSBCleanerEvent_OnCleanADSBTable;

            //11_13new
            // Двойной клик по строке таблицы самолетов
            aDSBControl.OnSelectedRowADSB += new EventHandler<string>(ADSBControl_OnSelectedRowADSB);

            // ----------------------------------------------------------------------------------------
            // From YAML

            string s = "";

            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

            // File .yaml was read
            if (s == "")
            {
                //GlobalVarMapMain.IP_DB = initMapYaml.IP_DB;
                //GlobalVarMapMain.NumbPort_DB = initMapYaml.NumPort_DB;
                //GlobalVarMapMain.SysCoord = initMapYaml.SysCoord;
                //GlobalVarMapMain.FormatCoord = initMapYaml.FormatCoord;

                // To Property Grid
                SettingsMap.Settings = new ClassSettings
                {
                    // 0 - DD.dddddd
                    // 1 - DD MM.mmmmmm
                    // 2-  DD MM SS.ssssss
                    FormCoord = (FormatCoordinates)initMapYaml.FormatCoord,
                    IP_DB = initMapYaml.IP_DB,
                    NumPort = initMapYaml.NumPort_DB,
                    // 0 - WGS84degree
                    // 1 - SK42degree
                    // 2 - SK42_XY
                    // 3 - Mercator
                    SysCoord = (SystemCoordinates)initMapYaml.SysCoord,
                    Path = initMapYaml.Path,
                    PathMatr=initMapYaml.PathMatr,
                    // 1 - Gegraphic
                    // 0 - Mercator
                    TypeProjection= (TypesProjection)initMapYaml.TypeMap
                };

                // ..............................................................................
                // Azimuth* SysCoord

                AzimutTask.Enums.EnumSysCoord enumSysCoord=new AzimutTask.Enums.EnumSysCoord();
                if ((initMapYaml.SysCoord == 2) || (initMapYaml.SysCoord == 3))
                    enumSysCoord = AzimutTask.Enums.EnumSysCoord.XY;
                else
                {
                    if(initMapYaml.FormatCoord==0)
                    {
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.Degree;
                    }
                    else
                    {
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.DdMmSs;
                    }

                } // degree
                AzimutTaskCntr.ChangeSystemCoord(enumSysCoord);
                // ..............................................................................


            } // IF( Файл .yaml was read)

            // File .yaml wasn't read
            else
            {
                MessageBox.Show("Can't open .yaml file");

            } // ELSE (// File .yaml wasn't read)

            // ----------------------------------------------------------------------------------------
            GlobalVarMapMain.objMainWindowG = this;
            // ----------------------------------------------------------------------------------------
            // Azimuth*

            DataContext = this;
            // ----------------------------------------------------------------------------------------
            // NationalCulture -> Получить разделитель дробных чисел

            DecSep = Convert.ToChar(NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator);
            // ----------------------------------------------------------------------------------------

            istraj = false;
            istrajRequest = false;
            istrajRDM = false;
            istrajRequestRDM = false;
            countAero = 0;
            countRDM = 0;
            // ----------------------------------------------------------------------------------------
            // Загрузить цвета для траекторий

            LoadColors();
            // ----------------------------------------------------------------------------------------


            // FOR PAVEL ******************************************************************************
            // *Pavel
            // NEW

            // Ввод с интерфейса
            LoadParametersGlobal();
            LoadParametersRLS();

            // Загрузка файла установок >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dir3_3 = "";
            string sltlng3_3 = "";
            int m5_3 = 0;

            dir3_3 = dir + "\\FILES\\" + "fileParameters.txt";

            try
            {
                StreamReader sr3_3 = new StreamReader(dir3_3);

                do
                {
                    sltlng3_3 = sr3_3.ReadLine();

                    if (DecSep == '.')
                        sltlng3_3 = sltlng3_3.Replace(',', '.');
                    if (DecSep == ',')
                        sltlng3_3 = sltlng3_3.Replace('.', ',');

                    string[] words3_3 = sltlng3_3.Split(new char[] { ' ' });


                    // Parameters
                    TextBox1.Text = Convert.ToString(words3_3[0]);   // t ожидания источника
                    TextBox101.Text = Convert.ToString(words3_3[1]); // t ожидания ВО
                    TextBox2.Text = Convert.ToString(words3_3[2]);   // Vdop
                    TextBox3.Text = Convert.ToString(words3_3[3]);   // CKOApl
                    TextBox4.Text = Convert.ToString(words3_3[4]);   // CKOAh
                    TextBox10.Text = Convert.ToString(words3_3[5]);   // StartInd
                    TextBox11.Text = Convert.ToString(words3_3[6]);   // StopInd
                    TextBox6_1.Text = Convert.ToString(words3_3[7]);   // CKOXZ
                    TextBox6_2.Text = Convert.ToString(words3_3[8]);   // CKOXY
                    TextBox6_4.Text = Convert.ToString(words3_3[9]);   // MatrixDyn
                    TextBox6_5.Text = Convert.ToString(words3_3[10]);   // MatrixDynInit
                    TextBox6_6.Text = Convert.ToString(words3_3[11]);   // Err_t
                    TextBox6_3.Text = Convert.ToString(words3_3[12]);   // NumberPointsInit
                    TextBox6_7.Text = Convert.ToString(words3_3[13]);   // К умножения реальных dtau
                    TextBox6_8.Text = Convert.ToString(words3_3[14]);   // Отсеивание по высоте
                    TextBox12_mnim.Text = Convert.ToString(words3_3[15]);   // игнорирование/инвертирование мнимых точек
                    TextBox12_HInp.Text = Convert.ToString(words3_3[16]);   // замена рассчитанной высоты введенной


                } while (sltlng3_3 != null);
                sr3_3.Close();

            }
            catch
            {

            }



            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Загрузка файла установок


            // ****************************************************************************** FOR PAVEL

        }
        // ******************************************************************************** Constructor

        // DoubleClickADSB ****************************************************************************
        // Обработчик двойного клика мыши на строке таблицы ADSB

        /*
                private void ADSBControl_OnSelectedRowADSB(object sender, string e)
                {
                    // НЕ пустая строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    if (e != "")
                    {
                        // ----------------------------------------------------------------------------------------
                        // Выделение оранжевым либо сброс обратно в серый

                        int index = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.ICAO == e));

                        if (index >= 0)
                        {
                            if (
                                (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 0) &&
                                //&&&
                                (GlobalVarMapMain.flairone == false)
                               )

                            {
                                mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 1;
                                //&&&
                                GlobalVarMapMain.flairone = true;
                                GlobalVarMapMain.ICAO_LAST = mapCtrl.objClassInterfaceMap.List_AP[index].ICAO;
                            }
                            else
                            {
                                mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 0;
                                //&&&
                                if (mapCtrl.objClassInterfaceMap.List_AP[index].ICAO == GlobalVarMapMain.ICAO_LAST)
                                {
                                    GlobalVarMapMain.flairone = false;
                                    GlobalVarMapMain.ICAO_LAST = "";
                                }
                            }
                            // ----------------------------------------------------------------------------------------
                            // Центровка

                            if (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 1)
                            {
                                // ....................................................................................
                                bool bvar = false;
                                double x = 0;
                                double y = 0;
                                double lat = 0;
                                double lon = 0;
                                // ....................................................................................
                                lat = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Latitude;
                                lon = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Longitude;
                                // ....................................................................................
                                var p = Mercator.FromLonLat(lon, lat);
                                x = p.X;
                                y = p.Y;
                                // ....................................................................................
                                // MercatorMap or GeographicMap?

                                string s = "";
                                InitMapYaml initMapYaml = new InitMapYaml();
                                s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                                // ....................................................................................
                                // MercatorMap

                                if (initMapYaml.TypeMap == 0)
                                {
                                    // Center map on pozition XY (Mercator)
                                    bvar = mapCtrl.objClassInterfaceMap.CenterMapToXY(x, y);
                                } // MercatorMap
                                  // ....................................................................................
                                  // GeographicMap

                                else
                                {
                                    // Center map on pozition LatLong (degree)
                                    bvar = mapCtrl.objClassInterfaceMap.CenterMapToLatLong(lat, lon);
                                } // GeographicMap
                                  // ....................................................................................

                            } // Выделили самолет оранжевым
                              // ----------------------------------------------------------------------------------------
                              // Clear and redraw

                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });
                            // ----------------------------------------------------------------------------------------

                        } // index>=0

                    } // НЕ пустая строка
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! НЕ пустая строка

                    // ПУСТАЯ строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    else
                    {
                        int index1 = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.flColor == 1));

                        if(index1>=0)
                        {
                            mapCtrl.objClassInterfaceMap.List_AP[index1].flColor = 0;
                            //&&&
                            if (mapCtrl.objClassInterfaceMap.List_AP[index1].ICAO == GlobalVarMapMain.ICAO_LAST)
                            {
                                GlobalVarMapMain.flairone = false;
                                GlobalVarMapMain.ICAO_LAST = "";
                            }

                            // Clear and redraw
                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });

                        } // index1>=0

                    } // ПУСТАЯ строка
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ПУСТАЯ строка


                }
        */
        // **************************************************************************** DoubleClickADSB


        // DoubleClickADSB ****************************************************************************
        // Обработчик двойного клика мыши на строке таблицы ADSB

        private void ADSBControl_OnSelectedRowADSB(object sender, string e)
                {
                    // НЕ пустая строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    if (e != "")
                    {
                        // ----------------------------------------------------------------------------------------
                        // Выделение оранжевым либо сброс обратно в серый

                        int index = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.ICAO == e));

                        if (index >= 0)
                        {
                    // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Клик на НЕвыделенной строке

                    if (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 0)
                    {
                        //.................................................................................
                        if (
                             (GlobalVarMapMain.flairone == false) // Нет выделенных строк
                           )
                        {
                            mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 1;
                            //&&&
                            GlobalVarMapMain.flairone = true;
                            GlobalVarMapMain.ICAO_LAST = mapCtrl.objClassInterfaceMap.List_AP[index].ICAO;
                        }
                        //.................................................................................
                        // Уже была выделенная строка

                        else
                        {
                            // Старую снимаем
                            int index2 = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.flColor == 1));
                            if(index2>=0)
                            {
                                mapCtrl.objClassInterfaceMap.List_AP[index2].flColor = 0;
                                if (mapCtrl.objClassInterfaceMap.List_AP[index2].ICAO == GlobalVarMapMain.ICAO_LAST)
                                {
                                    GlobalVarMapMain.flairone = false;
                                    GlobalVarMapMain.ICAO_LAST = "";
                                }
                            } // index2>=0

                            // Новую устанавливаем
                            mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 1;
                            GlobalVarMapMain.flairone = true;
                            GlobalVarMapMain.ICAO_LAST = mapCtrl.objClassInterfaceMap.List_AP[index].ICAO;

                        } // Уже была выделенная строка
                        //.................................................................................

                    } // Клик на НЕвыделенной строке
                    // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                    // Повторный клик для сброса в серый

                    else
                    {
                        mapCtrl.objClassInterfaceMap.List_AP[index].flColor = 0;
                        //&&&
                        if (mapCtrl.objClassInterfaceMap.List_AP[index].ICAO == GlobalVarMapMain.ICAO_LAST)
                        {
                            GlobalVarMapMain.flairone = false;
                            GlobalVarMapMain.ICAO_LAST = "";
                        }
                    }
                          // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

                    // ----------------------------------------------------------------------------------------
                    // Центровка

                    if (mapCtrl.objClassInterfaceMap.List_AP[index].flColor == 1)
                            {
                                // ....................................................................................
                                bool bvar = false;
                                double x = 0;
                                double y = 0;
                                double lat = 0;
                                double lon = 0;
                                // ....................................................................................
                                lat = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Latitude;
                                lon = mapCtrl.objClassInterfaceMap.List_AP[index].Coordinates.Longitude;
                                // ....................................................................................
                                var p = Mercator.FromLonLat(lon, lat);
                                x = p.X;
                                y = p.Y;
                                // ....................................................................................
                                // MercatorMap or GeographicMap?

                                string s = "";
                                InitMapYaml initMapYaml = new InitMapYaml();
                                s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                                // ....................................................................................
                                // MercatorMap

                                if (initMapYaml.TypeMap == 0)
                                {
                                    // Center map on pozition XY (Mercator)
                                    bvar = mapCtrl.objClassInterfaceMap.CenterMapToXY(x, y);
                                } // MercatorMap
                                  // ....................................................................................
                                  // GeographicMap

                                else
                                {
                                    // Center map on pozition LatLong (degree)
                                    bvar = mapCtrl.objClassInterfaceMap.CenterMapToLatLong(lat, lon);
                                } // GeographicMap
                                  // ....................................................................................

                            } // Выделили самолет оранжевым
                              // ----------------------------------------------------------------------------------------
                              // Clear and redraw

                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });
                            // ----------------------------------------------------------------------------------------

                        } // index>=0

                    } // НЕ пустая строка
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! НЕ пустая строка

                    // ПУСТАЯ строка !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    else
                    {
                        int index1 = mapCtrl.objClassInterfaceMap.List_AP.FindIndex(x => (x.flColor == 1));

                        if(index1>=0)
                        {
                            mapCtrl.objClassInterfaceMap.List_AP[index1].flColor = 0;
                            //&&&
                            if (mapCtrl.objClassInterfaceMap.List_AP[index1].ICAO == GlobalVarMapMain.ICAO_LAST)
                            {
                                GlobalVarMapMain.flairone = false;
                                GlobalVarMapMain.ICAO_LAST = "";
                            }

                            // Clear and redraw
                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });

                        } // index1>=0

                    } // ПУСТАЯ строка
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ПУСТАЯ строка


                }
        
        // **************************************************************************** DoubleClickADSB







        /*
                // -------------------------------------------------------------------------------------------------
                bool bvar = false;
                double x = 0;
                double y = 0;
                double lat = 0;
                double lon = 0;
                int i = 0;
                    // -------------------------------------------------------------------------------------------------
                    // Otl
                    // Для отладки здесь пока зададим конкретные координаты (из прошлого проекта)
                    // Mercator/degree

                    // Otl
                    //lat = 53.78746;
                    //lon = 25.71507;
                    // 19.07
                    //objClassInterfaceMap.List_JS[0].ISOwn = true;

                    for (i=0; i<objClassInterfaceMap.List_JS.Count; i++ )
                    {
                        if(objClassInterfaceMap.List_JS[i].ISOwn==true)
                        {
                            lat = objClassInterfaceMap.List_JS[i].Coordinates.Latitude;
                            lon = objClassInterfaceMap.List_JS[i].Coordinates.Longitude;
                            i = objClassInterfaceMap.List_JS.Count + 1;
                        }
        }

        var p = Mercator.FromLonLat(lon, lat);
        x = p.X;
                    y = p.Y;
                    // -------------------------------------------------------------------------------------------------
                    // MercatorMap or GeographicMap?

                    string s = "";
        InitMapYaml initMapYaml = new InitMapYaml();
        s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                    // -------------------------------------------------------------------------------------------------
                    // MercatorMap

                    if(initMapYaml.TypeMap==0)
                    {
                        // Center map on pozition XY (Mercator)
                        bvar = objClassInterfaceMap.CenterMapToXY(x, y);
                    } // MercatorMap
                    // -------------------------------------------------------------------------------------------------
                    // GeographicMap

                    else
                    {
                        // Center map on pozition LatLong (degree)
                        bvar = objClassInterfaceMap.CenterMapToLatLong(lat, lon);
                    } // GeographicMap
                    // -------------------------------------------------------------------------------------------------
                    if (bvar == false)
                    {
                        MessageBox.Show("Can't center map on this pozition");
                        return;

                    } // The base scale has been set
                    // -------------------------------------------------------------------------------------------------
        */


        // AZIMUTH ************************************************************************************
        // Azimuth*
        // BLOCK FOR AZIMUTH

        // ShowAzimutChange ***************************************************************************
        // Обработка изменения состояния пртички отображения азимута около станций

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

            //mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;
            if (AzimutTaskCntr.IsShowValue == true)
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = false;
            else
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = true;

            // !!! Расчет идет в перерисовке азимута
            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
        }
        // *************************************************************************** ShowAzimutChange

        // ChangeItemInTabControl *********************************************************************
        // Переход на вкладку в NabControl задач

        private void OnTabItemChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tabControl = sender as TabControl; // e.Source could have been used instead of sender as well
            TabItem item = tabControl.SelectedValue as TabItem;
            if (item.Name == "TabCtrlTasksIt1")
            {
                mapCtrl.objClassInterfaceMap.flAzimuth = true;
            }
            else
            {
                mapCtrl.objClassInterfaceMap.flAzimuth = false;
            }

        }
        // *********************************************************************** ChangeItemInTabControl

        // !!! См. еще клик мыши на карте

        // ButtonClear **********************************************************************************

        private void ButtonOnClear(object sender, EventArgs e)
        {
            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true)&&(mapCtrl.objClassInterfaceMap.ShowJS==true))
            {
                objClassFunctionsMain.ClearAzimuth();
            }
        }
        // *********************************************************************************** ButtonClear

        // ButtonExecute *********************************************************************************
        // Выполнить, если координаты источника ввели вручную

        private void ButtonOnExecute(object sender, AzimutTask.Models.ICoord e)
        {

            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
            {
                // !!! Здесь нужно занести координаты источника из окошек Таниного контрола
                // ?????????????????????
                //mapCtrl.objClassInterfaceMap.LatSource_Azimuth=
                //mapCtrl.objClassInterfaceMap.LongSource_Azimuth =
                //mapCtrl.objClassInterfaceMap.HSource_Azimuth =
                //int yy = (int)(e as AzimutTask.Models.CoordXY).X;
                //AzimutTask.Models mmm = new AzimutTask.Models();
                //var pp = e as AzimutTask.Models.CoordXY;

                //objClassFunctionsMain.ReadCurrentCoordinatesAzimuth();

                AzimutTask.Models.CoordXY modelXY = new AzimutTask.Models.CoordXY();
                AzimutTask.Models.CoordDegree modelDegree = new AzimutTask.Models.CoordDegree();
                AzimutTask.Models.CoordDdMmSs modelDdMmSs = new AzimutTask.Models.CoordDdMmSs();

                if (e.GetType() == typeof(AzimutTask.Models.CoordXY))
                {
                    //AzimutTask.Models.CoordXY modelXY = e as AzimutTask.Models.CoordXY;
                    modelXY = e as AzimutTask.Models.CoordXY;
                }
                else if (e.GetType() == typeof(AzimutTask.Models.CoordDegree))
                {
                    //AzimutTask.Models.CoordDegree modelDegree = e as AzimutTask.Models.CoordDegree;
                    modelDegree = e as AzimutTask.Models.CoordDegree;
                }
                else if (e.GetType() == typeof(AzimutTask.Models.CoordDdMmSs))
                {
                    //AzimutTask.Models.CoordDdMmSs modelDdMmSs = e as AzimutTask.Models.CoordDdMmSs;
                    modelDdMmSs = e as AzimutTask.Models.CoordDdMmSs;

                }

                objClassFunctionsMain.ReadCurrentCoordinatesAzimuth(modelXY, modelDegree, modelDdMmSs);

                // ----------------------------------------------------------------------------------------
                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;

                // !!! Расчет идет в перерисовке азимута
                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                // ----------------------------------------------------------------------------------------

            } // if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
        }
        // ******************************************************************************* ButtonExecute


        // ************************************************************************************ AZIMUTH

        // PropertyGridSettings ***********************************************************************
        /// <summary>
        /// New settings from control SettingsMap
        /// </summary>
        public void UpdateSettings(object sender, ControlSettingsMap.ClassSettings settings)
        {
            try
            {
                // ------------------------------------------------------------------------------------
                // Write to YAML

                string s = "";

                InitMapYaml initMapYaml = new InitMapYaml();
                s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);
                // ...........................................................................
                // File .yaml was read

                if (s == "")
                {
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // If Map type was changed, close map and open new map
                    // 999

                    if(initMapYaml.TypeMap!=((byte)SettingsMap.Settings.TypeProjection))
                    {
                        mapCtrl.CloseMap();
                        mapCtrl.CloseMatrix();
                        mapCtrl.OpenMap(SettingsMap.Settings.Path + "\\1_1-1.tif");
                        //mapCtrl.OpenMatrix(SettingsMap.Settings.Path + "\\");
                    }
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    initMapYaml.IP_DB = SettingsMap.Settings.IP_DB;
                    initMapYaml.NumPort_DB = SettingsMap.Settings.NumPort;
                    initMapYaml.SysCoord = (byte)SettingsMap.Settings.SysCoord;
                    initMapYaml.FormatCoord = (byte)SettingsMap.Settings.FormCoord;
                    //initMapYaml.Path = SettingsMap.Settings.Path+"\\1_1-1.tif";
                    initMapYaml.Path = SettingsMap.Settings.Path;
                    initMapYaml.PathMatr = SettingsMap.Settings.PathMatr;
                    initMapYaml.TypeMap = (byte)SettingsMap.Settings.TypeProjection;


                    // ..............................................................................
                    // Azimuth* SysCoord

                    AzimutTask.Enums.EnumSysCoord enumSysCoord = new AzimutTask.Enums.EnumSysCoord();
                    if ((initMapYaml.SysCoord == 2) || (initMapYaml.SysCoord == 3))
                        enumSysCoord = AzimutTask.Enums.EnumSysCoord.XY;
                    else
                    {
                        if (initMapYaml.FormatCoord == 0)
                        {
                            enumSysCoord = AzimutTask.Enums.EnumSysCoord.Degree;
                        }
                        else
                        {
                            enumSysCoord = AzimutTask.Enums.EnumSysCoord.DdMmSs;
                        }

                    } // degree
                    AzimutTaskCntr.ChangeSystemCoord(enumSysCoord);

                    // ..............................................................................

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // 19.07 GNSS
                    // Change SK in GNSS

                    long ichislo1 = 0;
                    double dchislo1 = 0;
                    long ichislo2 = 0;
                    double dchislo2 = 0;
                    double xxx = 0;
                    double yyy = 0;

                    if ((GlobalVarMapMain.GNSS_Lat != -1) && (GlobalVarMapMain.GNSS_Long != -1))
                    {
                        // WGS 84
                        if (initMapYaml.SysCoord == 0)
                        {
                            // Latitude
                            ichislo1 = (long)(GlobalVarMapMain.GNSS_Lat * 1000000);
                            dchislo1 = ((double)ichislo1) / 1000000;
                            // Longitude
                            ichislo2 = (long)(GlobalVarMapMain.GNSS_Long * 1000000);
                            dchislo2 = ((double)ichislo2) / 1000000;

                            mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + 
                                                         "W " + Convert.ToString(dchislo2) + '\xB0' +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                        } // WGS84

                        // SK42,m
                        else if (initMapYaml.SysCoord == 2)
                        {
                            ClassGeoCalculator.f_WGS84_Krug(GlobalVarMapMain.GNSS_Lat,
                                                            GlobalVarMapMain.GNSS_Long, 
                                                            ref xxx, ref yyy);

                            mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx) + " m" + "   " + "Y " + Convert.ToString((int)yyy) + " m" +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;
                        } // SK42,m

                        // SK42grad
                        else if (initMapYaml.SysCoord == 1)
                        {
                            double xxx1 = 0;
                            double yyy1 = 0;

                            ClassGeoCalculator.f_WGS84_SK42_BL(GlobalVarMapMain.GNSS_Lat,
                                                               GlobalVarMapMain.GNSS_Long,
                                                               ref xxx1, ref yyy1);

                            // Latitude
                            ichislo1 = (long)(xxx1 * 1000000);
                            dchislo1 = ((double)ichislo1) / 1000000;
                            // Longitude
                            ichislo2 = (long)(yyy1 * 1000000);
                            dchislo2 = ((double)ichislo2) / 1000000;

                            mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " +
                                                         "W " + Convert.ToString(dchislo2) + '\xB0' +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " +
                                                         GlobalVarMapMain.GNSS_time;

                        } //SK42grad

                        // Mercator,m
                        else if (initMapYaml.SysCoord == 3)
                        {
                            double xxx2 = 0;
                            double yyy2 = 0;

                            ClassGeoCalculator.f_WGS84_Mercator(GlobalVarMapMain.GNSS_Lat,
                                                                GlobalVarMapMain.GNSS_Long,
                                                                ref xxx2, ref yyy2);

                            mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx2) + " m" + "   " + "Y " + Convert.ToString((int)yyy2) + " m" +
                                                 "   " + Convert.ToString(GlobalVarMapMain.GNSS_Numb_Sat) + "   " + GlobalVarMapMain.GNSS_time;

                        } // Mercator,m


                    } // Lat,Long!=-1
                      // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      // Пишем класс обратно в файл

                    try
                    {
                        ClassYaml.WriteYaml(
                                            "Setting.yaml",
                                            initMapYaml
                                           );

                        // Change SystemCoordinates On the Map (Отображение внизу карты)
                        mapCtrl.DrawCurrentCoordinates();

                        // Azimuth*
                        // !!! Здесь нужно занести координаты источника в окошки Таниного контрола
                        objClassFunctionsMain.DrawCurrentCoordinatesAzimuth();

                    }
                    catch
                    {
                        MessageBox.Show("Can't write .yaml file");
                    }

                } // IF( Файл .yaml was read)
                  // ...........................................................................
                  // File .yaml wasn't read

                else
                {
                    MessageBox.Show("Can't read .yaml file");

                } // ELSE (// File .yaml wasn't read)
                  // ...........................................................................

                // ------------------------------------------------------------------------------------

            } // try get data from PropertyGrid

            catch
            {
                MessageBox.Show("Can't get data");
            }

        }
        // *********************************************************************** PropertyGridSettings

        // Mouse Click On the map *********************************************************************
        // 666
        private void MapCtrl_OnMouseCl(object sender, ClassArg e)
        {

            // All_Part *******************************************************************************
            long ichislo = 0;
            double dchislo = 0;

            //  WGS84
            GlobalVarMapMain.LAT_Rastr = e.lat;
            GlobalVarMapMain.LONG_Rastr = e.lon;
            GlobalVarMapMain.H_Rastr = e.h;
            // Mercator
            GlobalVarMapMain.X_Rastr = e.x;
            GlobalVarMapMain.Y_Rastr = e.y;
            // MGRS
            GlobalVarMapMain.Str_Rastr = e.str;

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string sall = "";
            // ******************************************************************************* All_Part

            // Triang *********************************************************************************

            if (GlobalVarMapMain.flTriang == true)
            {

                // ..................................................................................
                // Pel1

                if ((GlobalVarMapMain.flPeleng1==false)&&(GlobalVarMapMain.flPeleng2 == false))
                {

                    GlobalVarMapMain.flPeleng1 = true;

                    // Latitude
                    ichislo = (long)(e.lat * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    objWindowTriang.tasksTriang.textBox1.Text = "N " + Convert.ToString(dchislo) + '\xB0';
                    GlobalVarMapMain.LatPel1 = dchislo;

                    // Longitude
                    ichislo = (long)(e.lon * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    objWindowTriang.tasksTriang.textBox2.Text = "W " + Convert.ToString(dchislo) + '\xB0';
                    GlobalVarMapMain.LonPel1 = dchislo;

                    sall = mapCtrl.DrawImage(
                                                 GlobalVarMapMain.LAT_Rastr,
                                                 GlobalVarMapMain.LONG_Rastr,
                                                 // Path to image
                                                 dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );


                } // (GlobalVarMapMain.flPeleng1==false)&&(GlobalVarMapMain.flPeleng1 == false)
                  // ..................................................................................
                 // Pel2

                else if ((GlobalVarMapMain.flPeleng1 == true) && (GlobalVarMapMain.flPeleng2 == false))
                {
                    GlobalVarMapMain.flPeleng2 = true;

                    // Latitude
                    ichislo = (long)(e.lat * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    objWindowTriang.tasksTriang.textBox3.Text = "N " + Convert.ToString(dchislo) + '\xB0';
                    GlobalVarMapMain.LatPel2 = dchislo;

                    // Longitude
                    ichislo = (long)(e.lon * 1000000);
                    dchislo = ((double)ichislo) / 1000000;
                    objWindowTriang.tasksTriang.textBox4.Text = "W " + Convert.ToString(dchislo) + '\xB0';
                    GlobalVarMapMain.LonPel2 = dchislo;

                    sall = mapCtrl.DrawImage(
                                                 GlobalVarMapMain.LAT_Rastr,
                                                 GlobalVarMapMain.LONG_Rastr,
                                                 // Path to image
                                                 dir + "\\Images\\OtherObject\\" + "SquareRed.png",
                                                 // Text
                                                 "",
                                                 // Scale
                                                 0.6
                                      );



                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    // Otl
                    // !!! Проверка функции расстояния между точками с учетом кривизны Земли
                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                } // (GlobalVarMapMain.flPeleng1==true)&&(GlobalVarMapMain.flPeleng1 == false)
                  // ..................................................................................

                else
                {
                    MessageBox.Show("Coordinates are entered");
                    return;
                }
                // ..................................................................................


            } // GlobalVarMapMain.flTriang == true
              // ******************************************************************************* Triang

            // Azimuth ********************************************************************************
            // Azimuth*

            if ((mapCtrl.objClassInterfaceMap.flAzimuth == true) && (mapCtrl.objClassInterfaceMap.ShowJS == true))
            {
                // Source coordinates
                mapCtrl.objClassInterfaceMap.LatSource_Azimuth = GlobalVarMapMain.LAT_Rastr;
                mapCtrl.objClassInterfaceMap.LongSource_Azimuth = GlobalVarMapMain.LONG_Rastr;
                mapCtrl.objClassInterfaceMap.HSource_Azimuth = GlobalVarMapMain.H_Rastr;

                // !!! Здесь нужно занести координаты источника в окошки Таниного контрола
                objClassFunctionsMain.DrawCurrentCoordinatesAzimuth();

                mapCtrl.objClassInterfaceMap.CheckShowAzimuth = AzimutTaskCntr.IsShowValue;

                // !!! Расчет идет в перерисовке азимута
                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();

            }
            // ------------------------------------------------------------------------------------------


            /*

                        if (GlobalVarMapMain.flAzimuth == true)
                        {
                        // ----------------------------------------------------------------------------------------
                         objClassFunctionsMain.ClearAzimuth();
                        // ----------------------------------------------------------------------------------------
                        // Latitude
                        ichislo = (long)(e.lat * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt1.Text = "N " + Convert.ToString(dchislo) + '\xB0';

                        // Longitude
                        ichislo = (long)(e.lon * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt3.Text = "W " + Convert.ToString(dchislo) + '\xB0';

                        // Altitude
                        ichislo = (long)(e.h * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        objWindowAzimuth.tasksAzimuth.Txt5.Text = Convert.ToString(dchislo) + " m";

                        objWindowAzimuth.tasksAzimuth.Txt7.Text = string.Format("    MGRS: {0}", e.str);
                        // ----------------------------------------------------------------------------------------

                         // ----------------------------------------------------------------------------------------

                            double az = 0;
                            string s3 = "";

                            for (int i = 0; i < mapCtrl.objClassInterfaceMap.List_JS.Count; i++)
                            {

                                az = mapCtrl.CalcAzimuth(mapCtrl.objClassInterfaceMap.List_JS[i].Coordinates.Latitude, mapCtrl.objClassInterfaceMap.List_JS[i].Coordinates.Longitude,
                                                         GlobalVarMapMain.LAT_Rastr, GlobalVarMapMain.LONG_Rastr);

                                ichislo = (long)(az * 1000);
                                dchislo = ((double)ichislo) / 1000;
                                az = dchislo;
                                GlobalVarMapMain.listAzimuth.Add(az);


                            } // for

                            mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                                         GlobalVarMapMain.flAzimuth,
                                                                         GlobalVarMapMain.CheckShowAzimuth,
                                                                         GlobalVarMapMain.LAT_Rastr,
                                                                         GlobalVarMapMain.LONG_Rastr,
                                                                         GlobalVarMapMain.listAzimuth
                                                                         );

                            // ----------------------------------------------------------------------------------------

                        } // GlobalVarMapMain.flAzimuth == true
            */
            // ******************************************************************************** Azimuth

        }
        // ********************************************************************* Mouse Click On the map

        // Click Button of Azimuth task (WpfTasksControl) *******************************************************
        private void ButtonAz(object sender, EventArgs e)
        {
/*
            // -------------------------------------------------------------------------------------
            if (objWindowAzimuth == null)
            {
                objWindowAzimuth = new WindowAzimuth();
                objWindowAzimuth.Owner = this;
                GlobalVarMapMain.objWindowAzimuthG = objWindowAzimuth;
                objWindowAzimuth.Show();

                //ClassMapRastrClear.f_Clear_FormAz1();
            }
            else
            {
                objWindowAzimuth.Show();
            }

            GlobalVarMapMain.flAzimuth = true;
            // -------------------------------------------------------------------------------------

            // Otl ---------------------------------------------------------------------------------
            // --------------------------------------------------------------------------------- Otl
*/
        }
        // ******************************************************* Click Button of Azimuth task (WpfTasksControl)

        // Show azimuth title near station **********************************************************************

        private void CheckShowAzimuth(object sender, EventArgs e)
        {
/*
            GlobalVarMapMain.CheckShowAzimuth = true;

            mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                         GlobalVarMapMain.flAzimuth,
                                                         GlobalVarMapMain.CheckShowAzimuth,
                                                         GlobalVarMapMain.LAT_Rastr,
                                                         GlobalVarMapMain.LONG_Rastr,
                                                         GlobalVarMapMain.listAzimuth
                                                         );

        }
        private void UnCheckShowAzimuth(object sender, EventArgs e)
        {
            GlobalVarMapMain.CheckShowAzimuth = false;

            mapCtrl.objClassMapRastrReDraw.ReDrawAzimuth(
                                                         GlobalVarMapMain.flAzimuth,
                                                         GlobalVarMapMain.CheckShowAzimuth,
                                                         GlobalVarMapMain.LAT_Rastr,
                                                         GlobalVarMapMain.LONG_Rastr,
                                                         GlobalVarMapMain.listAzimuth
                                                         );
*/
        }

        // ********************************************************************** Show azimuth title near station


        // Click Button "Clear" in Azimuth Control **************************************************************

        public void ButtonClearAzimuth(object sender, EventArgs e)

        {
            objClassFunctionsMain.ClearAzimuth();
        }
        // ************************************************************** Click Button "Clear" in Azimuth Control

        // Click Button of Triangulation task (WpfTasksControl) *************************************************
        private void ButtonTriang(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------------------
            if (objWindowTriang == null)
            {
                objWindowTriang = new WindowTriang();
                objWindowTriang.Owner = this;
                GlobalVarMapMain.objWindowTriangG = objWindowTriang;
                objWindowTriang.Show();
            }
            else
            {
                objWindowTriang.Show();
            }
            // -------------------------------------------------------------------------------------------------
            GlobalVarMapMain.flTriang = true;
            // -------------------------------------------------------------------------------------------------

        }
        // ************************************************* Click Button of Triangulation task (WpfTasksControl)

        // Click Button "Calc" in Triangulation Control *********************************************************

        private void ButtonCalcTriang(object sender, EventArgs e)
        {
            objWindowTriang.Owner = this;
            objWindowTriang.Show();
            GlobalVarMapMain.flTriang = true;

            // Otl ---------------------------------------------------------------------------------------------
            // --------------------------------------------------------------------------------------------- Otl

            // Otl1 ---------------------------------------------------------------------------------------------
            double a1 = 0;
            double b1 = 0;
            double c1 = 0;
            double d1 = 0;
            double f1 = 0;
            double g1 = 0;
            double a2 = 0;
            double b2 = 0;
            double c2 = 0;
            double d2 = 0;
            double f2 = 0;
            double g2 = 0;

            GlobalVarMapMain.Pel1Grad = Convert.ToDouble(objWindowTriang.tasksTriang.textBox5.Text);
            GlobalVarMapMain.Pel2Grad = Convert.ToDouble(objWindowTriang.tasksTriang.textBox6.Text);

            double[] arr_Pel1 = new double[GlobalVarMapMain.numberofdots*2+1];     // R,Широта,долгота
            double[] arr_Pel2 = new double[GlobalVarMapMain.numberofdots * 2+1];     // R,Широта,долгота
            double[] arr_Pel_XYZ1 = new double[GlobalVarMapMain.numberofdots * 3+1];
            double[] arr_Pel_XYZ2 = new double[GlobalVarMapMain.numberofdots * 3+1];


            ClassBearing.f_Bearing(
                                   GlobalVarMapMain.Pel1Grad,
                                   GlobalVarMapMain.distance,
                                   GlobalVarMapMain.numberofdots,
                                   GlobalVarMapMain.LatPel1,
                                   GlobalVarMapMain.LonPel1,
                                   1,  // WGS84
                                   ref a1, ref b1, ref c1,
                                   ref d1, ref f1, ref g1,
                                   ref arr_Pel1,
                                   ref arr_Pel_XYZ1
                                   );

            ClassBearing.f_Bearing(
                                   GlobalVarMapMain.Pel2Grad,
                                   GlobalVarMapMain.distance,
                                   GlobalVarMapMain.numberofdots,
                                   GlobalVarMapMain.LatPel2,
                                   GlobalVarMapMain.LonPel2,
                                   1,  // WGS84
                                   ref a2, ref b2, ref c2,
                                   ref d2, ref f2, ref g2,
                                   ref arr_Pel2,
                                   ref arr_Pel_XYZ2
                                   );
            // .................................................................................................
            // Peleng1

            List<Mapsui.Geometries.Point> pointPel1 = new List<Mapsui.Geometries.Point>();
            int i1 = 0;

            pointPel1.Add(new Mapsui.Geometries.Point(GlobalVarMapMain.LonPel1, GlobalVarMapMain.LatPel1));

            for (i1=0;i1< GlobalVarMapMain.numberofdots * 2;i1+=2)
            {
                pointPel1.Add(new Mapsui.Geometries.Point(arr_Pel1[i1+1], arr_Pel1[i1]));

            }

            string s1 = mapCtrl.DrawLinesLatLong(
                                                // Degree
                                                pointPel1,
                                                // Color
                                                100,
                                                255,
                                                0,
                                                0
                                               );

            // .................................................................................................
            // Peleng2
            List<Mapsui.Geometries.Point> pointPel2 = new List<Mapsui.Geometries.Point>();
            int i2 = 0;

            pointPel2.Add(new Mapsui.Geometries.Point(GlobalVarMapMain.LonPel2, GlobalVarMapMain.LatPel2));

            for (i2 = 0; i2 < GlobalVarMapMain.numberofdots * 2; i2 += 2)
            {
                pointPel2.Add(new Mapsui.Geometries.Point(arr_Pel2[i2 + 1], arr_Pel2[i2 ]));

            }

            string s2 = mapCtrl.DrawLinesLatLong(
                                                // Degree
                                                pointPel2,
                                                // Color
                                                100,
                                                255,
                                                0,
                                                0
                                               );
            // .................................................................................................
            // Triangulation1
            // .................................................................................................
            // Triangulation2

            Coord JamStObj = new Coord();

            List<JamBearing> JamSt = new List<JamBearing>();

            JamBearing objJamBearing = new JamBearing();
            objJamBearing.Coordinate = new Coord();
            objJamBearing.Bearing = (float)GlobalVarMapMain.Pel1Grad;
            objJamBearing.Coordinate.Latitude = GlobalVarMapMain.LatPel1;
            objJamBearing.Coordinate.Longitude = GlobalVarMapMain.LonPel1;
            JamSt.Add(objJamBearing);

            JamBearing objJamBearing1 = new JamBearing();
            objJamBearing1.Coordinate = new Coord();
            objJamBearing1.Bearing = (float)GlobalVarMapMain.Pel2Grad;
            objJamBearing1.Coordinate.Latitude = GlobalVarMapMain.LatPel2;
            objJamBearing1.Coordinate.Longitude = GlobalVarMapMain.LonPel2;
            JamSt.Add(objJamBearing1);

            JamStObj=ClassBearing.DefineCoordTriang(JamSt);

            // Get current dir
            string dir1 = "";
            dir1 = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if ((JamStObj.Latitude!=-1) &&(JamStObj.Longitude!=-1))
            {
                string s5 = mapCtrl.DrawImage(
                                             JamStObj.Latitude,
                                             JamStObj.Longitude,
                                             // Path to image
                                             dir1 + "\\Images\\OtherObject\\" + "tr10.png",
                                             // Text
                                             "",
                                             // Scale
                                             0.6
                                  );
            }
            // .................................................................................................

            // --------------------------------------------------------------------------------------------- Otl1

        }
     // ********************************************************* Click Button "Calc" in Triangulation Control

     // Loaded ************************************************************************************************   
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
         {
            //MessageBox.Show("Load");
            objWindowAzimuth.Owner = this;
            objWindowTriang.Owner = this;


            //CultureInfo current = System.Threading.Thread.CurrentThread.CurrentUICulture;
            //if (current.TwoLetterISOLanguageName != "fr")
            //{
            //    CultureInfo newCulture = CultureInfo.CreateSpecificCulture("en-US");
            //    System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
            //    // Make current UI culture consistent with current culture.
            //    System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            //}

        }
        // ************************************************************************************************ Loaded

        // Closing ************************************************************************************************
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
     {
      // objWindowAzimuth.Close();
      // objWindowTriang.Close();

      // Out from application
      Application.Current.Shutdown();
     }
     // ************************************************************************************************ Closing

     // Settings -> Property Grid ****************************************************************************
     // The OLD -> Ybrat

        private void ButtonSettings_Click(object sender, RoutedEventArgs e)
        {
            // --------------------------------------------------------------------------------------------------
            // True
            if (GlobalVarMapMain.ButtonSettingsOn==true)
            {
                Image myImage1 = new Image();
                BitmapImage bi1 = new BitmapImage();
                bi1.BeginInit();
                bi1.UriSource = new Uri("Resources/RedKv.PNG", UriKind.Relative);
                bi1.EndInit();

                GlobalVarMapMain.ButtonSettingsOn = false;
                //ImSettings.Source = bi1;

                //PropGridCtrl.Visibility = Visibility.Collapsed;

                Col0.Width = new GridLength(1, GridUnitType.Pixel);

            } // True
            // --------------------------------------------------------------------------------------------------
            // False

            else
            {
                Image myImage3 = new Image();
                BitmapImage bi3 = new BitmapImage();
                bi3.BeginInit();
                bi3.UriSource = new Uri("Resources/file_document_box_outline.PNG", UriKind.Relative);
                bi3.EndInit();

                GlobalVarMapMain.ButtonSettingsOn = true;
                //ImSettings.Source = bi3;

                Col0.Width = new GridLength(451, GridUnitType.Pixel);


            } // False
            // --------------------------------------------------------------------------------------------------

        }
        // *************************************************************************** Settings -> Property Grid

        // DATA_BASE *********************************************************************************************
        /// <summary>
        /// Connection to Database
        /// </summary>
        /// 
        ClientDB clientDB;
        // ------------------------------------------------------------------------------------------------------
        private void ConnectBD_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                {
                    clientDB.Disconnect();
                    clientDB = null;
                }
                else
                {
                    GlobalVarMapMain.fltbl = false;

                    Name = "Map";

                    clientDB = new ClientDB(this.Name, $"{SettingsMap.Settings.IP_DB}:{SettingsMap.Settings.NumPort}");

                    InitClientDB();
                    clientDB.ConnectAsync();
                }
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
            }
            catch (Exception)
            { }

        }
        // ------------------------------------------------------------------------------------------------------

        void InitClientDB()
        {
            // На остальные события подписываемся после загрузки таблиц
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
        }
        // ------------------------------------------------------------------------------------------------------




/*
        string s1 = mapCtrl.DrawLinesLatLong(
                                            // Degree
                                            pointPel1,
                                            // Color
                                            100,
                                            255,
                                            0,
                                            0
                                           );
*/




        // GPS **************************************************************************************************
        // Updated table of GPS

        private void OnTempGNSSUp(object sender, TableEventArs<TempGNSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // Otl ----------------------------------------------------------
            // 19.07
            // ---------------------------------------------------------- Otl
            long ichislo1 = 0;
            double dchislo1 = 0;
            long ichislo2 = 0;
            double dchislo2 = 0;
            int i = 0;
            string s = "";

            // ---------------------------------------------------------------
            // SystemCoordinates?

            string s1 = "";
            InitMapYaml initMapYaml = new InitMapYaml();
            s = ClassYaml.ReadYaml("Setting.yaml", ref initMapYaml);

/*
            double Lt = 53.880843;
            double Ln = -27.60592;
            double xxx1 = 0;
            double yyy1 = 0;
            ClassGeoCalculator.f_WGS84_Krug(Lt, Ln, ref xxx1, ref yyy1);
*/
            // -------------------------------------------------------------------------------------------------
            // 19.07 GNSS

            // FOR
            for (i = 0; i < e.Table.Count; i++)
            {
                // .............................................................................................
                // Latitude
                if (e.Table[i].Location.Latitude < 0)
                    e.Table[i].Location.Latitude = -e.Table[i].Location.Latitude;
                ichislo1 = (long)(e.Table[i].Location.Latitude * 1000000);
                dchislo1 = ((double)ichislo1) / 1000000;
                // Longitude
                if (e.Table[i].Location.Longitude < 0)
                    e.Table[i].Location.Longitude = -e.Table[i].Location.Longitude;
                ichislo2 = (long)(e.Table[i].Location.Longitude * 1000000);
                //dchislo2 = ((double)ichislo2) / 1000000;
                dchislo2 = ((double)ichislo2) / 1000000;

                // .............................................................................................
                // Time

                s = e.Table[i].LocalTime.ToLongTimeString();
                // .............................................................................................

                // WGS 84
                if (initMapYaml.SysCoord == 0)
                {
                    if ((dchislo1 >= 0) && (dchislo2 >= 0))
                    {
                        mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }
                    else if ((dchislo1 >= 0) && (dchislo2 < 0))
                    {
                        mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }
                    else if ((dchislo1 < 0) && (dchislo2 >= 0))
                    {
                        mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }
                    else
                    {
                        mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }

                } // WGS 84

                // SK42,m
                else if (initMapYaml.SysCoord == 2)
                {
                    double xxx = 0;
                    double yyy = 0;

                    ClassGeoCalculator.f_WGS84_Krug(e.Table[i].Location.Latitude, -e.Table[i].Location.Longitude, ref xxx, ref yyy);

                    mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx) + " m" + "   " + "Y " + Convert.ToString((int)yyy) + " m" +
                                         "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                } // SK42,m

                // SK42grad
                else if (initMapYaml.SysCoord == 1)
                {
                    double xxx1 = 0;
                    double yyy1 = 0;

                    ClassGeoCalculator.f_WGS84_SK42_BL(e.Table[i].Location.Latitude, -e.Table[i].Location.Longitude, ref xxx1, ref yyy1);

                    // Latitude
                    ichislo1 = (long)(xxx1 * 1000000);
                    dchislo1 = ((double)ichislo1) / 1000000;
                    // Longitude
                    ichislo2 = (long)(yyy1 * 1000000);
                    dchislo2 = ((double)ichislo2) / 1000000;

                    if ((dchislo1 >= 0) && (dchislo2 >= 0))
                    {
                        mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }
                    else if ((dchislo1 >= 0) && (dchislo2 < 0))
                    {
                        mapCtrl.Txt9.Text = "GNSS:   N " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }
                    else if ((dchislo1 < 0) && (dchislo2 >= 0))
                    {
                        mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "E " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }
                    else
                    {
                        mapCtrl.Txt9.Text = "GNSS:   S " + Convert.ToString(dchislo1) + '\xB0' + "   " + "W " + Convert.ToString(dchislo2) + '\xB0' +
                                            "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                    }


                } //SK42grad

                // Mercator,m
                else if (initMapYaml.SysCoord == 3)
                {
                    double xxx2 = 0;
                    double yyy2 = 0;

                    ClassGeoCalculator.f_WGS84_Mercator(e.Table[i].Location.Latitude, -e.Table[i].Location.Longitude, ref xxx2, ref yyy2);

                    mapCtrl.Txt9.Text = "GNSS:   X " + Convert.ToString((int)xxx2) + " m" + "   " + "Y " + Convert.ToString((int)yyy2) + " m" +
                                         "   " + Convert.ToString(e.Table[i].NumberOfSatellites) + "   " + s;
                } // Mercator,m

                // .............................................................................................

            } // FOR
            // -------------------------------------------------------------------------------------------------
            if (e.Table.Count > 0)
            {
                GlobalVarMapMain.GNSS_Lat = e.Table[e.Table.Count - 1].Location.Latitude;
                GlobalVarMapMain.GNSS_Long = -e.Table[e.Table.Count - 1].Location.Longitude;
                GlobalVarMapMain.GNSS_time = s;
                GlobalVarMapMain.GNSS_Numb_Sat = e.Table[e.Table.Count - 1].NumberOfSatellites;
            }
            // -------------------------------------------------------------------------------------------------

        }
        // ************************************************************************************************** GPS

        // ASP *************************************************************************************************
        // Updated table of ASP

        private void OnTableAspUp(object sender, TableEventArs<TableASP> e)
        {

            // .................................................................................................
            if (GlobalVarMapMain.fltbl == false)
                return;
            // .................................................................................................

            // Otl ----------------------------------------------------------
            // ---------------------------------------------------------- Otl
            // .................................................................................................

            //mapCtrl.objClassInterfaceMap.List_JS = e.Table;

            List<TableASP> lstASP = new List<TableASP>();
            lstASP = e.Table;
            // .................................................................................................
            // Убрать без координат

            if (lstASP.Count != 0)
            {
                for (int iijjs = (lstASP.Count - 1); iijjs >= 0; iijjs--)
                {
                    if (
                        (lstASP[iijjs].Coordinates.Latitude == -1) ||
                        (lstASP[iijjs].Coordinates.Longitude == -1) ||
                        (lstASP[iijjs].Coordinates.Latitude == 0) ||
                        (lstASP[iijjs].Coordinates.Longitude == 0)
                        )
                        lstASP.Remove(lstASP[iijjs]);
                }
            }

            // 22_10
            if (lstASP.Count != 0)
            {
                for (int iijjss = (lstASP.Count - 1); iijjss >= 0; iijjss--)
                {
                    lstASP[iijjss].Coordinates.Latitude = lstASP[iijjss].Coordinates.Latitude < 0 ? lstASP[iijjss].Coordinates.Latitude * -1 : lstASP[iijjss].Coordinates.Latitude;
                    lstASP[iijjss].Coordinates.Longitude = lstASP[iijjss].Coordinates.Longitude < 0 ? lstASP[iijjss].Coordinates.Longitude * -1 : lstASP[iijjss].Coordinates.Longitude;
                }
            }

            // .................................................................................................
            // Map -> Clear all

            if ((lstASP.Count == 0) && (mapCtrl.objClassInterfaceMap.List_JS.Count != 0))
            {
                objClassFunctionsMain.ClearListJS();


                // IRIFRCH ------------------------------------------------------------------------
                // Удаляем SPj из внутреннего списка ИРИi

                int ipl1 = 0;

                // Лист ИРИ ФРЧ с пеленгами
                for (ipl1 = 0; ipl1 < mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count; ipl1++)
                {
                    // Удаляем внутренние списки в ИРИi
                    if (mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.Count != 0)
                    {
                        for (int i = mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.Count - 1; i >= 0; i--)
                        {
                            mapCtrl.objClassInterfaceMap.List_SRW_FRF[ipl1].list_JSBearing.RemoveAt(i);
                        }
                    }

                } // FOR IRIFRCH
                // ------------------------------------------------------------------------ IRIFRCH

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            /*
                        if (
                            (mapCtrl.objClassInterfaceMap.List_JS.Count != 0)
                            && (mapCtrl.objClassInterfaceMap.ShowJS == true)
                            )
                        {
                            Dispatcher.Invoke(() =>
                            {
                                mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                            });

                        }
            */
            // .................................................................................................
            // UpdateASP

            if (
                (lstASP.Count != 0)
                //&& (mapCtrl.objClassInterfaceMap.ShowJS == true)
                )
            {
                Dispatcher.Invoke(() =>
                {
                    //mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    objClassFunctionsMain.UpdateASP(lstASP);
                });

            }
            // .................................................................................................


            // Otl ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ  Otladka
/*
            for(int iu1=0;  iu1< mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count;iu1++)
               mapCtrl.objClassInterfaceMap.List_SRW_FRF[iu1].IsSelected = false;

            TempFWS obj;
            obj = new TempFWS();
            obj.Coordinates = new Coord();
            obj.Coordinates.Latitude = 53.863;
            obj.Coordinates.Longitude = 27.58;
            obj.FreqKHz = 30000;

            obj.ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
            JamDirect o1 = new JamDirect();
            o1.NumberASP = 111;
            o1.Bearing = 20;
            obj.ListQ.Add(o1);
            objClassFunctionsMain.AddOneIRIFRCH(obj);
            mapCtrl.objClassInterfaceMap.List_SRW_FRF[mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count - 1].IsSelected=true;
            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
*/
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl

        }
        // ************************************************************************************************* ASP

        // ИРИ ФРЧ ********************************************************************************************
        // Updated table of ИРИ ФРЧ

        private void OnTempFWSUp(object sender, TableEventArs<TempFWS> e)
        {

            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear all

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count != 0))
            {
                objClassFunctionsMain.ClearListFRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            Dispatcher.Invoke(() =>
            {
                objClassFunctionsMain.UpdateIRIFRCH(e.Table);
            });
            // .................................................................................................

        }
        // ******************************************************************************************** ИРИ ФРЧ

        // ИРИ ФРЧ1 *******************************************************************************************
        // Updated table of ИРИ ФРЧ

        private void OnTempFWSAddRange(object sender, TableEventArs<TempFWS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // Otl ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ  Otladka
/*
            for (int ii = 0; ii < 20; ii++)
            {
                e.Table[ii].Coordinates.Latitude = 56.18;
                e.Table[ii].Coordinates.Longitude = 22.98 + ii * 0.3;
            }
*/
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl

            // .................................................................................................
            // Map -> Clear all

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF.Count != 0))
            {
                objClassFunctionsMain.ClearListFRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            Dispatcher.Invoke(() =>
            {
                objClassFunctionsMain.UpdateIRIFRCH(e.Table);
            });
        }
        // ******************************************************************************************* ИРИ ФРЧ1

        // ИРИ ФРЧ ЦР ******************************************************************************************
        // Updated table of ИРИ ФРЧ ЦР

        private void OnTableReconFWSUp(object sender, TableEventArs<TableReconFWS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // Otl ----------------------------------------------------------
            // ---------------------------------------------------------- Otl

            // .................................................................................................
            // Map -> Clear all

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count != 0))
            {
                objClassFunctionsMain.ClearListFRHCR();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            Dispatcher.Invoke(() =>
            {
                objClassFunctionsMain.UpdateIRIFRCH_CR(e.Table);
            });
            // .................................................................................................

        }
        // ****************************************************************************************** ИРИ ФРЧ ЦР

        // ИРИ ФРЧ РП *****************************************************************************************
        // Updated table of ИРИ ФРЧ РП

        private void OnTableSuppressFWSUp(object sender, TableEventArs<TableSuppressFWS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // Otl ----------------------------------------------------------
            // ---------------------------------------------------------- Otl

            // .................................................................................................
            // Map -> Clear all

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count != 0))
            {
                objClassFunctionsMain.ClearListFRHRP();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            Dispatcher.Invoke(() =>
            {
                objClassFunctionsMain.UpdateIRIFRCH_RP(e.Table);
            });
            // .................................................................................................

        }
        // ***************************************************************************************** ИРИ ФРЧ РП

        // ИРИ ППРЧ РП ****************************************************************************************
        // Updated table of ИРИ ППРЧ РП
        // !!!  Не рисуем

        private void OnTableSuppressFHSSUp(object sender, TableEventArs<TableSuppressFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            /*
                        // Otl ----------------------------------------------------------
                        // ---------------------------------------------------------- Otl

               mapCtrl.objClassInterfaceMap.List_SRW_STRF_RS = e.Table;

                // ???????????????????????????????????????????
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });

            */
        }
        // **************************************************************************************** ИРИ ППРЧ РП

        // ИРИ ППРЧ *******************************************************************************************
        // Updated table of ИРИ ППРЧ
        // !!! Для очистки TableReconFHSS

        private void OnTableReconFHSSUp(object sender, TableEventArs<TableReconFHSS> e)
        {

            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableReconFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon.Count != 0))
            {
                objClassFunctionsMain.ClearListPPRH2();

                // Clear and redraw
                //Dispatcher.Invoke(() =>
                //{
                //    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                //});
                return;
            }
            // .................................................................................................

        }
        // ******************************************************************************************* ИРИ ППРЧ

        // ИРИ ППРЧ Координаты ********************************************************************************
        // Координаты для ИРИ ППРЧ
        // !!! Для очистки TableSourceFHSS и основного списка

        private void OnTableSourceFHSSUp(object sender, TableEventArs<TableSourceFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableSourceFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.Count != 0))
            {
                objClassFunctionsMain.ClearListPPRH1();
            }
            // .................................................................................................
            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count != 0))
            {
                objClassFunctionsMain.ClearListPPRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................

        }
        // ******************************************************************************** ИРИ ППРЧ Координаты

        // ИРИ ППРЧ1 *******************************************************************************************
        // TableReconFHSS

        private void OnTableReconFHSSAddRange(object sender, TableEventArs<TableReconFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableReconFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon.Count != 0))
            {
                objClassFunctionsMain.ClearListPPRH2();

                return;
            }
            // .................................................................................................
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon = e.Table;
            // .................................................................................................

        }
        // ******************************************************************************************* ИРИ ППРЧ1

        // ИРИ ППРЧ2 *******************************************************************************************
        // TableSourceFHSS-> Заполнение основного списка ИРИППРЧ

        private void OnTableSourceFHSSAddRange(object sender, TableEventArs<TableSourceFHSS> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // .................................................................................................
            // Map -> Clear TableSourceFHSS

            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.Count != 0))
            {
                objClassFunctionsMain.ClearListPPRH1();
            }
            // .................................................................................................
            if ((e.Table.Count == 0) && (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count != 0))
            {
                objClassFunctionsMain.ClearListPPRH();

                // Clear and redraw
                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });
                return;
            }
            // .................................................................................................
            Dispatcher.Invoke(() =>
            {
                objClassFunctionsMain.UpdateIRIPPRCH(mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon, e.Table);
            });
            // .................................................................................................

        }
        // ******************************************************************************************* ИРИ ППРЧ2

        // AIRPLANES ******************************************************************************************
        // Updated table of Airplanes
        // Этого обработчика нету
        // !!!OLD

        // 11_13
        private void OnTempADSB(object sender, TableEventArs<TempADSB> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // Otl ----------------------------------------------------------
            // ---------------------------------------------------------- Otl
            // --------------------------------------------------------------
            // !!!NEW
            //objClassFunctionsMain.GetListAirPlanes(e.Table);
            // --------------------------------------------------------------

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        } // Airplanes
          // ------------------------------------------------------------------------------------------------------
          // NEW Airplanes

        // ......................................................................................................
        // Приходит с пустым листом для очистки всех самолетов или если лист не пустой -> для удаления одного самолета
        // Тавлица -> кнопка Delete

        // 11_13
        private void HandlerUpdateTempADSB(object sender, TableEventArs<TempADSB> e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;
           // -----------------------------------------------------------------------------------------
            // Arsen_Tab
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            // ??????????????????????????????????   Бесполезная операция
            //aDSBControl.AddRange(e.Table);
            //});
            // -----------------------------------------------------------------------------------------
            // Удаление всех самолетов

            // Map -> Clear all
            if (e.Table.Count==0)
            {
                if (mapCtrl.objClassInterfaceMap.List_AP.Count != 0)
                {
                    objClassFunctionsMain.ClearListAP();

                    mapCtrl.Txt10.Text = "Number of aircraft:" + " " + "0";

                    Dispatcher.Invoke(() =>
                    {
                        aDSBControl.DEL_TAB();
                    });

                    // Clear and redraw
                    //mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    Dispatcher.Invoke(() =>
                    {
                        mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                    });
                }
            } // e.Table.Count==0
            // -----------------------------------------------------------------------------------------
            // Удаление одного самолета

            else
            {
                // Arsen
                aDSBControl.DelAirMain(e.Table);

                GlobalVarMapMain.listTableADSB = e.Table;
                objClassFunctionsMain.GetADSB_DB(GlobalVarMapMain.listTableADSB, 1);
                //GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.List_AP

                mapCtrl.Txt10.Text = "Number of aircraft:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);

                // ForArsen
                /*
                                for(int iICAO=0;iICAO< GlobalVarMapMain.sICAO.Count;iICAO++)
                                {
                                    Dispatcher.Invoke(() =>
                                    {
                                        aDSBControl.DeleteR(GlobalVarMapMain.sICAO[iICAO]);
                                    });
                                }
                */
                //Dispatcher.Invoke(() =>
                //{
                //    aDSBControl.DelAirMain(e.Table);
                //});


                Dispatcher.Invoke(() =>
                {
                    mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
                });

            }
            // -----------------------------------------------------------------------------------------

        }

        // 11_13
        private void ADSBCleanerEvent_OnCleanADSBTable(object sender, EventArgs e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            try
            {
                if (clientDB != null)
                {
                    clientDB.Tables[NameTable.TempADSB].Clear();
                }
            }
            catch (Exception)
            { }
        }
        // ......................................................................................................
        // Приходит по одной записи

        // 11_13*
        private void HandlerAddRecordTempADSB(object sender, TempADSB e)
        {
            if (GlobalVarMapMain.fltbl == false)
                return;

            // Arsen_Tab
            aDSBControl.AddRange(new List<TempADSB>() { e });

            // Map -> One запись
            // !!!NEW


            if(GlobalVarMapMain.fltmr_air==0)
            {
                GlobalVarMapMain.fltmr_air = 1;

                //System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                //GlobalVarMapMain.dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                //GlobalVarMapMain.dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                //GlobalVarMapMain.dispatcherTimer.Start();

            } // Tmr

            if (GlobalVarMapMain.listTableADSB.Count != 0)
            {
                for (int i = (GlobalVarMapMain.listTableADSB.Count - 1); i >= 0; i--)
                    GlobalVarMapMain.listTableADSB.Remove(GlobalVarMapMain.listTableADSB[i]);
            }



            // Otl
/*
            if (
                //(e.Coordinates.Latitude == -1) ||
                //(e.Coordinates.Longitude == -1) ||
                //(e.Coordinates.Latitude == 0) ||
                //(e.Coordinates.Longitude == 0) ||
                (
                (e.Coordinates.Latitude < GlobalVarMapMain.ltmin_air) ||
                (e.Coordinates.Latitude > GlobalVarMapMain.ltmax_air) ||
                (e.Coordinates.Longitude < GlobalVarMapMain.lnmin_air) ||
                (e.Coordinates.Longitude > GlobalVarMapMain.lnmax_air)
                )&&
                (e.Coordinates.Latitude!=-1)&&(e.Coordinates.Longitude!=-1)
                )
            {
                int www = 0;
            }
*/



                // ????????????????????????????????????
                if ((e.Coordinates.Latitude != -1) &&
                (e.Coordinates.Longitude != -1) &&
                (e.Coordinates.Latitude != 0) &&
                (e.Coordinates.Longitude != 0)
                //(e.Coordinates.Latitude>=GlobalVarMapMain.ltmin_air)&&
                //(e.Coordinates.Latitude <= GlobalVarMapMain.ltmax_air) &&
                //(e.Coordinates.Longitude >= GlobalVarMapMain.lnmin_air) &&
                //(e.Coordinates.Longitude <= GlobalVarMapMain.lnmax_air) 

                )
            {
                //objClassFunctionsMain.GetListAirPlanes(e);
                GlobalVarMapMain.listTableADSB.Add(e);
            }

            Dispatcher.Invoke(() =>
            {
                // ????????????????????????????????????????????????????

                if(GlobalVarMapMain.listTableADSB.Count!=0)
                 objClassFunctionsMain.GetADSB_DB(GlobalVarMapMain.listTableADSB,0);
                //objClassFunctionsMain.GetListAirPlanes(e);
                //objClassFunctionsMain.GetListAirPlanes(GlobalVarMapMain.listTableADSB);
                mapCtrl.Txt10.Text = "Number of air objects:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);

            });

        }
        // ......................................................................................................


        // ****************************************************************************************** AIRPLANES

        // Tmr ************************************************************************************************
/*
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // code goes here
            // Draw
            if (
                (GlobalVarMapMain.objMainWindowG.mapCtrl.objClassInterfaceMap.Show_AP == true) 
                )
                GlobalVarMapMain.objMainWindowG.mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
        }
*/
        // ************************************************************************************************ Tmr

        // ERROR_BD *******************************************************************************************

        private void HandlerErrorDataBase(object sender, OperationTableEventArgs e)
        {
            MessageBox.Show(e.GetMessage);

        }
        // ******************************************************************************************* ERROR_BD

        // DISCONNECT_BD **************************************************************************************

        private void HandlerDisconnect(object sender, ClientEventArgs e)
        {

            // Updated table of ИРИ ФРЧ
            (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable -= OnTempFWSUp;
            (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange -= OnTempFWSAddRange;

            // Updated table of ИРИ ФРЧ ЦР
            (clientDB.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable -= OnTableReconFWSUp;
            // Updated table of ИРИ ФРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable -= OnTableSuppressFWSUp;

            // ИРИ ППРЧ
            // Для очистки
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable -= OnTableReconFHSSUp;
            // For Coordinates
            // Для очистки
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable -= OnTableSourceFHSSUp;
            (clientDB.Tables[NameTable.TableReconFHSS] as ITableAddRange<TableReconFHSS>).OnAddRange -= OnTableReconFHSSAddRange;
            (clientDB.Tables[NameTable.TableSourceFHSS] as ITableAddRange<TableSourceFHSS>).OnAddRange -= OnTableSourceFHSSAddRange;

            // Updated table of ИРИ ППРЧ РП
            (clientDB.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable -= OnTableSuppressFHSSUp;
            // Updated table of ASP (СП)
            (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable -= OnTableAspUp;
            //fill ADSB table by planes + Map
            (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable -= HandlerUpdateTempADSB;
            (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord -= HandlerAddRecordTempADSB;
            //  GPS
            (clientDB.Tables[NameTable.TempGNSS] as ITableUpdate<TempGNSS>).OnUpTable -= OnTempGNSSUp;

            ButConnectBD.ShowDisconnect();
            objClassFunctionsMain.ClearLists();
            mapCtrl.Txt10.Text = "";

            //GlobalVarMapMain.dispatcherTimer.Stop();

            // Azimuth*
            objClassFunctionsMain.ClearAzimuth();
            // -----------------------------------------------------------------
            // Clear and redraw
            // ??????????????????

            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            // -----------------------------------------------------------------


        }
        // ************************************************************************************** DISCONNECT_BD

        // CONNECT_BD *****************************************************************************************

        private void HandlerConnect(object sender, ClientEventArgs e)
        {
            try
            {
                GlobalVarMapMain.fltbl = false;

                ButConnectBD.ShowConnect();
                objClassFunctionsMain.ClearLists();

                //objClassFunctionsMain.LoadTables(clientDB);
                LoadTables1();

                //GlobalVarMapMain.fltbl = true;

                // -------------------------------------------------------------------------------------------------
/*
                // Updated table of ИРИ ФРЧ
                // Для очистки
                (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable += OnTempFWSUp;
                (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange += OnTempFWSAddRange;

                // Updated table of ИРИ ФРЧ ЦР
                (clientDB.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable += OnTableReconFWSUp;
                // Updated table of ИРИ ФРЧ РП
                (clientDB.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable += OnTableSuppressFWSUp;
                // Updated table of ИРИ ППРЧ РП
                (clientDB.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable += OnTableSuppressFHSSUp;

                // ИРИ ППРЧ
                // Для очистки
                (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable += OnTableReconFHSSUp;
                // For Coordinates
                // Для очистки
                (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable += OnTableSourceFHSSUp;
                (clientDB.Tables[NameTable.TableReconFHSS] as ITableAddRange<TableReconFHSS>).OnAddRange += OnTableReconFHSSAddRange;
                (clientDB.Tables[NameTable.TableSourceFHSS] as ITableAddRange<TableSourceFHSS>).OnAddRange += OnTableSourceFHSSAddRange;

                // Updated table of ASP (СП)
                (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable += OnTableAspUp;

                // ??????????????????????????
                // Updated table of AP
                // OLD
                //(clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += OnTempADSB;
                //fill ADSB table by planes + Map
                (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += HandlerUpdateTempADSB;
                (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord += HandlerAddRecordTempADSB;

                //  GPS
                (clientDB.Tables[NameTable.TempGNSS] as ITableUpdate<TempGNSS>).OnUpTable += OnTempGNSSUp;
                // -------------------------------------------------------------------------------------------------
*/
            }
            catch (Exception ex)
            {
            }

        }
        // ***************************************************************************************** CONNECT_BD

        // LOAD_TABLES ****************************************************************************************
        private async void LoadTables1()
       //private void LoadTables1()
        {
            // JS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // JS

            // Здесь просто отрисуются все станции, которые получили, если нажата птичка по станциям
            var stations = await clientDB.Tables[NameTable.TableASP].LoadAsync<TableASP>();
            //mapCtrl.objClassInterfaceMap.List_JS = stations;

            // Убрать без координат
            if (stations.Count != 0)
            {
                for (int iijjs = (stations.Count - 1); iijjs >= 0; iijjs--)
                {
                    if (
                        (stations[iijjs].Coordinates.Latitude == -1) ||
                        (stations[iijjs].Coordinates.Longitude == -1) ||
                        (stations[iijjs].Coordinates.Latitude == 0) ||
                        (stations[iijjs].Coordinates.Longitude == 0)
                        )
                        stations.Remove(stations[iijjs]);
                }
            }


            // 22_10
            if (stations.Count != 0)
            {
                for (int iijjss = (stations.Count - 1); iijjss >= 0; iijjss--)
                {
                    stations[iijjss].Coordinates.Latitude = stations[iijjss].Coordinates.Latitude < 0 ? stations[iijjss].Coordinates.Latitude * -1 : stations[iijjss].Coordinates.Latitude;
                    stations[iijjss].Coordinates.Longitude = stations[iijjss].Coordinates.Longitude < 0 ? stations[iijjss].Coordinates.Longitude * -1 : stations[iijjss].Coordinates.Longitude;
                }
            }

            int flPel = -1;

            if ((mapCtrl.objClassInterfaceMap.ShowJS == true) && (mapCtrl.objClassInterfaceMap.Show_SRW_STRF_RS == true))
                flPel = 1;

            // Добавляем элементы из входного массива от БД (тип TableASP) в
            // массив классов АСП (List<ClassJS>)
            if(stations.Count!=0)
            {
                for (int iijjs1 = 0; iijjs1 < stations.Count; iijjs1++)
                {
                    objClassFunctionsMain.AddOneJS(stations[iijjs1],flPel);
                }

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> JS

            // ИРИ ФРЧ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            //List<TempFWS> lstTempFWS = new List<TempFWS>();
            var obj1 = await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>();
            //lstTempFWS = obj1;

            // Otl ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ  Otladka
/*
                        TempFWS obj;

                        Random rand = new Random();
                        Random rand1 = new Random();
                        int j = 0;
                        int j1 = 0;
                        int f = 50;
                        double lt_temp = 0;
                        double ln_temp = 0;

                        for (j1 = 0; j1 < 10; j1++)
                        {
                            for (j = 0; j < 5; j++)
                            {
                                lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                                ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                                obj = new TempFWS();
                                obj.Coordinates = new Coord();
                                obj.Coordinates.Latitude = lt_temp;
                                obj.Coordinates.Longitude = ln_temp;
                                obj.FreqKHz = f;
                                //e.Table.Add(obj);
                                obj1.Add(obj);

                        }
                             if (j1 < 4)
                                f += 50;
                            else
                                f += 250;
                        }

            obj1[0].ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
            JamDirect o1 = new JamDirect();
            o1.NumberASP = 111;
            o1.Bearing = 50;
            obj1[0].ListQ.Add(o1);
*/

/*
            for(int ii=0;ii<20;ii++)
            {
                obj1[ii].Coordinates.Latitude = 56.18;
                obj1[ii].Coordinates.Longitude = 22.98 + ii * 0.3;
            }
*/
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl

            // ----------------------------------------------------------------- OnTempFWSUp

            // Свой список __________________________________________________________________________
            // Создание своего списка классов ИРИ ФРЧ (в т.ч. для пеленгов)

            // Добавляем элементы из входного массива от БД (тип TempFWS) в
            // массив классов ИРИ ФРЧ (List<Class_IRIFRCH>)
            if (obj1.Count != 0)
            {
                for (int iijfr1 = 0; iijfr1 < obj1.Count; iijfr1++)
                {
                    objClassFunctionsMain.AddOneIRIFRCH(obj1[iijfr1]);
                }

            }

            // QQQ Otladka
            //mapCtrl.objClassInterfaceMap.List_SRW_FRF[0].IsSelected = true;
            // __________________________________________________________________________ Свой список


            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ

            // ИРИ ФРЧ ЦР >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj2 = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
            mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD = obj2;

            if (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count != 0)
            {
                for (int iijf1 = (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Count - 1); iijf1 >= 0; iijf1--)
                {
                    if (
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Latitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Longitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Latitude == 0) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1].Coordinates.Longitude == 0)
                        )
                        mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD.Remove(mapCtrl.objClassInterfaceMap.List_SRW_FRF_TD[iijf1]);
                }
            }

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ ЦР

            // ИРИ ФРЧ РП >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj3 = await clientDB.Tables[NameTable.TableSuppressFWS].LoadAsync<TableSuppressFWS>();
            mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS = obj3;

            if (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count != 0)
            {
                for (int iijf2 = (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Count - 1); iijf2 >= 0; iijf2--)
                {
                    if (
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Latitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Longitude == -1) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Latitude == 0) ||
                        (mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2].Coordinates.Longitude == 0)
                        )
                        mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS.Remove(mapCtrl.objClassInterfaceMap.List_SRW_FRF_RS[iijf2]);
                }
            }

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ФРЧ РП

            // ИРИ ППРЧ >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            var obj4 = await clientDB.Tables[NameTable.TableReconFHSS].LoadAsync<TableReconFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Recon = obj4;
            var obj4_1 = await clientDB.Tables[NameTable.TableSourceFHSS].LoadAsync<TableSourceFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord = obj4_1;

            // Otl1 ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ1  Otladka
            // Для ввода своих ППРЧ просто откомментировать этот кусок otl1
/*
                                    TableReconFHSS obj;
                                    TableSourceFHSS obj_1;

                                    Random rand = new Random();
                                    Random rand1 = new Random();
                                    int j = 0;
                                    int j1 = 0;
                                    int f = 50;
                                    int num = 0;
                                    double lt_temp = 0;
                                    double ln_temp = 0;

                                    for (j1 = 0; j1 < 10; j1++)

                                    {
                                        for (j = 0; j < 5; j++)
                                        {
                                            lt_temp = Convert.ToDouble(rand.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                                            ln_temp = Convert.ToDouble(rand1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                                            obj = new TableReconFHSS();
                                            obj_1 = new TableSourceFHSS();
                                            obj.Id = num;
                                            obj_1.IdFHSS = num;
                                            obj_1.Coordinates = new Coord();
                                            obj_1.Coordinates.Latitude = lt_temp;
                                            obj_1.Coordinates.Longitude = ln_temp;
                                            obj.FreqMinKHz = f;
                                            obj.FreqMaxKHz = f + 2;
                                            obj4.Add(obj);
                                            obj4_1.Add(obj_1);
                                            num += 1;

                                    }
                                         if (j1 < 4)
                                            f += 50;
                                        else
                                            f += 250;
                                    }
*/
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl1



            //obj1[0].ListQ = new System.Collections.ObjectModel.ObservableCollection<JamDirect>();
            //JamDirect o1 = new JamDirect();
            //o1.NumberASP = 111;
            //o1.Bearing = 50;
            //obj1[0].ListQ.Add(o1);

            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl



            /*
                        if (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count != 0)
                        {
                            for (int iijf3 = (mapCtrl.objClassInterfaceMap.List_SRW_STRF.Count - 1); iijf3 >= 0; iijf3--)
                            {
                                int indxf = -1;
                                indxf = mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord.FindIndex(x => (x.IdFHSS == mapCtrl.objClassInterfaceMap.List_SRW_STRF[iijf3].Id));
                                if (indxf >= 0)
                                {
                                    if (
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Latitude == -1) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Longitude == -1) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Latitude == 0) ||
                                        (mapCtrl.objClassInterfaceMap.List_SRW_STRF_Coord[indxf].Coordinates.Longitude == 0)
                                        )
                                        mapCtrl.objClassInterfaceMap.List_SRW_STRF.Remove(mapCtrl.objClassInterfaceMap.List_SRW_STRF[iijf3]);
                                }
                            }
                        }
            */


            // Свой список __________________________________________________________________________
            // Создание своего списка классов ИРИ ППРЧ (в т.ч. для пеленгов)

            // Добавляем элементы из входного массива от БД (тип TableReconFHSS+TableSourceFHSS) в
            // массив классов ИРИ ППРЧ (List<Class_IRIPPRCH>)

            if (obj4_1.Count != 0)
                        {
                            for (int iijfr5 = 0; iijfr5 < obj4_1.Count; iijfr5++)
                            {
                                objClassFunctionsMain.AddOneIRIPPRCH(obj4_1[iijfr5],obj4);
                            }

                        }

            // __________________________________________________________________________ Свой список

            // !!! Перерисовка в конце
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ППРЧ

            // ИРИ ППРЧ РП >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // !!! Не рисуем
            var obj5 = await clientDB.Tables[NameTable.TableSuppressFHSS].LoadAsync<TableSuppressFHSS>();
            mapCtrl.objClassInterfaceMap.List_SRW_STRF_RS = obj5;

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ИРИ ППРЧ РП

            // Airplanes >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // 11_13

            // NEW
            var objAirPl = await clientDB.Tables[NameTable.TempADSB].LoadAsync<TempADSB>();
            //objClassFunctionsMain.GetListAirPlanes_LoadDB(objAirPl);
            GlobalVarMapMain.listTableADSB = objAirPl;



            // Otl1 ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
            // QQQ2  Otladka
            // Для ввода своих ADSB просто откомментировать этот кусок otl1
/*
            TempADSB objADSB;

            Random randADSB = new Random();
            Random randADSB1 = new Random();

            int jADSB = 0;
            double lt_tempADSB = 0;
            double ln_tempADSB = 0;

                for (jADSB = 0; jADSB < 2500; jADSB++)
                {
                    lt_tempADSB = Convert.ToDouble(randADSB.Next(GlobalVarMapMain.lat1_temp1, GlobalVarMapMain.lat2_temp1)) / 100000;
                    ln_tempADSB = Convert.ToDouble(randADSB1.Next(GlobalVarMapMain.lon1_temp1, GlobalVarMapMain.lon2_temp1)) / 100000;
                    objADSB = new TempADSB();
                    objADSB.Coordinates = new Coord();

                    objADSB.ICAO = Convert.ToString(9990+jADSB);
                    objADSB.Coordinates.Latitude = lt_tempADSB;
                    objADSB.Coordinates.Longitude = ln_tempADSB;
                    GlobalVarMapMain.listTableADSB.Add(objADSB);
                }
*/
            // ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo Otl2








            // Если нет координат -> удаляем элемент из списка

            // AAA

            if (GlobalVarMapMain.listTableADSB.Count != 0)
            {
                for (int iijl = (GlobalVarMapMain.listTableADSB.Count - 1); iijl >= 0; iijl--)
                {
                    if (
                        (GlobalVarMapMain.listTableADSB[iijl].Coordinates.Latitude == -1) ||
                        (GlobalVarMapMain.listTableADSB[iijl].Coordinates.Longitude == -1) ||
                        (GlobalVarMapMain.listTableADSB[iijl].Coordinates.Latitude == 0) ||
                        (GlobalVarMapMain.listTableADSB[iijl].Coordinates.Longitude == 0) 
                        //(GlobalVarMapMain.listTableADSB[iijl].Coordinates.Latitude > GlobalVarMapMain.ltmax_air) ||
                        //(GlobalVarMapMain.listTableADSB[iijl].Coordinates.Latitude < GlobalVarMapMain.ltmin_air) ||
                        //(GlobalVarMapMain.listTableADSB[iijl].Coordinates.Longitude > GlobalVarMapMain.lnmax_air) ||
                        //(GlobalVarMapMain.listTableADSB[iijl].Coordinates.Longitude < GlobalVarMapMain.lnmin_air)
                        )
                        GlobalVarMapMain.listTableADSB.Remove(GlobalVarMapMain.listTableADSB[iijl]);
                }
            }

            // ...............................................................

            GlobalVarMapMain.fl_AirPlane = 0;
            objClassFunctionsMain.GetADSB_DB(GlobalVarMapMain.listTableADSB,0);

           //mapCtrl.Txt10.Text = "Количество самолетов:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);
            mapCtrl.Txt10.Text = "Number of aircraft:" + " " + Convert.ToString(mapCtrl.objClassInterfaceMap.List_AP.Count);

            // ForArsen
            List<TempADSB> lst1 = new List<TempADSB>();

            for (int ia1 = 0; ia1 < mapCtrl.objClassInterfaceMap.List_AP.Count; ia1++)
            {
                ModelsTablesDBLib.TempADSB ob1 = new TempADSB();

                ob1.Coordinates = new Coord();
                ob1.ICAO = mapCtrl.objClassInterfaceMap.List_AP[ia1].ICAO;
                ob1.Id = mapCtrl.objClassInterfaceMap.List_AP[ia1].Id;
                ob1.Coordinates.Altitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Altitude;
                ob1.Coordinates.Latitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Latitude;
                ob1.Coordinates.Longitude = mapCtrl.objClassInterfaceMap.List_AP[ia1].Coordinates.Longitude;
                ob1.Time = mapCtrl.objClassInterfaceMap.List_AP[ia1].Time;
                lst1.Add(ob1);
            }

            // Arsen_Tab
            //aDSBControl.AddRange(new List<TempADSB>() { lst1 });
            aDSBControl.AddRange(lst1);

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Airplanes

            // Clear and redraw >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            mapCtrl.objClassMapRastrReDraw.ReDrawMapAll();
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Clear and redraw

            // -------------------------------------------------------------------------------------------------
            // Подписка на все остальные события
            
           // Updated table of ИРИ ФРЧ
           // Для очистки
           (clientDB.Tables[NameTable.TempFWS] as ITableUpdate<TempFWS>).OnUpTable += OnTempFWSUp;
           (clientDB.Tables[NameTable.TempFWS] as ITableAddRange<TempFWS>).OnAddRange += OnTempFWSAddRange;

           // Updated table of ИРИ ФРЧ ЦР
           (clientDB.Tables[NameTable.TableReconFWS] as ITableUpdate<TableReconFWS>).OnUpTable += OnTableReconFWSUp;
           // Updated table of ИРИ ФРЧ РП
           (clientDB.Tables[NameTable.TableSuppressFWS] as ITableUpdate<TableSuppressFWS>).OnUpTable += OnTableSuppressFWSUp;
           // Updated table of ИРИ ППРЧ РП
           (clientDB.Tables[NameTable.TableSuppressFHSS] as ITableUpdate<TableSuppressFHSS>).OnUpTable += OnTableSuppressFHSSUp;

           // ИРИ ППРЧ
           // Для очистки
           (clientDB.Tables[NameTable.TableReconFHSS] as ITableUpdate<TableReconFHSS>).OnUpTable += OnTableReconFHSSUp;
           // For Coordinates
           // Для очистки
           (clientDB.Tables[NameTable.TableSourceFHSS] as ITableUpdate<TableSourceFHSS>).OnUpTable += OnTableSourceFHSSUp;
           (clientDB.Tables[NameTable.TableReconFHSS] as ITableAddRange<TableReconFHSS>).OnAddRange += OnTableReconFHSSAddRange;
           (clientDB.Tables[NameTable.TableSourceFHSS] as ITableAddRange<TableSourceFHSS>).OnAddRange += OnTableSourceFHSSAddRange;

           // Updated table of ASP (СП)
           (clientDB.Tables[NameTable.TableASP] as ITableUpdate<TableASP>).OnUpTable += OnTableAspUp;

           // ??????????????????????????
           // Updated table of AP
           // OLD
           //(clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += OnTempADSB;
           //fill ADSB table by planes + Map
           (clientDB.Tables[NameTable.TempADSB] as ITableUpdate<TempADSB>).OnUpTable += HandlerUpdateTempADSB;
           (clientDB.Tables[NameTable.TempADSB] as ITableUpRecord<TempADSB>).OnAddRecord += HandlerAddRecordTempADSB;

            //  GPS
            (clientDB.Tables[NameTable.TempGNSS] as ITableUpdate<TempGNSS>).OnUpTable += OnTempGNSSUp;
            // -------------------------------------------------------------------------------------------------
            GlobalVarMapMain.fltbl = true;
            // -------------------------------------------------------------------------------------------------


        }

        private void latRLS_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        // **************************************************************************************** LOAD_TABLES

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CONNECT_BD

        // ********************************************************************************************* DATA_BASE


    } // Class
} // Namespace
