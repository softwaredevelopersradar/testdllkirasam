﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMapRastr
{
    // Трасса источников
    public class TrackSource
    {
        public int ID { get; set; }

        // List of marks (sources)
        public List<TimeMarkCoord> TimeMarksCoord { get; set; }

        // Note(Примечание)
        public string Note { get; set; }

        // RLS
        public List<Station> stations = new List<Station>();

        // Строб
        public double StrobX;
        public double StrobY;
        public double StrobZ;

        // Constructor *******************************************************************

        public TrackSource(
                           // Лист станций
                           List<Station> _stations,
                           // Лист трасс ВО
                           ref List<TrackAirObject> _listTrackAirObject
                          )
        {
            stations = _stations;
        }
        // ******************************************************************* Constructor

        // AddMark *************************************************************************
        // Добавить отметку в эту трассу источников

        public void AddMarkSource(
                                  // Входная отметка
                                  TimeMarkCoord MarkSource
                                 )
        {
            if(TimeMarksCoord!=null)
            {
                // !!! До этого в головном алгоритме:
                // TmeMarkCoord ob1=new ...
                // ob1.Xe=...
                //ob1.Error=ob1.ErrorMeasurement(...

                //MarkSource.Xe = MarkSource.X;
                //MarkSource.Ye = MarkSource.Y;
                //MarkSource.Ze = MarkSource.Z;

                TimeMarksCoord.Add(MarkSource);
                TimeMarksCoord[TimeMarksCoord.Count - 1].ID = TimeMarksCoord.Count-1;

            } // TimeMarksCoord!=null
            else
            {
                ;
            }
        }
        // ************************************************************************* AddMark

        // DefineMarkTrack ************************************************************************
        // Проверка: подходит ли отметка к трассе источника

        public bool DefineMarkTrack(
                                  // Входная отметка
                                  TimeMarkCoord MarkSource,

                                  // Максимально допустимое время ожидания отметки
                                  double twait,
                                  // Допустимая скорость дрона
                                  double Vdop,
                                  // Тип матрицы ошибок измерений для инициализации фильтра
                                  bool flagStatDynInit

                                   )
        {
            double a = 0;
            double b = 0;
            double c = 0;

            if (TimeMarksCoord.Count > 0)
            {
                // ------------------------------------------------------------------------------------

                // Подходит по времени ожидания
                if ((MarkSource.Time - TimeMarksCoord[TimeMarksCoord.Count - 1].Time) <= twait)
                {
                    // .................................................................................
                    // Проверка по стробу

                    // Вычисляем строб (эллипсоид)
                    CalcStrobSource(MarkSource,Vdop, flagStatDynInit);

                    // !!! Полуоси эллипсоида -> это StrobX,StrobZ,StrobY
                    // Определяем, лежит ли отметка внутри эллипсоида
                    /*
                                        if(
                                           ( (((MarkSource.X - TimeMarksCoord[TimeMarksCoord.Count - 1].X) * (MarkSource.X - TimeMarksCoord[TimeMarksCoord.Count - 1].X)) /(StrobX*StrobX))+
                                             (((MarkSource.Y - TimeMarksCoord[TimeMarksCoord.Count - 1].Y) * (MarkSource.Y - TimeMarksCoord[TimeMarksCoord.Count - 1].Y)) / (StrobY * StrobY)) +
                                             (((MarkSource.Z - TimeMarksCoord[TimeMarksCoord.Count - 1].Z) * (MarkSource.Z - TimeMarksCoord[TimeMarksCoord.Count - 1].Z)) / (StrobZ * StrobZ)) )<=1
                                          )
                    */
                    if (
                       ((((MarkSource.X - TimeMarksCoord[TimeMarksCoord.Count - 1].X) * (MarkSource.X - TimeMarksCoord[TimeMarksCoord.Count - 1].X)) / (StrobX * StrobX)) +
                         //(((MarkSource.Y - TimeMarksCoord[TimeMarksCoord.Count - 1].Y) * (MarkSource.Y - TimeMarksCoord[TimeMarksCoord.Count - 1].Y)) / (StrobY * StrobY)) +
                         (((MarkSource.Z - TimeMarksCoord[TimeMarksCoord.Count - 1].Z) * (MarkSource.Z - TimeMarksCoord[TimeMarksCoord.Count - 1].Z)) / (StrobZ * StrobZ))) <= 1
                      )

                    {
                        return true;
                    }
                    else
                        return false;
                    // .................................................................................

                }// Подходит по времени ожидания
                 // ------------------------------------------------------------------------------------

                else
                    return false;
                // ------------------------------------------------------------------------------------

            } // TimeMarksCoord.Count > 0

            return false;
        }
        // ************************************************************************ DefineMarkTrack

        // CalcStrobSource ************************************************************************
        // Вычисление строба для источника
        // Это эллипсоид
        // !!! Матрица ошибок измерений считается при добавлении отметки 

        public void CalcStrobSource(
                                  // Входная отметка
                                  TimeMarkCoord MarkSource,

                                  // Допустимая скорость дрона
                                  double Vdop,
                                  // Тип матрицы ошибок измерений для инициализации фильтра
                                  bool flagStatDynInit
                                   )
        {
            double dtt = 0;
            double Sr = 0;
            double ThetaX = 0;
            double ThetaY = 0;
            double ThetaZ = 0;

            // ------------------------------------------------------------------------------------

            if (TimeMarksCoord.Count > 0)
            {
                dtt = MarkSource.Time - TimeMarksCoord[TimeMarksCoord.Count - 1].Time;

                // ------------------------------------------------------------------------------------
                Sr = Math.Sqrt(
                               Math.Pow((stations[0].Position.X - MarkSource.X), 2) +
                               Math.Pow((stations[0].Position.Y - MarkSource.Y), 2) +
                               Math.Pow((stations[0].Position.Z - MarkSource.Z), 2)
                              );
                // ------------------------------------------------------------------------------------
                // Theta(i-1)
                if (flagStatDynInit == true)
                {
                    ThetaX = TimeMarksCoord[TimeMarksCoord.Count - 1].Error[0, 0];
                    ThetaZ = TimeMarksCoord[TimeMarksCoord.Count - 1].Error[1, 1];
                    ThetaY = TimeMarksCoord[TimeMarksCoord.Count - 1].Error[2, 2];
                }
                else
                {
                    ThetaX = TimeMarksCoord[TimeMarksCoord.Count - 1].ErrorStat[0, 0];
                    ThetaZ = TimeMarksCoord[TimeMarksCoord.Count - 1].ErrorStat[1, 1];
                    ThetaY = TimeMarksCoord[TimeMarksCoord.Count - 1].ErrorStat[2, 2];
                }
                // ------------------------------------------------------------------------------------
                // Считаем строб

                StrobX = Vdop * dtt + ThetaX;
                StrobY = Vdop * dtt + ThetaY;
                StrobZ = Vdop * dtt + ThetaZ;
                // ------------------------------------------------------------------------------------

            } // TimeMarksCoord.Count > 0

        }
        // ************************************************************************ CalcStrobSource

        // Инициализация фильтра*******************************************************************
        // Алгоритм фильтрации по фиксированной выборке данных (Инициализация траекторного фильтра).
        // По трем первым идентифицированным отметкам производится квазиоптимальная линейная 
        // фильтрация и экстраполяция параметров траектории

        // Допущение: ошибки измерения координат являются независимыми
        // ****************************************************************************************

        public void InitKalman(
                            bool flagStatDynInit,
                           // Лист трасс ВО
                           ref List<TrackAirObject> _listTrackAirObject
                              )
        {
            // -------------------------------------------------------------------------------
            // Количество точек инициализации
            int NumberMarks = TimeMarksCoord.Count;

            int i = 0;
            int i1 = 0;
            int j = 0;
            int k = 0;

            Matrix objMatrix = new Matrix();

            // -------------------------------------------------------------------------------
            // Вектор измерений (3*NumberMarks)x1 Y=|x0 ... xn z0 ... zn y0 ... yn| транспонированная

            double[,] Y = new double[3* NumberMarks, 1];

            // Xi
            for(i=0; i< NumberMarks; i++)
            {
                Y[i, 0] = TimeMarksCoord[i].X;
            }
            // Zi
            for (i = NumberMarks; i < 2*NumberMarks; i++)
            {
                Y[i, 0] = TimeMarksCoord[i-NumberMarks].Z;
            }
            // Yi
            for (i = 2*NumberMarks; i < 3 * NumberMarks; i++)
            {
                Y[i, 0] = TimeMarksCoord[i-2*NumberMarks].Y;
            }
            // -------------------------------------------------------------------------------
            // Матрица Вандермота (3*NumberMarks)x(6) !!! 6-> по 3-м координатам

            // Матрица Вандермота (3*NumberMarks)x(6)
            double[,] A = new double[3 * NumberMarks, 6];
            // Транспонированная 
            double[,] AT = new double[6, 3 * NumberMarks];

            // Пачки (всегда 3: по 3-м координатам)
            for (j = 0; j < 3; j++)
            {
                // ..........................................................................
                // Передние нули

                for (k = 0; k < (2 * j); k++)
                {
                    for (i = (j * NumberMarks); i < ((j + 1) * NumberMarks); i++)
                    {
                        A[i, k] = 0;

                    } // FOR(i)

                } // FOR(k)%
                // ..........................................................................
                // 1,t(i)

                k = 2 * j;
                i1 = 0;
                for (i = (j * NumberMarks); i < ((j + 1) * NumberMarks); i++)
                {
                    A[i, k] = 1;
                    A[i, k + 1] = TimeMarksCoord[i1].Time - TimeMarksCoord[0].Time; // Время относительно 0-й отметки
                    i1 += 1;

                } // FOR(i)

                // ..........................................................................
                // Последние нули

                for (k = (2 * j + 2); k < 6; k++)
                {
                    for (i = (j * NumberMarks); i < ((j + 1) * NumberMarks); i++)
                    {
                        A[i, k] = 0;

                    } // FOR(i)

                } // FOR(k)
                // ..........................................................................

            } // FOR(j)

            // Транспонированная
            objMatrix.MTransp(
                    // Размер исходной
                    3 * NumberMarks,
                    6,
                    A,
                    ref AT
                   );

            // -------------------------------------------------------------------------------
            // Матрица ошибок измерений (3*NumberMarks)x(3*NumberMarks)

            double[,] R0 = new double[3 * NumberMarks, 3 * NumberMarks];

            // Пачки
            for (j = 0; j < NumberMarks; j++)
            {
                // ..........................................................................
                // Передние нули

                for (k = 0; k < (3 * j); k++)
                {
                    for (i = (j * 3); i < ((j + 1) * 3); i++)
                    {
                        R0[i, k] = 0;

                    } // FOR(i)

                } // FOR(k)
                // ..........................................................................
                // Di

                k = 3 * j;
                i = 3 * j;
                i1 = j;

                if (flagStatDynInit == true)
                {
                    R0[i, k] = TimeMarksCoord[i1].Error[0, 0];
                    R0[i, k + 1] = TimeMarksCoord[i1].Error[0, 1];
                    R0[i, k + 2] = TimeMarksCoord[i1].Error[0, 2];
                    R0[i + 1, k] = TimeMarksCoord[i1].Error[1, 0];
                    R0[i + 1, k + 1] = TimeMarksCoord[i1].Error[1, 1];
                    R0[i + 1, k + 2] = TimeMarksCoord[i1].Error[1, 2];
                    R0[i + 2, k] = TimeMarksCoord[i1].Error[2, 0];
                    R0[i + 2, k + 1] = TimeMarksCoord[i1].Error[2, 1];
                    R0[i + 2, k + 2] = TimeMarksCoord[i1].Error[2, 2];
                }
                else
                {
                    R0[i, k] = TimeMarksCoord[i1].ErrorStat[0, 0];
                    R0[i, k + 1] = TimeMarksCoord[i1].ErrorStat[0, 1];
                    R0[i, k + 2] = TimeMarksCoord[i1].ErrorStat[0, 2];
                    R0[i + 1, k] = TimeMarksCoord[i1].ErrorStat[1, 0];
                    R0[i + 1, k + 1] = TimeMarksCoord[i1].ErrorStat[1, 1];
                    R0[i + 1, k + 2] = TimeMarksCoord[i1].ErrorStat[1, 2];
                    R0[i + 2, k] = TimeMarksCoord[i1].ErrorStat[2, 0];
                    R0[i + 2, k + 1] = TimeMarksCoord[i1].ErrorStat[2, 1];
                    R0[i + 2, k + 2] = TimeMarksCoord[i1].ErrorStat[2, 2];

                }
                // ..........................................................................
                // Последние нули

                for (k = (3 * j + 3); k < (3 * NumberMarks); k++)
                {
                    for (i = (j * 3); i < ((j + 1) * 3); i++)
                    {
                        R0[i, k] = 0;

                    } // FOR(i)

                } // FOR(k)
               // ..........................................................................

            } // FOR(j)
            // -------------------------------------------------------------------------------
            // Обратная матрица ошибок измерений

            double[,] R0_Inv = new double[3 * NumberMarks, 3 * NumberMarks];
            int i4 = 3 * NumberMarks;

            objMatrix.MInv(
                  i4,
                  R0,
                  ref R0_Inv
                );
            // -------------------------------------------------------------------------------
            // Корреляционная матрица 6x6 ошибок оценок параметров траектории
            // Psi(n-1) для фильтра

            double[,] Psi0_1 = new double[6, 3*NumberMarks];
            double[,] Psi0_2 = new double[6, 6];
            double[,] Psi0_3 = new double[6, 6];
            int i5 = 6;

            Psi0_1 = objMatrix.MatrixMultiplication(AT, R0_Inv);
            Psi0_2 = objMatrix.MatrixMultiplication(Psi0_1, A);
            //MInv(i5, Psi0_2, ref objClassTraj.PSI_N_1);
            objMatrix.MInv(i5, Psi0_2, ref Psi0_3);

            // -------------------------------------------------------------------------------
            // Матрица оценок параметров траектории 6x1
            // S(n-1) для фильтра

            double[,] S_1 = new double[6, 3*NumberMarks];
            double[,] S_2 = new double[6, 3 * NumberMarks];
            double[,] S_3 = new double[6, 1];


            S_1 = objMatrix.MatrixMultiplication(Psi0_3, AT);
            S_2 = objMatrix.MatrixMultiplication(S_1, R0_Inv);
            //objClassTraj.S_N_1 = MatrixMultiplication(S_2, Y);
            S_3 = objMatrix.MatrixMultiplication(S_2, Y);

            // -------------------------------------------------------------------------------
            // Состояние траектории после инициализации фильтра пишем как первую точку фильтра
            // S(n)=|Xn Vx Zn Vz Yn Vy| транспонированная
            /*
                        ClassTrackPointFiltr objClassTrackPointFiltr = new ClassTrackPointFiltr();

                        objClassTrackPointFiltr.X = objClassTraj.S_N_1[0, 0];
                        objClassTrackPointFiltr.VX = objClassTraj.S_N_1[1, 0];
                        objClassTrackPointFiltr.Y = objClassTraj.S_N_1[4, 0];
                        objClassTrackPointFiltr.VY = objClassTraj.S_N_1[5, 0];
                        objClassTrackPointFiltr.Z = objClassTraj.S_N_1[2, 0];
                        objClassTrackPointFiltr.VZ = objClassTraj.S_N_1[3, 0];

                        objClassTraj.listPointsTrajFiltr_I = new List<ClassTrackPointFiltr>();
                        objClassTraj.listPointsTrajFiltr_I.Add(objClassTrackPointFiltr);
            */
            // -------------------------------------------------------------------------------
            // _listTrackAirObject -> это лист трасс ВО
            // TimeMarksCoord -> Список источников этой трассы

            TrackAirObject objTrackAirObject1 = new TrackAirObject(
                                                                   //parameters,
                                                                   stations
                                                                  );
            objTrackAirObject1.Marks = new List<Mark>();

            objMatrix.ChangeMatrix(Psi0_3, ref objTrackAirObject1.PSI_N_1); // 6x6
            objMatrix.ChangeMatrix(S_3, ref objTrackAirObject1.S_N_1);      // 6x6
            objMatrix.ChangeMatrix(Psi0_3, ref objTrackAirObject1.PSI_N); // 6x6
            objMatrix.ChangeMatrix(S_3, ref objTrackAirObject1.S_N);      // 6x6
            //objTrackAirObject1.PSI_N_1 = Psi0_3; // 6x6
            //objTrackAirObject1.S_N_1 = S_3;      // 6x1

            // Это будет  новая  отметка в новой траектории листа траекторий ВО
            Mark objMark1 = new Mark();

            if (TimeMarksCoord.Count > 0)
            {    
                // Реальные координаты (последняя точка списка источников) :XYZ в МЗСК
                objMark1.CoordReal = new TimeMarkCoord(
                                                       TimeMarksCoord[TimeMarksCoord.Count-1].Latitude,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Longitude,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Altitude,
                                                       //parameters,
                                                       stations,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Time,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Freq,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].dFreq,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1]._time

                                                      );

                // Фильтрованные координаты :XYZ в МЗСК (пока здесь лежат реальные)
                objMark1.CoordFilter = new TimeMarkCoord(
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Latitude,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Longitude,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Altitude,
                                                       //parameters,
                                                       stations,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Time,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Freq,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].dFreq,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1]._time
                                                      );

                // Экстраполированные координаты :XYZ в МЗСК (пока здесь лежат реальные)
                objMark1.CoordExtr = new TimeMarkCoord(
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Latitude,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Longitude,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Altitude,
                                                       //parameters,
                                                       stations,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Time,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].Freq,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1].dFreq,
                                                       TimeMarksCoord[TimeMarksCoord.Count - 1]._time

                                                      );


                // Фильтрованные координаты (Состояние траектории после инициализации фильтра):XYZ в МЗСК
                objMark1.CoordFilter.X = S_3[0, 0];
                objMark1.CoordFilter.VX = S_3[1, 0];
                objMark1.CoordFilter.Y = S_3[4, 0];
                objMark1.CoordFilter.VY = S_3[5, 0];
                objMark1.CoordFilter.Z = S_3[2, 0];
                objMark1.CoordFilter.VZ = S_3[3, 0];

                // Экстраполированные координаты -> Здесь пока равны фильтрованным
                objMark1.CoordExtr.X = objMark1.CoordFilter.X;
                objMark1.CoordExtr.Y = objMark1.CoordFilter.Y;
                objMark1.CoordExtr.Z = objMark1.CoordFilter.Z;
                objMark1.CoordExtr.VX = objMark1.CoordFilter.VX;
                objMark1.CoordExtr.VY = objMark1.CoordFilter.VY;
                objMark1.CoordExtr.VZ = objMark1.CoordFilter.VZ;

                // Время последней отметки
                objMark1.Time = TimeMarksCoord[TimeMarksCoord.Count - 1].Time;

            }
            // -------------------------------------------------------------------------------
            // Завязываем новую траекторию

            // В новой отметке новой траектории(это первая отметка)
            objMark1.Skip = false; // 0
            objMark1.ID = 0;

            // В новой траектории
            objTrackAirObject1.AddTek = false;
            objTrackAirObject1.dRR = -1;
            objTrackAirObject1.PopStrob = false;
            objTrackAirObject1.ID = _listTrackAirObject.Count;

            // Добавляем отметку в новую траекторию
            objTrackAirObject1.Marks.Add(objMark1);
            // Добавляем новую траекторию в лист траекторий
            _listTrackAirObject.Add(objTrackAirObject1);
            // -------------------------------------------------------------------------------

        }
        // ****************************************************************** Инициализация фильтра

    } // Class
} // Namespace
