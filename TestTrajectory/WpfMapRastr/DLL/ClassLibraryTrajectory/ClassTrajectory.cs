﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using System.Windows.Media;

namespace ClassLibraryTrajectory
{
    public partial class ClassTrajectory
    {
        // Генераторы случайных чисел (Гаусс)
        public ClassMRND objClassMRND_X;
        public ClassMRND objClassMRND_Y;
        public ClassMRND objClassMRND_Z;

        public ClassTrajectory()
        {
            objClassMRND_X = new ClassMRND();
            objClassMRND_Y = new ClassMRND();
            objClassMRND_Z = new ClassMRND();
        }


        // MAIN *********************************************************************************

        // FILTER_FILE **************************************************************************
        // !!! ВАРИАНТ ДЛЯ РАБОТЫ С ФАЙЛАМИ
        // lstmain- основной файл отметок
        // !!! При формировании листа первые отметки поместить в конец -> будем по мере обработки удалять их

        public List<ClassTraj> f_Trajectory(
                                 // Входной лист отметок
                                 List<ClassTrackPoint> lstmain,
                                 // Параметры для алгоритма формирования траекторий
                                 ClassParameters objClassParameters,
                                 // Флаг количества траекторий
                                 // =false -> одна траектория
                                 // =true -> несколько траекторий
                                 bool ModeNumbTraj

                                )
        {
            // ....................................................................................
            // Для записи файла Kalman

            // Get current dir
            string dir = "";
            dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dirK = "";
            dirK = dir + "\\FILES\\" + "Kalman.txt";
            StreamWriter swK = new StreamWriter(dirK, false, System.Text.Encoding.Default);
            // ....................................................................................

            // ....................................................................................

            // Выходной лист сформированных траекторий
            List<ClassTraj> list_tracks = new List<ClassTraj>();

            // Если в ходе обработки по траекториям отметка не вошла ни в одну траекторию 
            // (flTraj = false остался) -> это завязка новой траектории
            bool flTraj = false;

            // Счетчики
            int if1 = 0; // по отметкам
            int if2 = 0; // по траекториям

            double vij = 0;
            double dt = 0;
            double rij = 0;
            double Sdop = 0; // Строб
            // ....................................................................................
            // Строб

            double strobeMax = 0;

            //Sdop = twait * Vdop;
            Sdop = objClassParameters.strob;
            // ....................................................................................

            // IF1
            if (lstmain.Count != 0)
            {

                // FOR1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // Основной цикл обработки отметок входного листа

                // FOR1
                for (if1 = lstmain.Count - 1; if1 >= 0; if1--)
                {

                    //lstmain[if1].Z = 100;

                    // FOR2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // Цикл по листу сформированных траекторий

                    // !!! Если внутри FOR2 не установлен в true(не подошел ни в одну трассу) -> 
                    //     это новая трасса (траектория)
                    flTraj = false;

                    // FOR2
                    for (if2 = 0; if2 < list_tracks.Count; if2++)
                    {
                     // !!! Анализируем число отметок в i-й траектории

                         // IF3(Одна отметка) *********************************************************
                         // В анализируемой траектории пока была одна отметка

                         // IF3
                         if(list_tracks[if2].listPointsTraj_I.Count==1)
                         {

                            // Интервал времени между n-й и (n-1)-й отметками
                            dt = lstmain[if1].timePoint - list_tracks[if2].listPointsTraj_I[0].timePoint;

                            // IF6 (Одна траектория) ---------------------------------------------------
                            // Траектория одна, т.е. это была пока единственная отметка

                            // IF6
                            if(list_tracks.Count==1)
                            {

                                // IF8 (dt>twait) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // Время ожидания второй отметки превысило допустимое

                                // IF8
                                if(dt> objClassParameters.twait)
                                {
                                    // Заменяем старую отметку на новую
                                    //list_tracks[if2].listPointsTraj_I.Remove(list_tracks[if2].listPointsTraj_I[0]);
                                    list_tracks[if2].listPointsTraj_I.Add(lstmain[if1]);
                                    flTraj = true;
                                    break;

                                } // IF8 (Время ожидания второй отметки превысило допустимое)
                                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IF8 (dt>twait)

                                // ELSE_IF8 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // Время ожидания второй отметки НЕ превысило допустимое

                                else
                                {
                                    vij = Math.Sqrt(
                                                (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[0].X) *
                                                (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[0].X) +
                                                (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[0].Y) *
                                                (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[0].Y) +
                                                (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[0].Z) *
                                                (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[0].Z)
                                                )/dt;

                                    // IF9 ...................................................................
                                    // Скорость меньше допустимой
                                    // !!! Добавляем 2-ю отметку (при отсутствии других трасс)

                                    if((vij<= objClassParameters.Vdop)&&(vij>=0))
                                    {
                                        list_tracks[if2].listPointsTraj_I.Add(lstmain[if1]);
                                        flTraj = true;
                                        break;
                                    }
                                    // ................................................................... IF9

                                    // ELSE_IF9 ...............................................................
                                    // Возможно, завязка новой траектории
                                    // ???

                                    else
                                    {

                                        // TTT ....................................
                                        // Закомментировать для одной траектории

                                        if (ModeNumbTraj == true) // Несколько траекторий
                                        {
                                            ClassTraj objClassTraj2 = new ClassTraj();
                                            objClassTraj2.listPointsTraj_I = new List<ClassTrackPoint>();
                                            objClassTraj2.listPointsTraj_I.Add(lstmain[if1]);
                                            // Добавляем новую траекторию в список траекторий
                                            list_tracks.Add(objClassTraj2);
                                        }
                                        // ..........................................

                                        flTraj = true;
                                        break;
                                    }
                                    // ............................................................... ELSE_IF9

                                } // ELSE_IF8 (Время ожидания второй отметки НЕ превысило допустимое)
                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ELSE_IF8

                            } //IF6(Одна траектория)
                            // --------------------------------------------------- IF6 (Одна траектория)

                            // ELSE_IF6 ----------------------------------------------------------------
                            // число траекторий >1

                            // Else IF6
                            else
                            {
                                // Попала ли n-я отметка в строб (n-1)-й
                                rij = Math.Sqrt(
                                                (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X)*
                                                (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X)+
                                                (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) *
                                                (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) +
                                                (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z) *
                                                (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z) 
                                                );
                                dt = lstmain[if1].timePoint - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].timePoint;
                                vij = rij / dt;

                               // IF7 попала в строб
                                //if (rij<=Sdop)
                                if ((vij <= objClassParameters.Vdop) && (vij >= 0))
                                {
                                    // Добавляем отметку к траектории
                                    list_tracks[if2].listPointsTraj_I.Add(lstmain[if1]);
                                    flTraj = true;
                                    break;
                             
                                }
                            // else -> идем дальше

                            } // ELSE_IF6 (число траекторий >1)
                            // ---------------------------------------------------------------- ELSE_IF6


                        } // IF3(Одна отметка)
                        // ********************************************************** IF3(Одна отметка)

                        // IF4(Две отметки) **********************************************************
                        // В анализируемой траектории две отметки
                        // Т.Е. это анализ 3-ей пришедшей отметки

                        // IF4
                        else if (list_tracks[if2].listPointsTraj_I.Count == 2)
                        {
                            rij = Math.Sqrt(
                                            (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) *
                                            (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) +
                                            (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) *
                                            (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) +
                                            (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z) *
                                            (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z)
                                            );
                            dt = lstmain[if1].timePoint - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].timePoint;
                            vij = rij / dt;

                            // IF10 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Отметка попала в строб

                            // IF10
                            //if (rij<=Sdop)
                            if((vij>=0)&&(vij< objClassParameters.Vdop))
                            {
                                // Добавляем 3-юю отметку

                                list_tracks[if2].listPointsTraj_I.Add(lstmain[if1]);
                                flTraj = true;

                                // Инициализация фильтра Калмана
                                InitFilter(list_tracks[if2], 
                                           objClassParameters.CKO_X,
                                           objClassParameters.CKO_Y,
                                           objClassParameters.CKO_Z,
                                           // Параметры для алгоритма формирования траекторий
                                           objClassParameters
                                           );

                                break;

                            }
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IF10

                            // Else on IF10 -> идем дальше

                        } // IF4(Две отметки)
                        // ********************************************************** IF4(Две отметки)

                        // ELSE* (>=3 отметок) *******************************************************
                        // В анализируемой траектории >=3 отметки (инициализация фильтра уже прошла)

                        // ELSE* 
                        else 
                        {
                            rij = Math.Sqrt(
                                            (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) *
                                            (lstmain[if1].X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) +
                                            (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) *
                                            (lstmain[if1].Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) +
                                            (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z) *
                                            (lstmain[if1].Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z)
                                            );
                            dt = lstmain[if1].timePoint - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].timePoint;
                            vij = rij / dt;

                            // IF11_Попала в строб ..................................................

                            //if (rij<=Sdop)
                            if((vij>=0)&&(vij< objClassParameters.Vdop))
                            {

                                // Добавляем отметку в траекторию
                                list_tracks[if2].listPointsTraj_I.Add(lstmain[if1]);
                                flTraj = true;

                                // Фильтр Калмана
                                FilterKalm(
                                    list_tracks[if2],
                                    objClassParameters.CKOA_pl,
                                    objClassParameters.CKOA_H,

                                    objClassParameters.CKO_X,
                                    objClassParameters.CKO_Y,
                                    objClassParameters.CKO_Z,

                                    // Параметры для алгоритма формирования траекторий
                                    objClassParameters
                                          );

                                // File !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                // !!! Запись в файл Kalman

                                string sfilter = "";

/*
                                sfilter = Convert.ToString(lstmain[if1].timePoint * 1000) + " " + Convert.ToString(lstmain[if1].X) +
                                                          " " + Convert.ToString(lstmain[if1].Y) + " " + Convert.ToString(lstmain[if1].Z) +
                                                          " " +
                                                          Convert.ToString(list_tracks[if2].listPointsTrajFiltr_I[list_tracks[if2].listPointsTrajFiltr_I.Count - 1].X) + " " +
                                                          Convert.ToString(list_tracks[if2].listPointsTrajFiltr_I[list_tracks[if2].listPointsTrajFiltr_I.Count - 1].Y) + " " +
                                                          Convert.ToString(list_tracks[if2].listPointsTrajFiltr_I[list_tracks[if2].listPointsTrajFiltr_I.Count - 1].Z)+" "+
                                                          Convert.ToString(list_tracks[if2].listPointsTrajFiltr_I[list_tracks[if2].listPointsTrajFiltr_I.Count - 1].VX) +" "+
                                                          Convert.ToString(list_tracks[if2].listPointsTrajFiltr_I[list_tracks[if2].listPointsTrajFiltr_I.Count - 1].VY) + " " +
                                                          Convert.ToString(list_tracks[if2].listPointsTrajFiltr_I[list_tracks[if2].listPointsTrajFiltr_I.Count - 1].VZ);

                                swK.WriteLine(sfilter);
*/
                                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! File


                                break;

                            }
                            // .................................................. IF11_Попала в строб

                            // Else on IF11 -> идем дальше

                        } // ELSE* (>=3 отметок)
                        // ******************************************************* ELSE* (>=3 отметок)

                    } // FOR2
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FOR2

                    // IF2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // !!! Если внутри FOR2 flTraj не установлен в true(не подошел ни в одну трассу) -> 
                    //     это новая трасса (траектория)

                    // ???
                    if (flTraj==false)
                    {

                        // TTT ....................................................  
                        // Закомментировать для одной траектории 

                        if (ModeNumbTraj == true) // Несколько траекторий
                        {
                            // Класс новой траектории
                            ClassTraj objClassTraj1 = new ClassTraj();
                            objClassTraj1.listPointsTraj_I = new List<ClassTrackPoint>();
                            // Добавляем новую отметку
                            objClassTraj1.listPointsTraj_I.Add(lstmain[if1]);
                            // Удаляем отметку из основного списка
                            lstmain.Remove(lstmain[if1]);
                            // Добавляем новую траекторию в список траекторий
                            list_tracks.Add(objClassTraj1);
                        }
                        // .........................................................

                        // TTT ...............................................
                        // Откомментировать для одной траектории

                        else if (ModeNumbTraj == false) // Одна траектория
                        {
                            if ((list_tracks.Count != 0) && (lstmain.Count != -1))
                            // Удаляем отметку из основного списка
                            lstmain.Remove(lstmain[if1]);
                        }
                        // ....................................................


                    } // IF2 (flTraj==false)
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IF2

                    // ELSE_IF2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // (flTraj==true)
                    // Внутри FOR2 flTraj установлен в true(подошел в одну из трасс)

                    else
                    {
                        if(lstmain.Count > 0 )
                        lstmain.Remove(lstmain[if1]);

                    } // ELSE IF2 (flTraj==true)
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ELSE_IF2

                    // ???
                    if(list_tracks.Count==0)
                    {
                       // Класс новой траектории
                       ClassTraj objClassTraj1 = new ClassTraj();
                       objClassTraj1.listPointsTraj_I = new List<ClassTrackPoint>();
                       // Добавляем новую отметку
                       objClassTraj1.listPointsTraj_I.Add(lstmain[if1]);

                       // Добавляем новую траекторию в список траекторий
                       list_tracks.Add(objClassTraj1);

                        if (lstmain.Count > 0)
                        {
                            try
                            {
                                lstmain.Remove(lstmain[if1]);
                            }
                            catch
                            { }
                        }
                    }

                } // FOR1
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FOR1

            } // IF1 (lstmain.Count != 0)

            // ....................................................................................
            // Files
            swK.Close();

            return list_tracks;
            // ....................................................................................

        }
        // ************************************************************************** FILTER_FILE

        // FILTER_REAL **************************************************************************
        // !!! ВАРИАНТ ДЛЯ РАБОТЫ В РЕЖИМЕ РЕАЛЬНОГО ВРЕМЕНИ
        // На вход идет по одной отметке

        public void f_Trajectory1(
                                 // !!! Одна отметка
                                 ClassTrackPoint lstmain,
                                 // Параметры для алгоритма формирования траекторий
                                 ClassParameters objClassParameters,
                                 // Лист сформированных траекторий
                                 ref List<ClassTraj> list_tracks
                                )
        {

            //lstmain.Z = 100;

            // ....................................................................................

            // Выходной лист сформированных траекторий
            //List<ClassTraj> list_tracks = new List<ClassTraj>();

            // Если в ходе обработки по траекториям отметка не вошла ни в одну траекторию 
            // (flTraj = false остался) -> это завязка новой траектории
            bool flTraj = false;

            // Счетчики
            int if1 = 0; // по отметкам
            int if2 = 0; // по траекториям

            double vij = 0;
            double dt = 0;
            double rij = 0;
            double Sdop = 0; // Строб
            // ....................................................................................
            // Строб

            double strobeMax = 0;

            //Sdop = twait * Vdop;
            Sdop = objClassParameters.strob;
            // ....................................................................................

                    // FOR2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // Цикл по листу сформированных траекторий

                    // !!! Если внутри FOR2 не установлен в true(не подошел ни в одну трассу) -> 
                    //     это новая трасса (траектория)
                    flTraj = false;

                    // FOR2
                    for (if2 = 0; if2 < list_tracks.Count; if2++)
                    {
                        // !!! Анализируем число отметок в i-й траектории

                        // IF3(Одна отметка) *********************************************************
                        // В анализируемой траектории пока была одна отметка

                        // IF3
                        if (list_tracks[if2].listPointsTraj_I.Count == 1)
                        {

                            // Интервал времени между n-й и (n-1)-й отметками
                            dt = lstmain.timePoint - list_tracks[if2].listPointsTraj_I[0].timePoint;

                            // IF6 (Одна траектория) ---------------------------------------------------
                            // Траектория одна, т.е. это была пока единственная отметка

                            // IF6
                            if (list_tracks.Count == 1)
                            {

                                // IF8 (dt>twait) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // Время ожидания второй отметки превысило допустимое

                                // IF8
                                if (dt > objClassParameters.twait)
                                {
                                    // Заменяем старую отметку на новую
                                    //list_tracks[if2].listPointsTraj_I.Remove(list_tracks[if2].listPointsTraj_I[0]);
                                    list_tracks[if2].listPointsTraj_I.Add(lstmain);
                                    flTraj = true;
                                    break;

                                } // IF8 (Время ожидания второй отметки превысило допустимое)
                                  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IF8 (dt>twait)

                                // ELSE_IF8 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                // Время ожидания второй отметки НЕ превысило допустимое

                                else
                                {
                                    vij = Math.Sqrt(
                                                (lstmain.X - list_tracks[if2].listPointsTraj_I[0].X) *
                                                (lstmain.X - list_tracks[if2].listPointsTraj_I[0].X) +
                                                (lstmain.Y - list_tracks[if2].listPointsTraj_I[0].Y) *
                                                (lstmain.Y - list_tracks[if2].listPointsTraj_I[0].Y) +
                                                (lstmain.Z - list_tracks[if2].listPointsTraj_I[0].Z) *
                                                (lstmain.Z - list_tracks[if2].listPointsTraj_I[0].Z)
                                                ) / dt;

                                    // IF9 ...................................................................
                                    // Скорость меньше допустимой
                                    // !!! Добавляем 2-ю отметку (при отсутствии других трасс)

                                    if ((vij <= objClassParameters.Vdop) && (vij >= 0))
                                    {
                                        list_tracks[if2].listPointsTraj_I.Add(lstmain);
                                        flTraj = true;
                                        break;
                                    }
                                    // ................................................................... IF9

                                    // ELSE_IF9 ...............................................................
                                    // Возможно, завязка новой траектории
                                    // ???

                                    else
                                    {
                                        /*
                                         ClassTraj objClassTraj2 = new ClassTraj();
                                         objClassTraj2.listPointsTraj_I = new List<ClassTrackPoint>();
                                         objClassTraj2.listPointsTraj_I.Add(lstmain[if1]);
                                         flTraj = true;

                                         // Добавляем новую траекторию в список траекторий
                                         list_tracks.Add(objClassTraj2);
                                        */

                                        flTraj = true;
                                        break;
                                    }
                                    // ............................................................... ELSE_IF9

                                } // ELSE_IF8 (Время ожидания второй отметки НЕ превысило допустимое)
                                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ELSE_IF8

                            } //IF6(Одна траектория)
                            // --------------------------------------------------- IF6 (Одна траектория)

                            // ELSE_IF6 ----------------------------------------------------------------
                            // число траекторий >1

                            // Else IF6
                            else
                            {
                                // Попала ли n-я отметка в строб (n-1)-й
                                rij = Math.Sqrt(
                                                (lstmain.X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) *
                                                (lstmain.X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) +
                                                (lstmain.Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) *
                                                (lstmain.Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) +
                                                (lstmain.Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z) *
                                                (lstmain.Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z)
                                                );
                                dt = lstmain.timePoint - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].timePoint;
                                vij = rij / dt;

                                // IF7 попала в строб
                                //if (rij<=Sdop)
                                if ((vij <= objClassParameters.Vdop) && (vij >= 0))
                                {
                                    // Добавляем отметку к траектории
                                    list_tracks[if2].listPointsTraj_I.Add(lstmain);
                                    flTraj = true;
                                    break;

                                }
                                // else -> идем дальше

                            } // ELSE_IF6 (число траекторий >1)
                            // ---------------------------------------------------------------- ELSE_IF6

                        } // IF3(Одна отметка)
                        // ********************************************************** IF3(Одна отметка)

                        // IF4(Две отметки) **********************************************************
                        // В анализируемой траектории две отметки
                        // Т.Е. это анализ 3-ей пришедшей отметки

                        // IF4
                        else if (list_tracks[if2].listPointsTraj_I.Count == 2)
                        {
                            rij = Math.Sqrt(
                                            (lstmain.X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) *
                                            (lstmain.X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) +
                                            (lstmain.Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) *
                                            (lstmain.Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) +
                                            (lstmain.Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z) *
                                            (lstmain.Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z)
                                            );
                            dt = lstmain.timePoint - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].timePoint;
                            vij = rij / dt;

                            // IF10 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Отметка попала в строб

                            // IF10
                            //if (rij<=Sdop)
                            if ((vij >= 0) && (vij < objClassParameters.Vdop))
                            {
                                // Добавляем 3-юю отметку

                                list_tracks[if2].listPointsTraj_I.Add(lstmain);
                                flTraj = true;

                                // Инициализация фильтра Калмана
                                InitFilter(
                                           list_tracks[if2],
                                           objClassParameters.CKO_X,
                                           objClassParameters.CKO_Y,
                                           objClassParameters.CKO_Z,
                                           // Параметры для алгоритма формирования траекторий
                                           objClassParameters
                                          );

                                break;

                            }
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IF10

                            // Else on IF10 -> идем дальше


                        } // IF4(Две отметки)
                        // ********************************************************** IF4(Две отметки)

                        // ELSE* (>=3 отметок) *******************************************************
                        // В анализируемой траектории >=3 отметки (инициализация фильтра уже прошла)

                        // ELSE* 
                        else
                        {

                            rij = Math.Sqrt(
                                            (lstmain.X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) *
                                            (lstmain.X - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].X) +
                                            (lstmain.Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) *
                                            (lstmain.Y - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Y) +
                                            (lstmain.Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z) *
                                            (lstmain.Z - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].Z)
                                            );
                            dt = lstmain.timePoint - list_tracks[if2].listPointsTraj_I[list_tracks[if2].listPointsTraj_I.Count - 1].timePoint;
                            vij = rij / dt;

                            // IF11_Попала в строб ..................................................

                            //if (rij<=Sdop)
                            if ((vij >= 0) && (vij < objClassParameters.Vdop))
                            {

                                // Добавляем отметку в траекторию
                                list_tracks[if2].listPointsTraj_I.Add(lstmain);
                                flTraj = true;

                                // Фильтр Калмана
                                FilterKalm(
                                    list_tracks[if2],
                                    objClassParameters.CKOA_pl,
                                    objClassParameters.CKOA_H,

                                    objClassParameters.CKO_X,
                                    objClassParameters.CKO_Y,
                                    objClassParameters.CKO_Z,

                                    // Параметры для алгоритма формирования траекторий
                                   objClassParameters

                                          );

                                break;

                            }
                            // .................................................. IF11_Попала в строб

                            // Else on IF11 -> идем дальше


                        } // ELSE* (>=3 отметок)
                        // ******************************************************* ELSE* (>=3 отметок)

                    } // FOR2
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FOR2

                    // IF2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // !!! Если внутри FOR2 flTraj не установлен в true(не подошел ни в одну трассу) -> 
                    //     это новая трасса (траектория)

                    // ???
                    if (flTraj == false)
                    {
                        /*                        
                        // Класс новой траектории
                        ClassTraj objClassTraj1 = new ClassTraj();
                        objClassTraj1.listPointsTraj_I = new List<ClassTrackPoint>();

                        // Добавляем новую отметку
                        objClassTraj1.listPointsTraj_I.Add(lstmain[if1]);
                        // Удаляем отметку из основного списка
                        lstmain.Remove(lstmain[if1]);

                        // Добавляем новую траекторию в список траекторий
                        list_tracks.Add(objClassTraj1);
                        */

                        //if (list_tracks.Count != 0)
                        // Удаляем отметку из основного списка
                        //lstmain.Remove(lstmain[if1]);

                    } // IF2 (flTraj==false)
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IF2

                    // ELSE_IF2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // (flTraj==true)
                    // Внутри FOR2 flTraj установлен в true(подошел в одну из трасс)

                    else
                    {
                        //lstmain.Remove(lstmain[if1]);

                    } // ELSE IF2 (flTraj==true)
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ELSE_IF2

                    // ???
                    if (list_tracks.Count == 0)
                    {
                        // Класс новой траектории
                        ClassTraj objClassTraj1 = new ClassTraj();
                        objClassTraj1.listPointsTraj_I = new List<ClassTrackPoint>();

                        // Добавляем новую отметку
                        objClassTraj1.listPointsTraj_I.Add(lstmain);

                        // Добавляем новую траекторию в список траекторий
                        list_tracks.Add(objClassTraj1);
                        //lstmain.Remove(lstmain[if1]);
                    }

        }
        // ************************************************************************** FILTER_REAL


    } // Class
} // Namespace


