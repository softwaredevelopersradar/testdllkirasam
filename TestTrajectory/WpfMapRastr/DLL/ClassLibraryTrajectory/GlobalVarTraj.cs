﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClassLibraryTrajectory
{
    public class GlobalVarTraj
    {
        //!!! Это будет в проекте, из которого вызывается DLL
        // Общий лист отметок - это будет вход DLL
        //public static List<ClassTrackPoint> lstTrajectory = new List<ClassTrackPoint>();
        //public static List<List<ClassTrackPoint>> lstTracks = new List<List<ClassTrackPoint>>();
        // Лист сформированных траекторий - это выход DLL
        public static List<ClassTraj> lstTracks = new List<ClassTraj>();

    } // Class
} // Namespace
