﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClassLibraryTrajectory
{
    // Matrix actions
    public partial class ClassTrajectory
    {
        // Умножение матрицы на число
        // MatrixToChislo
        // Сложение матриц
        // AddMatrix
        // Вычитание матриц
        // SubMatrix
        // Замена матриц 
        // ChangeMatrix
        // Транспонирование матрицы NxM 
        // MTransp
        // Обратная матрица 
        // MInv
        // Вырезать столбец
        // CreateMatrixWithoutColumn
        // Вырезать строку
        // CreateMatrixWithoutRow
        // определитель матрицы размерностью NxN
        // Determ
        // метод для получения количества строк матрицы
        // RowsCount
        // метод для получения количества строк матрицы
        // RowsCount
        // метод для умножения матриц
        // MatrixMultiplication
        // Формирование единичной матрицы nxm
        // MatrixOne

        // Умножение матрицы на число *************************************************************

        public double[,] MatrixToChislo(double[,] matrixA, double ch)
        {
            int matdimx1 = 0;
            int matdimy1 = 0;
            // ------------------------------------------------------------------------------------
            matdimx1 = RowsCount(matrixA);
            matdimy1 = ColumnsCount(matrixA);
            // ------------------------------------------------------------------------------------
            var matrixC = new double[matdimx1, matdimy1];
            // ------------------------------------------------------------------------------------
            for (int x = 0; x < matdimx1; x++)
            {
                for (int y = 0; y < matdimy1; y++)
                {
                    matrixC[x, y] = matrixA[x, y] * ch;
                }
            }
            // ------------------------------------------------------------------------------------

            return matrixC;
        }
        // ************************************************************* Умножение матрицы на число

        // AddMatrix ******************************************************************************
        // Сложение матриц

        public double[,] AddMatrix(double[,] matrixA, double[,] matrixB)
        {
            int matdimx1 = 0;
            int matdimy1 = 0;
            int matdimx2 = 0;
            int matdimy2 = 0;
            // ------------------------------------------------------------------------------------
            matdimx1 = RowsCount(matrixA);
            matdimy1 = ColumnsCount(matrixA);
            matdimx2 = RowsCount(matrixB);
            matdimy2 = ColumnsCount(matrixB);
            // ------------------------------------------------------------------------------------

            if (
                (matdimy1 != matdimy2) || (matdimx1 != matdimx2)
                )
            {
                MessageBox.Show("Сложение невозможно!");
            }

            var matrixC = new double[matdimx1, matdimy1];
            // ------------------------------------------------------------------------------------
            for (int x = 0; x < matdimx1; x++)
            {
                for (int y = 0; y < matdimy1; y++)
                {
                    matrixC[x, y] = matrixA[x, y] + matrixB[x, y];
                }
            }
            // ------------------------------------------------------------------------------------

            return matrixC;
        }
        // ****************************************************************************** AddMatrix

        // SubMatrix ******************************************************************************
        // Вычитание матриц

        public double[,] SubMatrix(double[,] matrixA, double[,] matrixB)
        {
            int matdimx1 = 0;
            int matdimy1 = 0;
            int matdimx2 = 0;
            int matdimy2 = 0;
            // ------------------------------------------------------------------------------------
            matdimx1 = RowsCount(matrixA);
            matdimy1 = ColumnsCount(matrixA);
            matdimx2 = RowsCount(matrixB);
            matdimy2 = ColumnsCount(matrixB);
            // ------------------------------------------------------------------------------------

            if (
                (matdimy1 != matdimy2) || (matdimx1 != matdimx2)
                )
            {
                MessageBox.Show("Вычитание невозможно!");
            }

            var matrixC = new double[matdimx1, matdimy1];
            // ------------------------------------------------------------------------------------
            for (int x = 0; x < matdimx1; x++)
            {
                for (int y = 0; y < matdimy1; y++)
                {
                    matrixC[x, y] = matrixA[x, y] - matrixB[x, y];
                }
            }
            // ------------------------------------------------------------------------------------

            return matrixC;
        }
        // ****************************************************************************** SubMatrix

        // ChangeMatrix ***************************************************************************
        // Замена матриц (элементы  B заменяются элементами A)

        public void ChangeMatrix(double[,] matrixA, ref double[,] matrixB)
        {
            int matdimx1 = 0;
            int matdimy1 = 0;
            int matdimx2 = 0;
            int matdimy2 = 0;
            // ------------------------------------------------------------------------------------
            matdimx1 = RowsCount(matrixA);
            matdimy1 = ColumnsCount(matrixA);
            matdimx2 = RowsCount(matrixB);
            matdimy2 = ColumnsCount(matrixB);
            // ------------------------------------------------------------------------------------

            if (
                (matdimy1 != matdimy2) || (matdimx1 != matdimx2)
                )
            {
                MessageBox.Show("Замена невозможно!");
            }
            // ------------------------------------------------------------------------------------
            for (int x = 0; x < matdimx1; x++)
            {
                for (int y = 0; y < matdimy1; y++)
                {
                    matrixB[x, y] = matrixA[x, y];
                }
            }
            // ------------------------------------------------------------------------------------

            return;
        }
        // *************************************************************************** ChangeMatrix

        // TranspMatrix ***************************************************************************
        // Транспонирование матрицы NxM 

        public void MTransp(
                            int N,
                            int M,
                            double[,] Min,
                            ref double[,] trans
                           )
        {
            //int[,] trans = new int[M, N];

            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    trans[i, j] = Min[j, i];
                }
            }

        }
        // *************************************************************************** TranspMatrix

        // Обратная матрица ************************************************************************
        // !!! Матрица д. б. квадратной, определитель не равен нулю

        public void MInv(
                                // Рамерность матрицы
                                int nmatr,
                                // Input matrix
                                double[,] A,
                                // Output matrix (Inverse)
                                ref double[,] AObrat
                        )
        {

            double[,] ACopy = new double[nmatr, nmatr];

            //задаем обратную матрицу как единичную           
            // for1
            for (int i = 0; i < nmatr; i++)
            {
                // for2
                for (int j = 0; j < nmatr; j++)
                {
                    if (i == j)
                    { AObrat[i, j] = 1; }
                    else
                    { AObrat[i, j] = 0; }

                    ACopy[i, j] = A[i, j];    //создаем копию матрицы 
                } // for2
            } // for1

            //прямой ход
            for (int k = 0; k < nmatr; ++k)
            {
                double div = ACopy[k, k];
                for (int m = 0; m < nmatr; ++m)
                {// делим строку на выбранный элемент === 1  ф  ф
                    ACopy[k, m] /= div;
                    AObrat[k, m] /= div;
                }
                for (int i = k + 1; i < nmatr; ++i) //идем по столбц ниже полученой единицы
                {
                    double multi = ACopy[i, k]; //элемент, который хотим занулить
                    for (int j = 0; j < nmatr; ++j)// элемент по счету в строке i
                    {
                        ACopy[i, j] -= multi * ACopy[k, j];
                        AObrat[i, j] -= multi * AObrat[k, j];
                    }
                }

            }
            for (int i = 0; i < nmatr; i++)
            {
                for (int j = 0; j < nmatr; j++)
                {
                    j = j;
                }
            }

            //обратный ход            
            for (int kk = nmatr - 1; kk > 0; kk--)
            {
                for (int i = kk - 1; i + 1 > 0; i--)
                {
                    double multi2 = ACopy[i, kk];
                    for (int j = 0; j < nmatr; j++)
                    {
                        ACopy[i, j] -= multi2 * ACopy[kk, j];
                        AObrat[i, j] -= multi2 * AObrat[kk, j];
                    }
                }
            }

            //Console.WriteLine("проверка матрицы Copy");
            double[,] Ee = new double[nmatr, nmatr];
            int flagA = 0;
            for (int i = 0; i < nmatr; i++)
            {
                for (int j = 0; j < nmatr; j++)
                {
                    if (i == j)
                    { Ee[i, j] = 1; }
                    else
                    { Ee[i, j] = 0; }
                    if (Ee[i, j] != ACopy[i, j])
                    {
                        i = i;
                    }
                    else
                    { flagA = 1; }
                }
            }
            if (flagA == 1)
            {
                //Console.WriteLine("матрица стала единичной");
                flagA = flagA;
            }

            //проверка СмОбратной
            //CmObrat*Cm
            double[,] ProverkaA = new double[nmatr, nmatr];
            for (int i = 0; i < nmatr; i++)//строки
            {
                for (int j = 0; j < nmatr; j++)//столбцы
                {
                    for (int k = 0; k < nmatr; k++)
                    {
                        ProverkaA[i, j] += AObrat[i, k] * A[k, j];
                    }
                }
            }

            //Console.WriteLine("проверка матрицы Proverka");
            int flag11 = 0;
            for (int i = 0; i < nmatr; i++)
            {
                for (int j = 0; j < nmatr; j++)
                {
                    if (Math.Abs(Ee[i, j] - ProverkaA[i, j]) > 0.002)
                    {
                        i = i;
                    }
                    else
                    { flag11 = 1; }
                }
            }
            if (flag11 == 1)
            {
                //Console.WriteLine("матрица стала единичной");
                flag11 = flag11;
            }
            //Console.ReadLine();

        } // P/P
        // ************************************************************************ Обратная матрица

        // Определитель матрицы ********************************************************************

        // .........................................................................................
        // Вырезать столбец

        public double[,] CreateMatrixWithoutColumn(
                                                // Кол-во строк
                                                int M,
                                                // Кол-во столбцов
                                                int N,
                                                // Индекс столбца
                                                int column,
                                                // Input matrix
                                                double[,] a
                                                )
        {

            double[,] result = new double[M, N - 1];
            // Строки
            for (int i = 0; i < M; i++)
            {
                // Столбцы
                for (int j = 0; j < N; j++)
                {
                    if (j < column)
                        result[i, j] = a[i, j];
                    else
                        result[i, j] = a[i, j + 1];
                }
            }
            return result;
        }
        // .........................................................................................
        // Вырезать строку

        public double[,] CreateMatrixWithoutRow(
                                                // Кол-во строк
                                                int M,
                                                // Кол-во столбцов
                                                int N,
                                                // Индекс строки
                                                int row,
                                                // Input matrix
                                                double[,] a
                                               )
        {
            double[,] result = new double[M - 1, N];
            // Строки
            for (int i = 0; i < M; i++)
            {
                // Столбцы
                for (int j = 0; j < N; j++)
                {
                    if (i < row)
                        result[i, j] = a[i, j];
                    else
                        result[i, j] = a[i + 1, j];
                }
            }

            return result;
        }
        // .........................................................................................
        // минор матрицы(matrix) путем вычеркивания строки с номером row и колонки с номером column(исходная матрица не меняется).

        public double[,] GetMinor(double[,] matrix, int row, int column)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1)) throw new Exception(" Число строк в матрице не совпадает с числом столбцов");
            double[,] buf = new double[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if ((i != row) || (j != column))
                    {
                        if (i > row && j < column) buf[i - 1, j] = matrix[i, j];
                        if (i < row && j > column) buf[i, j - 1] = matrix[i, j];
                        if (i > row && j > column) buf[i - 1, j - 1] = matrix[i, j];
                        if (i < row && j < column) buf[i, j] = matrix[i, j];
                    }
                }
            return buf;
        }
        // .........................................................................................
        // определитель матрицы размерностью NxN

        public double Determ(double[,] matrix)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1)) throw new Exception(" Число строк в матрице не совпадает с числом столбцов");
            double det = 0;
            int Rank = matrix.GetLength(0);
            if (Rank == 1) det = matrix[0, 0];
            if (Rank == 2) det = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            if (Rank > 2)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    det += Math.Pow(-1, 0 + j) * matrix[0, j] * Determ(GetMinor(matrix, 0, j));
                }
            }
            return det;
        }
        // .........................................................................................

        // ******************************************************************** Определитель матрицы

        // Умножение матриц ************************************************************************

        // .........................................................................................
        // метод для получения количества строк матрицы

        public int RowsCount(double[,] matrix)
        {
            return matrix.GetUpperBound(0) + 1;
        }
        // .........................................................................................
        // метод для получения количества столбцов матрицы

        public int ColumnsCount(double[,] matrix)
        {
            return matrix.GetUpperBound(1) + 1;
        }
        // .........................................................................................
        // метод для умножения матриц

        public double[,] MatrixMultiplication(double[,] matrixA, double[,] matrixB)
        {
            if (ColumnsCount(matrixA) != RowsCount(matrixB))
            {
                MessageBox.Show("Умножение не возможно! Количество столбцов первой матрицы не равно количеству строк второй матрицы.");
            }

            var matrixC = new double[RowsCount(matrixA), ColumnsCount(matrixB)];

            for (int i = 0; i < RowsCount(matrixA); i++)
            {
                for (int j = 0; j < ColumnsCount(matrixB); j++)
                {
                    matrixC[i, j] = 0;

                    for (int k = 0; k < ColumnsCount(matrixA); k++)
                    {
                        matrixC[i, j] += matrixA[i, k] * matrixB[k, j];
                    }
                }
            }

            return matrixC;
        }
        // .........................................................................................

        // ************************************************************************ Умножение матриц

        // Единичная матрица ************************************************************************
        // Формирование единичной матрицы nxm

        public byte[,] MatrixOne(int n, int m)
        {
            byte[,] matr = new byte[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (i == j)
                    {
                        matr[i, j] = 1;
                    }
                    else
                    {
                        matr[i, j] = 0;
                    }

                }
            }

            return matr;
        }
        // ************************************************************************ Единичная матрица



    } // Class
} // Namespace
