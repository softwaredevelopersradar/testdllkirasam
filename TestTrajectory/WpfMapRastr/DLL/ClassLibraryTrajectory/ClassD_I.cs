﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryTrajectory
{
    public class ClassD_I
    {
        // Момент времени Ti
        public double ti = 0;

        // Матрица 3x3 ошибок измерения координат в момент времени Ti (в заданной точке)
        public double[,] D_I = new double[3, 3];

    } // Class
} // Namespace
