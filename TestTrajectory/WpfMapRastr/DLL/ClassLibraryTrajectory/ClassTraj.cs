﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryTrajectory
{
    public class ClassTraj
    {
        public int ID = 0;

        // Матрица оценок параметров траектории 6x1 (состояние фильтра на (n-1) шаге
        // (или после инициализации фильтра)
        // S(n-1)=|x Vx y Vy z Vz| транспонированная
        public double[,] S_N_1 = new double[6, 1];

        // Корреляционная матрица 6x6 ошибок оценок параметров траектории (на (n-1) 
        // шаге или после инициализации фильтра)
        public double[,] PSI_N_1 = new double[6, 6];

        // Входной лист отметок траектории
        public List<ClassTrackPoint> listPointsTraj_I = new List<ClassTrackPoint>();

        // Входной лист отметок траектории, обработанных фильтром
        public List<ClassTrackPointFiltr> listPointsTrajFiltr_I = new List<ClassTrackPointFiltr>();


    } // Class
} // Namespace
