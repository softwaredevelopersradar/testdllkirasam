﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryTrajectory
{
    // Класс отметки
    public class ClassTrackPoint
    {
        public double Latitude = 0;
        public double Longitude = 0;
        public double Altitude = 0;

        // Реальные координаты (МЗСК)
        public double X = 0;
        public double Y = 0;
        public double Z = 0;

        // Экстраполированные координаты
        public double Xe = 0;
        public double Ye = 0;
        public double Ze = 0;

        public double timePoint = 0;

    } // Class
} // Namespace
