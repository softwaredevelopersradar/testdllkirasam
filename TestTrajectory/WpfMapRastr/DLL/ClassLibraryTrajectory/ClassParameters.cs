﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryTrajectory
{
    // Parameters for filter
    public class ClassParameters
    {
        // Максимально допустимое время ожидания отметки
        public double twait = 0;
        // Допустимая скорость
        public double Vdop = 0;
        // СКО ускорения на плоскости
        public double CKOA_pl = 0;
        // СКО ускорения по высоте
        public double CKOA_H = 0;
        // СКО по координатам для формирования матрицы D 
        // ошибок измерения в данный момент времени
        public double CKO_X = 0;
        public double CKO_Y = 0;
        public double CKO_Z = 0;

        // Величина строба (расстояние от предыдущей отметки)
        // для идентификации отметки
        public double strob = 0;

        // Координаты станций в МЗСК
        // 1
        public double XRLS_MZSK = 0;
        public double YRLS_MZSK = 0;
        public double ZRLS_MZSK = 0;
        // 2
        public double XRLS_MZSK_2 = 0;
        public double YRLS_MZSK_2 = 0;
        public double ZRLS_MZSK_2 = 0;
        // 3
        public double XRLS_MZSK_3 = 0;
        public double YRLS_MZSK_3 = 0;
        public double ZRLS_MZSK_3 = 0;
        // 4
        public double XRLS_MZSK_4 = 0;
        public double YRLS_MZSK_4 = 0;
        public double ZRLS_MZSK_4 = 0;

        // Ошибки  измерения разности времени приема сигналов
        public double Error_dt1 = 0;
        public double Error_dt2 = 0;
        public double Error_dt3 = 0;

    } // Class
} // Namespace
