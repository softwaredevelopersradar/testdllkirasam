﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClassLibraryTrajectory
{
    // Additional functions for trajectory calculation
    public partial class ClassTrajectory
    {
        // Di *************************************************************************************
        // Расчет матрицы 3x3 ошибок измерения координат в момент времени Ti
        // ****************************************************************************************
        public ClassD_I CalcDI(
                               // Отметка траектории
                               //ClassTrackPoint objotm,

                               double CKO_X,
                               double CKO_Y,
                               double CKO_Z
                              )
        {
            //CKO_Z = 20;

            ClassD_I objClassD_I = new ClassD_I();

            objClassD_I.D_I[0, 0] = CKO_X * CKO_X;
            objClassD_I.D_I[1, 1] = CKO_Y * CKO_Y;
            objClassD_I.D_I[2, 2] = CKO_Z * CKO_Z;

            return objClassD_I;
        }
        // ************************************************************************************* Di


        // Di *************************************************************************************
        // Расчет матрицы 3x3 ошибок измерения координат в момент времени Ti
        // ****************************************************************************************
        public ClassD_I CalcDI_new(
                                   // Отметка траектории
                                   ClassTrackPoint objotm,
                                   // Параметры для алгоритма формирования траекторий
                                   ClassParameters objClassParameters
                                  )
        {
            var res = new double[3, 3];

            res = FSKO(
                     // Отметка траектории
                     objotm,
                     // Параметры для алгоритма формирования траекторий
                     objClassParameters
                     );

            ClassD_I objClassD_I = new ClassD_I();

            //objClassD_I.D_I[0, 0] = CKO_X * CKO_X;
            //objClassD_I.D_I[1, 1] = CKO_Y * CKO_Y;
            //objClassD_I.D_I[2, 2] = CKO_Z * CKO_Z;

            // 1-я строка виктора(x)-> в мою 2-ю (y)
            objClassD_I.D_I[1, 0] = res[0, 0];
            objClassD_I.D_I[1, 1] = res[0, 1];
            objClassD_I.D_I[1, 2] = res[0, 2];

            // 2-я строка виктора(y)-> в мою 3-ю (z)
            objClassD_I.D_I[2, 0] = res[1, 0];
            objClassD_I.D_I[2, 1] = res[1, 1];
            objClassD_I.D_I[2, 2] = res[1, 2];

            // 3-я строка виктора(z)-> в мою 1-ю (x)
            objClassD_I.D_I[0, 0] = res[2, 0];
            objClassD_I.D_I[0, 1] = res[2, 1];
            objClassD_I.D_I[0, 2] = res[2, 2];

            return objClassD_I;
        }
        // ************************************************************************************* Di


        // Di_New *********************************************************************************

        // F0 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        // Функция расчета координат ИРИ для 3 РДМ

        public double[,] Func_XYZ_3RDM(
                                         // Массив временных задержек 1*3
                                         double[,] matrdt,

                                         // Параметры для алгоритма формирования траекторий
                                         ClassParameters objClassParameters
                                      )
                {
                    // ------------------------------------------------------------------------------------
                    var A1 = new double[1, 15];

                    int i = 0;
                    double mk1 = 0;
                    double mk2 = 0;
                    double mk3 = 0;
                    double V = 0;
                    double U1 = 0;
                    double U2 = 0;
                    double U3 = 0;
                    double U4 = 0;
                    double Xs = 0;
                    double Z0 = 0;
                    double Ys = 0;
                    double Zs = 0;
                    double Z1 = 0;
                    double Y0 = 0;
                    double Y1 = 0;

                    double b1 = 0;
                    double b2 = 0;
                    double b3 = 0;
                    double b4 = 0;
                    double b5 = 0;
                    double AA = 0;
                    double BB = 0;
                    double CC = 0;

                    double C = 299792458;

                    var a = new double[2, 4];
                    var res = new double[2, 4];

                    // ------------------------------------------------------------------------------------
                    // A1

                    // dt
                    A1[0, 0] = matrdt[0, 0];
                    A1[0, 1] = matrdt[0, 1];
                    A1[0, 2] = matrdt[0, 2];
                    //A1[0, 0] = -matrdt[0, 0];
                    //A1[0, 1] = -matrdt[0, 1];
                    //A1[0, 2] = -matrdt[0, 2];

            // RLS1
                    A1[0, 3] = objClassParameters.YRLS_MZSK;  // X
                    A1[0, 4] = objClassParameters.ZRLS_MZSK;  // Y
                    A1[0, 5] = objClassParameters.XRLS_MZSK;  // Z

                    // RLS2
                    A1[0, 6] = objClassParameters.YRLS_MZSK_2;  // X
                    A1[0, 7] = objClassParameters.ZRLS_MZSK_2;  // Y
                    A1[0, 8] = objClassParameters.XRLS_MZSK_2;  // Z

                    // RLS3
                    A1[0, 9] = objClassParameters.YRLS_MZSK_3;  // X
                    A1[0, 10] = objClassParameters.ZRLS_MZSK_3;  // Y
                    A1[0, 11] = objClassParameters.XRLS_MZSK_3;  // Z

                    // RLS4
                    A1[0, 12] = objClassParameters.YRLS_MZSK_4;  // X
                    A1[0, 13] = objClassParameters.ZRLS_MZSK_4;  // Y
                    A1[0, 14] = objClassParameters.XRLS_MZSK_4;  // Z
                    // ------------------------------------------------------------------------------------
                    mk1= ((Math.Pow(A1[0,6],2) + Math.Pow(A1[0,7],2) + Math.Pow(A1[0,8],2)) - (Math.Pow(A1[0,3],2) + Math.Pow(A1[0,4],2) + Math.Pow(A1[0,5],2)) - Math.Pow((C * A1[0,0]),2)) / 2;
                    mk2= ((Math.Pow(A1[0,9],2) + Math.Pow(A1[0,10],2) + Math.Pow(A1[0,11],2)) - (Math.Pow(A1[0,3],2) + Math.Pow(A1[0,4],2) + Math.Pow(A1[0,5],2)) - Math.Pow((C * A1[0,1]),2)) / 2;
                    mk3= ((Math.Pow(A1[0,12],2) + Math.Pow(A1[0,13],2) + Math.Pow(A1[0,14],2)) - (Math.Pow(A1[0,3],2) + Math.Pow(A1[0,4],2) + Math.Pow(A1[0,5],2)) - Math.Pow((C * A1[0,2]),2)) / 2;
                    // ------------------------------------------------------------------------------------
                    a[0, 0]= A1[0, 1] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 9]);
                    a[0, 1]= A1[0, 1] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 10]);
                    a[0, 2]= A1[0, 1] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 11]);
                    a[0, 3]= A1[0, 1] * mk1 - A1[0, 0] * mk2;

                    a[1, 0]= A1[0, 2] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 12]);
                    a[1, 1]= A1[0, 2] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 13]);
                    a[1, 2]= A1[0, 2] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 14]);
                    a[1, 3]= A1[0, 2] * mk1 - A1[0, 0] * mk3;
                    // ------------------------------------------------------------------------------------
                    V= a[0, 0] * a[1, 1] - a[0, 1] * a[1, 0];
                    // ------------------------------------------------------------------------------------
                    // V!=0

                    if(V!=0)
                    {
                     U1 = a[0, 1] * a[1, 3] - a[0, 3] * a[1, 1];
                     U2 = a[0, 1] * a[1, 2] - a[0, 2] * a[1, 1];
                     U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                     U4 = a[1, 0] * a[0, 2] - a[0, 0] * a[1, 2];
                     Xs = U1 / V;
                     Z0 = U2 / V;
                     Ys = U3 / V;
                     Z1 = U4 / V;

                     b1 = (A1[0,5] - A1[0,14]) + Z0 * (A1[0,3] - A1[0,12]) + Z1 * (A1[0,4] - A1[0,13]);
                     b2 = Xs * (A1[0,3] - A1[0,12]) + Ys * (A1[0,4] - A1[0,13]) + mk3;
                     b3 = 1 + Math.Pow(Z0,2) + Math.Pow(Z1,2);
                     b4 = 2 * (Z0 * (Xs - A1[0,3]) + Z1 * (Ys - A1[0,4]) - A1[0,5]);
                     b5 = Math.Pow(A1[0,5],2) + Math.Pow((Xs - A1[0,3]),2) + Math.Pow((Ys - A1[0,4]),2);

                     AA = Math.Pow(b1,2) - Math.Pow((C * A1[0,2]),2) * b3;
                     BB = 2 * b1 * b2 - Math.Pow((C * A1[0,2]),2) * b4;
                     CC = Math.Pow(b2,2) - Math.Pow((C * A1[0,2]),2) * b5;

                     // возможно перепутываание индексации
                     res[0, 2] = (-BB - Math.Sqrt(Math.Pow(BB,2) - 4 * AA * CC)) / (2 * AA);
                     res[1, 2] = (-BB + Math.Sqrt(Math.Pow(BB,2) - 4 * AA * CC)) / (2 * AA);
                     res[0, 0] = Xs + res[0, 1] * Z0;
                     res[1, 0] = Xs + res[1, 1] * Z0;
                     res[0, 1] = Ys + res[0, 1] * Z1;
                     res[1, 1] = Ys + res[1, 1] * Z1;

                     res[0, 3] = 1;     // варинат решения № 1
                     res[1, 3] = 1;

                    } // V!=0
                   // ------------------------------------------------------------------------------------
                   // V==0

                  else
                    {
                    V = a[0, 0] * a[1, 2] - a[0, 2] * a[1, 0];

                    U1 = a[0, 2] * a[1, 3] - a[0, 3] * a[1, 2];
                    U2 = a[0, 2] * a[1, 1] - a[0, 1] * a[1, 2];
                    U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                    U4 = a[1, 0] * a[0, 1] - a[0, 0] * a[1, 1];
                    Xs = U1 / V;
                    Y0 = U2 / V;
                    Zs = U3 / V;
                    Y1 = U4 / V;

                    b1 = (A1[0,4] - A1[0,13]) + Y0 * (A1[0,3] - A1[0,12]) + Y1 * (A1[0,4] - A1[0,13]);
                    b2 = Xs * (A1[0,3] - A1[0,12]) + Zs * (A1[0,5] - A1[0,14]) + mk3;
                    b3 = 1 + Math.Pow(Y0,2) + Math.Pow(Y1,2);
                    b4 = 2 * (Y0 * (Xs - A1[0,3]) + Y1 * (Zs - A1[0,4]) - A1[0,5]);
                    b5 = Math.Pow(A1[0,4],2) + Math.Pow((Xs - A1[0,3]),2) + Math.Pow((Zs - A1[0,5]),2);

                    AA = Math.Pow(b1,2) - Math.Pow((C * A1[0,2]),2) * b3;
                    BB = 2 * b1 * b2 - Math.Pow((C * A1[0,2]),2) * b4;
                    CC = Math.Pow(b2,2) - Math.Pow((C * A1[0,2]),2) * b5;

                    // возможно перепутываание индексации
                    res[0, 2] = (-BB - Math.Sqrt(Math.Pow(BB,2) - 4 * AA * CC)) / (2 * AA);
                    res[1, 2] = (-BB + Math.Sqrt(Math.Pow(BB,2) - 4 * AA * CC)) / (2 * AA);
                    res[0, 0] = Xs + res[0, 2] * Y0;
                    res[1, 0] = Xs + res[1, 2] * Y0;
                    res[0, 1] = Zs + res[0, 2] * Y1;
                    res[1, 1] = Zs + res[1, 2] * Y1;

                    res[0, 3] = 2;     // варинат решения № 2
                    res[1, 3] = 2;

                  } // V==0
              // ------------------------------------------------------------------------------------
              return res;
              // ------------------------------------------------------------------------------------

                }
        // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& F0

        // F1 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        //расчет частных производных первого  порядка  для 3 РДМ комплекса

        public double[,] Func_Qa3RDM(
                                         // Отметка траектории
                                         //ClassTrackPoint objotm,

                                         // Массив временных задержек 1*3
                                         double[,] matrdt,

                                         // Параметры для алгоритма формирования траекторий
                                         ClassParameters objClassParameters,

                                         // Тип решения +/-
                                         byte TipRes
                                    )
        {

            //var A1:vector;
            //I: Integer;
            //mK1,mK2,mK3,V,U1,U2,U3,U4,Xs,Z0,Ys,Zs,Z1,Y0,Y1: extended;
            //b1,b2,b3,b4,b5,AA,BB,CC,S,X,Y,Z: extended;
            //a,res: matrix;

            // ------------------------------------------------------------------------------------
            var A1 = new double[1, 15];

            int i = 0;
            double mk1 = 0;
            double mk2 = 0;
            double mk3 = 0;
            double V = 0;
            double U1 = 0;
            double U2 = 0;
            double U3 = 0;
            double U4 = 0;
            double Xs = 0;
            double Z0 = 0;
            double Ys = 0;
            double Zs = 0;
            double Z1 = 0;
            double Y0 = 0;
            double Y1 = 0;

            double b1 = 0;
            double b2 = 0;
            double b3 = 0;
            double b4 = 0;
            double b5 = 0;
            double AA = 0;
            double BB = 0;
            double CC = 0;
            double S = 0;
            double X = 0;
            double Y = 0;
            double Z = 0;

            double C = 299792458;

            var a = new double[2, 4];
            var res = new double[3, 15];

            // ???????????????
            //dA1, da: matrix;
            var da = new double[2, 4];

            double dmk1 = 0;
            double dmk2 = 0;
            double dmk3 = 0;

            double dV = 0;
            double dU1 = 0;
            double dU2 = 0;
            double dU3 = 0;
            double dU4 = 0;
            double dXs = 0;
            double dZ0 = 0;
            double dYs = 0;
            double dZs = 0;
            double dZ1 = 0;
            double dY0 = 0;
            double dY1 = 0;

            double db1 = 0;
            double db2 = 0;
            double db3 = 0;
            double db4 = 0;
            double db5 = 0;
            double dAA = 0;
            double dBB = 0;
            double dCC = 0;
            double dS = 0;
            double dY = 0;
            double dX = 0;
            double dZ = 0;

            // ------------------------------------------------------------------------------------
            // A1

            // dt
            A1[0, 0] = matrdt[0, 0];
            A1[0, 1] = matrdt[0, 1];
            A1[0, 2] = matrdt[0, 2];

            // RLS1
            A1[0, 3] = objClassParameters.YRLS_MZSK;  // X
            A1[0, 4] = objClassParameters.ZRLS_MZSK;  // Y
            A1[0, 5] = objClassParameters.XRLS_MZSK;  // Z

            // RLS2
            A1[0, 6] = objClassParameters.YRLS_MZSK_2;  // X
            A1[0, 7] = objClassParameters.ZRLS_MZSK_2;  // Y
            A1[0, 8] = objClassParameters.XRLS_MZSK_2;  // Z

            // RLS3
            A1[0, 9] = objClassParameters.YRLS_MZSK_3;  // X
            A1[0, 10] = objClassParameters.ZRLS_MZSK_3;  // Y
            A1[0, 11] = objClassParameters.XRLS_MZSK_3;  // Z

            // RLS4
            A1[0, 12] = objClassParameters.YRLS_MZSK_4;  // X
            A1[0, 13] = objClassParameters.ZRLS_MZSK_4;  // Y
            A1[0, 14] = objClassParameters.XRLS_MZSK_4;  // Z

            // ------------------------------------------------------------------------------------
            mk1 = ((Math.Pow(A1[0, 6],2) + Math.Pow(A1[0, 7], 2) + Math.Pow(A1[0, 8], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow(C * A1[0, 0], 2)) / 2;
            mk2 = ((Math.Pow(A1[0, 9], 2) + Math.Pow(A1[0, 10], 2) + Math.Pow(A1[0, 11], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow(C * A1[0, 1], 2)) / 2;
            mk3 = ((Math.Pow(A1[0, 12], 2) + Math.Pow(A1[0, 13], 2) + Math.Pow(A1[0, 14], 2)) - (Math.Pow(A1[0, 3], 2) + Math.Pow(A1[0, 4], 2) + Math.Pow(A1[0, 5], 2)) - Math.Pow(C * A1[0, 2], 2)) / 2;
            // ------------------------------------------------------------------------------------
            a[0, 0] = A1[0, 1] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 9]);
            a[0, 1] = A1[0, 1] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 10]);
            a[0, 2] = A1[0, 1] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 11]);
            a[0, 3] = A1[0, 1] * mk1 - A1[0, 0] * mk2;

            a[1, 0] = A1[0, 2] * (A1[0, 3] - A1[0, 6]) - A1[0, 0] * (A1[0, 3] - A1[0, 12]);
            a[1, 1] = A1[0, 2] * (A1[0, 4] - A1[0, 7]) - A1[0, 0] * (A1[0, 4] - A1[0, 13]);
            a[1, 2] = A1[0, 2] * (A1[0, 5] - A1[0, 8]) - A1[0, 0] * (A1[0, 5] - A1[0, 14]);
            a[1, 3] = A1[0, 2] * mk1 - A1[0, 0] * mk3;
            // ------------------------------------------------------------------------------------
            V = a[0, 0] * a[1, 1] - a[0, 1] * a[1, 0];
            // ------------------------------------------------------------------------------------
            // V!=0

            if(V!=0)
            {

                U1 = a[0, 1] * a[1, 3] - a[0, 3] * a[1, 1];
                U2 = a[0, 1] * a[1, 2] - a[0, 2] * a[1, 1];
                U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                U4 = a[1, 0] * a[0, 2] - a[0, 0] * a[1, 2];
                Xs = U1 / V;
                Z0 = U2 / V;
                Ys = U3 / V;
                Z1 = U4 / V;

                b1 = (A1[0,5] - A1[0,14]) + Z0 * (A1[0,3] - A1[0,12]) + Z1 * (A1[0,4] - A1[0,13]);
                b2 = Xs * (A1[0,3] - A1[0,12]) + Ys * (A1[0,4] - A1[0,13]) + mk3;
                b3 = 1 + Math.Pow(Z0,2) + Math.Pow(Z1, 2);
                b4 = 2 * (Z0 * (Xs - A1[0,3]) + Z1 * (Ys - A1[0,4]) - A1[0,5]);
                b5 = Math.Pow(A1[0,5], 2) + Math.Pow((Xs - A1[0,3]),2) + Math.Pow((Ys - A1[0,4]),2);

                AA = Math.Pow(b1,2) - Math.Pow((C * A1[0,2]),2) * b3;
                BB = 2 * b1 * b2 - Math.Pow((C * A1[0,2]),2) * b4;
                CC = Math.Pow(b2, 2) - Math.Pow((C * A1[0,2]),2) * b5;
                S = Math.Sqrt(Math.Pow(BB, 2) - 4 * AA * CC);
                // возможно перепутываание индексации
                if (TipRes == 0)
                    Y = (-BB - S) / (2 * AA);
                else
                    Y = (-BB + S) / (2 * AA);

                X = Xs + Y * Z0;
                Z = Ys + Y * Z1;

            } // V!=0
            // ------------------------------------------------------------------------------------
            // V==0

            else
            {
                V = a[0, 0] * a[1, 2] - a[0, 2] * a[1, 0];

                U1 = a[0, 2] * a[1, 3] - a[0, 3] * a[1, 2];
                U2 = a[0, 2] * a[1, 1] - a[0, 1] * a[1, 2];
                U3 = a[1, 0] * a[0, 3] - a[0, 0] * a[1, 3];
                U4 = a[1, 0] * a[0, 1] - a[0, 0] * a[1, 1];
                Xs = U1 / V;
                Y0 = U2 / V;
                Ys = U3 / V;
                Y1 = U4 / V;

                b1 = (A1[0,4] - A1[0, 13]) + Y0 * (A1[0, 3] - A1[0, 12]) + Y1 * (A1[0, 4] - A1[0, 13]);
                b2 = Xs * (A1[0, 3] - A1[0, 12]) + Zs * (A1[0, 5] - A1[0, 14]) + mk3;
                b3 = 1 + Math.Pow(Y0,2) + Math.Pow(Y1,2);
                b4 = 2 * (Y0 * (Xs - A1[0, 3]) + Y1 * (Zs - A1[0, 4]) - A1[0, 5]);
                b5 = Math.Pow(A1[0, 4],2) + Math.Pow((Xs - A1[0, 3]),2) + Math.Pow((Zs - A1[0, 5]),2);

                AA = Math.Pow(b1,2) - Math.Pow((C * A1[0, 2]),2) * b3;
                BB = 2 * b1 * b2 - Math.Pow((C * A1[0, 2]),2) * b4;
                CC = Math.Pow(b2,2) - Math.Pow((C * A1[0, 2]),2) * b5;

                S = Math.Sqrt(Math.Pow(BB,2) - 4 * AA * CC);

                // возможно перепутываание индексации
                if (TipRes == 0)
                    Z = (-BB - S) / (2 * AA);
                else
                    Z = (-BB + S) / (2 * AA);

                X = Xs + Z * Y0;
                Y = Zs + Z * Y1;

            } // V==0
              // ------------------------------------------------------------------------------------
              // Единичная матрица 15x15

            //dA1:= FIM(length(A1));

            var dA1 = new byte[15, 15];
            dA1 = MatrixOne(15,15);
            // ------------------------------------------------------------------------------------

            // FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1F
            // FOR1

            for (i=0;i<=14;i++)
            {
                // ...................................................................................
                dmk1 = (A1[0,6] * dA1[i, 6] + A1[0,7] * dA1[i, 7] + A1[0,8] * dA1[i, 8]) -
                       (A1[0,3] * dA1[i, 3] + A1[0,4] * dA1[i, 4] + A1[0,5] * dA1[i, 5]) -
                       Math.Pow(C,2) * A1[0,0] * dA1[i, 0];

                dmk2 = (A1[0,9] * dA1[i, 9] + A1[0,10] * dA1[i, 10] + A1[0,11] * dA1[i, 11]) -
                       (A1[0,3] * dA1[i, 3] + A1[0,4] * dA1[i, 4] + A1[0,5] * dA1[i, 5]) -
                       Math.Pow(C,2) * A1[0,1] * dA1[i, 1];

                dmk3 = (A1[0,12] * dA1[i, 12] + A1[0,13] * dA1[i, 13] + A1[0,14] * dA1[i, 14]) -
                       (A1[0,3] * dA1[i, 3] + A1[0,4] * dA1[i, 4] + A1[0,5] * dA1[i, 5]) -
                       Math.Pow(C,2) * A1[0,2] * dA1[i, 2];
                // ...................................................................................
                da[0, 0] = (A1[0,3] - A1[0, 6]) * dA1[i, 1] + (dA1[i, 3] - dA1[i, 6]) * A1[0, 1] -
                           ((A1[0, 3] - A1[0, 9]) * dA1[i, 0] + (dA1[i, 3] - dA1[i, 9]) * A1[0, 0]);
                da[0, 1] = (A1[0, 4] - A1[0, 7]) * dA1[i, 1] + (dA1[i, 4] - dA1[i, 4]) * A1[0, 1] -
                           ((A1[0, 4] - A1[0, 10]) * dA1[i, 0] + (dA1[i, 4] - dA1[i, 10]) * A1[0, 0]);
                da[0, 2] = (A1[0, 5] - A1[0, 8]) * dA1[i, 1] + (dA1[i, 5] - dA1[i, 8]) * A1[0, 1] -
                           ((A1[0, 5] - A1[0, 11]) * dA1[i, 0] + (dA1[i, 5] - dA1[i, 11]) * A1[0, 0]);
                da[0, 3] = mk1 * dA1[i, 1] + dmk1 * A1[0, 1] - (mk2 * dA1[i, 0] + dmk2 * A1[0, 0]);


                da[1, 0] = (A1[0, 3] - A1[0, 6]) * dA1[i, 2] + (dA1[i, 3] - dA1[i, 6]) * A1[0, 2] -
                           ((A1[0, 3] - A1[0, 12]) * dA1[i, 0] + (dA1[i, 3] - dA1[i, 12]) * A1[0, 0]);
                da[1, 1] = (A1[0, 4] - A1[0, 7]) * dA1[i, 2] + (dA1[i, 4] - dA1[i, 4]) * A1[0, 2] -
                           ((A1[0, 4] - A1[0, 13]) * dA1[i, 0] + (dA1[i, 4] - dA1[i, 13]) * A1[0, 0]);
                da[1, 2] = (A1[0, 5] - A1[0, 8]) * dA1[i, 2] + (dA1[i, 5] - dA1[i, 8]) * A1[0, 2] -
                           ((A1[0, 5] - A1[0, 14]) * dA1[i, 0] + (dA1[i, 5] - dA1[i, 14]) * A1[0, 0]);
                da[1, 3] = mk1 * dA1[i, 2] + dmk1 * A1[0, 2] - (mk3 * dA1[i, 0] + dmk3 * A1[0, 0]);
                // ...................................................................................
                // V!=0

                if(V!=0)
                {
                    dV = (a[0, 0] * da[1, 1] + a[1, 1] * da[0, 0]) - (a[0, 1] * da[1, 0] + a[1, 0] * da[0, 1]);
                    dU1 = (a[0, 1] * da[1, 3] + a[1, 3] * da[0, 1]) - (a[0, 3] * da[1, 1] + a[1, 1] * da[0, 3]);
                    dU2 = (a[0, 1] * da[1, 2] + a[1, 2] * da[0, 1]) - (a[0, 2] * da[1, 1] + a[1, 1] * da[0, 2]);
                    dU3 = (a[1, 0] * da[0, 3] + a[0, 3] * da[1, 0]) - (a[0, 0] * da[1, 3] + a[1, 3] * da[0, 0]);
                    dU4 = (a[1, 0] * da[0, 2] + a[0, 2] * da[1, 0]) - (a[0, 0] * da[1, 2] + a[1, 2] * da[0, 0]);

                    dXs = (dU1 * V - U1 * dV) / Math.Pow(V,2);
                    dZ0 = (dU2 * V - U2 * dV) / Math.Pow(V,2);
                    dYs = (dU3 * V - U3 * dV) / Math.Pow(V,2);
                    dZ1 = (dU4 * V - U4 * dV) / Math.Pow(V,2);

                    db1 = dA1[i, 5] - dA1[i, 14] + dZ0 * (A1[0,3] - A1[0,12]) + Z0 * (dA1[i, 3] - dA1[i, 12]) +
                          dZ1 * (A1[0,4] - A1[0,13]) + Z1 * (dA1[i, 4] - dA1[i, 13]);
                    db2 = dXs * (A1[0,3] - A1[0,12]) + Xs * (dA1[i, 3] - dA1[i, 12]) +
                         dYs * (A1[0,4] - A1[0,13]) + Ys * (dA1[i, 4] - dA1[i, 13]) + dmk3;
                    db3 = 2 * Z0 * dZ0 + 2 * Z1 * dZ1;
                    db4 = 2 * (dZ0 * (Xs - A1[0,3]) + Z0 * (dXs - dA1[i, 3]) + dZ1 * (Ys - A1[0,4]) + Z1 * (dYs - dA1[i, 4]) - dA1[i, 5]);
                    db5 = 2 * (A1[0,5] * dA1[i, 5] + (Xs - A1[0,3]) * (dXs - dA1[i, 3]) + (Ys - A1[0,4]) * (dYs - dA1[i, 4]));

                    dAA = 2 * b1 * db1 - 2 * Math.Pow(C,2) * dA1[i, 2] * A1[0,2] * b3 - Math.Pow((C * A1[0,2]),2) * db3;
                    dBB = 2 * b1 * db2 + 2 * b2 * db1 - 2 * Math.Pow(C,2) * dA1[i, 2] * A1[0,2] * b4 - Math.Pow((C * A1[0,2]),2) * db4;
                    dCC = 2 * b2 * db2 - 2 * Math.Pow(C,2) * dA1[i, 2] * A1[0,2] * b5 - Math.Pow((C * A1[0,2]),2) * db5;

                    dS = (BB * dBB - 2 * CC * dAA - 2 * AA * dCC) / S;

                    if (TipRes == 0)
                        dY = (-(AA * dBB - BB * dAA) - (AA * dS - S * dAA)) / (2 * Math.Pow(AA, 2));
                    else
                        dY = (-(AA * dBB - BB * dAA) + (AA * dS - S * dAA)) / (2 * Math.Pow(AA,2));
                    dX = dXs + dZ0 * Y + Z0 * dY;
                    dZ = dYs + dZ1 * Y + Z1 * dY;

                    res[0, i] = dX;
                    res[1, i] = dZ;
                    res[2, i] = dY;

                } // V!=0
                // ...................................................................................
                // V==0

                else
                {
                    dV = (a[0, 0] * da[1, 2] + a[1, 2] * da[0, 0]) - (a[0, 2] * da[1, 0] + a[1, 0] * da[0, 2]);
                    dU1 = (a[0, 2] * da[1, 3] + a[1, 3] * da[0, 2]) - (a[0, 3] * da[1, 2] + a[1, 2] * da[0, 3]);
                    dU2 = (a[0, 2] * da[1, 1] + a[1, 1] * da[0, 2]) - (a[0, 1] * da[1, 2] + a[1, 2] * da[0, 1]);
                    dU3 = (a[0, 3] * da[1, 0] + a[1, 0] * da[0, 3]) - (a[0, 0] * da[1, 3] + a[1, 3] * da[0, 0]);
                    dU4 = (a[0, 1] * da[1, 0] + a[1, 0] * da[0, 1]) - (a[0, 0] * da[1, 1] + a[1, 1] * da[0, 0]);

                    dXs = (dU1 * V - U1 * dV) / Math.Pow(V,2);
                    dY0 = (dU2 * V - U2 * dV) / Math.Pow(V,2);
                    dYs = (dU3 * V - U3 * dV) / Math.Pow(V,2);
                    dY1 = (dU4 * V - U4 * dV) / Math.Pow(V,2);

                    db1 = dA1[i, 4] - dA1[i, 13] + dY0 * (A1[0,3] - A1[0,12]) + Y0 * (dA1[i, 3] - dA1[i, 12]) +
                         dY1 * (A1[0,5] - A1[0,14]) + Y1 * (dA1[i, 5] - dA1[i, 14]);
                    db2 = dXs * (A1[0,3] - A1[0,12]) + Xs * (dA1[i, 3] - dA1[i, 12]) +
                        dZs * (A1[0,5] - A1[0,14]) + Zs * (dA1[i, 5] - dA1[i, 14]) + dmk3;
                    db3 = 2 * Y0 * dY0 + 2 * Y1 * dY1;
                    db4 = 2 * (dY0 * (Xs - A1[0,3]) + Y0 * (dXs - dA1[i, 3]) + dY1 * (Zs - A1[0,5]) + Y1 * (dZs - dA1[i, 5]) - dA1[i, 5]);
                    db5 = 2 * (A1[0,4] * dA1[i, 4] + (Xs - A1[0,3]) * (dXs - dA1[i, 3]) + (Zs - A1[0,5]) * (dZs - dA1[i, 5]));

                    dAA = 2 * b1 * db1 - 2 * Math.Pow(C,2) * dA1[i, 2] * A1[0,2] * b3 - Math.Pow((C * A1[0,2]),2) * db3;
                    dBB = 2 * b1 * db2 + 2 * b2 * db1 - 2 * Math.Pow(C,2) * dA1[i, 2] * A1[0,2] * b4 - Math.Pow((C * A1[0,2]),2) * db4;
                    dCC = 2 * b2 * db2 - 2 * Math.Pow(C,2) * dA1[i, 2] * A1[0,2] * b5 - Math.Pow((C * A1[0,2]),2) * db5;

                    dS = (BB * dBB - 2 * CC * dAA - 2 * AA * dCC) / S;

                    if (TipRes == 0)
                        dY = (-(AA * dBB - BB * dAA) - (AA * dS - S * dAA)) / (2 * Math.Pow(AA, 2));
                    else
                        dY = (-(AA * dBB - BB * dAA) + (AA * dS - S * dAA)) / (2 * Math.Pow(AA,2));
                    dX = dXs + dZ0 * Y + Z0 * dY;
                    dZ = dYs + dZ1 * Y + Z1 * dY;

                    res[0, i] = dX;
                    res[1, i] = dY;
                    res[2, i] = dZ;

                } // V==0
                // ...................................................................................

            } // FOR1
            // FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1FOR1F

            return res;
        }
        // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& F1

        // F2 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

        // ........................................................................................
/*
        public double[,] FSKO(
                                         // Отметка траектории
                                         //ClassTrackPoint objotm,

                                         // Массив временных задержек 1*3
                                         double[,] matrdt,

                                         // Параметры для алгоритма формирования траекторий
                                         ClassParameters objClassParameters,

                                         // Тип решения +/-
                                         byte TipRes,

                                         // ???????????????
                                         // Число приемных пунктов
                                         double[,] K
                                    )
        {
            //матрица частных производных
            var Qa1 = new double[3, 15];
            var Qa1T = new double[15, 3];

            var res = new double[3, 3];

            //расчет частных производных
            Qa1 = Func_Qa3RDM(
                              // Массив временных задержек 1*3
                              matrdt,
                              // Параметры для алгоритма формирования траекторий
                              objClassParameters,
                              // Тип решения +/-
                              TipRes
                             );

            MTransp(
                     3,
                     15,
                     Qa1,
                     ref Qa1T
                    );
            res = MatrixMultiplication(MatrixMultiplication(Qa1, K), Qa1T);

            return res;

        }
*/
        // ........................................................................................
        // Расчет D[3,3] матрицы ошибок измерения (вариант2)

        public double[,] FSKO(
                              // Отметка траектории
                              ClassTrackPoint objotm,
                             // Параметры для алгоритма формирования траекторий
                             ClassParameters objClassParameters
                              )
        {
            // --------------------------------------------------------------------------------
            // Массив временных задержек 1*3
            var matrdt = new double[1, 3];

            //матрица частных производных
            var Qa1 = new double[3, 15];
            var Qa1T = new double[15, 3];

            // Ковариационная матрица ошибок наблюдения 15x15
            var matrixK = new double[15, 15];

            // Матрица ошибок измерения  3x3
            var res = new double[3, 3];
            // --------------------------------------------------------------------------------
            // Определение массива временных задержек

            matrdt = Calc_dt(
                           // Отметка траектории
                           objotm,
                           // Параметры для алгоритма формирования траекторий
                           objClassParameters
                            );
            // --------------------------------------------------------------------------------
            // Определение  TipRes

            // Тип решения +/-
            byte TipRes = 0;

            // Рассчитанные координаты ВО
            var XYZres = new double[2, 4];
            // Экстраполированные координаты ВО
            var VO = new double[1, 3];

            VO[0, 0] = objotm.Ye; // X victora
            VO[0, 1] = objotm.Ze; // Y victora
            VO[0, 2] = objotm.Xe; // Z victora

            XYZres = Func_XYZ_3RDM(
                                  // Массив временных задержек 1*3
                                  matrdt,
                                 // Параметры для алгоритма формирования траекторий
                                 objClassParameters
                                 );

            if ((XYZres[0, 2] - VO[0, 2]) <= 1)
                TipRes = 0;
            else
                TipRes= 1;
            // --------------------------------------------------------------------------------
            // Ковариационная матрица ошибок наблюдения 15x15

            matrixK = Calc_K(
                             // Параметры для алгоритма формирования траекторий
                             objClassParameters
                            );
            // --------------------------------------------------------------------------------
            // Расчет частных производных

            // Матрица частных производных
            Qa1 = Func_Qa3RDM(
                              // Массив временных задержек 1*3
                              matrdt,
                              // Параметры для алгоритма формирования траекторий
                              objClassParameters,
                              // Тип решения +/-
                              TipRes
                             );
            // Транспонированная матрица частных производных
            MTransp(
                     3,
                     15,
                     Qa1,
                     ref Qa1T
                    );
            // --------------------------------------------------------------------------------
            // Матрица ошибок измерения  3x3

            res = MatrixMultiplication(MatrixMultiplication(Qa1, matrixK), Qa1T);
            // --------------------------------------------------------------------------------

            return res;

        }
        // ........................................................................................

        // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& F2

        // F3 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        // Расчет ковариационной матрицы ошибок наблюдения D_Omega 15x15

        // По главной диагонали стоят квадраты априорных ошибок измерения разности времени приема сигналов и топопривязки
        // ошибки топопривязки имеют нулевые значения)

        // В файле "методика расчета ошибок местоопределения приложение к ПЗ траекторной обработки.doc"
        // эта переменная описана применительно к формуле (2.6) и имеет наименование D с индексом омега

        // ??????????????????????????????????
        public double[,] Calc_K(
                                 // Параметры для алгоритма формирования траекторий
                                 ClassParameters objClassParameters
                               )
        {

            var matrixK = new double[15, 15];
            int i = 0;
            int j = 0;

            /*
                        // ...................................................................................
                        var D_Omega = new double[15, 15];
                        var Sigma_Omega = new double[15, 15];
                        var r = new double[15, 15];

                        var matrix_temp1 = new double[15, 15];
                        var matrix_temp2 = new double[15, 15];
                        var matrix_temp3 = new double[15, 15];

                        int i = 0;
                        // ...................................................................................
                        // Sigma_Omega

                        Sigma_Omega[0, 0] = objClassParameters.Error_dt1;
                        for(i=1;i<15;i++)
                            Sigma_Omega[0, i] = 0;

                        Sigma_Omega[1, 0] = 0;
                        Sigma_Omega[1, 1] = objClassParameters.Error_dt2;
                        for (i = 1; i < 15; i++)
                            Sigma_Omega[0, i] = 0;
                        // ...................................................................................
            */

            // .....................................................................................
            matrixK[0, 0] = Math.Pow(objClassParameters.Error_dt1,2);
            for (i = 1; i < 15; i++)
                matrixK[0, i] = 0;

            matrixK[1, 0] = 0;
            matrixK[1, 1] = Math.Pow(objClassParameters.Error_dt2,2);
            for (i = 1; i < 15; i++)
                matrixK[0, i] = 0;

            matrixK[2, 0] = 0;
            matrixK[2, 1] = 0;
            matrixK[2, 2] = Math.Pow(objClassParameters.Error_dt3, 2);
            for (i = 3; i < 15; i++)
                matrixK[0, i] = 0;
            // .....................................................................................
            for (i = 3; i < 15; i++)
            {
                for (j = 0; j < 15; j++)
                {
                        matrixK[i, j] = 0;
                }
            }

            // .....................................................................................

            return matrixK;
        }
        // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& F3

        // ********************************************************************************* Di_New

        // Calc_dt ********************************************************************************

        public double[,] Calc_dt(
                                 // Отметка траектории
                                 ClassTrackPoint objotm,

                                 // Параметры для алгоритма формирования траекторий
                                 ClassParameters objClassParameters
                                )
        {
            double RCC = 0;

            // Расстояние между ППi и ИРИ
            double R0 = 0;
            double R1 = 0;
            double R2 = 0;
            double R3 = 0;

            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3
            double dr01 = 0;
            double dr02 = 0;
            double dr03 = 0;

            double cc = 299792458;

            var matrixC = new double[1, 3];

            double x = 0;
            double y = 0;
            double z = 0;
            double x2 = 0;
            double y2 = 0;
            double z2 = 0;
            double x3 = 0;
            double y3 = 0;
            double z3 = 0;
            double x4 = 0;
            double y4 = 0;
            double z4 = 0;

            x = objotm.Y;
            y = objotm.Z;
            z = objotm.X;
            x2 = objClassParameters.YRLS_MZSK_2;
            y2 = objClassParameters.ZRLS_MZSK_2;
            z2 = objClassParameters.XRLS_MZSK_2;
            x3 = objClassParameters.YRLS_MZSK_3;
            y3 = objClassParameters.ZRLS_MZSK_3;
            z3 = objClassParameters.XRLS_MZSK_3;
            x4 = objClassParameters.YRLS_MZSK_4;
            y4 = objClassParameters.ZRLS_MZSK_4;
            z4 = objClassParameters.XRLS_MZSK_4;

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            /*
                        RCC = Math.Sqrt(objotm.X * objotm.X + objotm.Y * objotm.Y + objotm.Z * objotm.Z);

                        R0 = RCC;

                        R1 = Math.Sqrt((objotm.X - objClassParameters.XRLS_MZSK_2) * (objotm.X - objClassParameters.XRLS_MZSK_2) 
                                       + (objotm.Y - objClassParameters.YRLS_MZSK_2) * (objotm.Y - objClassParameters.YRLS_MZSK_2) +
                                         (objotm.Z - objClassParameters.ZRLS_MZSK_2) * (objotm.Z - objClassParameters.ZRLS_MZSK_2));

                        R2 = Math.Sqrt((objotm.X - objClassParameters.XRLS_MZSK_3) * (objotm.X - objClassParameters.XRLS_MZSK_3)
                                       + (objotm.Y - objClassParameters.YRLS_MZSK_3) * (objotm.Y - objClassParameters.YRLS_MZSK_3) +
                                         (objotm.Z - objClassParameters.ZRLS_MZSK_3) * (objotm.Z - objClassParameters.ZRLS_MZSK_3));

                        R3 = Math.Sqrt((objotm.X - objClassParameters.XRLS_MZSK_4) * (objotm.X - objClassParameters.XRLS_MZSK_4)
                                       + (objotm.Y - objClassParameters.YRLS_MZSK_4) * (objotm.Y - objClassParameters.YRLS_MZSK_4) +
                                         (objotm.Z - objClassParameters.ZRLS_MZSK_4) * (objotm.Z - objClassParameters.ZRLS_MZSK_4));
            */

            RCC = Math.Sqrt(x * x + y * y + z * z);

            R0 = RCC;

            R1 = Math.Sqrt((x - x2) * (x - x2)
                           + (y - y2) * (y - y2) +
                             (z - z2) * (z - z2));

            R2 = Math.Sqrt((x - x3) * (x - x3)
                           + (y - y3) * (y - y3) +
                             (z - z3) * (z - z3));

            R3 = Math.Sqrt((x - x4) * (x - x4)
                           + (y - y4) * (y - y4) +
                             (z - z4) * (z - z4));


            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3
            dr01 = R1 - R0;
            dr02 = R2 - R0;
            dr03 = R3 - R0;

            matrixC[0,0] = dr01 / cc;
            matrixC[0, 1] = dr02 / cc;
            matrixC[0, 2] = dr03 / cc;

            return matrixC;
        }
        // ****************************************************************************************

        // Инициализация фильтра*******************************************************************
        // Алгоритм фильтрации по фиксированной выборке данных (Инициализация траекторного фильтра).
        // По трем первым идентифицированным отметкам производится квазиоптимальная линейная 
        // фильтрация и экстраполяция параметров траектории

        // Допущение: ошибки измерения координат являются независимыми
        // ****************************************************************************************

        public void InitFilter(
                               // Класс траектории, в котором уже есть первые три отметки
                               ClassTraj objClassTraj,

                               double CKO_X,
                               double CKO_Y,
                               double CKO_Z,

                               // Параметры для алгоритма формирования траекторий
                               ClassParameters objClassParameters
                              )
        {
            // -------------------------------------------------------------------------------------
            // Формируем лист 3-х первых отметок траектории

        //ClassTraj objClassTraj = new ClassTraj();

        List<ClassTrackPoint> objin = new List<ClassTrackPoint>();

            ClassTrackPoint objClassTrackPoint = new ClassTrackPoint();
            objClassTrackPoint = objClassTraj.listPointsTraj_I[0];

            // *&
            objClassTrackPoint.Xe = objClassTrackPoint.X;
            objClassTrackPoint.Ye = objClassTrackPoint.Y;
            objClassTrackPoint.Ze = objClassTrackPoint.Z;

            objin.Add(objClassTrackPoint);

            ClassTrackPoint objClassTrackPoint1 = new ClassTrackPoint();
            objClassTrackPoint1 = objClassTraj.listPointsTraj_I[1];

            // *&
            objClassTrackPoint1.Xe = objClassTrackPoint1.X;
            objClassTrackPoint1.Ye = objClassTrackPoint1.Y;
            objClassTrackPoint1.Ze = objClassTrackPoint1.Z;

            objin.Add(objClassTrackPoint1);

            ClassTrackPoint objClassTrackPoint2 = new ClassTrackPoint();
            objClassTrackPoint2 = objClassTraj.listPointsTraj_I[2];

            // *&
            objClassTrackPoint2.Xe = objClassTrackPoint2.X;
            objClassTrackPoint2.Ye = objClassTrackPoint2.Y;
            objClassTrackPoint2.Ze = objClassTrackPoint2.Z;

            objin.Add(objClassTrackPoint2);
            // -------------------------------------------------------------------------------------
            // Вектор измерений 9x1 Y=|x0 x1 x2 y0 y1 y2 z0 z1 z2| транспонированная

            double[,] Y = new double[9, 1];

            Y[0, 0] = objin[0].X;
            Y[1, 0] = objin[1].X;
            Y[2, 0] = objin[2].X;
            Y[3, 0] = objin[0].Y;
            Y[4, 0] = objin[1].Y;
            Y[5, 0] = objin[2].Y;
            Y[6, 0] = objin[0].Z;
            Y[7, 0] = objin[1].Z;
            Y[8, 0] = objin[2].Z;
            // -------------------------------------------------------------------------------------
            // Матрица Вандермота 9x6

            double[,] A = new double[9, 6];
            // Транспонированная 6x9
            double[,] AT = new double[6, 9];
            int i1 = 0;

            for (i1 = 0; i1 < 3; i1++) // 0..2 строки
            {
                A[i1, 0] = 1;
                A[i1, 1] = objin[i1].timePoint;
                A[i1, 2] = 0;
                A[i1, 3] = 0;
                A[i1, 4] = 0;
                A[i1, 5] = 0;
            }
            for (i1 = 3; i1 < 6; i1++) // 3..5 строки
            {
                A[i1, 0] = 0;
                A[i1, 1] = 0;
                A[i1, 2] = 1;
                A[i1, 3] = objin[i1 - 3].timePoint;
                A[i1, 4] = 0;
                A[i1, 5] = 0;
            }
            for (i1 = 6; i1 < 9; i1++) // 6..8 строки
            {
                A[i1, 0] = 0;
                A[i1, 1] = 0;
                A[i1, 2] = 0;
                A[i1, 3] = 0;
                A[i1, 4] = 1;
                A[i1, 5] = objin[i1 - 6].timePoint;
            }

            // Транспонированная
            MTransp(
                    9,
                    6,
                    A,
                    ref AT
                   );

            // -------------------------------------------------------------------------------------
            // Расчет матриц 3x3 ошибок измерения координат ИРИ в трех точках на основе метода линеаризации
            // (для трех моментов времени t0,t1,t2)

            List<ClassD_I> objD = new List<ClassD_I>();
            int i2 = 0;

            //for (i2 = 0; i2 < 3; i2++)
            //{
            // objD[i2].ti = objin[i2].timePoint;

            ClassD_I obj1ClassD_I = new ClassD_I();
            ClassD_I obj2ClassD_I = new ClassD_I();
            ClassD_I obj3ClassD_I = new ClassD_I();

            // *&
            obj1ClassD_I = CalcDI(
                               // Отметка траектории
                               // objin[0],
                               CKO_X,
                               CKO_Y,
                               CKO_Z
                              );
/*
            obj1ClassD_I = CalcDI_new(
                               // Отметка траектории
                                objin[0],
                               // Параметры для алгоритма формирования траекторий
                               objClassParameters
                              );

            objD.Add(obj1ClassD_I);
            objD[0].ti = objin[0].timePoint;
*/
            // *&
            obj2ClassD_I = CalcDI(
                               // Отметка траектории
                               //objin[1],
                               CKO_X,
                               CKO_Y,
                               CKO_Z
                              );
/*
            obj2ClassD_I = CalcDI_new(
                                // Отметка траектории
                                objin[1],
                               // Параметры для алгоритма формирования траекторий
                               objClassParameters
                              );
*/
            objD.Add(obj2ClassD_I);
            objD[1].ti = objin[1].timePoint;

            // *&
            obj3ClassD_I = CalcDI(
                               // Отметка траектории
                               //objin[2],
                               CKO_X,
                               CKO_Y,
                               CKO_Z
                              );
/*
            obj3ClassD_I = CalcDI_new(
                                // Отметка траектории
                                objin[2],
                               // Параметры для алгоритма формирования траекторий
                               objClassParameters
                              );
            objD.Add(obj3ClassD_I);
            objD[2].ti = objin[2].timePoint;
*/
            //}
            // -------------------------------------------------------------------------------------
            // Матрица ошибок измерений 9x9

            double[,] R0 = new double[9, 9];
            int i3 = 0;

            for (i3 = 0; i3 < 3; i3++) // Строки 0..2
            {
                R0[i3, 0] = objD[0].D_I[i3, 0];
                R0[i3, 1] = objD[0].D_I[i3, 1];
                R0[i3, 2] = objD[0].D_I[i3, 2];
                R0[i3, 3] = 0;
                R0[i3, 4] = 0;
                R0[i3, 5] = 0;
                R0[i3, 6] = 0;
                R0[i3, 7] = 0;
                R0[i3, 8] = 0;
            }
            for (i3 = 3; i3 < 6; i3++) // Строки 3..5
            {
                R0[i3, 0] = 0;
                R0[i3, 1] = 0;
                R0[i3, 2] = 0;
                R0[i3, 3] = objD[1].D_I[i3 - 3, 0];
                R0[i3, 4] = objD[1].D_I[i3 - 3, 1];
                R0[i3, 5] = objD[1].D_I[i3 - 3, 2];
                R0[i3, 6] = 0;
                R0[i3, 7] = 0;
                R0[i3, 8] = 0;
            }
            for (i3 = 6; i3 < 9; i3++) // Строки 6..8
            {
                R0[i3, 0] = 0;
                R0[i3, 1] = 0;
                R0[i3, 2] = 0;
                R0[i3, 3] = 0;
                R0[i3, 4] = 0;
                R0[i3, 5] = 0;
                R0[i3, 6] = objD[2].D_I[i3 - 6, 0];
                R0[i3, 7] = objD[2].D_I[i3 - 6, 1];
                R0[i3, 8] = objD[2].D_I[i3 - 6, 2];
            }
            // -------------------------------------------------------------------------------------
            // Обратная матрица ошибок измерений

            double[,] R0_Inv = new double[9, 9];
            int i4 = 9;

            MInv(
                  i4,
                  R0,
                  ref R0_Inv
                );
            // -------------------------------------------------------------------------------------
            // Корреляционная матрица 6x6 ошибок оценок параметров траектории

            double[,] Psi0_1 = new double[6, 9];
            double[,] Psi0_2 = new double[6, 6];
            //double[,] Psi0 = new double[6, 6];
            int i5 = 6;

            Psi0_1 = MatrixMultiplication(AT, R0_Inv);
            Psi0_2 = MatrixMultiplication(Psi0_1, A);
            //MInv(i5, Psi0_2,ref Psi0);
            MInv(i5, Psi0_2, ref objClassTraj.PSI_N_1);
            // -------------------------------------------------------------------------------------
            // Матрица оценок параметров траектории 6x1

            double[,] S_1 = new double[6, 9];
            double[,] S_2 = new double[6, 9];

            S_1 = MatrixMultiplication(objClassTraj.PSI_N_1, AT);
            S_2 = MatrixMultiplication(S_1, R0_Inv);
            objClassTraj.S_N_1 = MatrixMultiplication(S_2, Y);
            // -------------------------------------------------------------------------------------
            // Состояние траектории после инициализации фильтра пишем как первую точку фильтра
            // S(n)=|Xn Vx Yn Vy Zn Vz| транспонированная

            ClassTrackPointFiltr objClassTrackPointFiltr = new ClassTrackPointFiltr();

            objClassTrackPointFiltr.X = objClassTraj.S_N_1[0, 0];
            objClassTrackPointFiltr.VX = objClassTraj.S_N_1[1, 0];
            objClassTrackPointFiltr.Y = objClassTraj.S_N_1[2, 0];
            objClassTrackPointFiltr.VY = objClassTraj.S_N_1[3, 0];
            objClassTrackPointFiltr.Z = objClassTraj.S_N_1[4, 0];
            objClassTrackPointFiltr.VZ = objClassTraj.S_N_1[5, 0];

            objClassTraj.listPointsTrajFiltr_I = new List<ClassTrackPointFiltr>();
            objClassTraj.listPointsTrajFiltr_I.Add(objClassTrackPointFiltr);
            // -------------------------------------------------------------------------------------

            //return objClassTraj;
        }
        // ****************************************************************** Инициализация фильтра

        // Фильтр Калмана *************************************************************************

        public void FilterKalm(
                                 // I-я траектория
                                 ClassTraj objTraj_I,

                                 // Момент времени n-й отметки
                                 //double tn,
                                 // Момент времени (n-1)-й отметки
                                 //double tn_1,

                                 // СКО ускорений по плоскостным координатам
                                 double CKOA_Pl,
                                 // СКО ускорений по высоте
                                 double CKOA_H,

                                 double CKO_X,
                                 double CKO_Y,
                                 double CKO_Z,

                               // Параметры для алгоритма формирования траекторий
                               ClassParameters objClassParameters

                                )
        {
            // ------------------------------------------------------------------------------------
            // Vars

            int i = 0;

            // Интервал времени между n-й и (n-1)-й отметками
            double dtn = 0;

            // Случайные ускорения Axy-плоскость, Az - высота
            double Ax = 0;
            double Ay = 0;
            double Az = 0;

            // Динамическая матрица связи 6x6
            double[,] FN = new double[6, 6];
            double[,] FNT = new double[6, 6]; // транспонированная

            // Динамическая матрица возмущений 6x3
            double[,] GN = new double[6, 3];
            double[,] GNT = new double[3, 6]; // транспонированная

            // Матрица случайных ускорений 3x1
            // AN=|Ax Ay Az| транспонированная
            double[,] AN = new double[3, 1];
            // Дисперсионная матрица ускорений 6x6
            double[,] DA = new double[6, 6];
            // Экстраполированное значение состояния траектории (предсказание в текущий момент) 6x1
            double[,] SEN = new double[6, 1];

            // Экстраполированная корреляционная матрица ошибок оценки траектории 6x6
            double[,] PSI_Ext_N = new double[6, 6];

            // Матрица взаимосвязи между вектором измеряемых параметров 3x1 (координаты ВО) 
            // и вектором наблюдаемых параметров 6x1 (координаты ВО и составляющие вектора скорости 
            // по каждой из координат)
            // 3x6
            double[,] HN = new double[3, 6];
            double[,] HNT = new double[6, 3]; // транспонированная

            // Коэффициент усиления фильтра
            double[,] KN = new double[6, 3];

            // Состояние траектории на n-ом шаге (расчет оценки параметроа траектории)
            double[,] SN = new double[6, 1];

            // Корреляционная матрица ошибок оценки траектории 6x6
            double[,] PSI_N = new double[6, 6];

            // ------------------------------------------------------------------------------------
            // Интервал времени между n-й и (n-1)-й отметками

            if (objTraj_I.listPointsTraj_I.Count > 0)
            {
                dtn = objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].timePoint -
                     objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 2].timePoint;
            }
            // ------------------------------------------------------------------------------------
            // Динамическая матрица связи 6x6

            // 1-я строка
            FN[0, 0] = 1;
            FN[0, 1] = dtn;
            for (i = 0; i < 4; i++)
            { FN[0, i + 2] = 0; }
            // 2-я строка
            FN[1, 0] = 0;
            FN[1, 1] = 1;
            for (i = 0; i < 4; i++)
            { FN[1, i + 2] = 0; }
            // 3-я строка
            FN[2, 0] = 0; FN[2, 1] = 0; FN[2, 2] = 1; FN[2, 3] = dtn; FN[2, 4] = 0; FN[2, 5] = 0;
            // 4-я строка
            FN[3, 0] = 0; FN[3, 1] = 0; FN[3, 2] = 0; FN[3, 3] = 1; FN[3, 4] = 0; FN[3, 5] = 0;
            // 5-я строка
            for (i = 0; i < 4; i++)
            { FN[4, i] = 0; }
            FN[4, 4] = 1;
            FN[4, 5] = dtn;
            // 6-я строка
            for (i = 0; i < 5; i++)
            { FN[5, i] = 0; }
            FN[5, 5] = 1;

            // Транспонированная
            MTransp(
                    6,
                    6,
                    FN,
                    ref FNT
                   );
            // ------------------------------------------------------------------------------------
            // Динамическая матрица возмущений 6x3

            // 1-я строка
            GN[0, 0] = (dtn * dtn) / 2; GN[0, 1] = 0; GN[0, 2] = 0;
            // 2-я строка
            GN[1, 0] = dtn; GN[1, 1] = 0; GN[1, 2] = 0;
            // 3-я строка
            GN[2, 0] = 0; GN[2, 1] = (dtn * dtn) / 2; GN[2, 2] = 0;
            // 4-я строка
            GN[3, 0] = 0; GN[3, 1] = dtn; GN[3, 2] = 0;
            // 5-я строка
            GN[4, 0] = 0; GN[4, 1] = 0; GN[4, 2] = (dtn * dtn) / 2;
            // 6-я строка
            GN[5, 0] = 0; GN[5, 1] = 0; GN[5, 2] = dtn;

            // Транспонированная
            MTransp(
                    6,
                    3,
                    GN,
                    ref GNT
                   );
            // ------------------------------------------------------------------------------------
            // Случайные ускорения Axy-плоскость, Az - высота 
            // Нормальное распределение Гаусса

            Ax = objClassMRND_X.RandG(0, CKOA_Pl);
            Ay = objClassMRND_Y.RandG(0, CKOA_Pl);
            Az = objClassMRND_Z.RandG(0, CKOA_H);
            // ------------------------------------------------------------------------------------
            // Матрица случайных ускорений 3x1
            // AN=|Ax Ay Az| транспонированная

            AN[0, 0] = Ax;
            AN[1, 0] = Ay;
            AN[2, 0] = Az;
            // ------------------------------------------------------------------------------------
            // Дисперсионная матрица ускорений 6x6

            // 1-я строка
            DA[0, 0] = CKOA_Pl * CKOA_Pl;
            for (i = 0; i < 5; i++)
            { DA[0, i + 1] = 0; }
            // 2-я строка
            DA[1, 0] = 0;
            DA[1, 1] = CKOA_Pl * CKOA_Pl;
            for (i = 0; i < 4; i++)
            { DA[1, i + 2] = 0; }
            // 3-я строка
            DA[2, 0] = 0;
            DA[2, 1] = 0;
            DA[2, 2] = CKOA_Pl * CKOA_Pl;
            for (i = 0; i < 3; i++)
            { DA[2, i + 3] = 0; }
            // 4-я строка
            for (i = 0; i < 3; i++)
            { DA[3, i] = 0; }
            DA[3, 3] = CKOA_Pl * CKOA_Pl;
            DA[3, 4] = 0;
            DA[3, 5] = 0;
            // 5-я строка
            for (i = 0; i < 4; i++)
            { DA[4, i] = 0; }
            DA[4, 4] = CKOA_H * CKOA_H;
            DA[4, 5] = 0;
            // 6-я строка
            for (i = 0; i < 5; i++)
            { DA[5, i] = 0; }
            DA[5, 5] = CKOA_H * CKOA_H;
            // ------------------------------------------------------------------------------------
            // Экстраполированное значение состояния траектории (предсказание в текущий момент) 6x1

            double[,] SEN_Dop1 = new double[6, 1];
            double[,] SEN_Dop2 = new double[6, 1];

            SEN_Dop1 = MatrixMultiplication(FN, objTraj_I.S_N_1);
            SEN_Dop2 = MatrixMultiplication(GN, AN);
            SEN = AddMatrix(SEN_Dop1, SEN_Dop2);
            // ------------------------------------------------------------------------------------
            // Экстраполированная корреляционная матрица ошибок оценки траектории 6x6

            double[,] PSI_Ext_Dop1 = new double[6, 6];
            double[,] PSI_Ext_Dop2 = new double[6, 6];
            double[,] PSI_Ext_Dop3 = new double[6, 6];
            double[,] PSI_Ext_Dop4 = new double[6, 6];

            PSI_Ext_Dop1 = MatrixMultiplication(FN, objTraj_I.PSI_N_1);
            PSI_Ext_Dop2 = MatrixMultiplication(PSI_Ext_Dop1, FNT);
            PSI_Ext_Dop3 = MatrixMultiplication(GN, GNT);
            PSI_Ext_Dop4 = MatrixMultiplication(PSI_Ext_Dop3, DA);
            PSI_Ext_N = AddMatrix(PSI_Ext_Dop2, PSI_Ext_Dop4);
            // ------------------------------------------------------------------------------------
            // Матрица взаимосвязи между вектором измеряемых параметров 3x1 (координаты ВО) 
            // и вектором наблюдаемых параметров 6x1 (координаты ВО и составляющие вектора скорости 
            // по каждой из координат)
            // 3x6

            // 1-я строка
            HN[0, 0] = 1;
            for (i = 1; i < 6; i++)
            { HN[0, i] = 0; }
            // 2-я строка
            HN[1, 0] = 0;
            HN[1, 1] = 0;
            HN[1, 2] = 1;
            for (i = 3; i < 6; i++)
            { HN[1, i] = 0; }
            // 3-я строка
            for (i = 0; i < 4; i++)
            { HN[2, i] = 0; }
            HN[2, 4] = 1;
            HN[2, 5] = 0;

            // Транспонированная
            MTransp(
                    3,
                    6,
                    HN,
                    ref HNT
                   );
            // ------------------------------------------------------------------------------------
            // Расчет матрицы 3x3 ошибок измерения координат ИРИ в заданной точке 

            ClassD_I objDI = new ClassD_I();

            if (objTraj_I.listPointsTraj_I.Count != 0)
            {

                // n-я отметка
                //objDI.ti = objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].timePoint;
                //objDI = CalcDI(
                //               // Отметка траектории
                //               objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1],
                //
                //               CKO_X,
                //               CKO_Y,
                //               CKO_Z
                //              );

                // *&
                // Записываем в отметку экстраполированные значения
                objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].Xe = SEN[0, 0];
                objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].Ye = SEN[2, 0];
                objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].Ze = SEN[4, 0];

                // *&
                objDI = CalcDI(
                                   // Отметка траектории
                                   //objin[0],
                                   CKO_X,
                                   CKO_Y,
                                   CKO_Z
                                  );
/*
                objDI = CalcDI_new(
                                   // Отметка траектории
                                   objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1],
                                   // Параметры для алгоритма формирования траекторий
                                   objClassParameters
                                  );
*/

                objDI.ti = objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].timePoint;

            }
            // ------------------------------------------------------------------------------------
            // Коэффициент усиления фильтра

            double[,] KN_dop1 = new double[3, 6];
            double[,] KN_dop2 = new double[3, 3];
            double[,] KN_dop3 = new double[3, 3];
            double[,] KN_dop4 = new double[3, 3];
            double[,] KN_dop5 = new double[6, 3];

            KN_dop1 = MatrixMultiplication(HN, PSI_Ext_N);
            KN_dop2 = MatrixMultiplication(KN_dop1, HNT);
            KN_dop3 = AddMatrix(KN_dop2, objDI.D_I);

            MInv(
                  3,
                  KN_dop3,
                  ref KN_dop4
                );

            KN_dop5 = MatrixMultiplication(PSI_Ext_N, HNT);

            KN = MatrixMultiplication(KN_dop5, KN_dop4);
            // ------------------------------------------------------------------------------------
            // Состояние траектории на n-ом шаге (расчет оценки параметроа траектории) S(n) 6x1

            double[,] SN_dop1 = new double[3, 1];
            double[,] SN_dop2 = new double[3, 1];
            double[,] SN_dop3 = new double[6, 1];

            // ....................................................................................
            // Матрица n-го измерения (XYZ)

            double[,] YN = new double[3, 1];

            if (objTraj_I.listPointsTraj_I.Count != 0)
            {
                // n-я отметка
                YN[0, 0] = objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].X;
                YN[1, 0] = objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].Y;
                YN[2, 0] = objTraj_I.listPointsTraj_I[objTraj_I.listPointsTraj_I.Count - 1].Z;
            }
            // ....................................................................................

            SN_dop1 = MatrixMultiplication(HN, SEN);
            SN_dop2 = SubMatrix(YN, SN_dop1);
            SN_dop3 = MatrixMultiplication(KN, SN_dop2);
            SN = AddMatrix(SEN, SN_dop3);
            // ....................................................................................
            // Добавляем S(n) как следующую точку фильтра
            // S(n)=|Xn Vx Yn Vy Zn Vz| транспонированная

            ClassTrackPointFiltr objClassTrackPointFiltr = new ClassTrackPointFiltr();

            objClassTrackPointFiltr.X = SN[0, 0];
            objClassTrackPointFiltr.VX = SN[1, 0];
            objClassTrackPointFiltr.Y = SN[2, 0];
            objClassTrackPointFiltr.VY = SN[3, 0];
            objClassTrackPointFiltr.Z = SN[4, 0];
            objClassTrackPointFiltr.VZ = SN[5, 0];

            objTraj_I.listPointsTrajFiltr_I.Add(objClassTrackPointFiltr);
            // ....................................................................................
            // В классе траектории рассчитанное S(n) -> пишем на место S(n-1) для следующего шага

            ChangeMatrix(SN, ref objTraj_I.S_N_1);
            // ....................................................................................

            // ------------------------------------------------------------------------------------
            // Корреляционная матрица ошибок оценки траектории 6x6

            double[,] PSI_N_dop1 = new double[6, 6];
            double[,] PSI_N_dop2 = new double[6, 6];

            PSI_N_dop1 = MatrixMultiplication(KN, HN);
            PSI_N_dop2 = MatrixMultiplication(PSI_N_dop1, PSI_Ext_N);
            PSI_N = SubMatrix(PSI_Ext_N, PSI_N_dop2);

            // PSI_N пишем на место PSI_N_1 в классе траектории для следующего шага
            ChangeMatrix(PSI_N, ref objTraj_I.PSI_N_1);
            // ------------------------------------------------------------------------------------

        }
        // ************************************************************************* Фильтр Калмана

        // Вероятность принадлежности отметки траектории ******************************************
        // GlobalVarTraj.lstTracks
        // !!! Пока не используется

        public int CalcProbability(
                                  // i-я отметка
                                  ClassTrackPoint objClassTrackPoint,
                                  // j-я траектория
                                  ClassTraj objClassTraj,

                                 // СКО ускорений по плоскостным координатам
                                 double CKOA_Pl,
                                 // СКО ускорений по высоте
                                 double CKOA_H

                                     )
        {
            // ------------------------------------------------------------------------------------
            int i = 0;

            int Probability = 0;

            // Интервал времени между i-й и последней отметкой j-й траектории
            double dt = 0;

            // Матрица взаимосвязи между вектором измеряемых параметров 3x1 (координаты ВО) 
            // и вектором наблюдаемых параметров 6x1 (координаты ВО и составляющие вектора скорости 
            // по каждой из координат)
            // 3x6
            double[,] H = new double[3, 6];
            double[,] HT = new double[6, 3]; // транспонированная

            // Случайные ускорения Axy-плоскость, Az - высота
            double Ax = 0;
            double Ay = 0;
            double Az = 0;

            // Матрица случайных ускорений 3x1
            // AN=|Ax Ay Az| транспонированная
            double[,] AN = new double[3, 1];
            // Дисперсионная матрица ускорений 6x6
            double[,] DA = new double[6, 6];

            // Динамическая матрица связи 6x6
            double[,] FN = new double[6, 6];
            double[,] FNT = new double[6, 6]; // транспонированная

            // Динамическая матрица возмущений 6x3
            double[,] GN = new double[6, 3];
            double[,] GNT = new double[3, 6]; // транспонированная

            // Экстраполированное значение состояния траектории (предсказание в текущий момент) 6x1
            double[,] SEN = new double[6, 1];

            // Экстраполированная корреляционная матрица ошибок оценки траектории 6x6
            double[,] PSI_Ext_N = new double[6, 6];



            // ------------------------------------------------------------------------------------
            // Интервал времени между i-й и последней отметкой j-й траектории

            if (objClassTraj.listPointsTraj_I.Count > 0)
            {
                dt = objClassTrackPoint.timePoint -
                     objClassTraj.listPointsTraj_I[objClassTraj.listPointsTraj_I.Count - 1].timePoint;
            }
            // ------------------------------------------------------------------------------------
            // Матрица взаимосвязи между вектором измеряемых параметров 3x1 (координаты ВО) 
            // и вектором наблюдаемых параметров 6x1 (координаты ВО и составляющие вектора скорости 
            // по каждой из координат)
            // 3x6

            // 1-я строка
            H[0, 0] = 1;
            for (i = 0; i < 6; i++)
            { H[0, i + 1] = 0; }
            // 2-я строка
            H[1, 0] = 0;
            H[1, 1] = 0;
            H[1, 2] = 1;
            for (i = 0; i < 3; i++)
            { H[1, i + 3] = 0; }
            // 3-я строка
            for (i = 0; i < 4; i++)
            { H[2, i] = 0; }
            H[2, 4] = 1;
            H[2, 5] = 0;

            // Транспонированная
            MTransp(
                    3,
                    6,
                    H,
                    ref HT
                   );
            // ------------------------------------------------------------------------------------
            // Случайные ускорения Axy-плоскость, Az - высота 
            // Нормальное распределение Гаусса

            Ax = objClassMRND_X.RandG(0, CKOA_Pl);
            Ay = objClassMRND_Y.RandG(0, CKOA_Pl);
            Az = objClassMRND_Z.RandG(0, CKOA_H);
            // ------------------------------------------------------------------------------------
            // Матрица случайных ускорений 3x1
            // AN=|Ax Ay Az| транспонированная

            AN[0, 0] = Ax;
            AN[1, 0] = Ay;
            AN[2, 0] = Az;
            // ------------------------------------------------------------------------------------
            // Дисперсионная матрица ускорений 6x6

            // 1-я строка
            DA[0, 0] = CKOA_Pl * CKOA_Pl;
            for (i = 0; i < 5; i++)
            { DA[0, i + 1] = 0; }
            // 2-я строка
            DA[1, 0] = 0;
            DA[1, 1] = CKOA_Pl * CKOA_Pl;
            for (i = 0; i < 4; i++)
            { DA[1, i + 2] = 0; }
            // 3-я строка
            DA[2, 0] = 0;
            DA[2, 1] = 0;
            DA[2, 2] = CKOA_Pl * CKOA_Pl;
            for (i = 0; i < 3; i++)
            { DA[2, i + 3] = 0; }
            // 4-я строка
            for (i = 0; i < 3; i++)
            { DA[3, i] = 0; }
            DA[3, 3] = CKOA_Pl * CKOA_Pl;
            DA[3, 4] = 0;
            DA[3, 5] = 0;
            // 5-я строка
            for (i = 0; i < 4; i++)
            { DA[4, i] = 0; }
            DA[4, 4] = CKOA_H * CKOA_H;
            DA[4, 5] = 0;
            // 6-я строка
            for (i = 0; i < 5; i++)
            { DA[5, i] = 0; }
            DA[5, 5] = CKOA_H * CKOA_H;
            // ------------------------------------------------------------------------------------
            // Динамическая матрица связи 6x6

            // 1-я строка
            FN[0, 0] = 1;
            FN[0, 1] = dt;
            for (i = 0; i < 4; i++)
            { FN[0, i + 2] = 0; }
            // 2-я строка
            FN[1, 0] = 0;
            FN[1, 1] = 1;
            for (i = 0; i < 4; i++)
            { FN[1, i + 2] = 0; }
            // 3-я строка
            FN[2, 0] = 0; FN[2, 1] = 0; FN[2, 2] = 1; FN[2, 3] = dt; FN[2, 4] = 0; FN[2, 5] = 0;
            // 4-я строка
            FN[3, 0] = 0; FN[3, 1] = 0; FN[3, 2] = 0; FN[3, 3] = 1; FN[3, 4] = 0; FN[3, 5] = 0;
            // 5-я строка
            for (i = 0; i < 4; i++)
            { FN[4, i] = 0; }
            FN[4, 4] = 1;
            FN[4, 5] = dt;
            // 6-я строка
            for (i = 0; i < 5; i++)
            { FN[5, i] = 0; }
            FN[5, 5] = 1;

            // Транспонированная
            MTransp(
                    6,
                    6,
                    FN,
                    ref FNT
                   );
            // ------------------------------------------------------------------------------------
            // Динамическая матрица возмущений 6x3

            // 1-я строка
            GN[0, 0] = (dt * dt) / 2; GN[0, 1] = 0; GN[0, 2] = 0;
            // 2-я строка
            GN[1, 0] = dt; GN[1, 1] = 0; GN[1, 2] = 0;
            // 3-я строка
            GN[2, 0] = 0; GN[2, 1] = (dt * dt) / 2; GN[2, 2] = 0;
            // 4-я строка
            GN[3, 0] = 0; GN[3, 1] = dt; GN[3, 2] = 0;
            // 5-я строка
            GN[4, 0] = 0; GN[4, 1] = 0; GN[4, 2] = (dt * dt) / 2;
            // 6-я строка
            GN[5, 0] = 0; GN[5, 1] = 0; GN[5, 2] = dt;

            // Транспонированная
            MTransp(
                    6,
                    3,
                    GN,
                    ref GNT
                   );
            // ------------------------------------------------------------------------------------
            // Экстраполированное значение состояния траектории (предсказание в текущий момент) 6x1

            double[,] SEN_Dop1 = new double[6, 1];
            double[,] SEN_Dop2 = new double[6, 1];

            SEN_Dop1 = MatrixMultiplication(FN, objClassTraj.S_N_1);
            SEN_Dop2 = MatrixMultiplication(GN, AN);
            SEN = AddMatrix(SEN_Dop1, SEN_Dop2);
            // ------------------------------------------------------------------------------------
            // Экстраполированная корреляционная матрица ошибок оценки траектории 6x6

            double[,] PSI_Ext_Dop1 = new double[6, 6];
            double[,] PSI_Ext_Dop2 = new double[6, 6];
            double[,] PSI_Ext_Dop3 = new double[6, 6];
            double[,] PSI_Ext_Dop4 = new double[6, 6];

            PSI_Ext_Dop1 = MatrixMultiplication(FN, objClassTraj.PSI_N_1);
            PSI_Ext_Dop2 = MatrixMultiplication(PSI_Ext_Dop1, FNT);
            PSI_Ext_Dop3 = MatrixMultiplication(GN, GNT);
            PSI_Ext_Dop4 = MatrixMultiplication(PSI_Ext_Dop3, DA);
            PSI_Ext_N = AddMatrix(PSI_Ext_Dop2, PSI_Ext_Dop4);
            // ------------------------------------------------------------------------------------
            double a1 = 0;
            double a2 = 0;
            double a3 = 0;
            double a4 = 0;
            double a5 = 0;

            double[,] A1 = new double[3, 6];
            double[,] A2 = new double[3, 3];

            a1 = Math.Pow(Math.Sqrt(2 * Math.PI), 3);

            A1 = MatrixMultiplication(H, PSI_Ext_N);
            A2 = MatrixMultiplication(A1, HT);

            a2 = Determ(A2);
            a3 = Math.Sqrt(a2);
            a4 = a1 * a3;
            a5 = 1 / a4;
            // ------------------------------------------------------------------------------------
            // ------------------------------------------------------------------------------------
            // ------------------------------------------------------------------------------------
            // ------------------------------------------------------------------------------------
            // ------------------------------------------------------------------------------------
            // ------------------------------------------------------------------------------------

            return Probability;
        }
        // ****************************************** Вероятность принадлежности отметки траектории


    } // Class
} // Namespace
