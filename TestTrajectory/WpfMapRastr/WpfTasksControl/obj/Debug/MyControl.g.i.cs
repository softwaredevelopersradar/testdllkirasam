﻿#pragma checksum "..\..\MyControl.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "A6B39C4AD684CADF14B108721B4F297EA64749ED4A8C39DE76384560CC33A3C3"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfTasksControl;


namespace WpfTasksControl {
    
    
    /// <summary>
    /// UserControl1
    /// </summary>
    public partial class UserControl1 : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel PanelTasks1;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonAzimuthTsk;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonRoutsTsk;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonZPVTsk;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonControlJammingZoneTsk;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NavigationJammingZoneTsk;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SpoofingZoneTsk;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SectorsRRTsk;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SectorsRPTsk;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonTabTsk;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonScreenShotTsk;
        
        #line default
        #line hidden
        
        
        #line 197 "..\..\MyControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonTriangTsk;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfTasksControl;component/mycontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MyControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.PanelTasks1 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.ButtonAzimuthTsk = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\MyControl.xaml"
            this.ButtonAzimuthTsk.Click += new System.Windows.RoutedEventHandler(this.ButtonAzimuthTsk_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.ButtonRoutsTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.ButtonZPVTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.ButtonControlJammingZoneTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.NavigationJammingZoneTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.SpoofingZoneTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.SectorsRRTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 9:
            this.SectorsRPTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.ButtonTabTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 11:
            this.ButtonScreenShotTsk = ((System.Windows.Controls.Button)(target));
            return;
            case 12:
            this.ButtonTriangTsk = ((System.Windows.Controls.Button)(target));
            
            #line 197 "..\..\MyControl.xaml"
            this.ButtonTriangTsk.Click += new System.Windows.RoutedEventHandler(this.ButtonTriangTsk_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

