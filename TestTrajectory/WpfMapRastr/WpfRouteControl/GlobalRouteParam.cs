﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Pol
using System.Collections.ObjectModel; // forObservableCollection
using System.Collections.Specialized; // for ObservableCollection
using System.ComponentModel;
using System.Runtime.CompilerServices;

using ModelsTablesDBLib;

namespace WpfRouteControl
{
    public class GlobalRouteParam: INotifyPropertyChanged
    {
        public ObservableCollection<RouteParam> CollRouteParam { get; set; }

        public GlobalRouteParam()
        {
            CollRouteParam = new ObservableCollection<RouteParam> { };

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


    } // Class
} // Namespace
