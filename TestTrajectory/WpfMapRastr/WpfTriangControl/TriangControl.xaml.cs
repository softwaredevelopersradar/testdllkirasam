﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTriangControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        public event EventHandler OnCalcTriang;

        public UserControl1()
        {
            InitializeComponent();
        }

        // Click *************************************************************
        public void ButtonCalc_Click(object sender, RoutedEventArgs e)
        {
            OnCalcTriang?.Invoke(this, new EventArgs());
        }
        // ************************************************************* Click

    } // Class
} // Namespace
