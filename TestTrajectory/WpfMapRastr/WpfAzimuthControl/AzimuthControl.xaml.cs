﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Semen
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;

using System.Data;
using System.Reflection;
//using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using Microsoft.Win32;

namespace WpfAzimuthControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        public event EventHandler OnClearAzimuth;
        public event EventHandler OnCheckedShowAzimuth;
        public event EventHandler OnUnCheckedShowAzimuth;


        public UserControl1()
        {
            InitializeComponent();
        }

        // Clear **************************************************************************
        public void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            OnClearAzimuth?.Invoke(this, new EventArgs());

        }
        // ************************************************************************** Clear

        // CheckBox ***********************************************************************
        private void checkBoxShow_Checked(object sender, RoutedEventArgs e)
        {
            OnCheckedShowAzimuth?.Invoke(this, new EventArgs());
        }

        private void checkBoxShow_Unchecked(object sender, RoutedEventArgs e)
        {
            OnUnCheckedShowAzimuth?.Invoke(this, new EventArgs());
        }
        // *********************************************************************** CheckBox


    } // Class
} // Namespace
