﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
//using System.Windows.Forms;
using System.IO;
//using System.Drawing.Imaging;
using System.Windows.Input;

using GeoCalculator;

namespace fDRM
{
    public class ClassDRM_dll
    {

        // Module ******************************************************************************
        public double module(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        //  ****************************************************************************** Module

        // f_DRM ********************************************************************************
        // !!!  Возвращаемый ClassObjectTmp наследуется от ClassObject, т.е. на выходе имеем 
        //      Latitude, Longitude, Altitude (кроме x,y,z -> МЗСК -> НО это моя МЗСК, где z=высота ,)
        //      (это только в этой DLL. везде в других местах работаем в МЗСК Виктора: Y=высота) 


        public ClassObjectTmp f_DRM(

                           // !!! Индексы станций=1 (базовая),2,3,4
                           // Признак BaseStation=true для базовой
                           List<ClassObject> lstClassObject,

                           //=false -> мнимые точки инвертируем
                           // =true -> мнимые точки отбрасываем
                           bool flagMnim,

                           // Высота,которой заменим реально рассчитанную
                           // !!! Если=-1 -> замену не производим, используем обычный алгоритм
                           double HInput,

                           // =1 -> используется эта задержка
                           bool fldt1,
                           bool fldt2,
                           bool fldt3

                           // Otl
                           //ref int sh1,
                           //ref int sh2
                          )
        {

            int NN = 4;
            double[] x2 = new double[NN];
            double[] y2 = new double[NN];
            double[] z2 = new double[NN];
            double[] x3 = new double[NN];
            double[] y3 = new double[NN];
            double[] z3 = new double[NN];

            double rnz = 0;
            double deltaz = 0;
            double rzz = 0;
            double azz = 0;
            double bzz = 0;
            double fzz = 0;
            double Blat = 0;

            // Временный лмст для станций
            List<ClassObjectTmp> lstClassObjectTmp = new List<ClassObjectTmp>();
            ClassObjectTmp objClassObjectTmp = new ClassObjectTmp();
            // ------------------------------------------------------------------------------------
            // 03_01_2021

            double hsr = 0;
            for (int kj11 = 0; kj11 < lstClassObject.Count; kj11++)
            {
                hsr = hsr + lstClassObject[kj11].Altitude;
            }
            hsr = hsr / lstClassObject.Count;
                // ------------------------------------------------------------------------------------

            // Переписываем входные параметры станций во временный лист

            for (int kj1 = 0; kj1 < lstClassObject.Count; kj1++)
            {
                ClassObjectTmp objClassObjectTmp1 = new ClassObjectTmp();
                objClassObjectTmp1.Latitude = lstClassObject[kj1].Latitude;
                objClassObjectTmp1.Longitude = lstClassObject[kj1].Longitude;
                objClassObjectTmp1.Altitude = lstClassObject[kj1].Altitude;
                objClassObjectTmp1.tau = lstClassObject[kj1].tau;
                objClassObjectTmp1.BaseStation = lstClassObject[kj1].BaseStation;
                objClassObjectTmp1.IndexStation = lstClassObject[kj1].IndexStation;

                lstClassObjectTmp.Add(objClassObjectTmp1);
            }
            // ------------------------------------------------------------------------------------
            // Перевод координат станций в радианы

            for (int kj1 = 0; kj1 < lstClassObjectTmp.Count; kj1++)
            {
                lstClassObjectTmp[kj1].LatRad = lstClassObjectTmp[kj1].Latitude * Math.PI / 180;
                lstClassObjectTmp[kj1].LongRad = lstClassObjectTmp[kj1].Longitude * Math.PI / 180;
            }
            // ------------------------------------------------------------------------------------
            // Координаты станций в ГЦСК

            for (int kj = 0; kj < lstClassObjectTmp.Count; kj++)
            {
                //    // Координаты станций в ГЦСК
                    ClassGeoCalculator.f_BLH_XYZ_84_1
                                                     (
                                                      lstClassObjectTmp[kj].Latitude,
                                                      lstClassObjectTmp[kj].Longitude,
                                                      lstClassObjectTmp[kj].Altitude,
                                                      ref lstClassObjectTmp[kj].x1,
                                                      ref lstClassObjectTmp[kj].y1,
                                                      ref lstClassObjectTmp[kj].z1
                                                      );
            }
            // ------------------------------------------------------------------------------------
            // Координаты станций в МЗСК (начало координат в точке стояния базовой станции)
            // Z-  в зенит по местной нормали к поверхности эллипсоида (высота)
            // X-север Y(запад) - в горизонтальной плоскости и образуют правую систему координат

            int indexbaz = -1;
            indexbaz = lstClassObjectTmp.FindIndex(xx => (xx.BaseStation == true));
            if (indexbaz >= 0)
            {
                for (int kj2 = 0; kj2 < lstClassObjectTmp.Count; kj2++)
                {
                    if (lstClassObjectTmp[kj2].BaseStation == false)
                    {
                       ClassGeoCalculator.Geoc_MZSK(
                       // Base station
                       lstClassObjectTmp[indexbaz].LatRad, // rad
                       lstClassObjectTmp[indexbaz].LongRad,
                       lstClassObjectTmp[indexbaz].x1,
                       lstClassObjectTmp[indexbaz].y1,
                       lstClassObjectTmp[indexbaz].z1,

                       // Станции 2,3,4
                       lstClassObjectTmp[kj2].x1,
                       lstClassObjectTmp[kj2].y1,
                       lstClassObjectTmp[kj2].z1,

                       ref lstClassObjectTmp[kj2].x,
                       ref lstClassObjectTmp[kj2].y,
                       ref lstClassObjectTmp[kj2].z
                       );

                    } // if(NO base station)
                } // for
            } // if (indexbaz >= 0)

            // ------------------------------------------------------------------------------------

            // ------------------------------------------------------------------------------------
            // Скорость света
            //double vc = 3E8;
            double vc = 299792458;

            // Базы (расстояние от опорного ПП0 до j-го пункта приема ППj, j=1,2,3)
            double B01 = 0;
            double B02 = 0;
            double B03 = 0;

            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3
            double dr01 = 0;
            double dr02 = 0;
            double dr03 = 0;

            // Коэффициенты в системе линейных уравнений
            double K1 = 0;
            double K2 = 0;
            double K3 = 0;
            // ------------------------------------------------------------------------------------
            // ***
            // Для решения системы уравнений матричным методом (Крамера)

            double OPR = 0;

            double f1 = 0;
            double f2 = 0;
            double f3 = 0;
            double f4 = 0;
            double f5 = 0;
            double f6 = 0;
            double f7 = 0;
            double f8 = 0;
            double f9 = 0;

            double v1 = 0;
            double v2 = 0;
            double v3 = 0;
            double v4 = 0;
            double v5 = 0;
            double v6 = 0;
            double v7 = 0;
            double v8 = 0;
            double v9 = 0;

            double w1 = 0;
            double w2 = 0;
            double w3 = 0;
            double w4 = 0;
            double w5 = 0;
            double w6 = 0;

            double AA1 = 0;
            double BB1 = 0;
            double CC1 = 0;
            double DD1 = 0;

            // Дальность до ИРИ относительно начала координат
            double RC1 = 0;
            double RC1_1 = 0;


            double xxx1 = 0;
            double yyy1 = 0;
            double zzz1 = 0;
            double xxx1_1 = 0;
            double yyy1_1 = 0;
            double zzz1_1 = 0;
            // ------------------------------------------------------------------------------------
            // Отсортировываем массив в порядке возрастания индекса станций (1,2,3,4)
            // Элементы листа [0] (базовая),[1],[2],[3]

            var sortlstClassObjectTmp = lstClassObjectTmp.OrderBy(u => u.IndexStation);
            // ------------------------------------------------------------------------------------
            // Базы (расстояние от опорного ПП0 до j-го пункта приема ППj, j=1,2,3)

            B01 = Math.Sqrt((lstClassObjectTmp[1].x) * (lstClassObjectTmp[1].x) + 
                            (lstClassObjectTmp[1].y) * (lstClassObjectTmp[1].y) + 
                            (lstClassObjectTmp[1].z) * (lstClassObjectTmp[1].z));

            B02 = Math.Sqrt((lstClassObjectTmp[2].x) * (lstClassObjectTmp[2].x) + 
                            (lstClassObjectTmp[2].y) * (lstClassObjectTmp[2].y) + 
                            (lstClassObjectTmp[2].z) * (lstClassObjectTmp[2].z));

            B03 = Math.Sqrt((lstClassObjectTmp[3].x) * (lstClassObjectTmp[3].x) + 
                            (lstClassObjectTmp[3].y) * (lstClassObjectTmp[3].y) + 
                            (lstClassObjectTmp[3].z) * (lstClassObjectTmp[3].z));
            // ------------------------------------------------------------------------------------
            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3

            dr01 = lstClassObjectTmp[1].tau * vc;
            dr02 = lstClassObjectTmp[2].tau * vc;
            dr03 = lstClassObjectTmp[3].tau * vc;
            // ------------------------------------------------------------------------------------
            // Коэффициенты в системе линейных уравнений

            K1 = 0.5 * (B01 * B01 - dr01 * dr01);
            K2 = 0.5 * (B02 * B02 - dr02 * dr02);
            K3 = 0.5 * (B03 * B03 - dr03 * dr03);
            // ------------------------------------------------------------------------------------
            // ***
            // Для решения системы уравнений матричным методом (Крамера)

            OPR = lstClassObjectTmp[1].x * lstClassObjectTmp[2].y * lstClassObjectTmp[3].z -
                  lstClassObjectTmp[1].x * lstClassObjectTmp[2].z * lstClassObjectTmp[3].y -
                  lstClassObjectTmp[1].y * lstClassObjectTmp[2].x * lstClassObjectTmp[3].z +
                  lstClassObjectTmp[1].y * lstClassObjectTmp[2].z * lstClassObjectTmp[3].x +
                  lstClassObjectTmp[1].z * lstClassObjectTmp[2].x * lstClassObjectTmp[3].y -
                  lstClassObjectTmp[1].z * lstClassObjectTmp[2].y * lstClassObjectTmp[3].x;
            // ------------------------------------------------------------------------------------

            // OPR!=0 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (OPR != 0)
            {

                f1 = lstClassObjectTmp[2].y * lstClassObjectTmp[3].z - lstClassObjectTmp[2].z * lstClassObjectTmp[3].y;
                f2 = lstClassObjectTmp[1].z * lstClassObjectTmp[3].y - lstClassObjectTmp[1].y * lstClassObjectTmp[3].z;
                f3 = lstClassObjectTmp[1].y * lstClassObjectTmp[2].z - lstClassObjectTmp[1].z * lstClassObjectTmp[2].y;
                f4 = lstClassObjectTmp[2].z * lstClassObjectTmp[3].x - lstClassObjectTmp[2].x * lstClassObjectTmp[3].z;
                f5 = lstClassObjectTmp[1].x * lstClassObjectTmp[3].z - lstClassObjectTmp[1].z * lstClassObjectTmp[3].x;
                f6 = lstClassObjectTmp[1].z * lstClassObjectTmp[2].x - lstClassObjectTmp[1].x * lstClassObjectTmp[2].z;
                f7 = lstClassObjectTmp[2].x * lstClassObjectTmp[3].y - lstClassObjectTmp[2].y * lstClassObjectTmp[3].x;
                f8 = lstClassObjectTmp[1].y * lstClassObjectTmp[3].x - lstClassObjectTmp[1].x * lstClassObjectTmp[3].y;
                f9 = lstClassObjectTmp[1].x * lstClassObjectTmp[2].y - lstClassObjectTmp[1].y * lstClassObjectTmp[2].x;

                v1 = f1 / OPR;
                v2 = f2 / OPR;
                v3 = f3 / OPR;
                v4 = f4 / OPR;
                v5 = f5 / OPR;
                v6 = f6 / OPR;
                v7 = f7 / OPR;
                v8 = f8 / OPR;
                v9 = f9 / OPR;

                w1 = v1 * K1 + v2 * K2 + v3 * K3;
                w2 = -v1 * dr01 - v2 * dr02 - v3 * dr03;
                w3 = v4 * K1 + v5 * K2 + v6 * K3;
                w4 = -v4 * dr01 - v5 * dr02 - v6 * dr03;
                w5 = v7 * K1 + v8 * K2 + v9 * K3;
                w6 = -v7 * dr01 - v8 * dr02 - v9 * dr03;

                // ------------------------------------------------------------------------------------



                // ОБЫЧНЫЙ АЛГОРИТМ VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VE

                if (HInput == -1)
                {

                    // ***
                    // Для решения системы уравнений матричным методом (Крамера)

                    AA1 = w2 * w2 + w4 * w4 + w6 * w6 - 1;
                    BB1 = 2 * (w1 * w2 + w3 * w4 + w5 * w6);
                    CC1 = w1 * w1 + w3 * w3 + w5 * w5;

                    DD1 = BB1 * BB1 - 4 * AA1 * CC1;


                    //?????????????????????????????????????
                    // Change 16_12
                    // Мнимое решение
                    if (DD1 < 0)
                    {
                        // Было раньше
                        if (flagMnim == false)
                            DD1 = -DD1;
                        else
                        {
                            objClassObjectTmp.Altitude = -1;
                            objClassObjectTmp.Latitude = -1;
                            objClassObjectTmp.Longitude = -1;
                            objClassObjectTmp.FlagRezCalc = 1;
                            return objClassObjectTmp;
                        }

                    } // DD1<0
                      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    /*                       
                                            int p1 = 0;
                                          int p2 = 0;
                                            double vp1 = BB1 * BB1;
                                            double vp2 = 4 * AA1 * CC1;

                                            while (vp1 >= 10)
                                            {
                                                vp1 = vp1 / 10;
                                                p1 += 1;
                                            }
                                            while (vp2 >= 10)
                                            {
                                                vp2 = vp2 / 10;
                                                p2 += 1;
                                            }
                                            double vp3 = vp1 - vp2;
                                            DD1 = vp3;

                                            int f = 0;
                                            if (DD1 == 0)
                                                f = 0; // один корень
                                            else if (DD1 > 0)
                                                f = 1; // два вещественных корня
                                            else
                                                f = 2; // комплексные корни


                                            //if (DD < 0)
                                            //    DD = -DD;
                                            //DD = 0;
                    */

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                    RC1 = (-BB1 + Math.Sqrt(DD1)) / (2 * AA1);

                    //if (RC < 0)
                    RC1_1 = (-BB1 - Math.Sqrt(DD1)) / (2 * AA1);
                    // ------------------------------------------------------------------------------------
                    // Координаты ИРИ в МЗСК

                    xxx1 = w1 + w2 * RC1;
                    yyy1 = w3 + w4 * RC1;
                    zzz1 = w5 + w6 * RC1;

                    xxx1_1 = w1 + w2 * RC1_1;
                    yyy1_1 = w3 + w4 * RC1_1;
                    zzz1_1 = w5 + w6 * RC1_1;

                    // --------------------------------------------------------------------------------------
                    // Otl

                    if (
                        (zzz1 > 0) &&
                        (zzz1_1 > 0)
                       )
                    {
                        //sh1 += 1;
                    }
                    if (
                        (zzz1 < 0) &&
                        (zzz1_1 < 0)
                      )
                    {
                        //sh2 += 1;
                    }
                    // --------------------------------------------------------------------------------------

                    // VYBOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    //  Change 16_12
                    // Выбор решения

                    // ......................................................................................
                    // Если одна высота отрицательная, а другая положительная -> выбираем положительную

                    if ((zzz1 >= 0) && (zzz1_1 < 0))
                    {
                        objClassObjectTmp.x = xxx1;
                        objClassObjectTmp.y = yyy1;
                        objClassObjectTmp.z = zzz1;

                        // 08_03
                        //if (HInput != -1)
                        //    objClassObjectTmp.z = HInput;

                        objClassObjectTmp.FlagRezCalc = 0;
                    }
                    // ......................................................................................
                    // Если одна высота отрицательная, а другая положительная -> выбираем положительную

                    else if ((zzz1 < 0) && (zzz1_1 >= 0))
                    {
                        objClassObjectTmp.x = xxx1_1;
                        objClassObjectTmp.y = yyy1_1;
                        objClassObjectTmp.z = zzz1_1;

                        // 08_03
                        //if (HInput != -1)
                        //    objClassObjectTmp.z = HInput;

                        objClassObjectTmp.FlagRezCalc = 0;

                    }
                    // ......................................................................................
                    // Обе высоты отрицательные

                    else if ((zzz1 < 0) && (zzz1_1 < 0))
                    {
                        objClassObjectTmp.Altitude = -1;
                        objClassObjectTmp.Latitude = -1;
                        objClassObjectTmp.Longitude = -1;
                        objClassObjectTmp.FlagRezCalc = 2;
                        return objClassObjectTmp;

                    }
                    // ....................................................................................
                    // Обе высоты положительные

                    // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    // Анализ измеренных и рассчитанных по двум вариантам решения задержек

                    else
                    {
                        var matrixCtime = new double[1, 3];
                        var matrixCtime_1 = new double[1, 3];
                        matrixCtime = Calc_dt(
                                              lstClassObjectTmp,
                                              xxx1,
                                              yyy1,
                                              zzz1
                                             );
                        matrixCtime_1 = Calc_dt(
                                              lstClassObjectTmp,
                                              xxx1_1,
                                              yyy1_1,
                                              zzz1_1
                                             );


                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // Знаки задержек совпадают у первого набора координат

                        if (
                        (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                        (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                        (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&

                        (
                        (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime_1[0, 0])) ||
                        (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime_1[0, 1])) ||
                        (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime_1[0, 2]))
                        )

                        ) // IF

                        {
                            objClassObjectTmp.x = xxx1;
                            objClassObjectTmp.y = yyy1;
                            objClassObjectTmp.z = zzz1;

                            // 08_03
                            //if (HInput != -1)
                            //    objClassObjectTmp.z = HInput;

                            objClassObjectTmp.FlagRezCalc = 0;

                        }
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // Знаки задержек совпадают у второго набора координат

                        else if (

                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2])) &&

                            (
                            (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime[0, 0])) ||
                            (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime[0, 1])) ||
                            (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime[0, 2]))
                            )

                            )

                        {
                            objClassObjectTmp.x = xxx1_1;
                            objClassObjectTmp.y = yyy1_1;
                            objClassObjectTmp.z = zzz1_1;

                            // 08_03
                            //if (HInput != -1)
                            //    objClassObjectTmp.z = HInput;

                            objClassObjectTmp.FlagRezCalc = 0;

                        }
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // Знаки задержек совпадают у обоих наборов координат ->
                        // выбираем набор с меньшей высотой

                        else if (
                        (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                        (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                        (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&
                        (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                        (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                        (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2]))

                        ) // IF

                        {
                            if (
                                zzz1 <= zzz1_1
                                )
                            {
                                if (zzz1 >= hsr)
                                {
                                    objClassObjectTmp.x = xxx1;
                                    objClassObjectTmp.y = yyy1;
                                    objClassObjectTmp.z = zzz1;

                                    // 08_03
                                    //if (HInput != -1)
                                    //    objClassObjectTmp.z = HInput;

                                }
                                else
                                {
                                    objClassObjectTmp.x = xxx1_1;
                                    objClassObjectTmp.y = yyy1_1;
                                    objClassObjectTmp.z = zzz1_1;

                                    // 08_03
                                    //if (HInput != -1)
                                    //    objClassObjectTmp.z = HInput;

                                }
                            }
                            else
                            {
                                if (zzz1_1 >= hsr)
                                {
                                    objClassObjectTmp.x = xxx1_1;
                                    objClassObjectTmp.y = yyy1_1;
                                    objClassObjectTmp.z = zzz1_1;

                                    // 08_03
                                    //if (HInput != -1)
                                    //    objClassObjectTmp.z = HInput;

                                }
                                else
                                {
                                    objClassObjectTmp.x = xxx1;
                                    objClassObjectTmp.y = yyy1;
                                    objClassObjectTmp.z = zzz1;

                                    // 08_03
                                    //if (HInput != -1)
                                    //    objClassObjectTmp.z = HInput;

                                }

                            }


                            objClassObjectTmp.FlagRezCalc = 0;

                        }
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                        // Знаки задержек не совпадают ни у одногонабора координат 

                        else
                        {
                            objClassObjectTmp.Altitude = -1;
                            objClassObjectTmp.Latitude = -1;
                            objClassObjectTmp.Longitude = -1;
                            objClassObjectTmp.FlagRezCalc = 3;
                            return objClassObjectTmp;

                        }
                        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                    } // IF(обе высоты положительные)
                      // ........................................................................

                    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> VYBOR

                    // Otl
                    //objClassObjectTmp.x = xxx1_1;
                    //objClassObjectTmp.y = yyy1_1;
                    //objClassObjectTmp.z = zzz1_1;

                } // HInput==-1 -> Обычный алгоритм
                // VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VETKA1VE ОБЫЧНЫЙ АЛГОРИТМ

                // АЛГОРИТМ С фиксированной высотой VETKA2VETKA2VETKA2VETKA2VETKA2VETKA2VETKA2VETKA2VETK
                // HInput!=-1

                else
                {
                    double RCdop = 0;

                    // dt1,dt2,dt3 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 12
                    // При расчете используются dt1,dt2,dt3

                    if ((fldt1 == true) && (fldt2 == true) && (fldt3 == true))
                    {

                        // ------------------------------------------------------------------------------------
                        // Координаты ИРИ в МЗСК

                        // !!! Основная система уравнений
                        //xxx1 = w1 + w2 * RC1;
                        //yyy1 = w3 + w4 * RC1;
                        //zzz1 = w5 + w6 * RC1;


                        zzz1 = HInput;

                        if (w6 > 1E-11)
                            RC1 = (zzz1 - w5) / w6;
                        else
                            RC1 = (zzz1 - w5) / 1E-11;
                        // ......................................................................................
                        if (RC1 <= 0)
                        {
                            objClassObjectTmp.Altitude = -1;
                            objClassObjectTmp.Latitude = -1;
                            objClassObjectTmp.Longitude = -1;
                            objClassObjectTmp.FlagRezCalc = 5;
                            return objClassObjectTmp;

                        } // RC1<0
                          // ......................................................................................
                          // RC1>=0

                        else
                        {
                            xxx1 = w1 + w2 * RC1;
                            yyy1 = w3 + w4 * RC1;

                            objClassObjectTmp.x = xxx1;
                            objClassObjectTmp.y = yyy1;
                            objClassObjectTmp.z = zzz1;
                            objClassObjectTmp.FlagRezCalc = 0;

                        } // RC1>=0
                          // ......................................................................................

                    } // dt1==true&&dt2==true&&dt3==true
                      // 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 12 dt1,dt2,dt3

                    // dt1,dt2 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 
                    // При расчете используются dt1,dt2

                    else if ((fldt1 == true) && (fldt2 == true) && (fldt3 == false))
                    {

                        zzz1 = HInput;

                        // Для решения системы уравнений матричным методом (Крамера)
                        AA1 = w2 * w2 + w4 * w4 - 1;
                        BB1 = 2 * (w1 * w2 + w3 * w4);
                        CC1 = w1 * w1 + w3 * w3 + zzz1 * zzz1;

                        DD1 = BB1 * BB1 - 4 * AA1 * CC1;


                        //?????????????????????????????????????
                        // Мнимое решение
                        if (DD1 < 0)
                        {
                            // Было раньше
                            if (flagMnim == false)
                                DD1 = -DD1;
                            else
                            {
                                objClassObjectTmp.Altitude = -1;
                                objClassObjectTmp.Latitude = -1;
                                objClassObjectTmp.Longitude = -1;
                                objClassObjectTmp.FlagRezCalc = 1;
                                return objClassObjectTmp;
                            }

                        } // DD1<0

                        RC1 = (-BB1 + Math.Sqrt(DD1)) / (2 * AA1);

                        //if (RC < 0)
                        RC1_1 = (-BB1 - Math.Sqrt(DD1)) / (2 * AA1);
                        // ------------------------------------------------------------------------------------

                        // ####################################################################################
                        if ((RC1 <= 0) && (RC1_1 <= 0))
                        {
                            objClassObjectTmp.Altitude = -1;
                            objClassObjectTmp.Latitude = -1;
                            objClassObjectTmp.Longitude = -1;
                            objClassObjectTmp.FlagRezCalc = 5;
                            return objClassObjectTmp;

                        } // RC1<=0&&RC1_1<=0
                        // ####################################################################################

                        else if ((RC1 > 0) && (RC1_1 <= 0))
                        {
                            // Координаты ИРИ в МЗСК
                            xxx1 = w1 + w2 * RC1;
                            yyy1 = w3 + w4 * RC1;
                            zzz1 = HInput;

                            objClassObjectTmp.x = xxx1;
                            objClassObjectTmp.y = yyy1;
                            objClassObjectTmp.z = zzz1;

                            objClassObjectTmp.FlagRezCalc = 0;

                        }
                        // ####################################################################################

                        else if ((RC1 <= 0) && (RC1_1 > 0))
                        {
                            // Координаты ИРИ в МЗСК
                            xxx1 = w1 + w2 * RC1_1;
                            yyy1 = w3 + w4 * RC1_1;
                            zzz1 = HInput;

                            objClassObjectTmp.x = xxx1;
                            objClassObjectTmp.y = yyy1;
                            objClassObjectTmp.z = zzz1;

                            objClassObjectTmp.FlagRezCalc = 0;

                        }
                        // ####################################################################################
                        // RC1>0&&RC1_1>0

                        else
                        {
                            // Координаты ИРИ в МЗСК
                            xxx1 = w1 + w2 * RC1;
                            yyy1 = w3 + w4 * RC1;
                            zzz1 = HInput;

                            xxx1_1 = w1 + w2 * RC1_1;
                            yyy1_1 = w3 + w4 * RC1_1;
                            zzz1_1 = HInput;

                            // --------------------------------------------------------------------------------------

                            // VYBOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Выбор решения
                            // Обе высоты положительные

                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Анализ измеренных и рассчитанных по двум вариантам решения задержек

                            var matrixCtime = new double[1, 3];
                            var matrixCtime_1 = new double[1, 3];
                            matrixCtime = Calc_dt(
                                                  lstClassObjectTmp,
                                                  xxx1,
                                                  yyy1,
                                                  zzz1
                                                 );
                            matrixCtime_1 = Calc_dt(
                                                  lstClassObjectTmp,
                                                  xxx1_1,
                                                  yyy1_1,
                                                  zzz1_1
                                                 );


                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у первого набора координат

                            if (
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&

                            (
                            (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime_1[0, 0])) ||
                            (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime_1[0, 1])) ||
                            (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime_1[0, 2]))
                            )

                            ) // IF

                            {
                                objClassObjectTmp.x = xxx1;
                                objClassObjectTmp.y = yyy1;
                                objClassObjectTmp.z = zzz1;

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у второго набора координат

                            else if (

                                (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                                (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                                (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2])) &&

                                (
                                (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime[0, 0])) ||
                                (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime[0, 1])) ||
                                (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime[0, 2]))
                                )

                                )

                            {
                                objClassObjectTmp.x = xxx1_1;
                                objClassObjectTmp.y = yyy1_1;
                                objClassObjectTmp.z = zzz1_1;

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у обоих наборов координат ->
                            // выбираем набор с меньшей высотой

                            else if (
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2]))

                            ) // IF

                            {
                                if (
                                    zzz1 <= zzz1_1
                                    )
                                {
                                    if (zzz1 >= hsr)
                                    {
                                        objClassObjectTmp.x = xxx1;
                                        objClassObjectTmp.y = yyy1;
                                        objClassObjectTmp.z = zzz1;

                                    }
                                    else
                                    {
                                        objClassObjectTmp.x = xxx1_1;
                                        objClassObjectTmp.y = yyy1_1;
                                        objClassObjectTmp.z = zzz1_1;

                                    }
                                }
                                else
                                {
                                    if (zzz1_1 >= hsr)
                                    {
                                        objClassObjectTmp.x = xxx1_1;
                                        objClassObjectTmp.y = yyy1_1;
                                        objClassObjectTmp.z = zzz1_1;

                                    }
                                    else
                                    {
                                        objClassObjectTmp.x = xxx1;
                                        objClassObjectTmp.y = yyy1;
                                        objClassObjectTmp.z = zzz1;

                                    }

                                }

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек не совпадают ни у одногонабора координат 

                            else
                            {
                                objClassObjectTmp.Altitude = -1;
                                objClassObjectTmp.Latitude = -1;
                                objClassObjectTmp.Longitude = -1;
                                objClassObjectTmp.FlagRezCalc = 3;
                                return objClassObjectTmp;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> VYBOR

                        } // RC1>0&&RC1_1>0
                        // ####################################################################################

                    } // (fldt1 == true) && (fldt2 == true) && (fldt3 == false)
                    // 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12 12  dt1,dt2

                    // dt1,dt3 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13
                    // При расчете используются dt1,dt3

                    else if ((fldt1 == true) && (fldt2 == false) && (fldt3 == true))
                    {
                        zzz1 = HInput;

                        if (w6 > 1E-11)
                            RC1 = (zzz1 - w5) / w6;
                        else
                            RC1 = (zzz1 - w5) / 1E-11;

                        // ......................................................................................
                        if (RC1 <= 0)
                        {
                            objClassObjectTmp.Altitude = -1;
                            objClassObjectTmp.Latitude = -1;
                            objClassObjectTmp.Longitude = -1;
                            objClassObjectTmp.FlagRezCalc = 5;
                            return objClassObjectTmp;

                        } // RC1<0
                        // ......................................................................................
                        // RC1>0

                        else
                        {
                            zzz1 = HInput;
                            xxx1 = w1 + w2 * RC1;
                            yyy1 = Math.Sqrt(RC1*RC1-xxx1*xxx1-zzz1*zzz1);

                            zzz1_1 = HInput;
                            xxx1_1 = w1 + w2 * RC1;
                            yyy1_1 = -Math.Sqrt(RC1 * RC1 - xxx1_1 * xxx1_1 - zzz1_1 * zzz1_1);

                            // VYBOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Выбор решения
                            // Обе высоты положительные

                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Анализ измеренных и рассчитанных по двум вариантам решения задержек

                            var matrixCtime = new double[1, 3];
                            var matrixCtime_1 = new double[1, 3];
                            matrixCtime = Calc_dt(
                                                  lstClassObjectTmp,
                                                  xxx1,
                                                  yyy1,
                                                  zzz1
                                                 );
                            matrixCtime_1 = Calc_dt(
                                                  lstClassObjectTmp,
                                                  xxx1_1,
                                                  yyy1_1,
                                                  zzz1_1
                                                 );

                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у первого набора координат

                            if (
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&

                            (
                            (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime_1[0, 0])) ||
                            (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime_1[0, 1])) ||
                            (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime_1[0, 2]))
                            )

                            ) // IF

                            {
                                objClassObjectTmp.x = xxx1;
                                objClassObjectTmp.y = yyy1;
                                objClassObjectTmp.z = zzz1;

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у второго набора координат

                            else if (

                                (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                                (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                                (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2])) &&

                                (
                                (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime[0, 0])) ||
                                (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime[0, 1])) ||
                                (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime[0, 2]))
                                )

                                )

                            {
                                objClassObjectTmp.x = xxx1_1;
                                objClassObjectTmp.y = yyy1_1;
                                objClassObjectTmp.z = zzz1_1;

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у обоих наборов координат ->
                            // выбираем набор с меньшей высотой

                            else if (
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2]))

                            ) // IF

                            {
                                if (
                                    zzz1 <= zzz1_1
                                    )
                                {
                                    if (zzz1 >= hsr)
                                    {
                                        objClassObjectTmp.x = xxx1;
                                        objClassObjectTmp.y = yyy1;
                                        objClassObjectTmp.z = zzz1;

                                    }
                                    else
                                    {
                                        objClassObjectTmp.x = xxx1_1;
                                        objClassObjectTmp.y = yyy1_1;
                                        objClassObjectTmp.z = zzz1_1;

                                    }
                                }
                                else
                                {
                                    if (zzz1_1 >= hsr)
                                    {
                                        objClassObjectTmp.x = xxx1_1;
                                        objClassObjectTmp.y = yyy1_1;
                                        objClassObjectTmp.z = zzz1_1;

                                    }
                                    else
                                    {
                                        objClassObjectTmp.x = xxx1;
                                        objClassObjectTmp.y = yyy1;
                                        objClassObjectTmp.z = zzz1;

                                    }

                                }

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек не совпадают ни у одногонабора координат 

                            else
                            {
                                objClassObjectTmp.Altitude = -1;
                                objClassObjectTmp.Latitude = -1;
                                objClassObjectTmp.Longitude = -1;
                                objClassObjectTmp.FlagRezCalc = 3;
                                return objClassObjectTmp;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> VYBOR

                        } // RC1>=0
                        // ......................................................................................

                    } // (fldt1 == true) && (fldt2 == false) && (fldt3 == true)
                    // 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 13 dt1,dt3

                    // dt2,dt3 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 
                    // При расчете используются dt2,dt3

                    else if ((fldt1 == false) && (fldt2 == true) && (fldt3 == true))
                    {
                        zzz1 = HInput;

                        if (w6 > 1E-11)
                            RC1 = (zzz1 - w5) / w6;
                        else
                            RC1 = (zzz1 - w5) / 1E-11;

                        // ......................................................................................
                        if (RC1 <= 0)
                        {
                            objClassObjectTmp.Altitude = -1;
                            objClassObjectTmp.Latitude = -1;
                            objClassObjectTmp.Longitude = -1;
                            objClassObjectTmp.FlagRezCalc = 5;
                            return objClassObjectTmp;

                        } // RC1<0
                        // ......................................................................................
                        // RC1>0

                        else
                        {
                            zzz1 = HInput;
                            yyy1 = w3 + w4 * RC1;
                            xxx1 = Math.Sqrt(RC1 * RC1 - yyy1 * yyy1 - zzz1 * zzz1);

                            zzz1_1 = HInput;
                            yyy1_1 = w3 + w4 * RC1;
                            xxx1_1 = -Math.Sqrt(RC1 * RC1 - yyy1_1 * yyy1_1 - zzz1_1 * zzz1_1);

                            // VYBOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            // Выбор решения
                            // Обе высоты положительные

                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Анализ измеренных и рассчитанных по двум вариантам решения задержек

                            var matrixCtime = new double[1, 3];
                            var matrixCtime_1 = new double[1, 3];
                            matrixCtime = Calc_dt(
                                                  lstClassObjectTmp,
                                                  xxx1,
                                                  yyy1,
                                                  zzz1
                                                 );
                            matrixCtime_1 = Calc_dt(
                                                  lstClassObjectTmp,
                                                  xxx1_1,
                                                  yyy1_1,
                                                  zzz1_1
                                                 );

                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у первого набора координат

                            if (
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&

                            (
                            (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime_1[0, 0])) ||
                            (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime_1[0, 1])) ||
                            (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime_1[0, 2]))
                            )

                            ) // IF

                            {
                                objClassObjectTmp.x = xxx1;
                                objClassObjectTmp.y = yyy1;
                                objClassObjectTmp.z = zzz1;

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у второго набора координат

                            else if (

                                (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                                (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                                (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2])) &&

                                (
                                (Math.Sign(lstClassObjectTmp[1].tau) != Math.Sign(matrixCtime[0, 0])) ||
                                (Math.Sign(lstClassObjectTmp[2].tau) != Math.Sign(matrixCtime[0, 1])) ||
                                (Math.Sign(lstClassObjectTmp[3].tau) != Math.Sign(matrixCtime[0, 2]))
                                )

                                )

                            {
                                objClassObjectTmp.x = xxx1_1;
                                objClassObjectTmp.y = yyy1_1;
                                objClassObjectTmp.z = zzz1_1;

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек совпадают у обоих наборов координат ->
                            // выбираем набор с меньшей высотой

                            else if (
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime[0, 2])) &&
                            (Math.Sign(lstClassObjectTmp[1].tau) == Math.Sign(matrixCtime_1[0, 0])) &&
                            (Math.Sign(lstClassObjectTmp[2].tau) == Math.Sign(matrixCtime_1[0, 1])) &&
                            (Math.Sign(lstClassObjectTmp[3].tau) == Math.Sign(matrixCtime_1[0, 2]))

                            ) // IF

                            {
                                if (
                                    zzz1 <= zzz1_1
                                    )
                                {
                                    if (zzz1 >= hsr)
                                    {
                                        objClassObjectTmp.x = xxx1;
                                        objClassObjectTmp.y = yyy1;
                                        objClassObjectTmp.z = zzz1;

                                    }
                                    else
                                    {
                                        objClassObjectTmp.x = xxx1_1;
                                        objClassObjectTmp.y = yyy1_1;
                                        objClassObjectTmp.z = zzz1_1;

                                    }
                                }
                                else
                                {
                                    if (zzz1_1 >= hsr)
                                    {
                                        objClassObjectTmp.x = xxx1_1;
                                        objClassObjectTmp.y = yyy1_1;
                                        objClassObjectTmp.z = zzz1_1;

                                    }
                                    else
                                    {
                                        objClassObjectTmp.x = xxx1;
                                        objClassObjectTmp.y = yyy1;
                                        objClassObjectTmp.z = zzz1;

                                    }

                                }

                                objClassObjectTmp.FlagRezCalc = 0;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // Знаки задержек не совпадают ни у одногонабора координат 

                            else
                            {
                                objClassObjectTmp.Altitude = -1;
                                objClassObjectTmp.Latitude = -1;
                                objClassObjectTmp.Longitude = -1;
                                objClassObjectTmp.FlagRezCalc = 3;
                                return objClassObjectTmp;

                            }
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> VYBOR

                        } // RC1>=0
                        // ......................................................................................

                    } // (fldt1 == false) && (fldt2 == true) && (fldt3 == true)
                    // 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23 23  dt2,dt3

                } // HInput!=-1 -> АЛГОРИТМ С фиксированной высотой
                // VETKA2VETKA2VETKA2VETKA2VETKA2VETKA2VETKA2VETKA2VETK АЛГОРИТМ С фиксированной высотой


                // ------------------------------------------------------------------------------------
                // Координаты ИРИ в ГЦСК

                int indexbaz1 = -1;
                indexbaz1 = lstClassObjectTmp.FindIndex(xx => (xx.BaseStation == true));
                if (indexbaz1 >= 0)
                {
                   ClassGeoCalculator.MZSK_Geoc(
                   // Базовая
                   lstClassObjectTmp[indexbaz1].LatRad, // rad
                   lstClassObjectTmp[indexbaz1].LongRad,
                   // Базовая в ГЦСК
                   lstClassObjectTmp[indexbaz1].x1,
                   lstClassObjectTmp[indexbaz1].y1,
                   lstClassObjectTmp[indexbaz1].z1,
                   // ИРИ в МЗСК
                   objClassObjectTmp.x,
                   objClassObjectTmp.y,
                   objClassObjectTmp.z,
                   // ИРИ в ГЦСК
                   ref objClassObjectTmp.x1,
                   ref objClassObjectTmp.y1,
                   ref objClassObjectTmp.z1
                   );
 
                } // if (indexbaz1 >= 0)

                // ------------------------------------------------------------------------------------
                // ИРИ -> долгота,широта,высота

                ClassGeoCalculator.f_XYZ_BLH_84_1(
                                                  // ИРИ в ГЦСК
                                                  objClassObjectTmp.x1,
                                                  objClassObjectTmp.y1,
                                                  objClassObjectTmp.z1,
                                                  // ИРИ -> долгота,широта,высота
                                                  ref objClassObjectTmp.Latitude,
                                                  ref objClassObjectTmp.Longitude,
                                                  ref objClassObjectTmp.Altitude
                                                  );

                // ------------------------------------------------------------------------------------

                return objClassObjectTmp;

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> OPR!=0

            // OPR==0 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // !!!SLAE (Главный определитель в методе Крамера равен тнулю)

            // ------------------------------------------------------------------------------------
            int Nsl = 0;
            int fsl = 0;
            double xxxsl = 0;
            double yyysl = 0;
            double zzzsl = 0;
            double rsl = 0;

            Nsl = 3;
            // ------------------------------------------------------------------------------------

            // !!! в матрицу A входит столбец B
            double[,] Matr_in = new double[Nsl, Nsl + 1];
            double[] Ans_out = new double[Nsl];
            // ------------------------------------------------------------------------------------

            Matr_in[0, 0] = lstClassObjectTmp[1].x;
            Matr_in[0, 1] = lstClassObjectTmp[1].y;
            Matr_in[0, 2] = dr01;
            Matr_in[0, 3] = K1;
            Matr_in[1, 0] = lstClassObjectTmp[2].x;
            Matr_in[1, 1] = lstClassObjectTmp[2].y;
            Matr_in[1, 2] = dr02;
            Matr_in[1, 3] = K2;
            Matr_in[2, 0] = lstClassObjectTmp[3].x;
            Matr_in[2, 1] = lstClassObjectTmp[3].y;
            Matr_in[2, 2] = dr03;
            Matr_in[2, 3] = K3;

            // ------------------------------------------------------------------------------------

            fsl = f_SLAE(
                               Nsl,
                               Matr_in,
                               ref Ans_out
                        );
            // ------------------------------------------------------------------------------------
            // Координаты ИРИ в МЗСК

            // Высота
            double zzzzz = 0;
            zzzzz = Math.Sqrt(Math.Abs(Ans_out[2] * Ans_out[2] - Ans_out[0] * Ans_out[0] - Ans_out[1] * Ans_out[1]));

            // Change 16_12
            if(zzzzz<0)
            {
                objClassObjectTmp.Altitude = -1;
                objClassObjectTmp.Latitude = -1;
                objClassObjectTmp.Longitude = -1;
                objClassObjectTmp.FlagRezCalc = 4;
                return objClassObjectTmp;
            }


            objClassObjectTmp.x = Ans_out[0];
            objClassObjectTmp.y = Ans_out[1];
            objClassObjectTmp.z = zzzzz;

            // 08_03
            if (HInput != -1)
                objClassObjectTmp.z = HInput;

            // ------------------------------------------------------------------------------------
            // Координаты ИРИ в ГЦСК

            int indexbaz2 = -1;
            indexbaz2 = lstClassObjectTmp.FindIndex(xx => (xx.BaseStation == true));
            if (indexbaz2 >= 0)
            {
               ClassGeoCalculator.MZSK_Geoc(
                                            // Базовая
                                            lstClassObjectTmp[indexbaz2].LatRad, // rad
                                            lstClassObjectTmp[indexbaz2].LongRad,
                                            // Базовая в ГЦСК
                                            lstClassObjectTmp[indexbaz2].x1,
                                            lstClassObjectTmp[indexbaz2].y1,
                                            lstClassObjectTmp[indexbaz2].z1,
                                            // ИРИ в МЗСК
                                            objClassObjectTmp.x,
                                            objClassObjectTmp.y,
                                            objClassObjectTmp.z,
                                            // ИРИ в ГЦСК
                                            ref objClassObjectTmp.x1,
                                            ref objClassObjectTmp.y1,
                                            ref objClassObjectTmp.z1
                                            );

            } // if (indexbaz2 >= 0)

            // ------------------------------------------------------------------------------------
            // ИРИ -> долгота,широта,высота

            ClassGeoCalculator.f_XYZ_BLH_84_1(
                                              // ИРИ в ГЦСК
                                              objClassObjectTmp.x1,
                                              objClassObjectTmp.y1,
                                              objClassObjectTmp.z1,
                                              // ИРИ -> долгота,широта,высота
                                              ref objClassObjectTmp.Latitude,
                                              ref objClassObjectTmp.Longitude,
                                              ref objClassObjectTmp.Altitude
                                              );

            // ------------------------------------------------------------------------------------
            return objClassObjectTmp;
            // ------------------------------------------------------------------------------------

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> OPR==0

        }
        // ******************************************************************************** f_DRM

        // Calc_dt ********************************************************************************
        // Расчет временных задержек

        public double[,] Calc_dt(
                                // координаты SP в МЗСК
                                List<ClassObjectTmp> lstClassObjectTmp,

                                // координаты объекта в МЗСК
                                double X,
                                double Y,
                                double Z
                               )
        {
            // ....................................................................................
            double RCC = 0;

            // Расстояние между ППi и ИРИ
            double R0 = 0;
            double R1 = 0;
            double R2 = 0;
            double R3 = 0;

            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3
            double dr01 = 0;
            double dr02 = 0;
            double dr03 = 0;

            //double cc = 299792458;
            double cc = 299796459.2;

            var matrixC = new double[1, 3];
            // ....................................................................................

            RCC = Math.Sqrt(X * X + Y * Y + Z * Z);
            R0 = RCC;

            R1 = Math.Sqrt((X - lstClassObjectTmp[1].x) * (X - lstClassObjectTmp[1].x) +
                           (Y - lstClassObjectTmp[1].y) * (Y - lstClassObjectTmp[1].y) +
                           (Z - lstClassObjectTmp[1].z) * (Z - lstClassObjectTmp[1].z));

            R2 = Math.Sqrt((X - lstClassObjectTmp[2].x) * (X - lstClassObjectTmp[2].x) +
                           (Y - lstClassObjectTmp[2].y) * (Y - lstClassObjectTmp[2].y) +
                           (Z - lstClassObjectTmp[2].z) * (Z - lstClassObjectTmp[2].z));

            R3 = Math.Sqrt((X - lstClassObjectTmp[3].x) * (X - lstClassObjectTmp[3].x) +
                           (Y - lstClassObjectTmp[3].y) * (Y - lstClassObjectTmp[3].y) +
                           (Z - lstClassObjectTmp[3].z) * (Z - lstClassObjectTmp[3].z));

            // ....................................................................................
            // Разность расстояний от ИРИ до ПП0 и ИРИ до ППj, j=1,2,3

            dr01 = R1 - R0;
            dr02 = R2 - R0;
            dr03 = R3 - R0;

            matrixC[0, 0] = dr01 / cc;
            matrixC[0, 1] = dr02 / cc;
            matrixC[0, 2] = dr03 / cc;
            // .......................................................................................

            return matrixC;
        }
        // ****************************************************************************************


        // *************************************************************************************
        // System of linear algebraic equation
        // !!! матрица состоит из N строк и (N+1) столбцов (последний столбец - коэффициенты Bi)
        //  Метод Гаусса-Ньютона

        // Возврат:
        // 0 - нет решений
        // 1 - одно решение
        // 2 - множество решений
        // *************************************************************************************
        public int f_SLAE(
                           int n,
                           //int M,
                           double[,] a,
                           ref double[] ans
                          )
        {
          // -----------------------------------------------------------------------------------
            // !!! в матрицу A входит столбец B
            // Если хотя бы одно решение существует, то оно возвращается в векторе  ans

            // 1-одно решение
            // 2- бесконечно много
            // 3- нет решений
            //int flag_out = 0;

            int col = 0;
            int row = 0;

            //int n = 3;   // строки (for 3x4)
            //int n = 4;   // строки (for 4x5)

            int m = n; // ??????????????????

            //double[,] a = new double[n, n+1];

            int i = 0;
            int sel = 0;
            double EPS = 1E-9;

            //vector<int> where (m, -1); //?????????????
            int[] where = new int[m];
            for (i = 0; i < m; ++i)
                where[i] = -1;

            //ans.assign(m, 0); //??????????????????
            //double[] ans = new double[m];
            for (i = 0; i < m; ++i)
                ans[i] = 0;

            // --------------------------------------------------------------------------
            // FOR1
            for (col = 0; col < m; ++col)
            {
                sel = row;

                //for2
                // Находим строку с max элементом в текущем столбце и запоминаем ее номер
                for (i = row; i < n; ++i)
                {
                    if (module(a[i, col]) > module(a[sel, col]))
                        sel = i;
                } // for2

                // Это нулевой элемент -> переходим к следующей итерации
                if (module(a[sel, col]) < EPS)
                    continue;

                //for3
                // меняем местами текущую строку со срокой с max элементом
                for (i = col; i <= m; ++i)
                {
                    //swap(a[sel][i], a[row][i]);
                    var t = a[sel, i];
                    a[sel, i] = a[row, i];
                    a[row, i] = t;
                } // for3

                where[col] = row;  // ???????????????

                // for4
                for (i = 0; i < n; ++i)
                {
                    if (i != row)
                    {
                        double c = a[i, col] / a[row, col];
                        for (int j = col; j <= m; ++j)
                            a[i, j] -= a[row, j] * c;
                    }

                } // for4

                ++row;
                if (row >= n)
                    break;

            } //FOR1
            // --------------------------------------------------------------------------
            //ans.assign(m, 0); //??????????????????

            // for5
            for (i = 0; i < m; ++i)
            {
                if (where[i] != -1)
                    ans[i] = a[where[i], m] / a[where[i], i];

            } // for5
            // --------------------------------------------------------------------------
            // for6
            for (i = 0; i < n; ++i)
            {
                double sum = 0;

                for (int j = 0; j < m; ++j)
                    sum += ans[j] * a[i, j];

                if (module(sum - a[i, m]) > EPS)
                {
                    //flag_out = 3; // нет решений
                    return 0;
                }

            } // for6
            // --------------------------------------------------------------------------
            for (i = 0; i < m; ++i)
            {
                if (where[i] == -1)
                {
                    //flag_out = 2; // много решений
                    return 2;
                }
            }

            //flag_out=1; // Одно решение
            return 1;
            // --------------------------------------------------------------------------

            // ***************************************************************** Variant2


        } // P/P f_SLAE
          // *************************************************************************************

        // ***********************************************************************
        // Расстояние между двумя точками с учетом кривизны Земли

        // Входные параметры: 

        // Lat1,Long1,Lat2,Long2 (град) - широта и долгота точек
        // fl_84_42=1-> долгота и широта в WGS84, =2 -> SK42

        // Выходные параметры: 
        // возвращает расстояние в м

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // ***********************************************************************

        public double f_D_2Points

            (
                 // Широта и долгота стояния точек (град)
                 double Lat1,
                 double Long1,
                 double Lat2,
                 double Long2,

                 int fl_84_42

            )
        {

            // -----------------------------------------------------------------------
            double REarth = 0;
            double REarth_Lat = 0;
            double LatSr = 0;
            double LatRad1 = 0;
            double LatRad2 = 0;
            double LongRad1 = 0;
            double LongRad2 = 0;
            double dLat = 0;
            double dLong = 0;
            double dq = 0;
            double s = 0;
            // -----------------------------------------------------------------------
            // Средний радиус Земли (шар)

            if (fl_84_42 == 2) // Красовский
                REarth = 6371220;
            else
                REarth = 6378136.3; // WGS84
            // -----------------------------------------------------------------------
            // Широта и долгота (rad)

            LatRad1 = (Lat1 * Math.PI) / 180;   // grad->rad
            LatRad2 = (Lat2 * Math.PI) / 180;
            LongRad1 = (Long1 * Math.PI) / 180;
            LongRad2 = (Long2 * Math.PI) / 180;
            dLat = LatRad2 - LatRad1;
            dLong = LongRad2 - LongRad1;
            LatSr = (LatRad1 + LatRad2) / 2;
            // -----------------------------------------------------------------------
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

            // m  Old
            //REarthP_Pel = REarth_Pel; // Шар

            // Сжатие=(a-b)/a
            // 0.003352 -Красовский
            if (fl_84_42 == 2) // Красовский
                REarth_Lat = REarth * (1 - 0.003352 * Math.Sin(LatSr) * Math.Sin(LatSr));
            else
                REarth_Lat = REarth * (1 - 0.00669438 * Math.Sin(LatSr) * Math.Sin(LatSr)); // WGS84
            // -----------------------------------------------------------------------
            // 1-й вариант

            dq = 2 * Math.Asin(Math.Sqrt(Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                                         Math.Cos(LatRad1) * Math.Cos(LatRad2) * Math.Sin(dLong / 2) * Math.Sin(dLong / 2)));
            //s = REarth_Lat* dq;
            // -----------------------------------------------------------------------
            // !!! 2-й вариант

            dq = Math.Atan(Math.Sqrt((Math.Cos(LatRad2) * Math.Sin(dLong)) * (Math.Cos(LatRad2) * Math.Sin(dLong)) +
                                     (Math.Cos(LatRad1) * Math.Sin(LatRad2) - Math.Sin(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong)) *
                                     (Math.Cos(LatRad1) * Math.Sin(LatRad2) - Math.Sin(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong))) /
                           (Math.Sin(LatRad1) * Math.Sin(LatRad2) + Math.Cos(LatRad1) * Math.Cos(LatRad2) * Math.Cos(dLong)));
            // -----------------------------------------------------------------------
            s = REarth_Lat * dq;
            // -----------------------------------------------------------------------

            return s;

        } // Функция f_D_2Points
        //**************************************************************************






    } // Class
} // namespace
