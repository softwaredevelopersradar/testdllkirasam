﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fDRM
{
    public class ClassObject
    {
        public double Latitude = 0;
        public double Longitude = 0;
        public double Altitude = 0;
        public double tau = 0;
        public bool BaseStation = false;
        public int IndexStation = -1;

    } // Class
} // Namespace
